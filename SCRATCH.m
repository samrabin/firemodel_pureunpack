profile on

thisCtry_estFire_myrtc = cat(5,thisCtry_estFire_crop_myrt,...
                               thisCtry_estFire_past_myrt,...
                               thisCtry_estFire_othr_myrt,...
                               thisCtry_estFire_total_myrt) ;
                           
tic
sd_results = nan(years.num,regions_bsruns.num,4) ;
for y = 1:years.num
%     disp(['Year ' num2str(y) ' of ' num2str(years.num) '...'])
    thisYr_mrt = squeeze(thisCtry_estFire_myrtc(:,y,:,:,:)) ;
    for r = 1:regions_bsruns.num
%         disp(['   Region ' num2str(r) ' of ' num2str(regions_bsruns.num) '...'])
        thisYrReg_mtc = squeeze(thisYr_mrt(:,r,:,:)) ;
        for c = 1:4
%             disp(['      Cover ' num2str(c) ' of 4...'])
            thisYrRegLC_mt = thisYrReg_mtc(:,:,c) ;
            thisYrRegLC_tm = permute(thisYrRegLC_mt, [2 1]) ;
            clear thisYrRegLC_mt
            covMat_thisYrRegLC = cov(thisYrRegLC_tm) ;
            sd_results(y,r,c) = sqrt( ...
                                 sum(2*covMat_thisYrRegLC(:)) ...
                                 - sum(diag(covMat_thisYrRegLC))) ;
        end
    end
end
toc

profile viewer
profile off

clear thisCtry_estFire_myrtc