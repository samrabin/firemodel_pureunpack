function [LCfrac_crop,LCfrac_past,LCfrac_othr, ...
		  landarea] = ...
			import_LCdata(methods,years,months,regions)


if strcmp(methods.landcover,'hyde')
    disp('Loading HYDE v3.1 land cover data...')
    load('HYDE31prop_quartDeg_0009.mat')
    landarea = HYDE_landarea_km2 ; clear HYDE_landarea_km2
    landarea = repmat(landarea,[1 1 months.num]) ;
    LCcrop_tmp = LCprop_crop_HYDE31_0009(:,:,years.indices_s00) ;
    LCpast_tmp = LCprop_past_HYDE31_0009(:,:,years.indices_s00) ;
    
    LCcrop_tmp = single(LCcrop_tmp) ;
    LCpast_tmp = single(LCpast_tmp) ;
    LCothr_tmp = ones(size(LCcrop_tmp)) - (LCcrop_tmp + LCpast_tmp) ;
    
    % Correct for rounding errors in calculation of LCothr_tmp.
    if ~isempty(find(LCothr_tmp<0,1))
        if ~isempty(find(LCothr_tmp<0 & LCothr_tmp>-1e-6,1))
            LCothr_tmp(LCothr_tmp<0 & LCothr_tmp>-1e-6) = 0 ;
        else
            error('Something went wrong in calculating LCfrac_othr.')
        end
    end
    
    clear LCprop_*_HYDE31_0009
    LCfrac_crop = nan(size(regions.Marray),'single') ;
    LCfrac_past = nan(size(regions.Marray),'single') ;
    LCfrac_othr = nan(size(regions.Marray),'single') ;
    for y = 1:years.num
        monthTMP_start = (y-1)*12 + 1 ;
        monthTMP_end = monthTMP_start + 11 ;
        LCfrac_crop(:,:,monthTMP_start:monthTMP_end) = repmat(LCcrop_tmp(:,:,y),[1 1 12]) ;
        LCfrac_past(:,:,monthTMP_start:monthTMP_end) = repmat(LCpast_tmp(:,:,y),[1 1 12]) ;
        LCfrac_othr(:,:,monthTMP_start:monthTMP_end) = repmat(LCothr_tmp(:,:,y),[1 1 12]) ;
    end ; clear y monthTMP_*
    clear LC*_tmp
    
elseif strcmp(methods.landcover,'fake_3member')
    load(['fake3m_v' num2str(methods.F3Mv) '_obsBALC_0110'],...
         'LCarea_fakeC','LCarea_fakeP','LCarea_fakeO')
    landarea = LCarea_fakeC + LCarea_fakeP + LCarea_fakeO ;
    LCfrac_crop = LCarea_fakeC ./ landarea ;
    LCfrac_past = LCarea_fakeP ./ landarea ;
    LCfrac_othr = LCarea_fakeO ./ landarea ;
    
else error('methods.landcover improperly specified.')
end




