function [LCfrac_crop,LCfrac_past,LCfrac_othr, ...
		  LCdata_crop,LCdata_past,LCdata_othr, ...
		  landarea] = ...
			import_LCdata(methods,years,months,regions)


if strcmp(methods.landcover,'hyde')
    disp('Loading HYDE v3.1 land cover data...')
    load('HYDE31prop_quartDeg_0009.mat')
    landarea = HYDE_landarea_km2 ; clear HYDE_landarea_km2
    landarea_Marray = repmat(landarea,[1 1 months.num]) ;
    LCcrop_tmp = LCprop_crop_HYDE31_0009(:,:,years.indices_s00) ;
    LCpast_tmp = LCprop_past_HYDE31_0009(:,:,years.indices_s00) ;
elseif strcmp(methods.landcover,'fake_3member')

	% Fake classifications. Excludes Water (1) and sometimes FillValue/Unclassified (18).
    fake_crop = [13 15] ;   % Croplands, Cropland/NaturalVegetationMosaic
    if methods.F3Mv == 1
        fake_past = 8:11 ;   % OpenShrublands, WoodySavannas, Savannas, Grasslands
        fake_othr = [2:7 12 14 16 17] ;   % EvergreenNeedleleafForest, EvergreenBroadleafForest, DeciduousNeedleleafForest, DeciduousBroadleafForest, MixedForest, ClosedShrublands, PermanentWetlands, UrbanAndBuiltup, SnowAndIce, BarrenOrSparselyVegetated
        warning('Not including FillValue/Unclassified!')
    elseif methods.F3Mv == 2
        fake_past = [8 10] ;
        fake_othr = [2:7 9 11 12 14 16 17] ;
        warning('Not including FillValue/Unclassified!')
    elseif methods.F3Mv == 3
        fake_past = [8 10] ;
        fake_othr = [2:7 9 11 12 14 16 17 18] ;
    else error('methods.F3Mv improperly specified.')
    end
    
    load('MCD12C1_QD_0110.mat')
    MCD12C1_QD_0110 = MCD12C1_QD_0110(:,:,:,years.indices_s01) ;
    
%     landarea_Yarray = squeeze(sum(MCD12C1_QD_0110,3)) ;
    landarea_Yarray = squeeze(sum(MCD12C1_QD_0110(:,:,[fake_crop fake_past fake_othr],:),3)) ;
    water_mask = 0==min(squeeze(sum(MCD12C1_QD_0110(:,:,2:17,:),3)),[],3) ;   % Find grid cells that have no land other than Water (1) or FillValue/Unclassified (18) in at least one year.
    landarea_Yarray(repmat(water_mask,[1 1 years.num])) = NaN ;
    landarea = mean(landarea_Yarray,3) ;
    landarea_Marray = nan(720,1440,months.num) ;
    for y = 1:years.num
        monthTMP_start = (y-1)*12 + 1 ;
        monthTMP_end = monthTMP_start + 11 ;
        landarea_Marray(:,:,monthTMP_start:monthTMP_end) = repmat(landarea_Yarray(:,:,y),[1 1 12]) ;
    end
    
    LCcrop_tmp = squeeze(sum(MCD12C1_QD_0110(:,:,fake_crop,:),3)) ./ landarea_Yarray ;
    LCpast_tmp = squeeze(sum(MCD12C1_QD_0110(:,:,fake_past,:),3)) ./ landarea_Yarray ;
        
else error('methods.landcover improperly specified.')
end
LCcrop_tmp = single(LCcrop_tmp) ;
LCpast_tmp = single(LCpast_tmp) ;
LCothr_tmp = ones(size(LCcrop_tmp)) - (LCcrop_tmp + LCpast_tmp) ;

% Correct for rounding errors in calculation of LCothr_tmp.
if ~isempty(find(LCothr_tmp<0,1))
    if ~isempty(find(LCothr_tmp<0 & LCothr_tmp>-1e-6,1))
        LCothr_tmp(LCothr_tmp<0 & LCothr_tmp>-1e-6) = 0 ;
    else
        error('Something went wrong in calculating LCfrac_othr.')
    end
end

clear LCprop_*_HYDE31_0009
LCfrac_crop = nan(size(regions.Marray),'single') ;
LCfrac_past = nan(size(regions.Marray),'single') ;
LCfrac_othr = nan(size(regions.Marray),'single') ;
for y = 1:years.num
    monthTMP_start = (y-1)*12 + 1 ;
    monthTMP_end = monthTMP_start + 11 ;
    LCfrac_crop(:,:,monthTMP_start:monthTMP_end) = repmat(LCcrop_tmp(:,:,y),[1 1 12]) ;
    LCfrac_past(:,:,monthTMP_start:monthTMP_end) = repmat(LCpast_tmp(:,:,y),[1 1 12]) ;
    LCfrac_othr(:,:,monthTMP_start:monthTMP_end) = repmat(LCothr_tmp(:,:,y),[1 1 12]) ;
end ; clear y monthTMP_*
clear LC*_tmp
LCdata_crop = LCfrac_crop ;
LCdata_past = LCfrac_past ;
LCdata_othr = LCfrac_othr ;
if strcmp(methods.coverType,'area')
    LCdata_crop = LCdata_crop .* landarea_Marray ;
    LCdata_past = LCdata_past .* landarea_Marray ;
    LCdata_othr = LCdata_othr .* landarea_Marray ;
end