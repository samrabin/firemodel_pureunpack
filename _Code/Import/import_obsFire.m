function [obsFire, obsFireNorm, fire_normConstant] = ...
		 	import_obsFire(methods,months) ;


if strcmp(methods.fireType,'terra')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('MOD14CMH_2001-2008_ssr.mat')
    obsFire = single(MOD14CMH(:,:,months.start_s01:months.end_s01)) ;
    clear MOD14CMH
elseif strcmp(methods.fireType,'aqua')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
elseif strcmp(methods.fireType,'gfed')
    load('gfed4_BA_highres_01-10_km2.mat')
    obsFire = single(gfed4_BA_highres(:,:,months.start_s01:months.end_s01)) ;
    clear gfed4
elseif strcmp(methods.fireType,'gfed3')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('GFED3.1_2000-2009_BAkm2.mat')
    obsFire = single(gfed31(:,:,months.start_s00:months.end_s00)) ;
    clear gfed31
elseif strcmp(methods.fireType,'randerson_all')
    load('BA_all_highres_01-10.mat')
    obsFire = single(BA_all_highres(:,:,months.start_s01:months.end_s01)) ;
    clear BA_all_lowres
elseif strcmp(methods.fireType,'randerson_all_Cemis')
    load('C_all_highres_01-10.mat')
    obsFire = single(C_all_highres(:,:,months.start_s01:months.end_s01)) ;
    obsFire = obsFire .* (landarea_Marray*1e6) ;   % Convert from g/m2/month to g/month
    clear C_all_lowres
else error('methods.fireType improperly specified.')
end
clear BA_*res
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    [obsFireNorm,~,fire_normConstant] = normObs(obsFire,methods) ;
    obsFireNorm = single(obsFireNorm) ;
elseif strcmp(methods.normType,'none')
	obsFireNorm = [] ;
    fire_normConstant = max(obsFire,[],3) ;
else error('methods.normType improperly specified.')
end


end