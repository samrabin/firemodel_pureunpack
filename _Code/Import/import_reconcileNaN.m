function [landarea, regions, ...
		  LCfrac_crop,LCfrac_past,LCfrac_othr, ...
		  LCdata_crop,LCdata_past,LCdata_othr, ...
		  Marray_month, Marray_trueMonth, ...
		  obsFire, obsFireNorm, obsFire_yn, ...
		  fire_normConstant, ...
		  obsFire_frac] = ...
			import_reconcileNaN(methods, regions, landarea, ...
								years, months, ...
								LCfrac_crop,LCfrac_past,LCfrac_othr, ...
								Marray_month, Marray_trueMonth, ...
								obsFire, obsFireNorm, obsFire_yn, ...
								fire_normConstant)

% Find bad cells
% (Ignores any cells that ever have landarea==0.)
bad = ( isnan(regions.map) | isnan(mean(LCfrac_crop,3)) | isnan(mean(obsFire,3)) | min(landarea,[],3)==0 ) ;
if methods.ignoreNB & methods.ignoreNB_proper
	neverBurned = max(obsFire,[],3)==0 ;
	bad = (bad | neverBurned) ;
end
% if strcmp(methods.landcover,'fake_3member')
%     warning('Using MCD12 data, but to match previous bootstrap samples, excluding cells where MCD12 isn''t NaN but HYDE is.')
%     HYDE = load('HYDE31prop_quartDeg_0009.mat','HYDE_landarea_km2') ;
%     bad = (bad | isnan(HYDE.HYDE_landarea_km2)) ;
% end

regions.map(bad) = NaN ;
regions.Yarray = repmat(regions.map,[1 1 years.num]) ;
regions.Marray = repmat(regions.Yarray,[1 1 12]) ;
bad_array = repmat(bad,[1 1 months.num]) ;
landarea = landarea(~bad_array) ;
LCfrac_crop = LCfrac_crop(~bad_array) ;
LCfrac_past = LCfrac_past(~bad_array) ;
LCfrac_othr = LCfrac_othr(~bad_array) ;
Marray_month = Marray_month(~bad_array) ;
Marray_trueMonth = Marray_trueMonth(~bad_array) ;
if ~strcmp(methods.normType,'none')
    obsFireNorm = obsFireNorm(~bad_array) ;
end
fire_normConstant = fire_normConstant(~bad) ;
obsFire_yn = obsFire_yn(~bad_array) ;

obsFire = obsFire(~bad_array) ;
obsFire_frac = single(obsFire ./ landarea) ;

% Calculate LCdata
if strcmp(methods.coverType,'prop')
    LCdata_crop = LCfrac_crop ;
    LCdata_past = LCfrac_past ;
    LCdata_othr = LCfrac_othr ;
elseif strcmp(methods.coverType,'area')
    LCdata_crop = LCfrac_crop .* landarea ;
    LCdata_past = LCfrac_past .* landarea ;
    LCdata_othr = LCfrac_othr .* landarea ;
else error('methods.coverType improperly specified.')
end


end