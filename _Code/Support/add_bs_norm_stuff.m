function add_bs_norm_stuff(theseData,units)

    % Make histogram
    hist(theseData,30)
    figtmp_histN = hist(theseData,30) ;

    % Add normal curve
    hold on
    figtmp_mean = mean(theseData) ;
    figtmp_sd = std(theseData,1) ;   % Normalize by N, not N-1
    normcurve_figlims = get(gca,'XLim') ;
    normcurve_intvl = (normcurve_figlims(2) - normcurve_figlims(1))/1000 ;
    normcurve_fakeX = normcurve_figlims(1):normcurve_intvl:normcurve_figlims(2) ;
    normcurve_fakeY = normpdf(normcurve_fakeX,figtmp_mean,figtmp_sd) ;
    normcurve_fakeY = normcurve_fakeY .* (max(figtmp_histN) / max(normcurve_fakeY)) ;
    plot(normcurve_fakeX,normcurve_fakeY,'-k','LineWidth',3)
    hold off

    % Add data on normal distribution
    hold on
    Yticks = get(gca,'Ytick') ;
    Y1 = Yticks(length(Yticks)-1) ;
    Y3 = Yticks(length(Yticks)-2) ;
    Y2 = (Y1 + Y3) / 2 ;
    text(normcurve_fakeX(50),Y1,['Mean: ' num2str(round(figtmp_mean)) units])
    text(normcurve_fakeX(50),Y2,['95% CI: ' num2str(round(prctile(theseData,2.5))) ' to ' num2str(round(prctile(theseData,97.5))) units])
    text(normcurve_fakeX(50),Y3,['SD: ' num2str(round(figtmp_sd))])
    hold off

end