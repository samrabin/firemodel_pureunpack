%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Fix interpolated land-cover fractions so C+P+N+S=1 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Setup

cd '/Users/sam/Geodata/ESM_output_fromSergey_v2' ;
addpath(genpath(pwd)) ;
addpath(genpath('/Users/sam/Documents/Dropbox/Projects/FireModel_daily/'))


%% Import and prepare LC fraction data

% [landarea, ~] = geotiffread('/Users/sam/Geodata/General/LandArea_ORNL.tif') ;
% landarea = flipud(double(landarea)) ;

LCfrac_ntrl = ncread4daily('ntrl_v2.2001-2010.frac.nc','frac') ;
LCfrac_ntrl = LCfrac_ntrl(:,:,1:12:120) ;
LCfrac_scnd = ncread4daily('scnd_v2.2001-2010.frac.nc','frac') ;
LCfrac_scnd = LCfrac_scnd(:,:,1:12:120) ;
LCfrac_crop = ncread4daily('crop_v2.2001-2010.frac.nc','frac') ;
LCfrac_crop = LCfrac_crop(:,:,1:12:120) ;
LCfrac_past = ncread4daily('past_v2.2001-2010.frac.nc','frac') ;
LCfrac_past = LCfrac_past(:,:,1:12:120) ;

ok_gfdl = (LCfrac_ntrl >= 0) | (LCfrac_scnd >= 0) | (LCfrac_crop >= 0) | (LCfrac_past >= 0) ;

LCfrac_ntrl(~ok_gfdl) = NaN ;
LCfrac_ntrl(ok_gfdl & LCfrac_ntrl==-1) = 0 ;

LCfrac_scnd(~ok_gfdl) = NaN ;
LCfrac_scnd(ok_gfdl & LCfrac_scnd==-1) = 0 ;

LCfrac_crop(~ok_gfdl) = NaN ;
LCfrac_crop(ok_gfdl & LCfrac_crop==-1) = 0 ;

LCfrac_past(~ok_gfdl) = NaN ;
LCfrac_past(ok_gfdl & LCfrac_past==-1) = 0 ;

clear ok_gfdl

LCfrac_combined = LCfrac_crop + LCfrac_past + LCfrac_ntrl + LCfrac_scnd ;


%% Add/subtract cover proportionally so each grid cell sums to 1

iter = 0 ;
while ~isempty(find(roundn(LCfrac_combined(~isnan(LCfrac_combined)),-15)~=1,1))
    iter = iter + 1 ;
    disp(['Iteration ' num2str(iter) '...'])
    toAdd = 1 - LCfrac_combined ;
    prop2c = LCfrac_crop ./ LCfrac_combined ;
    prop2p = LCfrac_past ./ LCfrac_combined ;
    prop2n = LCfrac_ntrl ./ LCfrac_combined ;
    prop2s = LCfrac_scnd ./ LCfrac_combined ;
    LCfrac_crop = LCfrac_crop + toAdd.*prop2c ;
    LCfrac_past = LCfrac_past + toAdd.*prop2p ;
    LCfrac_ntrl = LCfrac_ntrl + toAdd.*prop2n ;
    LCfrac_scnd = LCfrac_scnd + toAdd.*prop2s ;
    LCfrac_combined = LCfrac_crop + LCfrac_past + LCfrac_ntrl + LCfrac_scnd ;
end ; clear iter prop2* toAdd LCtotal
disp('Done.')


%% Save 

filename = '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack/Input/LCfracs_annual_2001-2010_fromSergey_v2.mat' ;
save(filename,'LCfrac_ntrl','LCfrac_scnd','LCfrac_crop','LCfrac_past') ;


