function [allReg_Fc_test,allReg_Fp_test,allReg_Fo_test] = ...
    make_Fk_test_vectors(...
            regKey,allCells_test,...
            allReg_Fc_mr_tmp,allReg_Fp_mr_tmp,allReg_Fo_mr_tmp,...
            Marray_month,Marray_trueMonth,...
            months,regions_orig,methods,...
            varargin)

if length(varargin)==4
    allReg_Pc_mr_tmp = varargin{1} ;
    allReg_Pp_mr_tmp = varargin{2} ;
    allReg_Po_mr_tmp = varargin{3} ;
    allReg_const_mr_tmp = varargin{4} ;
elseif ~isempty(varargin)
    error('Either 0 or 4 allReg_P/const_mr_tmp invars required.')
end
        
numCells_test = length(find(allCells_test)) ;
allReg_Fc_test = nan(numCells_test,1) ;
allReg_Fp_test = nan(numCells_test,1) ;
allReg_Fo_test = nan(numCells_test,1) ;
if methods.efficient_Fs == 0
    Marray_tmp_Fk = Marray_month(allCells_test) ;
    months_num_tmp_Fk = months.num ;
elseif methods.efficient_Fs==0.1 || methods.efficient_Fs==1
    Marray_tmp_Fk = Marray_trueMonth(allCells_test) ;
    months_num_tmp_Fk = 12 ;
end
if methods.numUnpackSteps == 2
    allReg_Pc_test = nan(numCells_test*months.num,1) ;
    allReg_Pp_test = nan(numCells_test*months.num,1) ;
    allReg_Po_test = nan(numCells_test*months.num,1) ;
    allReg_const_test = nan(numCells_test*months.num,1) ;
    if methods.efficient_Ps == 0
        Marray_tmp_Pk = Marray_month(allCells_test) ;
        months_num_tmp_Pk = months.num ;
    elseif methods.efficient_Ps == 1
        Marray_tmp_Pk = Marray_trueMonth(allCells_test) ;
        months_num_tmp_Pk = 12 ;
    end
end
regKey_test = regKey(allCells_test) ;
for r = 1:regions_orig.num
    include_thisRegTest = (regKey_test==r) ;
    for m = 1:months_num_tmp_Fk
        include_thisRegTestMth_Fk = (include_thisRegTest & Marray_tmp_Fk==m) ;
        allReg_Fc_test = make_test_vectors_doIt(allReg_Fc_test,allReg_Fc_mr_tmp,include_thisRegTestMth_Fk) ;
        allReg_Fp_test = make_test_vectors_doIt(allReg_Fp_test,allReg_Fp_mr_tmp,include_thisRegTestMth_Fk) ;
        allReg_Fo_test = make_test_vectors_doIt(allReg_Fo_test,allReg_Fo_mr_tmp,include_thisRegTestMth_Fk) ;
    end
    if methods.numUnpackSteps == 2
        for m = 1:months_num_tmp_Pk
            include_thisRegTestMth_Pk = (include_thisRegTest & Marray_tmp_Pk==m) ;
            allReg_Pc_test = make_test_vectors_doIt(allReg_Pc_test,allReg_Pc_mr_tmp,include_thisRegTestMth_Pk) ;
            allReg_Pp_test = make_test_vectors_doIt(allReg_Pp_test,allReg_Pp_mr_tmp,include_thisRegTestMth_Pk) ;
            allReg_Po_test = make_test_vectors_doIt(allReg_Po_test,allReg_Po_mr_tmp,include_thisRegTestMth_Pk) ;
            allReg_const_test = make_test_vectors_doIt(allReg_const_test,allReg_const_mr_tmp,include_thisRegTestMth_Pk) ;
        end
    end
end


    function allReg_test = make_test_vectors_doIt(allReg_test,allReg_mr_tmp,include)
        allReg_test(include) = allReg_mr_tmp(m,r) ;
    end

end
