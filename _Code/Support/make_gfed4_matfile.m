%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Make MATfile with GFED4 monthly data %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Setup

cd '/Users/sam/Geodata/GFED4/Monthly'
addpath(genpath(pwd))

%% Import, permute, etc.
disp('Importing GFED4 data...')

yearList = 2001:2010 ;

gfed4 = nan(360,720,120) ;
for y = 1:length(yearList) ;
    thisYear = yearList(y) ;
    disp(['   ' num2str(thisYear) '...'])
    fileList = dir(['GFED4.0_MQ_' num2str(thisYear) '*_BA.hdf']) ;
    for f = 1:length(fileList)
        thisMonth = (y-1)*12 + f ;
        tmp = hdfread(fileList(f).name,'BurnedArea') ;
        tmp = double(tmp) * 0.01*0.01 ;   % Convert to km2. Not sure why GFED4 is like this, but it's in hundredths of hectares
        tmp_2 = tmp(1:2:720,:) + tmp(2:2:720,:) ; clear tmp
        gfed4(:,:,thisMonth) = tmp_2(:,1:2:1440) + tmp_2(:,2:2:1440) ; clear tmp_2
    end
end
disp('Done.')

gfed4 = flipdim(gfed4,1) ;

%% Save file

filename = '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack/Input/gfed4_monthly_2001-2010.mat' ;
save(filename,'gfed4')