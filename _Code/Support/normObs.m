function [ fire_norm , fire_normConstant_orig, fire_normConstant ] = normObs( fire_obs , methods )
%NORMOBS Pre-normalize for unpacking.
%   Normalizes observed fire_obs for each grid cell and month to each grid
%   cell's time series mean, then (optionally) scales up to realistic values
%   by multiplying by the mean monthly grid cell BA in the region.
%
%   Usage
%       [fire_norm,fire_normConstant] = normObs(fire_obs,methods)
%
%   To convert fire_norm back into real values, multiply by
%   fire_normConstant.
%
%   Arguments
%       fire_obs        A 3-dimensional array with 3rd dimension time.
%       (In structure "methods":)
%       normType        A string: 'max', 'mean', or 'median'.
%       zero_neverBurned      Should cells that never burned have
%                             their normConstant changed to 0? (0=No
%                             1=Yes.) If not, it'll be Inf, resulting
%                             in NaN values for fire_norm, meaning
%                             that cells that never burned will be
%                             ignored in Fk calculations. (This is
%                             how the NiceNumber results were
%                             obtained.)

% Set normStyle = 1 if run was performed before I had normStyle option
if ~isfield(methods,'normStyle')
    methods.normStyle = 1 ;
end

if ndims(fire_obs) == 3
    
    if methods.normStyle == 1
        
        [fire_norm,fire_normConstant] = ...
            normalize_doIt(fire_obs,methods) ;
        
    elseif methods.normStyle == 2
        
        Nyears = size(fire_obs,3) / 12 ;
        fire_norm = nan(size(fire_obs)) ;
        fire_normConstant = nan(size(fire_obs,1),size(fire_obs,2),Nyears) ;
        for y = 1:Nyears
            mStart = (y-1)*12 + 1 ;
            mEnd = mStart + 11 ;
            [TMP_fire_norm,TMP_fire_normConstant] = ...
                normalize_doIt(fire_obs,methods) ;
            fire_norm(:,:,mStart:mEnd) = TMP_fire_norm ;
            fire_normConstant(:,:,y) = TMP_fire_normConstant ;
            clear TMP* mStart mEnd
        end ; clear y
        
    else error('methods.normStyle is improperly specified.')
    end
    
    % Correct for when fire_normConstant = 0 (if doing correction)
    %%% If not doing correction, Fk calculation will ignore these grid cells,
    %%% because they will be NaN.
    %%% Wrecks a region's Fk calculations if all its fire_norm values are NaN.
    if ~methods.ignoreNB
        disp('Correcting for when norm_constant = 0...')
        if methods.normStyle == 1
            fire_normConst_Marray = repmat(fire_normConstant,[1 1 size(fire_norm,3)]) ;
        elseif methods.normStyle == 2
            fire_normConst_Marray = nan(size(fire_norm)) ;
            for y = 1:Nyears
                mStart = (y-1)*12 + 1 ;
                mEnd = mStart + 11 ;
                fire_normConst_Marray(:,:,mStart:mEnd) = repmat(fire_normConstant(:,:,y),[1 1 12]) ;
                clear mStart mEnd
            end ; clear y
        end
        fire_norm(fire_normConst_Marray==0) = methods.set_neverBurned ;
        fire_normConstant_orig = fire_normConstant ;
        fire_normConstant(fire_normConstant==0) = methods.set_neverBurned ;
        clear fire_normConst_Marray
    else fire_normConstant_orig = [] ;
    end
    
elseif ndims(fire_obs) == 2
    
    error('WTF is this about? fire_obs only has 2 dims???')
    
%     % Calculate normalization constants
%     %%% Makes a map showing the (max/mean/median) of each gridcell over the
%     %%% time series.
%     fire_obs_tmp = reshape(fire_obs,[methods.num_cells methods.num_months]) ;
%     if strcmp(methods.normType,'max')
%         fire_normConstant = max(fire_obs_tmp,[],2) ;
%     elseif strcmp(methods.normType,'mean')
%         fire_normConstant = mean(fire_obs_tmp,2) ;
%     elseif strcmp(methods.normType,'median')
%         fire_normConstant = median(fire_obs_tmp,2) ;
%     else
%         error('methods.normType is improperly specified.')
%     end
% 
%     num_obs = methods.num_months ;
% 
%     % Normalize
%     fire_norm = fire_obs ./ repmat(fire_normConstant,[num_obs 1]) ;
% 
%     % Correct for when fire_normConstant = 0 (if doing correction)
%     %%% If not doing correction, Fk calculation will ignore these grid cells,
%     %%% because they will be NaN.
%     %%% Wrecks a region's Fk calculations if all its fire_norm values are NaN.
%     if methods.set_neverBurned ~= -1
%         disp('Correcting for when norm_constant = 0...')
%         fire_norm(repmat(fire_normConstant,[num_obs 1])==0) = methods.set_neverBurned ;
%         fire_normConstant_orig = fire_normConstant ;
%         fire_normConstant(fire_normConstant==0) = methods.set_neverBurned ;
%     end
    
end   % If ndims() etc.


end   % Function


function [fire_norm,fire_normConstant] = ...
            normalize_doIt(fire_obs,methods)
        
        % Calculate normalization constants
        %%% Makes a map showing the (max/mean/median) of each gridcell over the
        %%% time series.
        if strcmp(methods.normType,'max')
            fire_normConstant = max(fire_obs,[],3) ;
        elseif strcmp(methods.normType,'mean')
            fire_normConstant = mean(fire_obs,3) ;
        elseif strcmp(methods.normType,'median')
            fire_normConstant = median(fire_obs,3) ;
        else
            error('methods.normType is improperly specified.')
        end

        num_obs = size(fire_obs,3) ;

        % Normalize
        fire_normConst_Marray = repmat(fire_normConstant,[1 1 num_obs]) ;
        fire_norm = fire_obs ./ fire_normConst_Marray ;

        
        
    end