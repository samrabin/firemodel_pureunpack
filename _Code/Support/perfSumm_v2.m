function perfSumm_v2(estFire2_crop,estFire2_past,estFire2_othr,obsFire,regKey_gfed,region_names,methods)
                     

disp('Mean annual burned area:')

estFire2_total = estFire2_crop + estFire2_past + estFire2_othr ;
regions_num = length(region_names) ;

showPerf_incl = true(size(estFire2_total)) ;
% showPerf_incl = (repmat(fire_normConstant_orig,[methods.num_months 1])==0) ;
% showPerf_incl = (repmat(fire_normConstant_orig,[methods.num_months 1])>0) ;

corr_allRegs = nan(regions_num,1) ;
errPerc_allRegs = nan(regions_num,1) ;
for r = 1:regions_num
    regName = char(region_names(r)) ;
    size(regKey_gfed)
    size(estFire2_total)
    est_thisReg_list = estFire2_total(regKey_gfed==r & showPerf_incl) ;
    thisReg_numCells = length(est_thisReg_list) / ((methods.num_months/12)*12) ;
    tmp = reshape(est_thisReg_list,[thisReg_numCells 12 (methods.num_months/12)]) ;
    est_thisReg = mean(sum(sum(tmp,2),1),3) ;
    clear tmp
    obs_thisReg_list = obsFire(regKey_gfed==r & showPerf_incl) ;
    tmp = reshape(obs_thisReg_list,[thisReg_numCells 12 (methods.num_months/12)]) ;
    obs_thisReg = mean(sum(sum(tmp,2),1),3) ;
    clear tmp
    errPerc_thisReg = 100 * (est_thisReg - obs_thisReg) / obs_thisReg ;
    errPerc_allRegs(r) = errPerc_thisReg ;
    
    corr_thisReg = roundn(corr(obs_thisReg_list,est_thisReg_list),-3) ;
    corr_allRegs(r) = corr_thisReg ;
    
    SSE_thisReg = sum((estFire2_total(regKey_gfed==r & showPerf_incl) - obsFire(regKey_gfed==r & showPerf_incl)).^2) ;
    maxSSE_thisReg = sum((0 - obsFire(regKey_gfed==r & showPerf_incl)).^2) ;
    SSE_performance = SSE_thisReg / maxSSE_thisReg ;
    
    disp([regName ': Obs. = ' num2str(roundn(obs_thisReg,0)) ', Est. = ' num2str(roundn(est_thisReg,0)) ' (' num2str(roundn(errPerc_thisReg,-1)) '% error, r=' num2str(corr_thisReg) ', SSEperf=' num2str(SSE_performance) ').'])
end ; clear r *thisReg* regName *SSE_thisReg SSE_performance

tmp = reshape(estFire2_total(showPerf_incl),[methods.num_cells 12 (methods.num_months/12)]) ;
global_est_total = mean(sum(sum(tmp,2),1),3) ;
clear tmp
tmp = reshape(estFire2_crop(showPerf_incl),[methods.num_cells 12 (methods.num_months/12)]) ;
global_est_crop = mean(sum(sum(tmp,2),1),3) ;
clear tmp
tmp = reshape(estFire2_past(showPerf_incl),[methods.num_cells 12 (methods.num_months/12)]) ;
global_est_past = mean(sum(sum(tmp,2),1),3) ;
clear tmp
tmp = reshape(estFire2_othr(showPerf_incl),[methods.num_cells 12 (methods.num_months/12)]) ;
global_est_othr = mean(sum(sum(tmp,2),1),3) ;
clear tmp


tmp = reshape(obsFire(showPerf_incl),[methods.num_cells 12 (methods.num_months/12)]) ;
global_obs = mean(sum(sum(tmp,2),1),3) ;
clear tmp
clear tmp
global_errPerc = 100*(global_est_total-global_obs) ./ global_obs ;
disp(['    Est CROP = ' num2str(roundn(global_est_crop,0))])
disp(['    Est PAST = ' num2str(roundn(global_est_past,0))])
disp(['    Est OTHR = ' num2str(roundn(global_est_othr,0))])
disp(['   Est TOTAL = ' num2str(roundn(global_est_total,0))])

disp(['   Obs TOTAL = ' num2str(roundn(global_obs,0)) ' (' num2str(roundn(global_errPerc,-1)) '% error)'])
% Find correlation coefficient for mean est vs obs
pearsons_r = corr(obsFire(showPerf_incl),estFire2_total(showPerf_incl)) ;
disp(['     Pearson''s r = ' num2str(pearsons_r)])

% Find sum of squared errors
sum_squared_errors = sum((estFire2_total(showPerf_incl)-obsFire(showPerf_incl)).^2) ;
fprintf('%s','     Sum of squared errors = ')
fprintf('%e\n',sum_squared_errors)

% How does it compare to guessing 0 for everything?
max_squared_errors = sum(obsFire(showPerf_incl).^2) ;
squared_errors_performance = sum_squared_errors / max_squared_errors ;
disp(['     "SSE performance" = ' num2str(squared_errors_performance)])

disp('Regional correlation coefficients:')
disp(num2str([corr_allRegs ; pearsons_r]))

disp('Regional percent error:')
disp(num2str([errPerc_allRegs ; global_errPerc]))

disp(' ') ; clear global_* regions_tmp regKey_gfed


methods