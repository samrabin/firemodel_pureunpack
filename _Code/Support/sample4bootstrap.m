%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Make 2000-long arrays for random sampling %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

outDir = '/archive/ssr/firemodel_pureunpack/sampleSets/' ;

% Number of bootstrapping runs?
methods.trainRuns = 2000 ;

% Number of output files?
methods.num_files = 5 ;

%%% What land cover data to use?
% methods.landcover = 'hyde' ;
methods.landcover = 'fake_3member' ;
if strcmp(methods.landcover,'fake_3member')
    % Which version of fake 3-member land cover?
%     methods.F3Mv = 1 ;
    methods.F3Mv = 2 ;
%     methods.F3Mv = 3 ;
end

% Ignore neverBurned grid cells in Fk calculations?
% methods.ignoreNB = false ;   % To match Magi et al. (2012)
methods.ignoreNB = true ;
if methods.ignoreNB
	% Do proper bootstrapping for ever-burned cells? Or original style?
%	methods.ignoreNB_proper = false ;   % Original style
	methods.ignoreNB_proper = true ;   % Proper bootstrapping
	if methods.ignoreNB_proper
		warning('Doing PROPER ignoreNB.')
	end
end


%% (1) Namelist

%%% What kind of observed fire data?
% methods.fireType = 'gfed3' ;   % To match Magi et al. (2012)
% methods.fireType = 'gfed' ;
methods.fireType = 'randerson_all' ;
% methods.fireType = 'randerson_all_Cemis' ;

%%% Constrain Fk values to >= 1?
methods.constrain = 0 ;
% methods.constrain = 1 ;   % To match Magi et al. (2012)

% Do "efficient" version of FcFp code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Fs = 0 ;   % To match Magi et al. (2012)
% methods.efficient_Fs = 1 ;

% Number of bootstrapping runs?
methods.trainRuns = 2000 ;

% Just need this as a placeholder
methods.normType = 'none' ;

%%% What years are we working with?
% years.list = 2003:2008 ;
% years.list = 2000:2009 ;   % To match Magi et al. (2012). Extent of HYDE 3.1.
years.list = 2001:2009 ;   % Overlap of HYDE 3.1 and Randerson.
% years.list = 2001:2003 ;   % To match Korontzi et al. (2006)

%%% What set of regions?
% methods.regions = 'gfed' ;
% methods.regions = 'v1' ;   % NAmER_WAAR_fixedWAAR
% methods.regions = 'v2' ;   % NAmER_WAAR+Turkey, Australian ecoregions (WWF), WWF-defined southern border for MIDE.
% methods.regions = 'v3' ;   % NAmER_WAAR (NO Turkey), Australian ecoregions (WWF), WWF-defined southern border for MIDE.
% methods.regions = 'v4' ;   % NAmER_WAAR+Turkey, Australian ecoregions (WWF), WWF-defined southern border for MIDE, NH/SH South American ecoregions (WWF).
% methods.regions = 'v5' ;   % NAmER_WAAR (NO Turkey), Australian ecoregions (WWF), WWF-defined southern border for MIDE, NH/SH South American ecoregions (WWF).
methods.regions = 'v5biomes' ;

%%% Minimum mean coverage in a region-month for an LC to be included in
%%% unpacking? I.e., mean(LCfrac_*)
%%% (<= this, won't be included) (fraction)
methods.min_coverage = 0 ;   % Only exclude covers that don't exist
% methods.min_coverage = 0.005 ;
% methods.min_coverage = 0.03 ;

%%% Do two-step unpacking (ie, Pk and Fk) or just one step (ie, Fk only a
%%% la Magi et al. 2012)?
methods.numUnpackSteps = 1 ;   % To match Magi et al. (2012)
% methods.numUnpackSteps = 2 ;

%%% Area or proportion land cover?
% methods.coverType = 'prop' ;   % To match Magi et al. (2012)
methods.coverType = 'area' ;

%%% Match input data (e.g., regress proportion cover vs. frac. burned)?
% methods.match = 0 ;   % To match Magi et al. (2012)
methods.match = 1 ;

% Check parameters for observed fire data
if strcmp(methods.fireType,'gfed3') && max(years.list)>2009
    error('GFED3 data only go through 2009.')
elseif (strcmp(methods.fireType,'gfed') || strcmp(methods.fireType,'randerson_all')) && min(years.list)<2001
    error('GFED4 and Randerson data start in 2001.')
elseif strcmp(methods.fireType,'terra') && min(years.list)>2008
    error('GFED4 data only go through 2008.')
end

%%% Include a constant term in the logistic regression?
methods.constant = 0 ;
% methods.constant = 1 ;

% Re-distribute negative OTHR BA?
methods.redistNegOTHR = 0 ;   % To match Magi et al. (2012)
% methods.redistNegOTHR = 1 ;

% Do "efficient" version of Pa code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Ps = 0 ;
% methods.efficient_Ps = 1 ;



%% (2) Setup

% Working directory and path
addpath(genpath('/home/ssr/firemodel_pureunpack/'))
addpath(genpath('/archive/ssr/firemodel_pureunpack/'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices_s00 = (years.start - 1999):(years.end - 1999) ;
years.indices_s01 = (years.start - 2000):(years.end - 2000) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start_s00 = (years.start-2000)*12 + 1 ;
months.end_s00 = months.start_s00 + months.num - 1 ;
months.start_s01 = (years.start-2001)*12 + 1 ;
months.end_s01 = months.start_s01 + months.num - 1 ;
methods.num_months = months.num ;


%% (3) Import data

% Regions
disp('Loading regions...')
if length(methods.regions)>=8 && strcmp(methods.regions(1:8),'v5biomes')
    load('regions_v5biomes.mat')
    regions_all = regions ;
    regions = regions_all.v51 ;
else
    load('regions_v4.mat')
    regions_all = regions ;
    eval(['regions = regions.' methods.regions ' ;']) ;
end
regions.map = single(regions.map) ;
regions.numCells = nan(regions.num,1) ;
regions.Yarray = repmat(regions.map,[1 1 years.num]) ;
regions.Marray = repmat(regions.Yarray,[1 1 12]) ;

% Month datasets
Marray_month = zeros(720,1440,months.num,'int16') ;
for m = 1:months.num
    Marray_month(:,:,m) = m*ones(720,1440) ;
end ; clear m
Marray_trueMonth = zeros(720,1440,12,'int8') ;
for m = 1:12
    Marray_trueMonth(:,:,m) = m*ones(720,1440) ;
end ; clear m
Marray_trueMonth = repmat(Marray_trueMonth,[1 1 years.num]) ;

% Land covers
[LCfrac_crop,LCfrac_past,LCfrac_othr, ...
		  LCdata_crop,LCdata_past,LCdata_othr, ...
		  landarea] = ...
			import_LCdata(methods,years,months,regions) ;

% Observed fire
[obsFire, obsFireNorm, fire_normConstant] = ...
		 	import_obsFire(methods,months) ;
obsFire_yn = (obsFire>0) ;

% Reconcile NaNs across products
[landarea, regions, ...
		  LCfrac_crop,LCfrac_past,LCfrac_othr, ...
		  LCdata_crop,LCdata_past,LCdata_othr, ...
		  Marray_month, Marray_trueMonth, ...
		  ~, ~, ~, ~, ~] = ...
			import_reconcileNaN(methods, regions, landarea, ...
								years, months, ...
								LCfrac_crop,LCfrac_past,LCfrac_othr, ...
								LCdata_crop,LCdata_past,LCdata_othr, ...
								Marray_month, Marray_trueMonth, ...
								obsFire, obsFireNorm, obsFire_yn, ...
								fire_normConstant) ;
clear obsFire*

regKey = regions.Marray(~isnan(regions.Marray)) ;
regKey_oneMonth = regions.map(~isnan(regions.map)) ;

regions.numCells = nan(regions.num,1) ;
for r = 1:regions.num
   	regions.numCells(r) = length(find(regions.map==r)) ;
end ; clear r
methods.num_cells = length(find(~isnan(regions.map))) ;
if methods.num_cells ~= sum(regions.numCells)
	error('methods.num_cells ~= sum(regions.numCells)')
end

disp('Done.') ; disp(' ')


%% (4) Choose random samples, save each file

regKey_oneMonth = regions.map(~isnan(regions.map)) ;

for r = 1:regions.num
    chooseFrom(r).ok = find(regKey_oneMonth==r) ;
end

for f = 1:methods.num_files
    disp(['Choosing random samples: File ' num2str(f) ' of ' num2str(methods.num_files) '.'])
    
    if strcmp(methods.landcover,'fake_3member')
    	lctext = ['F3Mv' num2str(methods.F3Mv)] ;
    else
    	lctext = '' ;
    end
    if methods.ignoreNB & methods.ignoreNB_proper
        tmp = num2str(years.list([1 length(years.list)])') ;
        y1 = tmp(1,3:4) ;
        y2 = tmp(2,3:4) ;
        filename = [outDir 'samples_' methods.regions '_' lctext '_ignoreNB_' y1 y2 '_' methods.fireType '_' num2str(f) '.mat'] ;
        clear tmp y1 y2
    else
        filename = [outDir 'samples' lctext '_' num2str(f) '.mat'] ;
    end
    clear lctext
    if ~isempty(dir(filename))
%         error('Output file already exists. Change name to avoid overwriting.')
        warning(['Output file ' num2str(f) ' already exists. Skipping...'])
        continue
    end
    
    choseTrain = zeros(sum(regions.numCells),methods.trainRuns,'uint32') ;
    tic
    for t = 1:methods.trainRuns
        choseTrain_tmp = uint32([]) ;
        
        for r = 1:regions.num   % Make a one-month-long vector of chosen indices
        	if regions.numCells(r) > 0
	            choseTrain_tmp = cat(1,choseTrain_tmp,randsample(chooseFrom(r).ok,regions.numCells(r),true)) ;
	        end
        end ; clear r
        
        choseTrain(:,t) = choseTrain_tmp ;
        
        clear choseTrain_tmp
        
        if rem(t,100)==0 && t~=methods.trainRuns
            disp(['   Done with ' num2str(t) ' of ' num2str(methods.trainRuns) '...'])
        end
        
    end ; clear t
    
    saving_tic = tic ;
    disp(['Done choosing samples (' toc_hms(toc) '). Saving...'])
    save(filename, 'choseTrain')
    
    disp(['Done saving (' toc_hms(toc(saving_tic)) ').']) ; disp(' ')
    clear choseTrain filename saving_tic
end ; clear f

disp('Done with all files.')