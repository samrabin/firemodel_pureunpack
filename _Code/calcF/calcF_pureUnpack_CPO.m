function [Fc,Fp,Fo,Fc_m,Fp_m,Fo_m] = ...
    calcF_pureUnpack_CPO(thisReg,months,methods)

Fc = nan(size(thisReg.month)) ;
Fp = nan(size(thisReg.month)) ;
Fo = nan(size(thisReg.month)) ;

if methods.efficient_Fs==0
    thisReg_months = thisReg.month ;
    Fc_m = nan(months.num,1) ;
    Fp_m = nan(months.num,1) ;
    Fo_m = nan(months.num,1) ;
    for m = 1:months.num
        [tmp_c,tmp_p,tmp_o] = calcF_pureUnpack_CPO_doIt(m) ;
        Fc(thisReg_months==m) = tmp_c ;
        Fp(thisReg_months==m) = tmp_p ;
        Fo(thisReg_months==m) = tmp_o ;
        Fc_m(m) = tmp_c ;
        Fp_m(m) = tmp_p ;
        Fo_m(m) = tmp_o ;
        clear tmp_*
    end
elseif methods.efficient_Fs==0.1
    thisReg_months = thisReg.month ;
    Fc_m = nan(12,1) ;
    Fp_m = nan(12,1) ;
    Fo_m = nan(12,1) ;
    for m = 1:12
        theseMonths = m:12:months.num ;
        years.num = length(theseMonths) ;
        Fc_y = nan(years.num,1) ;
        Fp_y = nan(years.num,1) ;
        Fo_y = nan(years.num,1) ;
        for y = 1:years.num
            [tmp_c,tmp_p,tmp_o] = ...
                calcF_pureUnpack_CPO_doIt(theseMonths(y)) ;
            Fc_y(y) = tmp_c ;
            Fp_y(y) = tmp_p ;
            Fo_y(y) = tmp_o ;
        end ; clear tmp_*
        Fc(thisReg.trueMonth==m) = median(Fc_y) ;
        Fp(thisReg.trueMonth==m) = median(Fp_y) ;
        Fo(thisReg.trueMonth==m) = median(Fo_y) ;
        Fc_m(m) = median(Fc_y) ;
        Fp_m(m) = median(Fp_y) ;
        Fo_m(m) = median(Fo_y) ;
    end    
elseif methods.efficient_Fs==1
    thisReg_months = thisReg.trueMonth ;
    Fc_m = nan(12,1) ;
    Fp_m = nan(12,1) ;
    Fo_m = nan(12,1) ;
    for m = 1:12
        [tmp_c,tmp_p,tmp_o] = calcF_pureUnpack_CPO_doIt(m) ;
        Fc(thisReg_months==m) = tmp_c ;
        Fp(thisReg_months==m) = tmp_p ;
        Fo(thisReg_months==m) = tmp_o ;
        Fc_m(m) = tmp_c ;
        Fp_m(m) = tmp_p ;
        Fo_m(m) = tmp_o ;
        clear tmp_*
    end
end


    function [Fc_fromDoIt,Fp_fromDoIt,Fo_fromDoIt] = calcF_pureUnpack_CPO_doIt(m)

        % Find cells that burned this month
        didBurn = logical(thisReg.obsFire_yn(thisReg_months==m)) ;
        
        % Continue with calculation?
        if sum(double(didBurn)) <= 1
%             warning('v8_calcFcFp:LE1burned','<= 1 grid cell burned. Setting all Fk''s there to zero.')
            Fc_fromDoIt = 0 ;
            Fp_fromDoIt = 0 ;
            Fo_fromDoIt = 0 ;
        else
            % Get input data for this month and region
            crop = thisReg.crop(thisReg_months==m) ;
            past = thisReg.past(thisReg_months==m) ;
            othr = thisReg.othr(thisReg_months==m) ;
            obs_BA = thisReg.obsFire(thisReg_months==m) ;

            % Remove cells that never burned in this month
            crop = crop(didBurn) ;
            past = past(didBurn) ;
            othr = othr(didBurn) ;
            obs_BA = obs_BA(didBurn) ;
            
            % Set up matrices M and D
            M_11 = cov(crop,crop) ; M_11 = M_11(1,2) ;   % The variance of crop.
            M_11 = M_11 + mean(crop)^2 ;
            M_12 = cov(crop,past) ; M_12 = M_12(1,2) ;
            M_12 = M_12 + mean(crop)*mean(past) ;
            M_21 = M_12 ;
            M_13 = cov(crop,othr) ; M_13 = M_13(1,2) ;
            M_13 = M_13 + mean(crop)*mean(othr) ;
            M_31 = M_13 ;
            M_22 = cov(past,past) ; M_22 = M_22(1,2) ;   % The variance of past.
            M_22 = M_22 + mean(past)^2 ;
            M_23 = cov(past,othr) ; M_23 = M_23(1,2) ;
            M_23 = M_23 + mean(past)*mean(othr) ;
            M_32 = M_23 ;
            M_33 = cov(othr,othr) ; M_33 = M_33(1,2) ;   % The variance of othr.
            M_33 = M_33 + mean(othr)^2 ;
            M = [M_11 M_12 M_13 ; M_21 M_22 M_23 ; M_31 M_32 M_33] ;
            clear M_*

            if isequal(inv(M),inf(size(M)))   % If matrix is singular, set all Fk's to zero.
%                 warning('v8_calcFcFp:SingularM','Matrix M is singular. Setting all Fk''s there to zero.')
                Fc_fromDoIt = 0 ;
                Fp_fromDoIt = 0 ;
                Fo_fromDoIt = 0 ;
            else
                D_1 = cov(crop,obs_BA) ; D_1 = D_1(1,2) ;
                D_1 = D_1 + mean(crop)*mean(obs_BA) ;
                D_2 = cov(past,obs_BA) ; D_2 = D_2(1,2) ;
                D_2 = D_2 + mean(past)*mean(obs_BA) ;
                D_3 = cov(othr,obs_BA) ; D_3 = D_3(1,2) ;
                D_3 = D_3 + mean(othr)*mean(obs_BA) ;
                D = [D_1 ; D_2 ; D_3] ;
                clear D_*

                % Calculate F_tmp
                if methods.constrain == 0
                    F_tmp = M \ D ;
                elseif methods.constrain == 1

                    % Calculate initial Fk values
                    constrain(1).M = M ;
                    constrain(1).D = D ;
                    constrain(1).F_col = constrain(1).M \ constrain(1).D ;
                    F_col_tmp = constrain(1).F_col ;   % For the purposes of the "while" condition

                    % Constrain Fk values, if needed
                    nc = 0 ;   % Number of constrains
                    while  ~isempty(find( F_col_tmp<0 , 1 ))
                        nc = nc + 1 ;
                        if nc > 10
                            close(w)
                            error(['Probable infinite constraining loop (>10 iterations). F_col_tmp = ' num2str(F_col_tmp') ', m=' num2str(m) '.'])
                        end

                        constrain(nc+1).ltZero = find(constrain(nc).F_col<0) ;
                        constrain(nc+1).gteZero = find(constrain(nc).F_col>=0) ;

                        constrain(nc+1).M = constrain(nc).M ;
                        constrain(nc+1).D = constrain(nc).D ;
                        constrain(nc+1).M(constrain(nc+1).ltZero,:) = [] ;
                        constrain(nc+1).M(:,constrain(nc+1).ltZero) = [] ;
                        constrain(nc+1).D(constrain(nc+1).ltZero) = [] ;

                        constrain(nc+1).F_col = constrain(nc+1).M \ constrain(nc+1).D ;
                        F_col_tmp = constrain(nc+1).F_col ;   % For the purposes of the "while" condition

                    end

                    nf = nc + 1 ;   % How many times were Fk's calculated, including original + all constrains?
                    if nf > 1
                        for f = fliplr(2:nf)
                            fMinus1_F_tmpcol = constrain(f-1).F_col ;
                            fMinus1_F_tmpcol(constrain(f).ltZero) = 0 ;
                            fMinus1_F_tmpcol(constrain(f).gteZero) = constrain(f).F_col ;
                            constrain(f-1).F_col = fMinus1_F_tmpcol ;
                        end
                    elseif nf ~= 1
                        close(w)
                        error('WTF')
                    end

                    F_tmp = constrain(1).F_col ;
                end   % "If constraining" loop

                if ~isempty(find(isinf(F_tmp) | isnan(F_tmp),1))
%                     error('Some Fk is Inf or NaN.')
                    F_tmp = pinv(M)*D ;
                end
                
                Fc_fromDoIt = F_tmp(1) ;
                Fp_fromDoIt = F_tmp(2) ;
                Fo_fromDoIt = F_tmp(3) ;
            end   % "If M not singular" loop   
        end   % "If more than 1 cell burned" loop
    end
    
end
    
    