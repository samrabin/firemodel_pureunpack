function [P,Pc,Pp,Pd,Po,const] = calcP_pureUnpack_CPDO(thisReg,methods,months)

P = nan(size(thisReg.crop),'single') ;

if methods.efficient_Ps==0
    thisReg_months = thisReg.month ;
    Pc = nan(months.num,1,'single') ;
    Pp = nan(months.num,1,'single') ;
    Pd = nan(months.num,1,'single') ;
    Po = nan(months.num,1,'single') ;
    const = nan(months.num,1,'single') ;
    for m = 1:months.num
        [tmp1,tmp2,tmp3,tmp4,tmp5,tmp6] = calcP_pureUnpack_CPDO_doIt(m) ;
        P(thisReg_months==m) = tmp1 ;
        Pc(m) = tmp2 ;
        Pp(m) = tmp3 ;
        Pd(m) = tmp4 ;
        Po(m) = tmp5 ;
        const(m) = tmp6 ;
        clear tmp*
    end
elseif methods.efficient_Ps==1
    thisReg_months = thisReg.trueMonth ;
    Pc = nan(12,1,'single') ;
    Pp = nan(12,1,'single') ;
    Pd = nan(12,1,'single') ;
    Po = nan(12,1,'single') ;
    const = nan(12,1,'single') ;
    for m = 1:12
        [tmp1,tmp2,tmp3,tmp4,tmp5,tmp6] = calcP_pureUnpack_CPDO_doIt(m) ;
        P(thisReg_months==m) = tmp1 ;
        Pc(m) = tmp2 ;
        Pp(m) = tmp3 ;
        Pd(m) = tmp4 ;
        Po(m) = tmp5 ;
        const(m) = tmp6 ;
        clear tmp*
    end
end

    function [P_fromDoIt,Pc_fromDoIt,Pp_fromDoIt,Pd_fromDoIt,Po_fromDoIt,const_fromDoIt] = ...
                calcP_pureUnpack_CPDO_doIt(m)
        
        % Get input data for this month and region
        crop = thisReg.crop(thisReg_months==m) ;
        past = thisReg.past(thisReg_months==m) ;
        defo = thisReg.defo(thisReg_months==m) ;
        othr = thisReg.othr(thisReg_months==m) ;
        obsFireYN = thisReg.obsFire_yn(thisReg_months==m) ;
        if isempty(obsFireYN)
            error(['This month (' num2str(m) ') is not included in thisReg.fire_yesNo.'])
        end
        land = [crop past defo othr] ;

        % Calculate probability of burning
        didBurn = find(obsFireYN==1) ;
        if length(didBurn) <=1
%             warning('v8_calcPa:Pa_zero','<= 1 (restricted) grid cell burned. Setting Pa to zero.')
            P_fromDoIt = zeros(size(crop)) ;
            Pc_fromDoIt = 0 ;
            Pp_fromDoIt = 0 ;
            Pd_fromDoIt = 0 ;
            Po_fromDoIt = 0 ;
            const_fromDoIt = 0 ;
        else

            % Perform logistic regression
            if methods.constant == 0
                Pk = glmfit(land,obsFireYN,'binomial','constant','off') ;
                constant = 0 ;
            elseif methods.constant == 1
                P_tmp = glmfit(land,obsFireYN,'binomial') ;
                constant = P_tmp(1) ;
                Pk = P_tmp(2:5) ; clear P_tmp
            else
                error('methods.constant is improperly specified.')
            end

            % Calculate P
            theSum = crop*Pk(1) + past*Pk(2) + defo*Pk(3) + othr*Pk(4) + constant ;
            P_fromDoIt = exp(theSum) ./ (exp(theSum) + 1) ;
            P_fromDoIt(isinf(exp(theSum))) = 1 ;
            Pc_fromDoIt = Pk(1) ;
            Pp_fromDoIt = Pk(2) ;
            Pd_fromDoIt = Pk(3) ;
            Po_fromDoIt = Pk(4) ;
            const_fromDoIt = constant ;

        end
    end

end