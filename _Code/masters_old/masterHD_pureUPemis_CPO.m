%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Run new version of 3-member (CPO) unpacking, at quarter-degree %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Notes
methods.prefix = 'REGgfed_constrain_AA_NOprenorm' ;

%%% What kind of fire emissions data?
methods.emisData = 'gfed31' ;
% methods.emisType = 'CO2' ;
methods.emisType = 'C' ;
methods.prefix = ['UPemis_' methods.emisData '_' methods.emisType '_' methods.prefix] ;


%% (1) Namelist


%%% What years are we working with?
years.list = 1997:2009 ;
% years.list = 2003:2008 ;
% years.list = 2000:2009 ;   % To match Magi et al. (2012). Extent of HYDE 3.1.
% years.list = 2001:2009 ;   % Overlap of HYDE 3.1 and Randerson.
% years.list = 2001:2003 ;   % To match Korontzi et al. (2006)

%%% What set of regions?
methods.regions = 'gfed' ;
% methods.regions = 'v1' ;   % NAmER_WAAR_fixedWAAR
% methods.regions = 'v2' ;   % NAmER_WAAR+Turkey, Australian ecoregions (WWF), WWF-defined southern border for MIDE.
% methods.regions = 'v3' ;   % NAmER_WAAR (NO Turkey), Australian ecoregions (WWF), WWF-defined southern border for MIDE.
% methods.regions = 'v4' ;   % NAmER_WAAR+Turkey, Australian ecoregions (WWF), WWF-defined southern border for MIDE, NH/SH South American ecoregions (WWF).
% methods.regions = 'v5' ;   % NAmER_WAAR (NO Turkey), Australian ecoregions (WWF), WWF-defined southern border for MIDE, NH/SH South American ecoregions (WWF).

%%% Are we normalizing?
methods.normType = 'none' ;
% methods.normType = 'max' ;   % To match Magi et al. (2012)
% methods.normType = 'mean' ;
% methods.normType = 'median' ;
if ~strcmp(methods.normType,'none')
    %%% Should cells that never burned have their normConstant changed to 0?
    %%% (0=No, 1=Yes.) If not, it'll be Inf, resulting in NaN values for
    %%% emis_norm, meaning that cells that never burned will be ignored in Fk
    %%% calculations. (This is how the NiceNumber results were obtained.)
    %     methods.set_neverBurned = -1 ; warning('You really should do something with neverBurned cells...')
    methods.set_neverBurned = 0 ;
    %     methods.set_neverBurned = 1 ;
end

%%% What land cover data to use?
methods.landcover = 'hyde' ;
% methods.landcover = 'fake_3member' ;

%%% Do two-step unpacking (ie, Pk and Fk) or just one step (ie, Fk only a
%%% la Magi et al. 2012)?
methods.numUnpackSteps = 1 ;   % To match Magi et al. (2012)
% methods.numUnpackSteps = 2 ;

%%% Area or proportion land cover?
% methods.coverType = 'prop' ;   % To match Magi et al. (2012)
methods.coverType = 'area' ;

%%% Match input data (e.g., regress proportion cover vs. frac. burned)?
% methods.match = 0 ;   % To match Magi et al. (2012)
methods.match = 1 ;

%%% Include a constant term in the logistic regression?
methods.constant = 0 ;
% methods.constant = 1 ;

%%% Constrain Fk values to >= 1?
% methods.constrain = 0 ;
methods.constrain = 1 ;   % To match Magi et al. (2012)

% Re-distribute negative OTHR BA?
methods.redistNegOTHR = 0 ;   % To match Magi et al. (2012)
% methods.redistNegOTHR = 1 ;

% Do "efficient" version of Pa code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Ps = 0 ;
% methods.efficient_Ps = 1 ;

% Do "efficient" version of FcFp code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Fs = 0 ;   % To match Magi et al. (2012)
% methods.efficient_Fs = 0.1 ;   % Inefficient method, then take median of Fk values for each month (e.g. Jan.)
% methods.efficient_Fs = 1 ;


%% (2) Setup

% Working directory and path
cd '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack'
addpath(genpath(pwd))
addpath(genpath('/Users/sam/Geodata/'))
addpath(genpath('/Users/sam/Documents/Dropbox/Projects/FireModel_daily/support'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices_s97 = (years.start - 1996):(years.end - 1996) ;
years.indices_s01 = (years.start - 2000):(years.end - 2000) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start_s97 = (years.start-1997)*12 + 1 ;
months.end_s97 = months.start_s97 + months.num - 1 ;
months.start_s01 = (years.start-2001)*12 + 1 ;
months.end_s01 = months.start_s01 + months.num - 1 ;


%% (3) Import data

% Regions
disp('Loading regions...')
load('/Users/sam/Geodata/New_regions_v4/MATLAB_QGIS/regions_v4.mat')
regions_all = regions ;
eval(['regions = regions.' methods.regions ' ;']) ;
basisregions = importdata('basisregions.txt',' ') ;
basisregions = flipud(basisregions) ;
basisregions(basisregions==0) = NaN ;
regions.map = single(basisregions) ; warning('Setting region map to GFED-original!') ; clear basisregions
regions.Yarray = repmat(regions.map,[1 1 years.num]) ;
regions.Marray = repmat(regions.map,[1 1 months.num]) ;
regKey = regions.Marray(~isnan(regions.Marray)) ;

% % Flammability
% load('/Users/sam/Documents/Dropbox/AGU 2013/flammability.mat')
% tmp = nan([720 1440 120],'single') ;
% tmp(~isnan(repmat(regions.map,[1 1 120]))) = flammability ;
% tmp = tmp(:,:,months.start_s01:months.end_s01) ;
% flammability = tmp(~isnan(regions.Marray)) ;
% clear tmp
% tmp = nan([720 1440 120],'single') ;
% tmp(~isnan(repmat(regions.map,[1 1 120]))) = lightning ;
% tmp = tmp(:,:,months.start_s01:months.end_s01) ;
% lightning = tmp(~isnan(regions.Marray)) ;
% clear tmp

% Month datasets
Marray_month = zeros(360,720,months.num,'int16') ;
for m = 1:months.num
    Marray_month(:,:,m) = m*ones(360,720) ;
end ; clear m
Marray_trueMonth = zeros(360,720,12,'int8') ;
for m = 1:12
    Marray_trueMonth(:,:,m) = m*ones(360,720) ;
end ; clear m
Marray_trueMonth = repmat(Marray_trueMonth,[1 1 years.num]) ;

% Land covers
if strcmp(methods.landcover,'hyde')
    disp('Loading HYDE v3.1 land cover data...')
    load('/Users/sam/Geodata/HYDE31/MAT-files/HYDE31prop_halfDeg_9709.mat')
    landarea = HYDE_landarea_km2 ; clear HYDE_landarea_km2
    landarea_Marray = repmat(landarea,[1 1 months.num]) ;
    LCcrop_tmp = LCprop_crop_HYDE31_9709(:,:,years.indices_s97) ;
    LCpast_tmp = LCprop_past_HYDE31_9709(:,:,years.indices_s97) ;
elseif strcmp(methods.landcover,'fake_3member')
    if years.start<2001 ; error('MCD12 MAT-file only goes back to 2001.') ; end
    error('Make a MAT-file for half-deg MCD12C1.')
    
    % Fake classifications. Excludes Water (1) and FillValue/Unclassified (18).
    fake_crop = [13 15] ;   % Croplands, Cropland/NaturalVegetationMosaic
    fake_past = 8:11 ;   % OpenShrublands, WoodySavannas, Savannas, Grasslands
    fake_othr = [2:7 12 14 16 17] ;   % EvergreenNeedleleafForest, EvergreenBroadleafForest, DeciduousNeedleleafForest, DeciduousBroadleafForest, MixedForest, ClosedShrublands, PermanentWetlands, UrbanAndBuiltup, SnowAndIce, BarrenOrSparselyVegetated
    
    load('/Users/sam/Geodata/MCD12C1_quartDeg/MCD12C1_QD_0110.mat')
    MCD12C1_QD_0110 = MCD12C1_QD_0110(:,:,:,years.indices_s01) ;
    
    landarea_Yarray = squeeze(sum(MCD12C1_QD_0110,3)) ;
    water_mask = 0==min(squeeze(sum(MCD12C1_QD_0110(:,:,2:17,:),3)),[],3) ;   % Find grid cells that have no land other than Water (1) or FillValue/Unclassified (18) in at least one year.
    landarea_Yarray(repmat(water_mask,[1 1 years.num])) = NaN ;
    landarea = mean(landarea_Yarray,3) ;
    landarea_Marray = nan(360,720,months.num) ;
    for y = 1:years.num
        monthTMP_start = (y-1)*12 + 1 ;
        monthTMP_end = monthTMP_start + 11 ;
        landarea_Marray(:,:,monthTMP_start:monthTMP_end) = repmat(landarea_Yarray(:,:,y),[1 1 12]) ;
    end
    
    LCcrop_tmp = squeeze(sum(MCD12C1_QD_0110(:,:,fake_crop,:),3)) ./ landarea_Yarray ;
    LCpast_tmp = squeeze(sum(MCD12C1_QD_0110(:,:,fake_past,:),3)) ./ landarea_Yarray ;

else error('methods.landcover improperly specified.')
end
LCothr_tmp = ones(size(LCcrop_tmp)) - (LCcrop_tmp + LCpast_tmp) ;
clear LCprop_*_HYDE31_9709
if ~isempty(find(isnan(mean(LCcrop_tmp+LCpast_tmp,3)) & ~isnan(regions.map),1))
%     badHYDEnans = isnan(LCcrop_tmp(:,:,1)) ;
    badHYDEnans = (isnan(mean(LCcrop_tmp,3)) | isnan(mean(LCpast_tmp,3))) ; warning('New way of reconciling NaN''s from different datasets.')
    regions.map(badHYDEnans) = NaN ;
    regions.Marray = repmat(regions.map,[1 1 months.num]) ;
    regions.Yarray = repmat(regions.map,[1 1 years.num]) ;
    Marray_month = Marray_month(~isnan(regions.Marray)) ;
    Marray_trueMonth = Marray_trueMonth(~isnan(regions.Marray)) ;
    regKey = regions.Marray(~isnan(regions.Marray)) ;
end
methods.num_cells = length(find(~isnan(regions.map))) ;
methods.num_months = months.num ;
LCfrac_crop = nan(size(regions.Marray),'single') ;
LCfrac_past = nan(size(regions.Marray),'single') ;
LCfrac_othr = nan(size(regions.Marray),'single') ;
for y = 1:years.num
    monthTMP_start = (y-1)*12 + 1 ;
    monthTMP_end = monthTMP_start + 11 ;
    LCfrac_crop(:,:,monthTMP_start:monthTMP_end) = repmat(LCcrop_tmp(:,:,y),[1 1 12]) ;
    LCfrac_past(:,:,monthTMP_start:monthTMP_end) = repmat(LCpast_tmp(:,:,y),[1 1 12]) ;
    LCfrac_othr(:,:,monthTMP_start:monthTMP_end) = repmat(LCothr_tmp(:,:,y),[1 1 12]) ;
end ; clear y monthTMP_*
clear LC*_tmp
LCfrac_crop = LCfrac_crop(~isnan(regions.Marray)) ;
LCfrac_past = LCfrac_past(~isnan(regions.Marray)) ;
LCfrac_othr = LCfrac_othr(~isnan(regions.Marray)) ;
LCdata_crop = LCfrac_crop ;
LCdata_past = LCfrac_past ;
LCdata_othr = LCfrac_othr ;
if strcmp(methods.coverType,'area')
    if strcmp(methods.landcover,'hyde')
        landarea_tmp = landarea(~isnan(regions.map)) ;
        landarea_tmp = repmat(landarea_tmp,[months.num 1]) ;
        LCdata_crop = LCdata_crop .* landarea_tmp ;
        LCdata_past = LCdata_past .* landarea_tmp ;
        LCdata_othr = LCdata_othr .* landarea_tmp ;
        clear landarea_tmp
    elseif strcmp(methods.landcover,'fake_3member')
        landarea_tmp = landarea_Marray(~isnan(regions.Marray)) ;
        LCdata_crop = LCdata_crop .* landarea_tmp ;
        LCdata_past = LCdata_past .* landarea_tmp ;
        LCdata_othr = LCdata_othr .* landarea_tmp ;
        clear landarea_tmp
    end
end

% Observed emissions
disp('Loading observed emissions data...')
if strcmp(methods.emisData,'gfed31')
    load(['/Volumes/Repository/Geodata_Repository/GFED_31_emissions/MATfiles/Totals/GFED3.1_' methods.emisType '_97-11.mat'])
    obsEmis = single(GFED31_emissions(:,:,months.start_s97:months.end_s97)) ;
    obsEmis = (obsEmis*1e6) .* landarea_Marray ;   % Convert g/m2 to g/km2, then multiply by km2 land area
    clear GFED31_emissions
else error('methods.emisData improperly specified.')
end
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    [obsEmisNorm,emis_normConstant_orig,emis_normConstant] = normObs(obsEmis,methods) ;
    obsEmisNorm = single(obsEmisNorm(~isnan(regions.Marray))) ;
    emis_normConstant_orig = single(emis_normConstant_orig(~isnan(regions.map))) ;
    emis_normConstant = single(emis_normConstant(~isnan(regions.map))) ;
elseif strcmp(methods.normType,'none')
    emis_normConstant = max(obsEmis,[],3) ;
    emis_normConstant = emis_normConstant(~isnan(regions.map)) ;
else error('methods.normType improperly specified.')
end
obsEmis_dens = single(obsEmis ./ landarea_Marray) ;
obsEmis = obsEmis(~isnan(regions.Marray)) ;
clear BA_*res
obsEmis_dens = single(obsEmis_dens(~isnan(regions.Marray))) ;
obsEmis_yn = single(obsEmis>0) ;

% % Remove cells that are NaN in HYDE data
% if ~isempty(find(isnan(LCfrac_crop),1))
%     disp('Correcting for HYDE NaN''s.')
%     notnan_hyde = ~isnan(LCfrac_crop) ;
%     LCfrac_crop = LCfrac_crop(notnan_hyde) ;
%     LCfrac_past = LCfrac_past(notnan_hyde) ;
%     LCfrac_othr = LCfrac_othr(notnan_hyde) ;
%     LCdata_crop = LCdata_crop(notnan_hyde) ;
%     LCdata_past = LCdata_past(notnan_hyde) ;
%     LCdata_othr = LCdata_othr(notnan_hyde) ;
%     Marray_month = Marray_month(notnan_hyde) ;
%     Marray_trueMonth = Marray_trueMonth(notnan_hyde) ;
%     obsEmis = obsEmis(notnan_hyde) ;
%     if ~strcmp(methods.normType,'none')
%         obsEmisNorm = obsEmisNorm(notnan_hyde) ;
%     end
%     obsEmis_dens = obsEmis_dens(notnan_hyde) ;
%     obsEmis_yn = obsEmis_yn(notnan_hyde) ;
%     tmp = notnan_hyde(1:methods.num_cells) ;
%     emis_normConstant = emis_normConstant(tmp) ;
%     emis_normConstant_orig = emis_normConstant_orig(tmp) ;
%     clear tmp
% end

disp('Done.') ; disp(' ')
    
    
%% (4) Do unpacking

disp('Setting up for unpacking...')

if methods.numUnpackSteps == 2
    allReg_P = nan(size(obsEmis)) ;
    if methods.efficient_Ps == 0
        allReg_Pc_mr = nan(months.num,regions.num,'single') ;
        allReg_Pp_mr = nan(months.num,regions.num,'single') ;
        allReg_Po_mr = nan(months.num,regions.num,'single') ;
        allReg_const_mr = nan(months.num,regions.num,'single') ;
    elseif methods.efficient_Ps == 1
        allReg_Pc_mr = nan(12,regions.num,'single') ;
        allReg_Pp_mr = nan(12,regions.num,'single') ;
        allReg_Po_mr = nan(12,regions.num,'single') ;
        allReg_const_mr = nan(12,regions.num,'single') ;
    end
end
allReg_Fc = nan(size(obsEmis),'single') ;
allReg_Fp = nan(size(obsEmis),'single') ;
allReg_Fo = nan(size(obsEmis),'single') ;
if methods.efficient_Fs == 0
    allReg_Fc_mr = nan(months.num,regions.num,'single') ;
    allReg_Fp_mr = nan(months.num,regions.num,'single') ;
    allReg_Fo_mr = nan(months.num,regions.num,'single') ;
elseif methods.efficient_Fs==0.1 || methods.efficient_Fs==1
    allReg_Fc_mr = nan(12,regions.num,'single') ;
    allReg_Fp_mr = nan(12,regions.num,'single') ;
    allReg_Fo_mr = nan(12,regions.num,'single') ;
end

disp('Performing unpacking...')
for r = 1:regions.num
    disp(['   Region ' num2str(r) ' of ' num2str(regions.num) ': ' regions.names{r} '...'])
    
    % Set up thisReg
    thisReg_indices = (regKey==r) ;
    thisReg.crop = LCdata_crop(thisReg_indices) ;
    thisReg.past = LCdata_past(thisReg_indices) ;
    thisReg.othr = LCdata_othr(thisReg_indices) ;
    if strcmp(methods.normType,'none')
        thisReg.obsFire = obsEmis(thisReg_indices) ;
    else
        thisReg.obsFire = obsEmisNorm(thisReg_indices) ;
    end
    thisReg.month = Marray_month(thisReg_indices) ;
    thisReg.trueMonth = Marray_trueMonth(thisReg_indices) ;
    
    % Calculate probability of fire, if doing so
    if methods.numUnpackSteps == 2
        thisReg.obsFire_yn = obsEmis_yn(thisReg_indices) ;
        [P,Pc,Pp,Po,const] = calcP_pureUnpack_CPO(thisReg,methods,months) ;
        if ~isempty(find(isnan(P),1))
            error(['Some value of P is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
        end
        allReg_P(thisReg_indices) = P ;
        allReg_Pc_mr(:,r) = Pc ;
        allReg_Pp_mr(:,r) = Pp ;
        allReg_Po_mr(:,r) = Po ;
        allReg_const_mr(:,r) = const ;
        clear P Pc Pp Po const
    end
    
    % Calculate Fk's
    if methods.numUnpackSteps == 1
        [Fc,Fp,Fo,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_CPO_1step(thisReg,months,methods) ;
    elseif methods.numUnpackSteps == 2
        [Fc,Fp,Fo,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_CPO(thisReg,months,methods) ;
    end
    if ~isempty(find(isnan(Fc_m.*Fp_m.*Fo_m),1))
        error(['Some Fk is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
    end
    allReg_Fc(thisReg_indices) = Fc ;
    allReg_Fp(thisReg_indices) = Fp ;
    allReg_Fo(thisReg_indices) = Fo ;
    allReg_Fc_mr(:,r) = Fc_m ;
    allReg_Fp_mr(:,r) = Fp_m ;
    allReg_Fo_mr(:,r) = Fo_m ;
    clear Fc Fp Fo Fc_m Fp_m Fo_m
    
end
disp('Done.') ; disp('   ')


%% (5) Calculate amount of estimated emissions
disp('Calculating amount of estimated emissions...')

if methods.numUnpackSteps == 2
    estEmis_crop = allReg_P .* allReg_Fc .* LCdata_crop ;
    estEmis_past = allReg_P .* allReg_Fp .* LCdata_past ;
    estEmis_othr = allReg_P .* allReg_Fo .* LCdata_othr ;
elseif methods.numUnpackSteps == 1
    estEmis_crop = allReg_Fc .* LCdata_crop ;
    estEmis_past = allReg_Fp .* LCdata_past ;
    estEmis_othr = allReg_Fo .* LCdata_othr ;
end

% De-normalize, if necessary
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    estEmis_crop = estEmis_crop .* repmat(emis_normConstant,[months.num 1]) ;
    estEmis_past = estEmis_past .* repmat(emis_normConstant,[months.num 1]) ;
    estEmis_othr = estEmis_othr .* repmat(emis_normConstant,[months.num 1]) ;
end

estEmis_total = estEmis_crop + estEmis_past + estEmis_othr ;

% Re-distribute negative fire, if doing so
if methods.redistNegOTHR == 1
    disp('Re-distributing negative fire...')
    [estEmis2_crop, estEmis2_past, estEmis2_othr] = reDistNeg_pureUnpack_CPO( ...
        estEmis_crop, estEmis_past, estEmis_othr, ...
        LCfrac_crop,  LCfrac_past,  LCfrac_othr) ;
    estEmis2_total = estEmis2_crop + estEmis2_past + estEmis2_othr ;
else
    estEmis2_crop = estEmis_crop ;
    estEmis2_past = estEmis_past ;
    estEmis2_othr = estEmis_othr ;
    estEmis2_total = estEmis_total ;
end

disp('Done.') ; disp(' ')


%% (6) Display performance by region

disp(['Mean annual fire emissions of ' methods.emisType ':'])

% regions_tmp = regions_all.gfed ;
regions_tmp = regions ; warning('Always uses current regions set!')
if exist('badHYDEnans','var')
    regions_tmp.map(badHYDEnans) = NaN ;
end
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;
regKey_tmp = regions_tmp.Marray(~isnan(regions_tmp.Marray)) ;

showPerf_incl = true(size(estEmis_total)) ;
% showPerf_incl = (repmat(emis_normConstant_orig,[months.num 1])==0) ;
% showPerf_incl = (repmat(emis_normConstant_orig,[months.num 1])>0) ;

corr_allRegs = nan(regions_tmp.num,1) ;
errPerc_allRegs = nan(regions_tmp.num,1) ;
for r = 1:regions_tmp.num
    regName = char(regions_tmp.names(r)) ;
    est_thisReg_list = estEmis2_total(regKey_tmp==r & showPerf_incl) ;
    est_thisReg = sum(est_thisReg_list)/years.num ;
    obs_thisReg_list = obsEmis(regKey_tmp==r & showPerf_incl) ;
    obs_thisReg = sum(obs_thisReg_list)/years.num ;
    errPerc_thisReg = 100 * (est_thisReg - obs_thisReg) / obs_thisReg ;
    errPerc_allRegs(r) = errPerc_thisReg ;
    
    corr_thisReg = roundn(corr(obs_thisReg_list,est_thisReg_list),-3) ;
    corr_allRegs(r) = corr_thisReg ;
    
    normConstant_thisReg = repmat(emis_normConstant,[months.num 1]) ;
    normConstant_thisReg = normConstant_thisReg(regKey_tmp==r) ;
    corrCellsWithSomeBurn(r,1) = roundn(corr(obs_thisReg_list(normConstant_thisReg>0),est_thisReg_list(normConstant_thisReg>0)),-3) ;
    clear normConstant_thisReg
    
    SSE_thisReg = sum((estEmis2_total(regKey_tmp==r & showPerf_incl) - obsEmis(regKey_tmp==r & showPerf_incl)).^2) ;
    maxSSE_thisReg = sum((0 - obsEmis(regKey_tmp==r & showPerf_incl)).^2) ;
    SSE_performance = SSE_thisReg / maxSSE_thisReg ;
    
    disp([regName ': Obs. = ' num2str(roundn(obs_thisReg,0)) ', Est. = ' num2str(roundn(est_thisReg,0)) ' (' num2str(roundn(errPerc_thisReg,-1)) '% error, r=' num2str(corr_thisReg) ', SSEperf=' num2str(SSE_performance) ').'])
end ; clear r *thisReg* regName *SSE_thisReg SSE_performance
global_obs = sum(obsEmis(showPerf_incl))/years.num ;
global_est = sum(estEmis2_total(showPerf_incl))/years.num ;
global_errPerc = 100*(global_est-global_obs) ./ global_obs ;
disp(['Global: Obs. = ' num2str(roundn(global_obs,0)) ', Est. = ' num2str(roundn(global_est,0)) ' (' num2str(roundn(global_errPerc,-1)) '% error).'])

% Find correlation coefficient for mean est vs obs
pearsons_r = corr(obsEmis(showPerf_incl),estEmis2_total(showPerf_incl)) ;
disp(['     Pearson''s r = ' num2str(pearsons_r)])
tmp = repmat(emis_normConstant,[months.num 1]) ;
pearsons_r_CellsWithSomeBurn = corr(obsEmis(showPerf_incl & tmp>0),estEmis2_total(showPerf_incl & tmp>0)) ;

% Find sum of squared errors
sum_squared_errors = sum((estEmis2_total(showPerf_incl)-obsEmis(showPerf_incl)).^2) ;
fprintf('%s','     Sum of squared errors = ')
fprintf('%e\n',sum_squared_errors)

% How does it compare to guessing 0 for everything?
max_squared_errors = sum(obsEmis(showPerf_incl).^2) ;
squared_errors_performance = sum_squared_errors / max_squared_errors ;
disp(['     "SSE performance" = ' num2str(squared_errors_performance)])

disp('Regional correlation coefficients:')
disp(num2str([corr_allRegs ; pearsons_r]))

disp('Regional correlation coefficients in cells that burned at least once ever:')
disp(num2str([corrCellsWithSomeBurn ; pearsons_r_CellsWithSomeBurn]))

disp('Regional percent error:')
disp(num2str([errPerc_allRegs ; global_errPerc]))

disp(' ') ; clear global_* regions_tmp regKey_tmp

total = 0 ;

tmp = estEmis2_crop ;
tmp = reshape(tmp,[methods.num_cells,methods.num_months]) ;
tmp = sum(tmp,2) ;
tmp = sum(tmp) / years.num ;
total = total + tmp ;
disp(['Average annual estimated BA (km2), CROP = ' num2str(tmp)])

tmp = estEmis2_past ;
tmp = reshape(tmp,[methods.num_cells,methods.num_months]) ;
tmp = sum(tmp,2) ;
tmp = sum(tmp) / years.num ;
total = total + tmp ;
disp(['Average annual estimated BA (km2), PAST = ' num2str(tmp)])

tmp = estEmis2_othr ;
tmp = reshape(tmp,[methods.num_cells,methods.num_months]) ;
tmp = sum(tmp,2) ;
tmp = sum(tmp) / years.num ;
total = total + tmp ;
disp(['Average annual estimated BA (km2), OTHR = ' num2str(tmp)])

disp(['AVERAGE ANNUAL ESTIMATED BA (km2), TOTAL = ' num2str(total)])
disp(' ')

clear total tmp

methods


%% Save data
disp('Saving MAT-file...')

filename = ['/Volumes/Repository/UnpackEmis_output/MAT-files/' methods.prefix '.mat'] ;
if ~isempty(dir(filename))
    error('Change filename to avoid overwriting existing data.')
end
save(filename,'-regexp','allReg_F*')
save(filename,'-regexp','estEmis*','-append')
save(filename,'methods','-append')

disp('Done.') ; disp(' ')


%% Save maps of estimated BA and BFrac to GeoTIFF

outDir = '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack/Output_QD/maps/' ;

R = georasterref('Latlim',[-90 90],'Lonlim',[-180 180],'RasterSize',[720 1440]) ;
addpath(genpath('~/Documents/Dropbox/Projects/FireModel_globLoc/'))

filename = [outDir methods.prefix '_' methods.emisData '.tif'] ;
if isempty(dir(filename))
    disp('Saving maps of observed fire...')
    map = nan(size(regions.Marray)) ;
    map(~isnan(regions.Marray)) = obsEmis ;
    
    map_BA = 12*mean(map,3) ;
        
    save_for_qgis(filename,map_BA,R) ;
%     imwrite(flipud(map_BA),filename,'TIFF','Compression','none','Resolution',[720 360])
        
    clear map_BA*
end

disp('Saving maps of CROP fire...')
map = nan(size(regions.Marray)) ;
map(~isnan(regions.Marray)) = estEmis2_crop ;
map_BA = 12*mean(map,3) ;
map_LC = nan(size(regions.Marray)) ;
map_LC(~isnan(regions.Marray)) = LCfrac_crop ;
map_BAfrac = map ./ (map_LC .* landarea_Marray) ;
map_BAfrac = 12*mean(map_BAfrac,3) ;
map_BAfrac(max(map_LC,[],3)==0) = -1 ;
filename = [outDir methods.prefix '_estBA_crop.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
save_for_qgis(filename,map_BA,R) ;
filename = [outDir methods.prefix '_estBAfrac_crop.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
save_for_qgis(filename,map_BAfrac,R) ;
clear map_*

disp('Saving maps of PAST fire...')
map = nan(size(regions.Marray)) ;
map(~isnan(regions.Marray)) = estEmis2_past ;
map_BA = 12*mean(map,3) ;
map_LC = nan(size(regions.Marray)) ;
map_LC(~isnan(regions.Marray)) = LCfrac_past ;
map_BAfrac = map ./ (map_LC .* landarea_Marray) ;
map_BAfrac = 12*mean(map_BAfrac,3) ;
map_BAfrac(max(map_LC,[],3)==0) = -1 ;
filename = [outDir methods.prefix '_estBA_past.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
save_for_qgis(filename,map_BA,R) ;
filename = [outDir methods.prefix '_estBAfrac_past.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
save_for_qgis(filename,map_BAfrac,R) ;
clear map_*

disp('Saving maps of OTHR fire...')
map = nan(size(regions.Marray)) ;
map(~isnan(regions.Marray)) = estEmis2_othr ;
map_BA = 12*mean(map,3) ;
map_LC = nan(size(regions.Marray)) ;
map_LC(~isnan(regions.Marray)) = LCfrac_othr ;
map_BAfrac = map ./ (map_LC .* landarea_Marray) ;
map_BAfrac = 12*mean(map_BAfrac,3) ;
map_BAfrac(max(map_LC,[],3)==0) = -1 ;
filename = [outDir methods.prefix '_estBA_othr.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
save_for_qgis(filename,map_BA,R) ;
filename = [outDir methods.prefix '_estBAfrac_othr.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
save_for_qgis(filename,map_BAfrac,R) ;
clear map_*

disp('Saving maps of TOTAL EST. fire...')
map = nan(size(regions.Marray)) ;
map(~isnan(regions.Marray)) = estEmis2_total ;
map_BA = 12*mean(map,3) ;
map_BAfrac = map ./ landarea_Marray ;
map_BAfrac = 12*mean(map_BAfrac,3) ;
filename = [outDir methods.prefix '_estBA_total.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
save_for_qgis(filename,map_BA,R) ;
filename = [outDir methods.prefix '_estBAfrac_total.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
save_for_qgis(filename,map_BAfrac,R) ;
clear map_BA*

disp('Done.')

beep ; pause(0.15) ; beep ; pause(0.15) ; beep


%% Display normed NF/BA seasonality: One region

% region_toPlot = 1 ;
region_toPlot = 32 ;   % WAAR

% regions_tmp = regions_all.gfed ;
regions_tmp = regions_all.v1 ;
if exist('badHYDEnans','var')
    regions_tmp.map(badHYDEnans) = NaN ;
end
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;
regKey_tmp = regions_tmp.Marray(~isnan(regions_tmp.Marray)) ;

disp(['Plotting normed CPO seasonality: ' regions_tmp.names{region_toPlot}])

dailyv3_seasonality_CPOnorm_oneReg(regions_tmp,region_toPlot,estEmis2_crop,estEmis2_past,estEmis2_othr,estEmis2_total,obsEmis,'mean')
clear tmp_regions Nf2 region_toPlot


%% Display normed NF/BA seasonality: One country

% load('~/Documents/Dropbox/AGU 2013/MATfiles/rabinV9.mat')

% country_toPlot = 41 ; country_name = 'India' ;
country_toPlot = 185 ; country_name = 'Ukraine' ;
% country_toPlot = 5 ; country_name = 'USA' ;
% country_toPlot = 69 ; country_name = 'Mali' ;

regions_tmp.map = single(flipud(imread('~/Geodata/New_regions_v3/Processed_rasters/Countries_raster.tif','TIF'))) ;
regions_tmp.map(isnan(regions.map)) = NaN ;
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;

disp(['Plotting normed CPO seasonality: ' country_name])

dailyv3_seasonality_CPOnormPlusFlamLtng_oneReg(regions_tmp,country_toPlot,estEmis2_crop,estEmis2_past,estEmis2_othr,flammability,lightning,'mean',0)
clear regions_tmp Nf2 country_toPlot

filename = ['~/Documents/Dropbox/AGU 2013/Regional_BAnorm_seasonality/rabinV9flam_' country_name '.eps'] ;
if ~isempty(dir(filename))
    error('Change filename to avoid overwriting existing data.')
end
saveas(gca,filename,'epsc')


%% Display NONnormed NF/BA seasonality: One country

% load('~/Documents/Dropbox/AGU 2013/MATfiles/rabinV7.mat')

% country_toPlot = 41 ; country_name = 'India' ;
country_toPlot = 185 ; country_name = 'Ukraine' ;
% country_toPlot = 5 ; country_name = 'USA' ;
% country_toPlot = 69 ; country_name = 'Mali' ;

regions_tmp.map = single(flipud(imread('~/Geodata/New_regions_v3/Processed_rasters/Countries_raster.tif','TIF'))) ;
regions_tmp.map(isnan(regions.map)) = NaN ;
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;

disp(['Plotting normed CPO seasonality: ' country_name])

dailyv3_seasonality_CPOunnorm_oneReg(regions_tmp,country_toPlot,estEmis2_crop,estEmis2_past,estEmis2_othr,'mean',0)
clear regions_tmp Nf2 country_toPlot

% filename = ['~/Documents/Dropbox/AGU 2013/Regional_BAnorm_seasonality/rabinV4mod14flam_' country_name '.eps'] ;
% if ~isempty(dir(filename))
%     error('Change filename to avoid overwriting existing data.')
% end
% saveas(gca,filename,'epsc')


%% Display non-normed NF/BA CROP seasonality: One country

% load('~/Documents/Dropbox/AGU 2013/MATfiles/rabinV7.mat')

country_toPlot = 41 ; country_name = 'India' ;
% country_toPlot = 185 ; country_name = 'Ukraine' ;
% country_toPlot = 5 ; country_name = 'USA' ;

regions_tmp.map = single(flipud(imread('~/Geodata/New_regions_v3/Processed_rasters/Countries_raster.tif','TIF'))) ;
regions_tmp.map(isnan(regions.map)) = NaN ;
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;
regKey_tmp = regions_tmp.Marray(~isnan(regions_tmp.Marray)) ;

disp(['Plotting normed CPO seasonality: ' country_name])

if ~exist('dailyv3_seasonality_CROPunnorm_oneReg','file')
    addpath(genpath('~/Documents/Dropbox/Projects/FireModel_globLoc/'))
end
dailyv3_seasonality_CROPunnorm_oneReg(regions_tmp,country_toPlot,estEmis2_crop,'mean',0)
clear regions_tmp Nf2 country_toPlot

filename = ['~/Documents/Dropbox/AGU 2013/Regional_BAnorm_seasonality/rabinV10CROPonly_' country_name '.eps'] ;
if ~isempty(dir(filename))
    error('Change filename to avoid overwriting existing data.')
end
saveas(gca,filename,'epsc')


%% Display non-normed NF/BA CROP seasonality Obs+Est: One country

load('~/Documents/Dropbox/AGU 2013/MATfiles/rabinV10.mat')

country_toPlot = 41 ; country_name = 'India' ;
% country_toPlot = 185 ; country_name = 'Ukraine' ;
% country_toPlot = 5 ; country_name = 'USA' ;

regions_tmp.map = single(flipud(imread('~/Geodata/New_regions_v3/Processed_rasters/Countries_raster.tif','TIF'))) ;
regions_tmp.map(isnan(regions.map)) = NaN ;
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;
regKey_tmp = regions_tmp.Marray(~isnan(regions_tmp.Marray)) ;

load('gfed4_BAcrop_lowres_01-10_km2.mat')
obsEmis_crop = gfed4_BAcrop_lowres ;
clear gfed4_BAcrop_lowres
obsEmis_crop = obsEmis_crop(:,:,months.start_s01:months.end_s01) ;
obsEmis_crop = obsEmis_crop(~isnan(regions_tmp.Marray)) ;

disp(['Plotting normed CPO seasonality: ' country_name])

if ~exist('dailyv3_seasonality_CROPunnorm_oneReg','file')
    addpath(genpath('~/Documents/Dropbox/Projects/FireModel_globLoc/'))
end
dailyv3_seasonality_CROPunnormOE_oneReg(regions_tmp,country_toPlot,estEmis2_crop,obsEmis_crop,'mean',0)
clear regions_tmp Nf2 country_toPlot

filename = ['~/Documents/Dropbox/AGU 2013/Regional_BAnorm_seasonality/rabinV10CROPonlyOE_' country_name '.eps'] ;
if ~isempty(dir(filename))
    error('Change filename to avoid overwriting existing data.')
end
saveas(gca,filename,'epsc')


%% Save one-region normed NF/BA seasonality plots to file: All regions

% load('/Users/sam/Documents/Dropbox/AGU 2013/MATfiles/rabinV2.mat')

regions_tmp_toUse = 'gfed' ;
% regions_tmp_toUse = 'pls5v2' ;
eval(['regions_tmp = regions_all.' regions_tmp_toUse ';']) ;
if exist('badHYDEnans','var')
    regions_tmp.map(badHYDEnans) = NaN ;
end
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;

for r = 1:regions_tmp.num
    disp(['Plotting normed CPO seasonality: ' regions_tmp.names{r}])
    filename = ['~/Documents/Dropbox/AGU 2013/Regional_BAnorm_seasonality/rabinV10flam_' regions_tmp_toUse '_' regions_tmp.names{r} '.eps'] ;
    if ~isempty(dir(filename))
%         error('Change filename to avoid overwriting existing data.')
    end
    dailyv3_seasonality_CPOnormPlusFlamLtng_oneReg(regions_tmp,r,estEmis2_crop,estEmis2_past,estEmis2_othr,flammability,lightning,'mean',0)
    saveas(gca,filename,'epsc')
    close
end ; clear r

clear tmp_regions


%% Save one-region normed Fk seasonality plots to file: All regions

regions_tmp_toUse = 'gfed' ;
% regions_tmp_toUse = 'pls5v2' ;
eval(['regions_tmp = regions_all.' regions_tmp_toUse ';']) ;
if exist('badHYDEnans','var')
    regions_tmp.map(badHYDEnans) = NaN ;
end
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;

for r = 1:regions_tmp.num
    disp(['Plotting normed CPO seasonality: ' regions_tmp.names{r}])
    filename = ['~/Documents/Dropbox/AGU 2013/Regional_FkNORM_seasonality/rabinV10_' regions_tmp_toUse '_' regions_tmp.names{r} '.eps'] ;
    if ~isempty(dir(filename))
        error('Change filename to avoid overwriting existing data.')
    end
    seasonality_Fk_oneReg(regions_tmp,r,allReg_Fc,allReg_Fp,allReg_Fo,'mean')
    saveas(gca,filename,'epsc')
    close
end ; clear r


%% Display annual average worldwide fire of each type

% load('/Users/sam/Documents/Dropbox/AGU 2013/MATfiles/rabinV6.mat')

total = 0 ;

tmp = estEmis2_crop ;
tmp = reshape(tmp,[methods.num_cells,methods.num_months]) ;
tmp = sum(tmp,2) ;
tmp = sum(tmp) / years.num ;
total = total + tmp ;
disp(['Average annual estimated BA (km2), CROP = ' num2str(tmp)])

tmp = estEmis2_past ;
tmp = reshape(tmp,[methods.num_cells,methods.num_months]) ;
tmp = sum(tmp,2) ;
tmp = sum(tmp) / years.num ;
total = total + tmp ;
disp(['Average annual estimated BA (km2), PAST = ' num2str(tmp)])

tmp = estEmis2_othr ;
tmp = reshape(tmp,[methods.num_cells,methods.num_months]) ;
tmp = sum(tmp,2) ;
tmp = sum(tmp) / years.num ;
total = total + tmp ;
disp(['Average annual estimated BA (km2), OTHR = ' num2str(tmp)])

disp(['AVERAGE ANNUAL ESTIMATED BA (km2), TOTAL = ' num2str(total)])
disp(' ')

clear total tmp


%% Save maps of estimated fraction burning for each LC type to GeoTIFF

outDir = '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack/Output_QD/maps_LCfracBurn/' ;
% R2 = georasterref('Latlim',[-90 90],'Lonlim',[-180 180],'RasterSize',[720 1440]) ;
% R = georasterref('LatitudeLimits',[-90 90],'LongitudeLimits',[-180 180],'RasterSize',[720 1440]) ;
[~,R] = geotiffread('LandArea_ORNL.tif') ;
if ~exist('save_for_qgis','file')
    addpath(genpath('~/Documents/Dropbox/Projects/FireModel_globLoc/'))
end

filename = [outDir methods.prefix '_estFracBurned_crop.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
map = nan(size(regions.Marray)) ;
map(~isnan(regions.Marray)) = estEmis2_crop ;
map = 12*mean(map,3) ;
map = map .* (landarea.*LCfrac_crop) ;
save_for_qgis(filename,map,R) ;
clear map

filename = [outDir methods.prefix '_estFracBurned_past.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
map = nan(size(regions.Marray)) ;
map(~isnan(regions.Marray)) = estEmis2_past ;
map = 12*mean(map,3) ;
map = map .* (landarea.*LCfrac_past) ;
save_for_qgis(filename,map,R) ;
clear map

filename = [outDir methods.prefix '_estFracBurned_othr.tif'] ;
if ~isempty(dir(filename)) ; error('Change filename to avoid overwriting files.') ; end
map = nan(size(regions.Marray)) ;
map(~isnan(regions.Marray)) = estEmis2_othr ;
map = 12*mean(map,3) ;
map = map .* (landarea.*LCfrac_othr) ;
save_for_qgis(filename,map,R) ;
clear map

%% Confidence intervals!!!

% Start out by just looking at one parameter set: May 2002 (month 17) in INDS (reg. 24)
thisMonth = 17 ;
thisReg = 24 ;

% Fc_200205_INDS = allReg_Fc_mr(thisMonth,thisReg) ;
num_gridcells = length(find(regions.map==thisReg)) ;
t_crit = abs(tinv(0.05/2,num_gridcells)) ;
inThis_MonthReg = (Marray_month==thisMonth & regKey==thisReg) ;

MSE_thisFc = sum((obsEmis(inThis_MonthReg)-estEmis2_total(inThis_MonthReg)).^2) / (num_gridcells-3-1) ;
SE_thisFc = sqrt(MSE_thisFc) ;



%%%%%%
% http://stats.stackexchange.com/questions/27916/standard-errors-for-multiple-regression-coefficients

X = [repmat(allReg_Fc_mr(thisMonth,thisReg),[num_gridcells 1]) repmat(allReg_Fp_mr(thisMonth,thisReg),[num_gridcells 1]) repmat(allReg_Fo_mr(thisMonth,thisReg),[num_gridcells 1])] ;
Xt = X' ;
XtX = Xt*X ;

%% Calculate correlation coefficient of cropland predictions

regions_tmp_toUse = 'orig' ;
% regions_tmp_toUse = 'pls5v2' ;
eval(['regions_tmp = regions_all.' regions_tmp_toUse ';']) ;
if exist('badHYDEnans','var')
    regions_tmp.map(badHYDEnans) = NaN ;
end
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;
tmp_regKey = regions_tmp.Marray(~isnan(regions_tmp.Marray)) ;

load('gfed4_BAcrop_lowres_01-10_km2.mat')
obsEmis_crop = gfed4_BAcrop_lowres ;
clear gfed4_BAcrop_lowres
obsEmis_crop = obsEmis_crop(:,:,months.start_s01:months.end_s01) ;
obsEmis_crop = obsEmis_crop(~isnan(regions_tmp.Marray)) ;

disp(' ')
disp(['Region set: ' regions_tmp_toUse])
for r = 1:regions_tmp.num
    tmp_obs = obsEmis_crop(tmp_regKey==r) ;
    tmp_est = estEmis2_crop(tmp_regKey==r) ;
    tmp_corr = corr(tmp_obs,tmp_est) ;
    disp([regions_tmp.names{r} ': r = ' num2str(tmp_corr)])
end

%% How well do regional numbers from unpacking match those from observations?

load('gfed4_BAcrop_lowres_01-10_km2.mat')
obsEmis_crop = gfed4_BAcrop_lowres ;
clear gfed4_BAcrop_lowres

regions_tmp_toUse = 'orig' ;
% regions_tmp_toUse = 'pls5v2' ;
eval(['regions_tmp = regions_all.' regions_tmp_toUse ';']) ;
if exist('badHYDEnans','var')
    regions_tmp.map(badHYDEnans) = NaN ;
end
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;
tmp_regKey = regions_tmp.Marray(~isnan(regions_tmp.Marray)) ;

obsEmis_crop = obsEmis_crop(:,:,months.start_s01:months.end_s01) ;
obsEmis_crop = obsEmis_crop(~isnan(regions_tmp.Marray)) ;

disp(' ')
disp(['Region set: ' regions_tmp_toUse])
for r = 1:regions_tmp.num
    BAcrop_obs_allReg(r,1) = sum(obsEmis_crop(tmp_regKey==r)) / years.num ;
    BAcrop_est_allReg(r,1) = sum(estEmis_crop(tmp_regKey==r)) / years.num ;
end
corr(BAcrop_obs_allReg,BAcrop_est_allReg)
figure ; plot(BAcrop_obs_allReg,BAcrop_est_allReg,'.b') ;





