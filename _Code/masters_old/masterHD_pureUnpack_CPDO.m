%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Run 4-member (CPDO) unpacking %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Notes
prefix = 'REGnamerwaar_Fkredist_min0.005_AA_NOprenorm_randerson' ;

%% (1) Namelist

%%% What years are we working with?
% years.list = 2003:2008 ;
% years.list = 2000:2009 ;   % To match Magi et al. (2012). Extent of HYDE 3.1.
years.list = 2001:2009 ;   % Overlap of HYDE 3.1 and Randerson.
% years.list = 2001:2003 ;   % To match Korontzi et al. (2006)

%%% What set of regions?
% methods.regions = 'pls5v2' ;
methods.regions = 'NAmER_WAAR' ;

%%% Minimum mean coverage in a region-month for an LC to be included in
%%% unpacking? I.e., mean(LCfrac_*)
%%% (<= this, won't be included) (fraction)
% methods.min_coverage = 0 ;   % Only exclude covers that don't exist
methods.min_coverage = 0.005 ;
% methods.min_coverage = 0.03 ;

%%% Do two-step unpacking (ie, Pk and Fk) or just one step (ie, Fk only a
%%% la Magi et al. 2012)?
methods.numUnpackSteps = 1 ;   % To match Magi et al. (2012)
% methods.numUnpackSteps = 2 ;

%%% Area or proportion land cover?
% methods.coverType = 'prop' ;
methods.coverType = 'area' ;

%%% Match input data (e.g., regress proportion cover vs. frac. burned)?
% methods.match = 0 ;
methods.match = 1 ;

%%% What kind of observed fire data?
% methods.fireType = 'gfed31' ;
% methods.fireType = 'gfed' ;
methods.fireType = 'randerson_all' ;

%%% Include a constant term in the logistic regression?
methods.constant = 0 ;
% methods.constant = 1 ;

%%% Constrain Fk values to >= 1?
methods.constrain = 0 ;
% methods.constrain = 1 ;

% Re-distribute negative OTHR BA?
% methods.redistNegOTHR = 0 ;
methods.redistNegOTHR = 1 ;

%%% Are we normalizing?
methods.normType = 'none' ;
% methods.normType = 'max' ;
% methods.normType = 'mean' ;
% methods.normType = 'median' ;
if strcmp(methods.normType,'median')
    error('Don''t use median. Need to figure out what to do if median = 0. Setting norm_constant to 0 is bad, but then what to choose?')
end
if ~strcmp(methods.normType,'none')
    %%% Should cells that never burned have their normConstant changed to 0?
    %%% (0=No, 1=Yes.) If not, it'll be Inf, resulting in NaN values for 
    %%% fire_norm, meaning that cells that never burned will be ignored in Fk
    %%% calculations. (This is how the NiceNumber results were obtained.)
%     methods.set_neverBurned = -1 ; warning('You really should do something with neverBurned cells...')
    methods.set_neverBurned = 0 ;
%     methods.set_neverBurned = 1 ;
end

% Do "efficient" version of Pa code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Ps = 0 ;
% methods.efficient_Ps = 1 ;

% Do "efficient" version of FcFp code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Fs = 0 ;
% methods.efficient_Fs = 0.1 ;   % Inefficient method, then take median of Fk values for each month (e.g. Jan.)
% methods.efficient_Fs = 1 ;


%% (2) Setup

% Working directory and path
cd '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack'
addpath(genpath(pwd))
addpath(genpath('/Users/sam/Geodata/'))
addpath(genpath('/Users/sam/Documents/Dropbox/Projects/FireModel_daily/support'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices_s00 = (years.start - 1999):(years.end - 1999) ;
years.indices_s01 = (years.start - 2000):(years.end - 2000) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start_s00 = (years.start-2000)*12 + 1 ;
months.end_s00 = months.start_s00 + months.num - 1 ;
months.start_s01 = (years.start-2001)*12 + 1 ;
months.end_s01 = months.start_s01 + months.num - 1 ;


%% (3) Import data

% Regions
disp('Loading region & land area...')
load('/Users/sam/Geodata/New_regions_v3/newRegions_v3.mat')
regions_all = regions ;
eval(['regions = regions.' methods.regions ' ;']) ;
regions.Yarray = repmat(regions.map,[1 1 years.num]) ;
regions.Marray = repmat(regions.map,[1 1 months.num]) ;
regKey = regions.Marray(~isnan(regions.Marray)) ;

% Gridcell and land area
edge_lats = [-90:0.5:90]' ;
dist_atEquator_1deg = 40075/360 ;   % Circumference @ equator = 40,075 km
distances = cos(deg2rad(edge_lats)) * dist_atEquator_1deg*0.5 ;
dist_pairs = [distances(1:length(distances)-1) distances(2:length(distances))] ;
dist_long = (40008/2) / 360 ;   % Circumference through poles = 40,008 km
gridarea = 0.5 * dist_long * (dist_pairs(:,1) + dist_pairs(:,2)) ;
gridarea = repmat(single(gridarea),[1 720]) ;
clear edge_lats dist_atEquator_1deg distances dist_pairs dist_long
gicew = flipud(single(dlmread('~/Geodata/LandUseHarm_1990-2020_relevant/gicew.1700.txt',' ',[6 0 365 719]))) ;
landarea = gridarea .* (1-gicew) ;
landarea(landarea==0) = NaN ;
clear gridarea

% Month datasets
Marray_month = zeros(360,720,months.num,'int16') ;
for m = 1:months.num
    Marray_month(:,:,m) = m*ones(360,720) ;
end ; clear m
Marray_trueMonth = zeros(360,720,12,'int8') ;
for m = 1:12
    Marray_trueMonth(:,:,m) = m*ones(360,720) ;
end ; clear m
Marray_trueMonth = repmat(Marray_trueMonth,[1 1 years.num]) ;

%%%%%%%%%%%%%%%%%%%
%%% Land covers %%%
%%%%%%%%%%%%%%%%%%%
disp('Loading land cover data...')
% Import and trim to appropriate years
load('/Users/sam/Geodata/LandUseHarm_1990-2020_relevant/MATfiles/LandUseHarm_1990-2020.mat')
types = {'crop';'past';'ntrl';'scnd';'n2cp';'s2cp'} ;
for t = 1:length(types)
    thisType = types{t} ;
    eval(['LCfrac_' thisType '_tmp = LCfrac_' thisType '(:,:,years.start-1989:years.end-1989) ;']) ;
end ; clear t thisType types
% Calculate DEFO area in appropriate grid cells
load('/Users/sam/Geodata/WWF terr_ecos--Fractions/wwf_frac_TropicalBroadleafForest.mat')
doDEFO = (tropfor_halfdeg >= 0.5) ;
clear tropfor_halfdeg
LCfrac_defo_tmp = zeros(size(LCfrac_crop_tmp),'single') ;
LCfrac_defo_tmp(doDEFO) = LCfrac_n2cp_tmp(doDEFO) + LCfrac_s2cp_tmp(doDEFO) ;
LCfrac_defo_tmp(isnan(LCfrac_n2cp_tmp)) = NaN ;
% Calculate OTHR area
LCfrac_othr_tmp = LCfrac_ntrl_tmp + LCfrac_scnd_tmp ;
LCfrac_othr_tmp(~doDEFO) = LCfrac_othr_tmp(~doDEFO) + LCfrac_n2cp_tmp(~doDEFO) + LCfrac_s2cp_tmp(~doDEFO) ;
clear LCfrac_*2cp* LCfrac_ntrl* LCfrac_scnd* doDEFO
% Normalize LCfrac's so that they sum to one (i.e., make them fraction of
% LAND instead of fraction of GRIDCELL)
LCfrac_cpdo = LCfrac_crop_tmp + LCfrac_past_tmp + LCfrac_defo_tmp + LCfrac_othr_tmp ;
LCfrac_crop_tmp = LCfrac_crop_tmp ./ LCfrac_cpdo ;
LCfrac_past_tmp = LCfrac_past_tmp ./ LCfrac_cpdo ;
LCfrac_defo_tmp = LCfrac_defo_tmp ./ LCfrac_cpdo ;
LCfrac_othr_tmp = LCfrac_othr_tmp ./ LCfrac_cpdo ;
clear LCfrac_cpdo
% Make sure NaNs align for regions.map, LC maps, and land area map
if ~isempty(find(isnan(mean(LCfrac_crop_tmp+LCfrac_past_tmp+LCfrac_defo_tmp+LCfrac_othr_tmp,3)) & ~isnan(regions.map),1))
    badHYDEnans = (isnan(mean(LCfrac_crop_tmp,3)) | isnan(mean(LCfrac_past_tmp,3)) | isnan(mean(LCfrac_defo_tmp,3)) | isnan(mean(LCfrac_othr_tmp,3))) ;
    regions.map(badHYDEnans) = NaN ;
    regions.Marray = repmat(regions.map,[1 1 months.num]) ;
    regions.Yarray = repmat(regions.map,[1 1 years.num]) ;
    Marray_month = Marray_month(~isnan(regions.Marray)) ;
    Marray_trueMonth = Marray_trueMonth(~isnan(regions.Marray)) ;
    regKey = regions.Marray(~isnan(regions.Marray)) ;
end
methods.num_cells = length(find(~isnan(regions.map))) ;
methods.num_months = months.num ;
% Fill in to monthly
LCfrac_crop = nan(size(regions.Marray)) ;
LCfrac_past = nan(size(regions.Marray)) ;
LCfrac_defo = nan(size(regions.Marray)) ;
LCfrac_othr = nan(size(regions.Marray)) ;
for y = 1:years.num
    startMonth = (y-1)*12 + 1 ;
    endMonth = startMonth + 11 ;
    LCfrac_crop(:,:,startMonth:endMonth) = repmat(LCfrac_crop_tmp(:,:,y),[1 1 12]) ;
    LCfrac_past(:,:,startMonth:endMonth) = repmat(LCfrac_past_tmp(:,:,y),[1 1 12]) ;
    LCfrac_defo(:,:,startMonth:endMonth) = repmat(LCfrac_defo_tmp(:,:,y),[1 1 12]) ;
    LCfrac_othr(:,:,startMonth:endMonth) = repmat(LCfrac_othr_tmp(:,:,y),[1 1 12]) ;
end ; clear y startMonth endMonth LCfrac_*_tmp
% Calculate LCdata variables
LCdata_crop = LCfrac_crop ;
LCdata_past = LCfrac_past ;
LCdata_defo = LCfrac_defo ;
LCdata_othr = LCfrac_othr ;
if strcmp(methods.coverType,'area')
    landarea_array = repmat(landarea,[1 1 months.num]) ;
    LCdata_crop = LCdata_crop .* landarea_array ;
    LCdata_past = LCdata_past .* landarea_array ;
    LCdata_defo = LCdata_defo .* landarea_array ;
    LCdata_othr = LCdata_othr .* landarea_array ;
    clear landarea_array
end
% Save as vectors without NaNs
LCfrac_crop = LCfrac_crop(~isnan(regions.Marray)) ;
LCfrac_past = LCfrac_past(~isnan(regions.Marray)) ;
LCfrac_defo = LCfrac_defo(~isnan(regions.Marray)) ;
LCfrac_othr = LCfrac_othr(~isnan(regions.Marray)) ;
LCdata_crop = LCdata_crop(~isnan(regions.Marray)) ;
LCdata_past = LCdata_past(~isnan(regions.Marray)) ;
LCdata_defo = LCdata_defo(~isnan(regions.Marray)) ;
LCdata_othr = LCdata_othr(~isnan(regions.Marray)) ;
%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%


% Determine regions that will have CPDO unpacking done
disp('Doing CPDO unpacking in...')
regions.defo = zeros(regions.num,1) ;
for r = 1:regions.num
    if ~isempty(find(LCfrac_defo(regKey==r)>0,1))
        regions.defo(r) = 1 ;
        disp(['   ' regions.names{r}])
    end
end
disp('That''s all.')


%%%%%%%%%%%%%%%%%%%%%
%%% Observed fire %%%
%%%%%%%%%%%%%%%%%%%%%
disp('Loading observed fire data...')
if strcmp(methods.fireType,'gfed')
    load('~/Geodata/GFED4/MATfiles/gfed4_BA_lowres_01-10_km2.mat')
    obsFire = single(gfed4_BA_lowres(:,:,months.start_s01:months.end_s01)) ;
    clear gfed4_BA_lowres
elseif strcmp(methods.fireType,'gfed3')
    load('/Volumes/Repository/Geodata_Repository/GFED_31/GFED3.1_2000-2009_BAkm2.mat')
    obsFire = single(gfed31(:,:,months.start_s00:months.end_s00)) ;
    clear gfed31
elseif strcmp(methods.fireType,'randerson_all')
    load('~/Geodata/Randerson--SmallFires/MATfiles/BA_all_lowres_01-10.mat')
    obsFire = single(BA_all_lowres(:,:,months.start_s01:months.end_s01)) ;
    clear BA_all_lowres
else error('methods.fireType improperly specified.')
end
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    [obsFireNorm,fire_normConstant_orig,fire_normConstant] = normObs(obsFire,methods) ;
    obsFireNorm = obsFireNorm(~isnan(regions.Marray)) ;
    fire_normConstant_orig = fire_normConstant_orig(~isnan(regions.map)) ;
    fire_normConstant = fire_normConstant(~isnan(regions.map)) ;
elseif ~strcmp(methods.normType,'none')
    error('methods.normType not (properly) specified.')
end
obsFire_frac = obsFire ./ repmat(landarea,[1 1 months.num]) ;
obsFire = obsFire(~isnan(regions.Marray)) ;
obsFire_frac = obsFire_frac(~isnan(regions.Marray)) ;
obsFire_yn = double(obsFire>0) ;
%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%

disp('Done.') ; disp(' ')


%% (4) Do unpacking

disp('Setting up for unpacking...')

if methods.numUnpackSteps == 2
    allReg_P = nan(size(obsFire)) ;
    if methods.efficient_Ps == 0
        allReg_Pc_mr = nan(months.num,regions.num,'single') ;
        allReg_Pp_mr = nan(months.num,regions.num,'single') ;
        allReg_Pd_mr = nan(months.num,regions.num,'single') ;
        allReg_Po_mr = nan(months.num,regions.num,'single') ;
        allReg_const_mr = nan(months.num,regions.num,'single') ;
    elseif methods.efficient_Ps == 1
        allReg_Pc_mr = nan(12,regions.num,'single') ;
        allReg_Pp_mr = nan(12,regions.num,'single') ;
        allReg_Pd_mr = nan(12,regions.num,'single') ;
        allReg_Po_mr = nan(12,regions.num,'single') ;
        allReg_const_mr = nan(12,regions.num,'single') ;
    end
end
allReg_Fc = nan(size(obsFire)) ;
allReg_Fp = nan(size(obsFire)) ;
allReg_Fd = nan(size(obsFire)) ;
allReg_Fo = nan(size(obsFire)) ;
if methods.efficient_Fs == 0
    allReg_Fc_mr = nan(months.num,regions.num) ;
    allReg_Fp_mr = nan(months.num,regions.num) ;
    allReg_Fd_mr = nan(months.num,regions.num) ;
    allReg_Fo_mr = nan(months.num,regions.num) ;
elseif methods.efficient_Fs==0.1 || methods.efficient_Fs==1
    allReg_Fc_mr = nan(12,regions.num) ;
    allReg_Fp_mr = nan(12,regions.num) ;
    allReg_Fd_mr = nan(12,regions.num) ;
    allReg_Fo_mr = nan(12,regions.num) ;
end

disp('Performing unpacking...')
for r = 1:regions.num
    disp(['   Region ' num2str(r) ' of ' num2str(regions.num) ': ' regions.names{r} '...'])
    
    % Set up thisReg
    thisReg_indices = (regKey==r) ;
    thisReg.crop = LCdata_crop(thisReg_indices) ;
    thisReg.past = LCdata_past(thisReg_indices) ;
    if regions.defo(r)==1 ; thisReg.defo = LCdata_defo(thisReg_indices) ; end
    thisReg.othr = LCdata_othr(thisReg_indices) ;
    if strcmp(methods.normType,'none')
        thisReg.obsFire = obsFire(thisReg_indices) ;
    else
        thisReg.obsFire = obsFireNorm(thisReg_indices) ;
    end
    thisReg.obsFire_yn = obsFire_yn(thisReg_indices) ;
    thisReg.month = Marray_month(thisReg_indices) ;
    thisReg.trueMonth = Marray_trueMonth(thisReg_indices) ;
    thisReg.landarea = landarea(regions.map==r) ;
    
    % Calculate probability of fire, if doing so
    if methods.numUnpackSteps == 2
        thisReg.obsFire_yn = obsFire_yn(thisReg_indices) ;
        if regions.defo(r)==1
            [P,Pc,Pp,Pd,Po,const] = calcP_pureUnpack_CPDO(thisReg,methods,months) ;
        else
            [P,Pc,Pp,Po,const] = calcP_pureUnpack_CPO(thisReg,methods,months) ;
        end
        if ~isempty(find(isnan(P),1))
            error(['Some value of P is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
        end
        allReg_P(thisReg_indices) = P ;
        allReg_Pc_mr(:,r) = Pc ;
        allReg_Pp_mr(:,r) = Pp ;
        if regions.defo(r)==1 ; allReg_Pd_mr(:,r) = Pd ; else allReg_Pd_mr(:,r) = NaN ; end
        allReg_Po_mr(:,r) = Po ;
        allReg_const_mr(:,r) = const ;
        clear P Pc Pp Pd Po const
    end
    
    % Calculate Fk's
    if methods.numUnpackSteps == 1
        if regions.defo(r)==1
%             [Fc,Fp,Fd,Fo,Fc_m,Fp_m,Fd_m,Fo_m] = calcF_pureUnpack_CPDO_1step(thisReg,months,methods) ;
            [Fc,Fp,Fd,Fo,Fc_m,Fp_m,Fd_m,Fo_m] = calcF_pureUnpack_noZeros_CPDO_1step(thisReg,months,methods) ;
        else
%             [Fc,Fp,Fo,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_CPO_1step(thisReg,months,methods) ;
            [Fc,Fp,Fo,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_noZeros_CPO_1step(thisReg,months,methods) ;
        end
    elseif methods.numUnpackSteps == 2
        if regions.defo(r)==1
%             [Fc,Fp,Fd,Fo,Fc_m,Fp_m,Fd_m,Fo_m] = calcF_pureUnpack_CPDO(thisReg,months,methods) ;
            [Fc,Fp,Fd,Fo,Fc_m,Fp_m,Fd_m,Fo_m] = calcF_pureUnpack_noZeros_CPDO(thisReg,months,methods) ;
        else
%             [Fc,Fp,Fo,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_CPO(thisReg,months,methods) ;
            [Fc,Fp,Fo,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_noZeros_CPO(thisReg,months,methods) ;
        end
    end
%     if regions.defo(r)==1
%         if ~isempty(find(isnan(Fc_m.*Fp_m.*Fd_m.*Fo_m),1))
%             error(['Some Fk is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
%         end
%     else
%         if ~isempty(find(isnan(Fc_m.*Fp_m.*Fo_m),1))
%             error(['Some Fk is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
%         end
%     end
    allReg_Fc(thisReg_indices) = Fc ;
    allReg_Fp(thisReg_indices) = Fp ;
    if regions.defo(r)==1 ; allReg_Fd(thisReg_indices) = Fd ; else allReg_Fd(thisReg_indices) = NaN ; end
    allReg_Fo(thisReg_indices) = Fo ;
    allReg_Fc_mr(:,r) = Fc_m ;
    allReg_Fp_mr(:,r) = Fp_m ;
    if regions.defo(r)==1 ; allReg_Fd_mr(:,r) = Fd_m ; else allReg_Fd_mr(:,r) = NaN ; end
    allReg_Fo_mr(:,r) = Fo_m ;
    clear Fc Fp Fd Fo Fc_m Fp_m Fd_m Fo_m
    
end
disp('Done.') ; disp('   ')


%% (5) Calculate amount of estimated fire
disp('Calculating amount of estimated fire...')

if methods.numUnpackSteps == 2
    estFire_crop = allReg_P .* allReg_Fc .* LCdata_crop ;
    estFire_past = allReg_P .* allReg_Fp .* LCdata_past ;
    estFire_defo = allReg_P .* allReg_Fd .* LCdata_defo ;
    estFire_othr = allReg_P .* allReg_Fo .* LCdata_othr ;
elseif methods.numUnpackSteps == 1
    estFire_crop = allReg_Fc .* LCdata_crop ;
    estFire_past = allReg_Fp .* LCdata_past ;
    estFire_defo = allReg_Fd .* LCdata_defo ;
    estFire_othr = allReg_Fo .* LCdata_othr ;
end

% Set estFire to zero when land area in that type is zero.
% (Otherwise, since Fk is NaN, estFire would be NaN.)
estFire_crop(isnan(allReg_Fc)) = 0 ;
estFire_past(isnan(allReg_Fp)) = 0 ;
estFire_defo(isnan(allReg_Fd)) = 0 ;
estFire_othr(isnan(allReg_Fo)) = 0 ;

% De-normalize, if necessary
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    estFire_crop = estFire_crop .* repmat(fire_normConstant,[months.num 1]) ;
    estFire_past = estFire_past .* repmat(fire_normConstant,[months.num 1]) ;
    estFire_defo = estFire_defo .* repmat(fire_normConstant,[months.num 1]) ;
    estFire_othr = estFire_othr .* repmat(fire_normConstant,[months.num 1]) ;
end

estFire_total = estFire_crop + estFire_past + estFire_othr ;

% Re-distribute negative fire, if doing so
if methods.redistNegOTHR == 1
    disp('Re-distributing negative fire...')
    [estFire2_crop, estFire2_past, estFire2_defo, estFire2_othr] = reDistNeg_pureUnpack_CPDO( ...
        estFire_crop, estFire_past, estFire_defo, estFire_othr, ...
        LCfrac_crop,  LCfrac_past, LCfrac_defo,  LCfrac_othr) ;
    estFire2_total = estFire2_crop + estFire2_past + estFire2_defo + estFire2_othr ;
else
    estFire2_crop = estFire_crop ;
    estFire2_past = estFire_past ;
    estFire2_defo = estFire_defo ;
    estFire2_othr = estFire_othr ;
    estFire2_total = estFire_total ;
end

disp('Done.') ; disp(' ')


%% (6) Display performance by region

disp('Mean annual burned area:')

eval(['regions_tmp = regions_all.' methods.regions ' ;']) ;
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;
regKey_tmp = regions_tmp.Marray(~isnan(regions.Marray)) ;

showPerf_incl = true(size(estFire_total)) ;
% showPerf_incl = (repmat(fire_normConstant_orig,[months.num 1])==0) ;
% showPerf_incl = (repmat(fire_normConstant_orig,[months.num 1])>0) ;

corr_allRegs = nan(regions_tmp.num,1) ;
errPerc_allRegs = nan(regions_tmp.num,1) ;
for r = 1:regions_tmp.num
    regName = char(regions_tmp.names(r)) ;
    if regions.defo(r)==1
        regName = [regName '*'] ;
    else
        regName = [regName ' '] ;
    end
    est_thisReg_list = estFire2_total(regKey_tmp==r & showPerf_incl) ;
    est_thisReg = sum(est_thisReg_list)/years.num ;
    obs_thisReg_list = obsFire(regKey_tmp==r & showPerf_incl) ;
    obs_thisReg = sum(obs_thisReg_list)/years.num ;
    errPerc_thisReg = 100 * (est_thisReg - obs_thisReg) / obs_thisReg ;
    errPerc_allRegs(r) = errPerc_thisReg ;
    corr_thisReg = roundn(corr(obs_thisReg_list,est_thisReg_list),-3) ;
    corr_allRegs(r) = corr_thisReg ;
    
    SSE_thisReg = sum((estFire2_total(regKey_tmp==r & showPerf_incl) - obsFire(regKey_tmp==r & showPerf_incl)).^2) ;
    maxSSE_thisReg = sum((0 - obsFire(regKey_tmp==r & showPerf_incl)).^2) ;
    SSE_performance = SSE_thisReg / maxSSE_thisReg ;
    
    disp([regName ': Obs. = ' num2str(roundn(obs_thisReg,0)) ', Est. = ' num2str(roundn(est_thisReg,0)) ' (' num2str(roundn(errPerc_thisReg,-1)) '% error, r=' num2str(corr_thisReg) ', SSEperf=' num2str(SSE_performance) ').'])
end ; clear r *thisReg* regName *SSE_thisReg SSE_performance
global_obs = sum(obsFire(showPerf_incl))/years.num ;
global_est = sum(estFire2_total(showPerf_incl))/years.num ;
global_errPerc = 100*(global_est-global_obs) ./ global_obs ;
disp(['Global: Obs. = ' num2str(roundn(global_obs,0)) ', Est. = ' num2str(roundn(global_est,0)) ' (' num2str(roundn(global_errPerc,-1)) '% error).'])

% Find correlation coefficient for mean est vs obs
pearsons_r = corr(obsFire(showPerf_incl),estFire2_total(showPerf_incl)) ;
disp(['     Pearson''s r = ' num2str(pearsons_r)])

% Find sum of squared errors
sum_squared_errors = sum((estFire2_total(showPerf_incl)-obsFire(showPerf_incl)).^2) ;
fprintf('%s','     Sum of squared errors = ')
fprintf('%e\n',sum_squared_errors)

% How does it compare to guessing 0 for everything?
max_squared_errors = sum(obsFire(showPerf_incl).^2) ;
squared_errors_performance = sum_squared_errors / max_squared_errors ;
disp(['     "SSE performance" = ' num2str(squared_errors_performance)])

disp('Regional correlation coefficients:')
disp(num2str([corr_allRegs ; pearsons_r]))

disp('Regional percent error:')
disp(num2str([errPerc_allRegs ; global_errPerc]))

disp(' ') ; clear global_* regions_tmp regKey_tmp


%% Save data
disp('Saving MAT-file...')

filename = ['/Volumes/Repository/Unpacking_HD_CPDO_output/' prefix '.mat'] ;
if ~isempty(dir(filename))
    error('Change filename to avoid overwriting existing data.')
end
save(filename,'-regexp','allReg_F*')
if methods.numUnpackSteps==2
    save(filename,'-regexp','allReg_P*','-append')
end
save(filename,'-regexp','estFire*','-append')
save(filename,'methods','-append')
disp('Done.') ; disp(' ')


%% Display NF/BA seasonality: One region

region_toPlot = 32 ;
% region_type = 'pls5v2' ;
region_type = 'NAmER_WAAR' ;
disp(['Plotting normed CPO seasonality: ' regions.names{region_toPlot}])

tmp = load('/Users/sam/Geodata/New_regions_v3/newRegions_v3.mat') ;
eval(['tmp.regions = tmp.regions.' region_type ';']) ;
tmp.regions.Marray = repmat(regions.map,[1 1 months.num]) ;
tmp_regions = tmp.regions ;
clear tmp region_type

dailyv3_seasonality_CPOnorm_oneReg(tmp_regions,region_toPlot,estFire2_crop,estFire2_past,estFire2_othr,estFire2_total,obsFire,'mean')
clear tmp_regions Nf2 region_toPlot


%% Make seasonality plots of Fk for individual regions

region_toPlot = 32 ;
% region_type = 'pls5v2' ;
region_type = 'NAmER_WAAR' ;
disp(['Plotting normed Fk seasonality: ' regions.names{region_toPlot}])

tmp = load('/Users/sam/Geodata/New_regions_v3/newRegions_v3.mat') ;
eval(['tmp.regions = tmp.regions.' region_type ';']) ;
tmp.regions.Marray = repmat(regions.map,[1 1 months.num]) ;
tmp_regions = tmp.regions ;
clear tmp region_type

seasonality_Fk_oneReg(tmp_regions,region_toPlot,allReg_Fc,allReg_Fp,allReg_Fo,'mean')
clear tmp_regions Nf2 region_toPlot
