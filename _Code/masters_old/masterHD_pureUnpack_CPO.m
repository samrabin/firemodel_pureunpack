%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Run new version of 3-member (CPO) unpacking %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Notes
% (1) Use GFDL data (Hurtt) for land use

%% Namelist

%%% What years are we working with?
years.list = 2003:2008 ;

%%% What set of regions?
% methods.regions = 'pls5v2' ;
methods.regions = 'NAmER_WAAR' ;

%%% Area or proportion land cover?
% methods.coverType = 'prop' ;
methods.coverType = 'area' ;

%%% Match input data (e.g., regress proportion cover vs. frac. burned)?
% methods.match = 0 ;
methods.match = 1 ;

%%% What kind of observed fire data?
% methods.fireType = 'terra' ;
% methods.fireType = 'aqua' ;
methods.fireType = 'gfed' ;

%%% Include a constant term in the logistic regression?
methods.constant = 0 ;
% methods.constant = 1 ;

%%% Constrain Fk values to >= 1?
methods.constrain = 0 ;
% methods.constrain = 1 ;

% Re-distribute negative OTHR BA?
methods.redistNegOTHR = 0 ;
% methods.redistNegOTHR = 1 ;

%%% Are we normalizing?
% methods.normType = 'none' ;
% methods.normType = 'max' ;
methods.normType = 'mean' ;
% methods.normType = 'median' ;
if strcmp(methods.normType,'median')
    error('Don''t use median. Need to figure out what to do if median = 0. Setting norm_constant to 0 is bad, but then what to choose?')
end
if ~strcmp(methods.normType,'none')
    %%% Should cells that never burned have their normConstant changed to 0?
    %%% (0=No, 1=Yes.) If not, it'll be Inf, resulting in NaN values for 
    %%% fire_norm, meaning that cells that never burned will be ignored in Fk
    %%% calculations. (This is how the NiceNumber results were obtained.)
%     methods.set_neverBurned = -1 ; warning('You really should do something with neverBurned cells...')
    methods.set_neverBurned = 0 ;
%     methods.set_neverBurned = 1 ;
end

% Do "efficient" version of Pa code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Ps = 0 ;
% methods.efficient_Ps = 1 ;

% Do "efficient" version of FcFp code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Fs = 0 ;
% methods.efficient_Fs = 0.1 ;   % Inefficient method, then take median of Fk values for each month (e.g. Jan.)
% methods.efficient_Fs = 1 ;


%% Setup

% Working directory and path
cd '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack'
addpath(genpath(pwd))
addpath(genpath('/Users/sam/Geodata/'))
addpath(genpath('/Users/sam/Documents/Dropbox/Projects/FireModel_daily/support'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices = (years.start - 1999):(years.end - 1999) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start = (years.start-2000)*12 + 1 ;
months.end = months.start + months.num - 1 ;


%% Import data

% Regions & land area
disp('Loading region & land area...')
load('/Users/sam/Geodata/New_regions_v3/newRegions_v3.mat')
regions_all = regions ;
eval(['regions = regions.' methods.regions ' ;']) ;
regions.Yarray = repmat(regions.map,[1 1 years.num]) ;
regions.Marray = repmat(regions.map,[1 1 months.num]) ;
regKey = regions.Marray(~isnan(regions.Marray)) ;
% [landarea, ~] = geotiffread('LandArea_ORNL.tif') ;
[landarea, ~] = imread('LandArea_ORNL.tif','TIF') ;
landarea = flipud(double(landarea)) ;

% Month datasets
Marray_month = nan(360,720,months.num) ;
for m = months.start:months.end
    thisIndex = m - months.start + 1 ;
    Marray_month(:,:,thisIndex) = m*ones(360,720) ;
end ; clear m thisIndex
Marray_month = Marray_month(~isnan(regions.Marray)) ;
Marray_trueMonth = nan(360,720,12) ;
for m = 1:12
    Marray_trueMonth(:,:,m) = m*ones(360,720) ;
end ; clear m
Marray_trueMonth = repmat(Marray_trueMonth,[1 1 years.num]) ;
Marray_trueMonth = Marray_trueMonth(~isnan(regions.Marray)) ;

% Land covers
disp('Loading land cover data...')
load('/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack/Input/LCfracs_annual_2001-2010_fromSergey_v2.mat') ;
LCfrac_ntrl_tmp = LCfrac_ntrl(:,:,(years.start-2000):(years.end-2000)) ;
LCfrac_scnd_tmp = LCfrac_scnd(:,:,(years.start-2000):(years.end-2000)) ;
LCfrac_crop_tmp = LCfrac_crop(:,:,(years.start-2000):(years.end-2000)) ;
LCfrac_past_tmp = LCfrac_past(:,:,(years.start-2000):(years.end-2000)) ;
LCfrac_ntrl = nan(size(regions.Marray)) ;
LCfrac_scnd = nan(size(regions.Marray)) ;
LCfrac_crop = nan(size(regions.Marray)) ;
LCfrac_past = nan(size(regions.Marray)) ;
for y = 1:years.num
    startMonth = (y-1)*12 + 1 ;
    endMonth = startMonth + 11 ;
    LCfrac_ntrl(:,:,startMonth:endMonth) = repmat(LCfrac_ntrl_tmp(:,:,y),[1 1 12]) ;
    LCfrac_scnd(:,:,startMonth:endMonth) = repmat(LCfrac_scnd_tmp(:,:,y),[1 1 12]) ;
    LCfrac_crop(:,:,startMonth:endMonth) = repmat(LCfrac_crop_tmp(:,:,y),[1 1 12]) ;
    LCfrac_past(:,:,startMonth:endMonth) = repmat(LCfrac_past_tmp(:,:,y),[1 1 12]) ;
end ; clear y startMonth endMonth LCfrac_*_tmp
LCdata_ntrl = LCfrac_ntrl ;
LCdata_scnd = LCfrac_scnd ;
LCdata_crop = LCfrac_crop ;
LCdata_past = LCfrac_past ;
if strcmp(methods.coverType,'area')
    landarea_array = repmat(landarea,[1 1 months.num]) ;
    LCdata_ntrl = LCdata_ntrl .* landarea_array ;
    LCdata_scnd = LCdata_scnd .* landarea_array ;
    LCdata_crop = LCdata_crop .* landarea_array ;
    LCdata_past = LCdata_past .* landarea_array ;
    clear landarea_array
end
LCfrac_ntrl = LCfrac_ntrl(~isnan(regions.Marray)) ;
LCfrac_scnd = LCfrac_scnd(~isnan(regions.Marray)) ;
LCfrac_crop = LCfrac_crop(~isnan(regions.Marray)) ;
LCfrac_past = LCfrac_past(~isnan(regions.Marray)) ;
LCfrac_othr = LCfrac_ntrl + LCfrac_scnd ;
LCdata_ntrl = LCdata_ntrl(~isnan(regions.Marray)) ;
LCdata_scnd = LCdata_scnd(~isnan(regions.Marray)) ;
LCdata_crop = LCdata_crop(~isnan(regions.Marray)) ;
LCdata_past = LCdata_past(~isnan(regions.Marray)) ;
LCdata_othr = LCdata_ntrl + LCdata_scnd ;
clear LCfrac_ntrl LCfrac_scnd LCdata_ntrl LCdata_scnd

% Observed fire
disp('Loading observed fire data...')
if strcmp(methods.fireType,'terra')
    error('Add option to use Terra # fires.')
elseif strcmp(methods.fireType,'aqua')
    error('Add option to use Aqua # fires.')
elseif strcmp(methods.fireType,'gfed')
    load('gfed4_monthly_2001-2010.mat')
    obsFire = gfed4(:,:,months.start:months.end) ;
    clear gfed4
else error('methods.fireType improperly specified.')
end
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    [obsFireNorm,fire_normConstant_orig,fire_normConstant] = normObs(obsFire,methods) ;
    obsFireNorm = obsFireNorm(~isnan(regions.Marray)) ;
    fire_normConstant_orig = fire_normConstant_orig(~isnan(regions.map)) ;
    fire_normConstant = fire_normConstant(~isnan(regions.map)) ;
elseif ~strcmp(methods.normType,'none')
    error('methods.normType not (properly) specified.')
end
obsFire_frac = obsFire ./ repmat(landarea,[1 1 months.num]) ;
obsFire = obsFire(~isnan(regions.Marray)) ;
obsFire_frac = obsFire_frac(~isnan(regions.Marray)) ;
obsFire_yn = double(obsFire>0) ;

disp('Done.') ; disp(' ')


%% Do unpacking

disp('Setting up for unpacking...')

allReg_P = nan(size(obsFire)) ;
if methods.efficient_Ps == 0
    allReg_Pc_mr = nan(months.num,regions.num) ;
    allReg_Pp_mr = nan(months.num,regions.num) ;
    allReg_Po_mr = nan(months.num,regions.num) ;
    allReg_const_mr = nan(months.num,regions.num) ;
elseif methods.efficient_Ps == 1
    allReg_Pc_mr = nan(12,regions.num) ;
    allReg_Pp_mr = nan(12,regions.num) ;
    allReg_Po_mr = nan(12,regions.num) ;
    allReg_const_mr = nan(12,regions.num) ;
end
allReg_Fc = nan(size(obsFire)) ;
allReg_Fp = nan(size(obsFire)) ;
allReg_Fo = nan(size(obsFire)) ;
if methods.efficient_Fs == 0
    allReg_Fc_mr = nan(months.num,regions.num) ;
    allReg_Fp_mr = nan(months.num,regions.num) ;
    allReg_Fo_mr = nan(months.num,regions.num) ;
elseif methods.efficient_Fs==0.1 || methods.efficient_Fs==1
    allReg_Fc_mr = nan(12,regions.num) ;
    allReg_Fp_mr = nan(12,regions.num) ;
    allReg_Fo_mr = nan(12,regions.num) ;
end

disp('Performing unpacking...')
for r = 1:regions.num
    disp(['   Region ' num2str(r) ' of ' num2str(regions.num) ': ' regions.names{r} '...'])
    
    % Set up thisReg
    thisReg_indices = (regKey==r) ;
    thisReg.crop = LCdata_crop(thisReg_indices) ;
    thisReg.past = LCdata_past(thisReg_indices) ;
    thisReg.othr = LCdata_othr(thisReg_indices) ;
    if strcmp(methods.normType,'none')
        thisReg.obsFire = obsFire(thisReg_indices) ;
    else
        thisReg.obsFire = obsFireNorm(thisReg_indices) ;
    end
    thisReg.obsFire_yn = obsFire_yn(thisReg_indices) ;
    thisReg.month = Marray_month(thisReg_indices) ;
    thisReg.trueMonth = Marray_trueMonth(thisReg_indices) ;
    
    % Calculate probability of fire
    [P,Pc,Pp,Po,const] = calcP_pureUnpack_CPO(thisReg,methods,months) ;
    if ~isempty(find(isnan(P),1))
        error(['Some value of P is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
    end
    allReg_P(thisReg_indices) = P ;
    allReg_Pc_mr(:,r) = Pc ;
    allReg_Pp_mr(:,r) = Pp ;
    allReg_Po_mr(:,r) = Po ;
    allReg_const_mr(:,r) = const ;
    clear P Pc Pp Po const
    
    % Calculate Fk's
    [Fc,Fp,Fo,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_CPO(thisReg,months,methods) ;
    if ~isempty(find(isnan(Fc_m.*Fp_m.*Fo_m),1))
        error(['Some Fk is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
    end
    allReg_Fc(thisReg_indices) = Fc ;
    allReg_Fp(thisReg_indices) = Fp ;
    allReg_Fo(thisReg_indices) = Fo ;
    allReg_Fc_mr(:,r) = Fc_m ;
    allReg_Fp_mr(:,r) = Fp_m ;
    allReg_Fo_mr(:,r) = Fo_m ;
    clear Fc Fp Fo Fc_m Fp_m Fo_m
    
end
disp('Done.') ; disp('   ')


%% Calculate amount of estimated fire
disp('Calculating amount of estimated fire...')

estFire_crop = allReg_P .* allReg_Fc .* LCdata_crop ;
estFire_past = allReg_P .* allReg_Fp .* LCdata_past ;
estFire_othr = allReg_P .* allReg_Fo .* LCdata_othr ;

% De-normalize, if necessary
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    estFire_crop = estFire_crop .* repmat(fire_normConstant,[months.num 1]) ;
    estFire_past = estFire_past .* repmat(fire_normConstant,[months.num 1]) ;
    estFire_othr = estFire_othr .* repmat(fire_normConstant,[months.num 1]) ;
end

estFire_total = estFire_crop + estFire_past + estFire_othr ;

% Re-distribute negative fire, if doing so
if methods.redistNegOTHR == 1
    disp('Re-distributing negative fire...')
    [estFire2_crop, estFire2_past, estFire2_othr] = reDistNeg_pureUnpack_CPO( ...
        estFire_crop, estFire_past, estFire_othr, ...
        LCfrac_crop,  LCfrac_past,  LCfrac_othr) ;
    estFire2_total = estFire2_crop + estFire2_past + estFire2_othr ;
else
    estFire2_crop = estFire_crop ;
    estFire2_past = estFire_past ;
    estFire2_othr = estFire_othr ;
    estFire2_total = estFire_total ;
end

disp('Done.') ; disp(' ')


%% Display performance by region

disp('Mean annual burned area:')

regions_tmp = regions_all.pls5v2 ;
regions_tmp.Marray = repmat(regions_tmp.map,[1 1 months.num]) ;
regKey_tmp = regions_tmp.Marray(~isnan(regions_tmp.Marray)) ;

showPerf_incl = true(size(estFire_total)) ;
% showPerf_incl = (repmat(fire_normConstant_orig,[months.num 1])==0) ;
% showPerf_incl = (repmat(fire_normConstant_orig,[months.num 1])>0) ;

corr_allRegs = nan(regions_tmp.num,1) ;
errPerc_allRegs = nan(regions_tmp.num,1) ;
for r = 1:regions_tmp.num
    regName = char(regions_tmp.names(r)) ;
    est_thisReg_list = estFire2_total(regKey_tmp==r & showPerf_incl) ;
    est_thisReg = sum(est_thisReg_list)/years.num ;
    obs_thisReg_list = obsFire(regKey_tmp==r & showPerf_incl) ;
    obs_thisReg = sum(obs_thisReg_list)/years.num ;
    errPerc_thisReg = 100 * (est_thisReg - obs_thisReg) / obs_thisReg ;
    errPerc_allRegs(r) = errPerc_thisReg ;
    corr_thisReg = roundn(corr(obs_thisReg_list,est_thisReg_list),-3) ;
    corr_allRegs(r) = corr_thisReg ;
    
    SSE_thisReg = sum((estFire2_total(regKey_tmp==r & showPerf_incl) - obsFire(regKey_tmp==r & showPerf_incl)).^2) ;
    maxSSE_thisReg = sum((0 - obsFire(regKey_tmp==r & showPerf_incl)).^2) ;
    SSE_performance = SSE_thisReg / maxSSE_thisReg ;
    
    disp([regName ': Obs. = ' num2str(roundn(obs_thisReg,0)) ', Est. = ' num2str(roundn(est_thisReg,0)) ' (' num2str(roundn(errPerc_thisReg,-1)) '% error, r=' num2str(corr_thisReg) ', SSEperf=' num2str(SSE_performance) ').'])
end ; clear r *thisReg* regName *SSE_thisReg SSE_performance
global_obs = sum(obsFire(showPerf_incl))/years.num ;
global_est = sum(estFire2_total(showPerf_incl))/years.num ;
global_errPerc = 100*(global_est-global_obs) ./ global_obs ;
disp(['Global: Obs. = ' num2str(roundn(global_obs,0)) ', Est. = ' num2str(roundn(global_est,0)) ' (' num2str(roundn(global_errPerc,-1)) '% error).'])

% Find correlation coefficient for mean est vs obs
pearsons_r = corr(obsFire(showPerf_incl),estFire2_total(showPerf_incl)) ;
disp(['     Pearson''s r = ' num2str(pearsons_r)])

% Find sum of squared errors
sum_squared_errors = sum((estFire2_total(showPerf_incl)-obsFire(showPerf_incl)).^2) ;
fprintf('%s','     Sum of squared errors = ')
fprintf('%e\n',sum_squared_errors)

% How does it compare to guessing 0 for everything?
max_squared_errors = sum(obsFire(showPerf_incl).^2) ;
squared_errors_performance = sum_squared_errors / max_squared_errors ;
disp(['     "SSE performance" = ' num2str(squared_errors_performance)])

disp('Regional correlation coefficients:')
disp(num2str([corr_allRegs ; pearsons_r]))

disp('Regional percent error:')
disp(num2str([errPerc_allRegs ; global_errPerc]))

disp(' ') ; clear global_* regions_tmp regKey_tmp


%% Display NF/BA seasonality: One region

region_toPlot = 32 ;
% region_type = 'pls5v2' ;
region_type = 'NAmER_WAAR' ;
disp(['Plotting normed CPO seasonality: ' regions.names{region_toPlot}])

tmp = load('/Users/sam/Geodata/New_regions_v3/newRegions_v3.mat') ;
eval(['tmp.regions = tmp.regions.' region_type ';']) ;
tmp.regions.Marray = repmat(regions.map,[1 1 months.num]) ;
tmp_regions = tmp.regions ;
clear tmp region_type

dailyv3_seasonality_CPOnorm_oneReg(tmp_regions,region_toPlot,estFire2_crop,estFire2_past,estFire2_othr,estFire2_total,obsFire,'mean')
clear tmp_regions Nf2 region_toPlot


%% Make seasonality plots of Fk for individual regions

region_toPlot = 32 ;
% region_type = 'pls5v2' ;
region_type = 'NAmER_WAAR' ;
disp(['Plotting normed Fk seasonality: ' regions.names{region_toPlot}])

tmp = load('/Users/sam/Geodata/New_regions_v3/newRegions_v3.mat') ;
eval(['tmp.regions = tmp.regions.' region_type ';']) ;
tmp.regions.Marray = repmat(regions.map,[1 1 months.num]) ;
tmp_regions = tmp.regions ;
clear tmp region_type

seasonality_Fk_oneReg(tmp_regions,region_toPlot,allReg_Fc,allReg_Fp,allReg_Fo,'mean')
clear tmp_regions Nf2 region_toPlot
