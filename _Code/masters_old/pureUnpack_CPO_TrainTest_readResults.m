%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Look at results of CPO_TrainTest %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% What prefix to use?
prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randerson' ;


%% Setup 

cd '/Volumes/Repository/Unpacking_TestTrain/'
addpath(genpath(pwd))


%% Import data


% Make empty datasets
SSE_allRegs_rt = [] ;
allReg_Fc_mrt = [] ;
allReg_Fp_mrt = [] ;
allReg_Fo_mrt = [] ;
estBAall_crop_rt = [] ;
estBAall_past_rt = [] ;
estBAall_othr_rt = [] ;
estBAall_total_rt = [] ;
estBAtest_crop_rt = [] ;
estBAtest_past_rt = [] ;
estBAtest_othr_rt = [] ;
estBAtest_total_rt = [] ;
mapTrain_yn_YXt = [] ;
obsBAtest_rt = [] ;

filelist = dir([prefix '_*.mat']) ;
f = 1 ;
while ~isempty(dir([prefix '_' num2str(f) '.mat']))
    disp(['Loading file ' num2str(f) '...'])
    filelist = dir([prefix '_' num2str(f) '.mat']) ;
    thisFile = load(filelist.name) ;
    if f == 1
        methods_out = thisFile.methods_out ;
        obsBAall_rt = thisFile.obsBAall_rt(:,1) ;
    end
    SSE_allRegs_rt = cat(2,SSE_allRegs_rt,thisFile.SSE_allRegs_rt) ;
    allReg_Fc_mrt = cat(3,allReg_Fc_mrt,thisFile.allReg_Fc_mrt) ;
    allReg_Fp_mrt = cat(3,allReg_Fp_mrt,thisFile.allReg_Fp_mrt) ;
    allReg_Fo_mrt = cat(3,allReg_Fo_mrt,thisFile.allReg_Fo_mrt) ;
    estBAall_crop_rt = cat(2,estBAall_crop_rt,thisFile.estBAall_crop_rt) ;
    estBAall_past_rt = cat(2,estBAall_past_rt,thisFile.estBAall_past_rt) ;
    estBAall_othr_rt = cat(2,estBAall_othr_rt,thisFile.estBAall_othr_rt) ;
    estBAall_total_rt = cat(2,estBAall_total_rt,thisFile.estBAall_total_rt) ;
    estBAtest_crop_rt = cat(2,estBAtest_crop_rt,thisFile.estBAtest_crop_rt) ;
    estBAtest_past_rt = cat(2,estBAtest_past_rt,thisFile.estBAtest_past_rt) ;
    estBAtest_othr_rt = cat(2,estBAtest_othr_rt,thisFile.estBAtest_othr_rt) ;
    estBAtest_total_rt = cat(2,estBAtest_total_rt,thisFile.estBAtest_total_rt) ;
    mapTrain_yn_YXt = cat(3,mapTrain_yn_YXt,thisFile.mapTrain_yn_YXt) ;
    obsBAtest_rt = cat(2,obsBAtest_rt,thisFile.obsBAtest_rt) ;
    clear thisFile filelist
    f = f + 1 ;
end
disp('Done.') ; disp(' ')


%% Histograms

figure ;

subplot(3,1,1)
hist(estBAall_crop_rt(15,:)/1e4,15)
set(gca,'FontSize',18)
subplot(3,1,2)
hist(estBAall_past_rt(15,:)/1e4,15)
set(gca,'FontSize',20)
subplot(3,1,3)
hist(estBAall_othr_rt(15,:)/1e4,15)
set(gca,'FontSize',20)


%% Histogram: Total error

figure ;
hist((estBAtest_total_rt(15,:)-obsBAtest_rt(15,:))/1e4,20)
set(gca,'FontSize',20)

