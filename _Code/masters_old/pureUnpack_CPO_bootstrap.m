%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Run 3-member (CPO) unpacking, with proper bootstrapping %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randerson' ;

outDir = '/Volumes/Repository/Unpacking_ProperBootstrap/' ;


%% (1) Namelist

% Number of bootstrapping runs?
methods.trainRuns = 2000 ;

%%% What years are we working with?
% years.list = 2003:2008 ;
% years.list = 2000:2009 ;   % To match Magi et al. (2012). Extent of HYDE 3.1.
years.list = 2001:2009 ;   % Overlap of HYDE 3.1 and Randerson.
% years.list = 2001:2003 ;   % To match Korontzi et al. (2006)

%%% What set of regions?
% methods.regions = 'gfed' ;
% methods.regions = 'v1' ;   % NAmER_WAAR_fixedWAAR
% methods.regions = 'v2' ;   % NAmER_WAAR+Turkey, Australian ecoregions (WWF), WWF-defined southern border for MIDE.
% methods.regions = 'v3' ;   % NAmER_WAAR (NO Turkey), Australian ecoregions (WWF), WWF-defined southern border for MIDE.
% methods.regions = 'v4' ;   % NAmER_WAAR+Turkey, Australian ecoregions (WWF), WWF-defined southern border for MIDE, NH/SH South American ecoregions (WWF).
methods.regions = 'v5' ;   % NAmER_WAAR (NO Turkey), Australian ecoregions (WWF), WWF-defined southern border for MIDE, NH/SH South American ecoregions (WWF).

%%% Minimum mean coverage in a region-month for an LC to be included in
%%% unpacking? I.e., mean(LCfrac_*)
%%% (<= this, won't be included) (fraction)
methods.min_coverage = 0 ;   % Only exclude covers that don't exist
% methods.min_coverage = 0.005 ;
% methods.min_coverage = 0.03 ;

%%% Are we normalizing?
methods.normType = 'none' ;
% methods.normType = 'max' ;   % To match Magi et al. (2012)
% methods.normType = 'mean' ;
% methods.normType = 'median' ;
if ~strcmp(methods.normType,'none')
    %%% Should cells that never burned have their normConstant changed to 0?
    %%% (0=No, 1=Yes.) If not, it'll be Inf, resulting in NaN values for
    %%% fire_norm, meaning that cells that never burned will be ignored in Fk
    %%% calculations. (This is how the NiceNumber results were obtained.)
    %     methods.set_neverBurned = -1 ; warning('You really should do something with neverBurned cells...')
    methods.set_neverBurned = 0 ;
    %     methods.set_neverBurned = 1 ;
end

%%% What land cover data to use?
methods.landcover = 'hyde' ;
% methods.landcover = 'fake_3member' ;

%%% Do two-step unpacking (ie, Pk and Fk) or just one step (ie, Fk only a
%%% la Magi et al. 2012)?
methods.numUnpackSteps = 1 ;   % To match Magi et al. (2012)
% methods.numUnpackSteps = 2 ;

%%% Area or proportion land cover?
% methods.coverType = 'prop' ;   % To match Magi et al. (2012)
methods.coverType = 'area' ;

%%% Match input data (e.g., regress proportion cover vs. frac. burned)?
% methods.match = 0 ;   % To match Magi et al. (2012)
methods.match = 1 ;

%%% What kind of observed fire data?
% methods.fireType = 'terra' ;
% methods.fireType = 'aqua' ;
% methods.fireType = 'gfed3' ;   % To match Magi et al. (2012)
% methods.fireType = 'gfed' ;
methods.fireType = 'randerson_all' ;
if strcmp(methods.fireType,'gfed3') && max(years.list)>2009
    error('GFED3 data only go through 2009.')
elseif (strcmp(methods.fireType,'gfed') || strcmp(methods.fireType,'randerson_all')) && min(years.list)<2001
    error('GFED4 and Randerson data start in 2001.')
elseif strcmp(methods.fireType,'terra') && min(years.list)>2008
    error('GFED4 data only go through 2008.')
end

%%% Include a constant term in the logistic regression?
methods.constant = 0 ;
% methods.constant = 1 ;

%%% Constrain Fk values to >= 1?
% methods.constrain = 0 ;
methods.constrain = 1 ;   % To match Magi et al. (2012)

% Re-distribute negative OTHR BA?
methods.redistNegOTHR = 0 ;   % To match Magi et al. (2012)
% methods.redistNegOTHR = 1 ;

% Do "efficient" version of Pa code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Ps = 0 ;
% methods.efficient_Ps = 1 ;

% Do "efficient" version of FcFp code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
% methods.efficient_Fs = 0 ;   % To match Magi et al. (2012)
% methods.efficient_Fs = 0.1 ;   % Inefficient method, then take median of Fk values for each month (e.g. Jan.)
methods.efficient_Fs = 1 ;


%% (2) Setup

% Working directory and path
cd '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack'
addpath(genpath(pwd))
addpath(genpath('/Users/sam/Geodata/'))
addpath(genpath('/Users/sam/Documents/Dropbox/Projects/FireModel_daily/support'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices_s00 = (years.start - 1999):(years.end - 1999) ;
years.indices_s01 = (years.start - 2000):(years.end - 2000) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start_s00 = (years.start-2000)*12 + 1 ;
months.end_s00 = months.start_s00 + months.num - 1 ;
months.start_s01 = (years.start-2001)*12 + 1 ;
months.end_s01 = months.start_s01 + months.num - 1 ;
methods.num_months = months.num ;


%% (3) Import data

% Regions
disp('Loading regions_orig...')
load('/Users/sam/Geodata/New_regions_v4/MATLAB_QGIS/regions_v4.mat')
regions_all = regions ;
eval(['regions_orig = regions.' methods.regions ' ;']) ;
clear regions
regions_orig.map = single(regions_orig.map) ;
regions_orig.numCells = nan(regions_orig.num,1) ;
regions_orig.Yarray = repmat(regions_orig.map,[1 1 years.num]) ;
regions_orig.Marray = repmat(regions_orig.Yarray,[1 1 12]) ;

% Month datasets
Marray_month = zeros(720,1440,months.num,'int16') ;
for m = 1:months.num
    Marray_month(:,:,m) = m*ones(720,1440) ;
end ; clear m
Marray_trueMonth = zeros(720,1440,12,'int8') ;
for m = 1:12
    Marray_trueMonth(:,:,m) = m*ones(720,1440) ;
end ; clear m
Marray_trueMonth = repmat(Marray_trueMonth,[1 1 years.num]) ;

% Land covers
if strcmp(methods.landcover,'hyde')
    disp('Loading HYDE v3.1 land cover data...')
    load('/Users/sam/Geodata/HYDE31/MAT-files/HYDE31prop_quartDeg_0009.mat')
    landarea = HYDE_landarea_km2 ; clear HYDE_landarea_km2
    landarea_Marray = repmat(landarea,[1 1 months.num]) ;
    LCcrop_tmp = LCprop_crop_HYDE31_0009(:,:,years.indices_s00) ;
    LCpast_tmp = LCprop_past_HYDE31_0009(:,:,years.indices_s00) ;
elseif strcmp(methods.landcover,'fake_3member')
    
    % Fake classifications. Excludes Water (1) and FillValue/Unclassified (18).
    fake_crop = [13 15] ;   % Croplands, Cropland/NaturalVegetationMosaic
    fake_past = 8:11 ;   % OpenShrublands, WoodySavannas, Savannas, Grasslands
    fake_othr = [2:7 12 14 16 17] ;   % EvergreenNeedleleafForest, EvergreenBroadleafForest, DeciduousNeedleleafForest, DeciduousBroadleafForest, MixedForest, ClosedShrublands, PermanentWetlands, UrbanAndBuiltup, SnowAndIce, BarrenOrSparselyVegetated
    
    load('/Users/sam/Geodata/MCD12C1_quartDeg/MCD12C1_QD_0110.mat')
    MCD12C1_QD_0110 = MCD12C1_QD_0110(:,:,:,years.indices_s01) ;
    
    landarea_Yarray = squeeze(sum(MCD12C1_QD_0110,3)) ;
    water_mask = 0==min(squeeze(sum(MCD12C1_QD_0110(:,:,2:17,:),3)),[],3) ;   % Find grid cells that have no land other than Water (1) or FillValue/Unclassified (18) in at least one year.
    landarea_Yarray(repmat(water_mask,[1 1 years.num])) = NaN ;
    landarea = mean(landarea_Yarray,3) ;
    landarea_Marray = nan(720,1440,months.num) ;
    for y = 1:years.num
        monthTMP_start = (y-1)*12 + 1 ;
        monthTMP_end = monthTMP_start + 11 ;
        landarea_Marray(:,:,monthTMP_start:monthTMP_end) = repmat(landarea_Yarray(:,:,y),[1 1 12]) ;
    end
    
    LCcrop_tmp = squeeze(sum(MCD12C1_QD_0110(:,:,fake_crop,:),3)) ./ landarea_Yarray ;
    LCpast_tmp = squeeze(sum(MCD12C1_QD_0110(:,:,fake_past,:),3)) ./ landarea_Yarray ;
    
else error('methods.landcover improperly specified.')
end
LCcrop_tmp = single(LCcrop_tmp) ;
LCpast_tmp = single(LCpast_tmp) ;
LCothr_tmp = ones(size(LCcrop_tmp)) - (LCcrop_tmp + LCpast_tmp) ;
clear LCprop_*_HYDE31_0009
LCfrac_crop = nan(size(regions_orig.Marray),'single') ;
LCfrac_past = nan(size(regions_orig.Marray),'single') ;
LCfrac_othr = nan(size(regions_orig.Marray),'single') ;
for y = 1:years.num
    monthTMP_start = (y-1)*12 + 1 ;
    monthTMP_end = monthTMP_start + 11 ;
    LCfrac_crop(:,:,monthTMP_start:monthTMP_end) = repmat(LCcrop_tmp(:,:,y),[1 1 12]) ;
    LCfrac_past(:,:,monthTMP_start:monthTMP_end) = repmat(LCpast_tmp(:,:,y),[1 1 12]) ;
    LCfrac_othr(:,:,monthTMP_start:monthTMP_end) = repmat(LCothr_tmp(:,:,y),[1 1 12]) ;
end ; clear y monthTMP_*
clear LC*_tmp
LCdata_crop = LCfrac_crop ;
LCdata_past = LCfrac_past ;
LCdata_othr = LCfrac_othr ;
if strcmp(methods.coverType,'area')
    LCdata_crop = LCdata_crop .* landarea_Marray ;
    LCdata_past = LCdata_past .* landarea_Marray ;
    LCdata_othr = LCdata_othr .* landarea_Marray ;
end

% Observed fire
disp('Loading observed fire data...')
if strcmp(methods.fireType,'terra')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('/Users/sam/Documents/Dropbox/Projects/FireModel_globLoc/input/MOD14CMH_2001-2008_ssr.mat')
    obsFire = single(MOD14CMH(:,:,months.start_s01:months.end_s01)) ;
    clear MOD14CMH
elseif strcmp(methods.fireType,'aqua')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
elseif strcmp(methods.fireType,'gfed')
    load('gfed4_BA_highres_01-10_km2.mat')
    obsFire = single(gfed4_BA_highres(:,:,months.start_s01:months.end_s01)) ;
    clear gfed4
elseif strcmp(methods.fireType,'gfed3')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('GFED3.1_2000-2009_BAkm2.mat')
    obsFire = single(gfed31(:,:,months.start_s00:months.end_s00)) ;
    clear gfed31
elseif strcmp(methods.fireType,'randerson_all')
    load('BA_all_highres_01-10.mat')
    obsFire = single(BA_all_highres(:,:,months.start_s01:months.end_s01)) ;
    clear BA_all_lowres
else error('methods.fireType improperly specified.')
end
clear BA_*res
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    [obsFireNorm,fire_normConstant_orig,fire_normConstant] = normObs(obsFire,methods) ;
    obsFireNorm = single(obsFireNorm(~isnan(regions_orig.Marray))) ;
elseif strcmp(methods.normType,'none')
    fire_normConstant = max(obsFire,[],3) ;
else error('methods.normType improperly specified.')
end
obsFire = single(obsFire) ;
obsFire_yn = (obsFire>0) ;


% Reconcile NaNs across products
bad = ( isnan(regions_orig.map) | isnan(mean(LCfrac_crop,3)) | isnan(mean(obsFire,3)) ) ;
regions_orig.map(bad) = NaN ;
regions_orig.Yarray = repmat(regions_orig.map,[1 1 years.num]) ;
regions_orig.Marray = repmat(regions_orig.Yarray,[1 1 12]) ;
landarea(bad) = NaN ;
bad_array = repmat(bad,[1 1 months.num]) ;
LCfrac_crop = LCfrac_crop(~bad_array) ;
LCfrac_past = LCfrac_past(~bad_array) ;
LCfrac_othr = LCfrac_othr(~bad_array) ;
LCdata_crop = LCdata_crop(~bad_array) ;
LCdata_past = LCdata_past(~bad_array) ;
LCdata_othr = LCdata_othr(~bad_array) ;
Marray_month = Marray_month(~bad_array) ;
Marray_trueMonth = Marray_trueMonth(~bad_array) ;
if ~strcmp(methods.normType,'none')
    obsFireNorm = obsFireNorm(~bad_array) ;
    fire_normConstant = fire_normConstant(~bad) ;
    fire_normConstant_orig = fire_normConstant_orig(~bad) ;
end
obsFire_yn = obsFire_yn(~bad_array) ;

obsFire_frac = single(obsFire ./ landarea_Marray) ;
obsFire = obsFire(~bad_array) ;
obsFire_frac = obsFire_frac(~bad_array) ;
clear landarea_Marray bad_array

regKey = regions_orig.Marray(~isnan(regions_orig.Marray)) ;

for r = 1:regions_orig.num
    regions_orig.numCells(r) = length(find(regions_orig.map==r)) ;
end
    
methods.num_cells = length(find(~isnan(regions_orig.map))) ;

landarea_vectorONE = landarea(~isnan(landarea)) ;
landarea_vectorALL = repmat(landarea_vectorONE,[years.num 1]) ;

disp('Done.') ; disp(' ')


%% (4) Do unpacking and get results for each run, saving every 10

disp('Setting up for unpacking...')

numCells_train = sum(regions_orig.numCells) ;

% Set up for output
regions_tmp = regions_all.gfed ;
regions_tmp.map(bad) = NaN ;
regions_tmp.Marray = repmat(single(regions_tmp.map),[1 1 months.num]) ;
allCells_tmp = regions_tmp.Marray(~isnan(regions_tmp.Marray)) ;
SSE_allRegs_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
estBAtest_crop_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
estBAtest_past_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
estBAtest_othr_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
obsBAtest_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
estBAall_crop_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
estBAall_past_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
estBAall_othr_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
obsBAall_rt = nan(regions_tmp.num+1,methods.trainRuns,'single') ;
estBAall_crop = nan(size(regKey),'single') ;
estBAall_past = nan(size(regKey),'single') ;
estBAall_othr = nan(size(regKey),'single') ;

if methods.numUnpackSteps == 2
    if methods.efficient_Ps == 0
        allReg_Pc_mrt = nan(months.num,regions_orig.num,methods.trainRuns,'single') ;
        allReg_Pp_mrt = nan(months.num,regions_orig.num,methods.trainRuns,'single') ;
        allReg_Po_mrt = nan(months.num,regions_orig.num,methods.trainRuns,'single') ;
        allReg_const_mrt = nan(months.num,regions_orig.num,methods.trainRuns,'single') ;
    elseif methods.efficient_Ps == 1
        allReg_Pc_mrt = nan(12,regions_orig.num,methods.trainRuns,'single') ;
        allReg_Pp_mrt = nan(12,regions_orig.num,methods.trainRuns,'single') ;
        allReg_Po_mrt = nan(12,regions_orig.num,methods.trainRuns,'single') ;
        allReg_const_mrt = nan(12,regions_orig.num,methods.trainRuns,'single') ;
    end
end
if methods.efficient_Fs == 0
    allReg_Fc_mrt = nan(months.num,regions_orig.num,methods.trainRuns,'single') ;
    allReg_Fp_mrt = nan(months.num,regions_orig.num,methods.trainRuns,'single') ;
    allReg_Fo_mrt = nan(months.num,regions_orig.num,methods.trainRuns,'single') ;
elseif methods.efficient_Fs==0.1 || methods.efficient_Fs==1
    allReg_Fc_mrt = nan(12,regions_orig.num,methods.trainRuns,'single') ;
    allReg_Fp_mrt = nan(12,regions_orig.num,methods.trainRuns,'single') ;
    allReg_Fo_mrt = nan(12,regions_orig.num,methods.trainRuns,'single') ;
end

% for t = 1 ; warning('Only doing one run!')
for t = 1:methods.trainRuns
    disp(['Run ' num2str(t) ' of ' num2str(methods.trainRuns) ':'])
    
    tic_thisRun = tic ;
    
    fprintf('%s','   Setting up for this run...')
    tic
    
    if methods.numUnpackSteps == 2
        if methods.efficient_Ps == 0
            allReg_Pc_mr_tmp = nan(months.num,regions_orig.num,'single') ;
            allReg_Pp_mr_tmp = nan(months.num,regions_orig.num,'single') ;
            allReg_Po_mr_tmp = nan(months.num,regions_orig.num,'single') ;
            allReg_const_mr_tmp = nan(months.num,regions_orig.num,'single') ;
        elseif methods.efficient_Ps == 1
            allReg_Pc_mr_tmp = nan(12,regions_orig.num,'single') ;
            allReg_Pp_mr_tmp = nan(12,regions_orig.num,'single') ;
            allReg_Po_mr_tmp = nan(12,regions_orig.num,'single') ;
            allReg_const_mr_tmp = nan(12,regions_orig.num,'single') ;
        end
    end
    
    if methods.efficient_Fs == 0
        allReg_Fc_mr_tmp = nan(months.num,regions_orig.num,'single') ;
        allReg_Fp_mr_tmp = nan(months.num,regions_orig.num,'single') ;
        allReg_Fo_mr_tmp = nan(months.num,regions_orig.num,'single') ;
    elseif methods.efficient_Fs==0.1 || methods.efficient_Fs==1
        allReg_Fc_mr_tmp = nan(12,regions_orig.num,'single') ;
        allReg_Fp_mr_tmp = nan(12,regions_orig.num,'single') ;
        allReg_Fo_mr_tmp = nan(12,regions_orig.num,'single') ;
    end
    
    % Choose sample
    regions.num = regions_orig.num ;
    regions.map = regions_orig.map ;
    regions.names = regions_orig.names ;
    choseTrain = [] ;
    regKey_oneMonth = regions.map(~isnan(regions.map)) ;
    for r = 1:regions.num   % Make a one-month-long vector of chosen indices
        chooseFrom = find(regKey_oneMonth==r) ;
        choseTrain = cat(1,choseTrain,randsample(chooseFrom,regions_orig.numCells(r),true)) ;
    end ; clear r
    allCells_train = nan(size(regKey)) ;
    for m = 1:months.num   % Make a regKey-length vector showing which values of regKey-length vectors are being sampled
        xstart = (m-1)*methods.num_cells + 1 ;
        xend = xstart + methods.num_cells - 1 ;
        toAdd = (m-1)*methods.num_cells ;
        allCells_train(xstart:xend) = choseTrain + toAdd ;
    end ; clear m
    regKey_train = regKey(allCells_train) ;
        
%     tmp = repmat(mapTrain_yn,[1 1 months.num]) ;
%     allCells_train = tmp(~isnan(regions_orig.Marray)) ;
%     clear tmp
%     allCells_test = double(allCells_train) ;
%     allCells_test(allCells_test==0) = 2 ;
%     allCells_test = allCells_test - 1 ;
%     allCells_test = logical(allCells_test) ;
%     if ~isempty(find(isnan(allCells_train) | isnan(allCells_test),1))
%         error('NaN somewhere in allCells_train/test.')
%     elseif ~isempty(find((allCells_train==0 & allCells_test==0) | (allCells_train==1 & allCells_test==1),1))
%         error('allCells_train and allCells_test not perfectly complementary.')
%     end
    
    allRegTrain_Fc = nan(size(regKey),'single') ;
    allRegTrain_Fp = nan(size(regKey),'single') ;
    allRegTrain_Fo = nan(size(regKey),'single') ;
    if methods.numUnpackSteps==2
        allRegTrain_P = nan(size(regKey),'single') ;
    end
    
    fprintf('%s\n',[' Done (' toc_hms(toc) ').'])
    
    landarea_vectorONE_chosen = landarea_vectorONE(choseTrain) ;
    
    fprintf('%s','   Unpacking...')
    tic
    for r = 1:regions.num
        
        % Set up thisReg
        thisReg_indices = allCells_train(regKey==r) ;
        thisReg.crop = LCdata_crop(thisReg_indices) ;
        thisReg.past = LCdata_past(thisReg_indices) ;
        thisReg.othr = LCdata_othr(thisReg_indices) ;
        if strcmp(methods.normType,'none')
            thisReg.obsFire = obsFire(thisReg_indices) ;
        else
            thisReg.obsFire = obsFireNorm(thisReg_indices) ;
        end
        thisReg.month = Marray_month(thisReg_indices) ;
        thisReg.trueMonth = Marray_trueMonth(thisReg_indices) ;
        if methods.efficient_Fs==1
            tmp = landarea_vectorONE_chosen(regKey_oneMonth==r) ;
            thisReg.landarea = repmat(tmp,[years.num,1]) ;
            clear tmp
        else
            thisReg.landarea = landarea_vectorONE_chosen(regKey_oneMonth==r) ;
        end
        
        % Calculate probability of fire, if doing so
        if methods.numUnpackSteps == 2
            thisReg.obsFire_yn = obsFire_yn(thisReg_indices) ;
            [P_train,Pc,Pp,Po,const] = calcP_pureUnpack_CPO(thisReg,methods,months) ;
            if ~isempty(find(isnan(P),1))
                error(['Some value of P is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
            end
            allRegTrain_P(regKey_train) = P_train ;
            allReg_Pc_mr_tmp(:,r) = Pc ;
            allReg_Pp_mr_tmp(:,r) = Pp ;
            allReg_Po_mr_tmp(:,r) = Po ;
            allReg_const_mr_tmp(:,r) = const ;
            clear P_train Pc Pp Po const
        end
        
        % Calculate Fk's
        if methods.numUnpackSteps == 1
            [Fc_train,Fp_train,Fo_train,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_noZeros_CPO_1step(thisReg,months,methods) ;
        elseif methods.numUnpackSteps == 2
            [Fc_train,Fp_train,Fo_train,Fc_m,Fp_m,Fo_m] = calcF_pureUnpack_noZeros_CPO(thisReg,months,methods) ;
        end
        allReg_Fc_mr_tmp(:,r) = Fc_m ;
        allReg_Fp_mr_tmp(:,r) = Fp_m ;
        allReg_Fo_mr_tmp(:,r) = Fo_m ;
        allRegTrain_Fc(regKey_train==r) = Fc_train ;
        allRegTrain_Fp(regKey_train==r) = Fp_train ;
        allRegTrain_Fo(regKey_train==r) = Fo_train ;
        clear Fc_m Fp_m Fo_m
        
        clear thisReg
    end ; clear r
    
    % Save parameter values for this run
    if methods.numUnpackSteps==2
        allReg_Pc_mrt(:,:,t) = allReg_Pc_mr_tmp ;
        allReg_Pp_mrt(:,:,t) = allReg_Pp_mr_tmp ;
        allReg_Po_mrt(:,:,t) = allReg_Po_mr_tmp ;
        allReg_const_mrt(:,:,t) = allReg_const_mr_tmp ;
    end
    allReg_Fc_mrt(:,:,t) = allReg_Fc_mr_tmp ;
    allReg_Fp_mrt(:,:,t) = allReg_Fp_mr_tmp ;
    allReg_Fo_mrt(:,:,t) = allReg_Fo_mr_tmp ;
    
    
    fprintf('%s\n',[' Done (' toc_hms(toc) ').'])
    
    % Save every 10 iterations
    if rem(t,10)==0
        fprintf('%s','   Saving progress...')
        tic
        if t==10
            suffix = 1 ;
            basename = [prefix '_' num2str(suffix)] ;
            while ~isempty(dir([outDir basename '.mat']))
                suffix = suffix + 1 ;
                basename = [prefix '_' num2str(suffix)] ;
            end
        end
        filename = [outDir basename '.mat'] ;
        
        save(filename,...
            'allReg_Fc_mrt','allReg_Fp_mrt','allReg_Fo_mrt')
        if methods.numUnpackSteps==2
            save(filename,...
                'allReg_Pc_mrt','allReg_Pp_mrt','allReg_Po_mrt',...
                '-append')
        end
        fprintf('%s\n',[' Done (' toc_hms(toc) ').'])
    end
    
    disp(['Done with run ' num2str(t) ' of ' num2str(methods.trainRuns) ' (' toc_hms(toc(tic_thisRun)) ').']) ; disp(' ')
end




%% (5) Save

methods_out = methods ;

if ~exist('filename','var')
    suffix = 1 ;
    basename = [prefix '_' num2str(suffix)] ;
    while ~isempty(dir([outDir basename '.mat']))
        suffix = suffix + 1 ;
        basename = [prefix '_' num2str(suffix)] ;
    end
    filename = [outDir basename '.mat'] ;
end

save(filename,'methods_out',...
              'allReg_Fc_mrt','allReg_Fp_mrt','allReg_Fo_mrt') ;
if methods.numUnpackSteps==2
    save(filename,...
         'allReg_Pc_mrt','allReg_Pp_mrt','allReg_Po_mrt',...
         '-append')
end


