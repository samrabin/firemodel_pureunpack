function [NfBa_crop, NfBa_past, NfBa_othr] = reDistNeg_pureUnpack_CPO( ...
    NfBa_crop,NfBa_past,NfBa_othr, ...
    LCfrac_crop,LCfrac_past,LCfrac_othr)

theseEstFires = [NfBa_crop NfBa_past NfBa_othr] ;

theseCovers = [LCfrac_crop LCfrac_past LCfrac_othr] ;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% When all three are negative %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
allNeg = (theseEstFires(:,1)<0 & theseEstFires(:,2)<0 & theseEstFires(:,3)<0) ;
theseEstFires(allNeg,:) = 0 ;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% When CROP or PAST is negative, but not OTHR %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% CROP
% Find cells with negative fire for CROP
negFireCells = find(theseEstFires(:,1)<0) ;

% Extract fire data for those cells
theseEstFires_negFireCells = theseEstFires(negFireCells,:) ;

% Re-apportion negative BA
negFireToAdd = theseEstFires_negFireCells(:,1) ;
if max(negFireToAdd) >= 0
    error('Re-apportioning CROP: max(negFireToAdd) >= 0')
end
theseEstFires_negFireCells(:,3) = theseEstFires_negFireCells(:,3) ...
    + negFireToAdd ;
theseEstFires_negFireCells(:,1) = 0 ;
theseEstFires(negFireCells,:) = theseEstFires_negFireCells ;
clear *negFire*


% PAST
% Find cells with negative fire for PAST
negFireCells = find(theseEstFires(:,2)<0) ;

% Extract cover and fire data for those cells
theseEstFires_negFireCells = theseEstFires(negFireCells,:) ;

% Re-apportion negative BA
negFireToAdd = theseEstFires_negFireCells(:,2) ;
if max(negFireToAdd) >= 0
    error('Re-apportioning PAST: max(negFireToAdd) >= 0')
end
theseEstFires_negFireCells(:,3) = theseEstFires_negFireCells(:,3) ...
    + negFireToAdd ;
theseEstFires_negFireCells(:,2) = 0 ;
theseEstFires(negFireCells,:) = theseEstFires_negFireCells ;
clear *negFire*



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% When OTHR is negative %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%% When OTHR is negative (in a model run), at least one of CROP or PAST
%%%%% is going to be zero (because negative BA must have come from
%%%%% somewhere, and Li-OTHR model doesn't generate negative values).
%%%%% Therefore, it makes more sense to re-distribute negative
%%%%% OTHR BA only to the non-zero LC type, if any. Now adding that as an
%%%%% option.

% Find cells with negative fire for OTHR
negFireCells = find(theseEstFires(:,3)<0) ;

% Extract cover and fire data for those cells
theseCovers_negFireCells = theseCovers(negFireCells,:) ;
theseEstFires_negFireCells = theseEstFires(negFireCells,:) ;

if max(theseEstFires_negFireCells(:,3)) >= 0
    error('Re-apportioning OTHR: max(theseEstFires_negFireCells(:,3)) >= 0')
end

% Re-apportion negative BA
fire_crop = theseEstFires_negFireCells(:,1) ;
fire_past = theseEstFires_negFireCells(:,2) ;
fire_othr = theseEstFires_negFireCells(:,3) ;
prop_to_crop = theseCovers_negFireCells(:,1) ...
    ./ (theseCovers_negFireCells(:,1) ...
    + theseCovers_negFireCells(:,2)) ;
prop_to_past = theseCovers_negFireCells(:,2) ...
    ./ (theseCovers_negFireCells(:,1) ...
    + theseCovers_negFireCells(:,2)) ;
fire_crop = fire_crop + (fire_othr.*prop_to_crop) ;
fire_past = fire_past + (fire_othr.*prop_to_past) ;
% Correct for when there's no pasture or cropland area
fire_crop(theseCovers_negFireCells(:,1)+theseCovers_negFireCells(:,2)==0) = 0 ;
fire_past(theseCovers_negFireCells(:,1)+theseCovers_negFireCells(:,2)==0) = 0 ;

% Save for next step
theseEstFires_negFireCells(:,1) = fire_crop ;
theseEstFires_negFireCells(:,2) = fire_past ;
theseEstFires_negFireCells(:,3) = 0 ;
theseEstFires(negFireCells,:) = theseEstFires_negFireCells ;


%%%%%%%%%%%%%%%%%
%%% Finish up %%%
%%%%%%%%%%%%%%%%%

% Correct for when there's just too much negative BA
theseEstFires(theseEstFires<0) = 0 ;

% Save re-apportioned/corrected values
NfBa_crop = theseEstFires(:,1) ;
NfBa_past = theseEstFires(:,2) ;
NfBa_othr = theseEstFires(:,3) ;



end