%% Setup

cd '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack/Troubleshooting_20140415/'
addpath(genpath(pwd))

% prefix = 'TS_out_v2_201404151808_' ;
prefix = 'TS_out_v1_201404161008_' ;
% prefix = 'TS_out_v2_201404161117_' ;

methods.num_months = 108 ;
methods.num_cells = 231035 ;

pause(1)

%% Import and process data

filelist = dir([prefix '*.mat']) ;

estCROP_samples = nan(length(filelist),1) ;
estPAST_samples = nan(length(filelist),1) ;
estOTHR_samples = nan(length(filelist),1) ;
estTOTAL_samples = nan(length(filelist),1) ;
obsTOTAL_samples = nan(length(filelist),1) ;

disp('Processing files...')
for f = 1:length(filelist)
    disp(['File ' num2str(f) ' of ' num2str(length(filelist)) '...'])
    if strcmp(filelist(f).name,[prefix 'summary.mat'])
        continue
    end
    thisFile = load(filelist(f).name) ;
    
    if f==1 && isfield(thisFile,'estFire_actual_crop')
        estCROP_actual = nan(length(filelist),1) ;
        estPAST_actual = nan(length(filelist),1) ;
        estOTHR_actual = nan(length(filelist),1) ;
        estTOTAL_actual = nan(length(filelist),1) ;
    end
    
    % Calculate totals
    

    % Calculate global estimates: Sample
    tmp = reshape(thisFile.estFire_samples_crop,[methods.num_cells 12 (methods.num_months/12)]) ;
    estCROP_samples(f) = mean(sum(sum(tmp,2),1),3) ;
    clear tmp
    tmp = reshape(thisFile.estFire_samples_past,[methods.num_cells 12 (methods.num_months/12)]) ;
    estPAST_samples(f) = mean(sum(sum(tmp,2),1),3) ;
    clear tmp
    tmp = reshape(thisFile.estFire_samples_othr,[methods.num_cells 12 (methods.num_months/12)]) ;
    estOTHR_samples(f) = mean(sum(sum(tmp,2),1),3) ;
    clear tmp
    thisFile_estFire_samples_total = thisFile.estFire_samples_crop + thisFile.estFire_samples_past + thisFile.estFire_samples_othr ;
    tmp = reshape(thisFile_estFire_samples_total,[methods.num_cells 12 (methods.num_months/12)]) ;
    estTOTAL_samples(f) = mean(sum(sum(tmp,2),1),3) ;
    clear tmp
    tmp = reshape(thisFile.obsFire_samples,[methods.num_cells 12 (methods.num_months/12)]) ;
    obsTOTAL_samples(f) = mean(sum(sum(tmp,2),1),3) ;
    clear tmp
    
    if isfield(thisFile,'estFire_actual_crop')
        % Calculate global estimates: Actual
        thisFile_estFire_actual_total = thisFile.estFire_actual_crop + thisFile.estFire_actual_past + thisFile.estFire_actual_othr ;
        tmp = reshape(thisFile.estFire_actual_crop,[methods.num_cells 12 (methods.num_months/12)]) ;
        estCROP_actual(f) = mean(sum(sum(tmp,2),1),3) ;
        clear tmp
        tmp = reshape(thisFile.estFire_actual_past,[methods.num_cells 12 (methods.num_months/12)]) ;
        estPAST_actual(f) = mean(sum(sum(tmp,2),1),3) ;
        clear tmp
        tmp = reshape(thisFile.estFire_actual_othr,[methods.num_cells 12 (methods.num_months/12)]) ;
        estOTHR_actual(f) = mean(sum(sum(tmp,2),1),3) ;
        clear tmp
        tmp = reshape(thisFile_estFire_actual_total,[methods.num_cells 12 (methods.num_months/12)]) ;
        estTOTAL_actual(f) = mean(sum(sum(tmp,2),1),3) ;
        clear tmp
    end
    
    clear thisFile
end ; clear f

disp('Done.') ; disp(' ')


%% Save data

filename = [prefix 'summary.mat'] ;
save(filename,...
    'estCROP_samples','estPAST_samples','estOTHR_samples','estTOTAL_samples',...
    'obsTOTAL_samples')
if exist('estTOTAL_actual','var')
    save(filename,...
        'estCROP_actual','estPAST_actual','estOTHR_actual','estTOTAL_actual',...
        '-append')
end


%% Make histograms

figure ; hist(estTOTAL_samples)
figure ; hist(obsTOTAL_samples)
figure ; hist(obsTOTAL_samples - estTOTAL_samples)

figure ; hist((obsTOTAL_samples - estTOTAL_samples)./obsTOTAL_samples)
