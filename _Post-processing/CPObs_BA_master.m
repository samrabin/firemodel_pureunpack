%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculate burned area estimates based on bootstrapped parameters %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_allowNegFk_effFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_allowNegFk_effFk_no0s_AA_NOprenorm_randersonCemis' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randersonCemis' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_PNmean_randerson' ;
prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_randerson' ;

inDir = '~/Geodata/Unpacking_bootstrap_analysis/Output/' ;

%%% What years are we working with?
% years.list = 2003:2008 ;
% years.list = 2000:2009 ;   % To match Magi et al. (2012). Extent of HYDE 3.1.
years.list = 2001:2009 ;   % Overlap of HYDE 3.1 and Randerson.
% years.list = 2001:2003 ;   % To match Korontzi et al. (2006)


%% (1) Import bootstrapping run results (and get Methods) 
pause(1)
disp('Importing parameter sets from bootstrapping runs...')

% Make empty datasets
allReg_Fc_mrt = [] ;
allReg_Fp_mrt = [] ;
allReg_Fo_mrt = [] ;

f = 1 ;
while ~isempty(dir([inDir prefix '_' num2str(f) '.mat']))
    disp(['Loading file ' num2str(f) '...'])
    filename = [inDir prefix '_' num2str(f) '.mat'] ;
    thisFile = load(filename) ;
    if f == 1
        methods = thisFile.methods_out ;
    end
    
    tmp = ~isnan(squeeze(thisFile.allReg_Fc_mrt(1,1,:))) ;
    tmp2 = thisFile.allReg_Fc_mrt(:,:,tmp) ;
    tmp3 = thisFile.exclC_mrt(:,:,tmp) ;
    tmp2(isnan(tmp2) & tmp3) = 0 ;
    allReg_Fc_mrt = cat(3,allReg_Fc_mrt,tmp2) ;
    clear tmp tmp2 tmp3
    tmp = ~isnan(squeeze(thisFile.allReg_Fp_mrt(1,1,:))) ;
    tmp2 = thisFile.allReg_Fp_mrt(:,:,tmp) ;
    tmp3 = thisFile.exclP_mrt(:,:,tmp) ;
    tmp2(isnan(tmp2) & tmp3) = 0 ;
    allReg_Fp_mrt = cat(3,allReg_Fp_mrt,tmp2) ;
    clear tmp tmp2 tmp3
    tmp = ~isnan(squeeze(thisFile.allReg_Fo_mrt(1,1,:))) ;
    tmp2 = thisFile.allReg_Fo_mrt(:,:,tmp) ;
    tmp3 = thisFile.exclO_mrt(:,:,tmp) ;
    tmp2(isnan(tmp2) & tmp3) = 0 ;
    allReg_Fo_mrt = cat(3,allReg_Fo_mrt,tmp2) ;
    if size(tmp2,3) < thisFile.methods_out.trainRuns
        warning(['This run not complete: Only ' num2str(size(tmp2,3)) ' of ' num2str(thisFile.methods_out.trainRuns) ' runs.'])
    end
    clear tmp tmp2 tmp3
    
    if isfield(thisFile,'toc_allruns')
        if f==1
            show_allruns_time = true ;
            toc_allruns = thisFile.toc_allruns ;
        else
            toc_allruns = toc_allruns + thisFile.toc_allruns ;
        end
    else
        show_allruns_time = false ;
    end
    
    clear thisFile filename
    f = f + 1 ;
end
disp('Done.') ; disp(' ')

if isempty(allReg_Fc_mrt)
    error('Could not find any data.')
end

Nruns = size(allReg_Fc_mrt,3) ;

if show_allruns_time
    disp([num2str(Nruns) ' runs in ' num2str(roundn(toc_allruns/3600,-1)) ' hours of total computing time.']) ; disp(' ')
end


%% (2) Setup

% Working directory and path
cd '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack'
addpath(genpath(pwd))
addpath(genpath('/Users/sam/Geodata/'))
addpath(genpath('/Users/sam/Documents/Dropbox/Projects/FireModel_daily/support'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices_s00 = (years.start - 1999):(years.end - 1999) ;
years.indices_s01 = (years.start - 2000):(years.end - 2000) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start_s00 = (years.start-2000)*12 + 1 ;
months.end_s00 = months.start_s00 + months.num - 1 ;
months.start_s01 = (years.start-2001)*12 + 1 ;
months.end_s01 = months.start_s01 + months.num - 1 ;


%% (3) Import data

% Regions
disp('Loading regions_bsruns...')
load('/Users/sam/Geodata/New_regions_v4/MATLAB_QGIS/regions_v4.mat')
regions_all = regions ;
eval(['regions_bsruns = regions.' methods.regions ' ;']) ;
clear regions
regions_bsruns.map = single(regions_bsruns.map) ;
regions_bsruns.numCells = nan(regions_bsruns.num,1) ;
regions_bsruns.Yarray = repmat(regions_bsruns.map,[1 1 years.num]) ;
regions_bsruns.Marray = repmat(regions_bsruns.Yarray,[1 1 12]) ;

% Month datasets
Marray_month = zeros(720,1440,months.num,'int16') ;
for m = 1:months.num
    Marray_month(:,:,m) = m*ones(720,1440) ;
end ; clear m
% Marray_trueMonth = zeros(720,1440,12,'int8') ;
% for m = 1:12
%     Marray_trueMonth(:,:,m) = m*ones(720,1440) ;
% end ; clear m
% Marray_trueMonth = repmat(Marray_trueMonth,[1 1 years.num]) ;

% Land covers
if strcmp(methods.landcover,'hyde')
    disp('Loading HYDE v3.1 land cover data...')
    load('/Users/sam/Geodata/HYDE31/MAT-files/HYDE31prop_quartDeg_0009.mat')
    landarea = HYDE_landarea_km2 ; clear HYDE_landarea_km2
    landarea_Marray = repmat(landarea,[1 1 months.num]) ;
    LCcrop_tmp = LCprop_crop_HYDE31_0009(:,:,years.indices_s00) ;
    LCpast_tmp = LCprop_past_HYDE31_0009(:,:,years.indices_s00) ;
    clear LCprop_*_HYDE31_0009
elseif strcmp(methods.landcover,'fake_3member')
    
    % Fake classifications. Excludes Water (1) and FillValue/Unclassified (18).
    fake_crop = [13 15] ;   % Croplands, Cropland/NaturalVegetationMosaic
    fake_past = 8:11 ;   % OpenShrublands, WoodySavannas, Savannas, Grasslands
    fake_othr = [2:7 12 14 16 17] ;   % EvergreenNeedleleafForest, EvergreenBroadleafForest, DeciduousNeedleleafForest, DeciduousBroadleafForest, MixedForest, ClosedShrublands, PermanentWetlands, UrbanAndBuiltup, SnowAndIce, BarrenOrSparselyVegetated
    
    load('/Users/sam/Geodata/MCD12C1_quartDeg/MCD12C1_QD_0110.mat')
    MCD12C1_QD_0110 = MCD12C1_QD_0110(:,:,:,years.indices_s01) ;
    
    landarea_Yarray = squeeze(sum(MCD12C1_QD_0110,3)) ;
    water_mask = 0==min(squeeze(sum(MCD12C1_QD_0110(:,:,2:17,:),3)),[],3) ;   % Find grid cells that have no land other than Water (1) or FillValue/Unclassified (18) in at least one year.
    landarea_Yarray(repmat(water_mask,[1 1 years.num])) = NaN ;
    landarea = mean(landarea_Yarray,3) ;
    landarea_Marray = nan(720,1440,months.num) ;
    for y = 1:years.num
        monthTMP_start = (y-1)*12 + 1 ;
        monthTMP_end = monthTMP_start + 11 ;
        landarea_Marray(:,:,monthTMP_start:monthTMP_end) = repmat(landarea_Yarray(:,:,y),[1 1 12]) ;
    end
    
    LCcrop_tmp = squeeze(sum(MCD12C1_QD_0110(:,:,fake_crop,:),3)) ./ landarea_Yarray ;
    LCpast_tmp = squeeze(sum(MCD12C1_QD_0110(:,:,fake_past,:),3)) ./ landarea_Yarray ;
    
else error('methods.landcover improperly specified.')
end
LCcrop_tmp = single(LCcrop_tmp) ;
LCpast_tmp = single(LCpast_tmp) ;
LCothr_tmp = ones(size(LCcrop_tmp)) - (LCcrop_tmp + LCpast_tmp) ;
clear LCprop_*_HYDE31_0009
LCfrac_crop = nan(size(regions_bsruns.Marray),'single') ;
LCfrac_past = nan(size(regions_bsruns.Marray),'single') ;
LCfrac_othr = nan(size(regions_bsruns.Marray),'single') ;
for y = 1:years.num
    monthTMP_start = (y-1)*12 + 1 ;
    monthTMP_end = monthTMP_start + 11 ;
    LCfrac_crop(:,:,monthTMP_start:monthTMP_end) = repmat(LCcrop_tmp(:,:,y),[1 1 12]) ;
    LCfrac_past(:,:,monthTMP_start:monthTMP_end) = repmat(LCpast_tmp(:,:,y),[1 1 12]) ;
    LCfrac_othr(:,:,monthTMP_start:monthTMP_end) = repmat(LCothr_tmp(:,:,y),[1 1 12]) ;
end ; clear y monthTMP_*
clear LC*_tmp

% Observed fire
disp('Loading observed fire data...')
if strcmp(methods.fireType,'terra')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('/Users/sam/Documents/Dropbox/Projects/FireModel_globLoc/input/MOD14CMH_2001-2008_ssr.mat')
    obsFire = single(MOD14CMH(:,:,months.start_s01:months.end_s01)) ;
    clear MOD14CMH
elseif strcmp(methods.fireType,'aqua')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
elseif strcmp(methods.fireType,'gfed')
    load('gfed4_BA_highres_01-10_km2.mat')
    obsFire = single(gfed4_BA_highres(:,:,months.start_s01:months.end_s01)) ;
    clear gfed4
elseif strcmp(methods.fireType,'gfed3')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('GFED3.1_2000-2009_BAkm2.mat')
    obsFire = single(gfed31(:,:,months.start_s00:months.end_s00)) ;
    clear gfed31
elseif strcmp(methods.fireType,'randerson_all')
    load('BA_all_highres_01-10.mat')
    obsFire = single(BA_all_highres(:,:,months.start_s01:months.end_s01)) ;
    clear BA_all_lowres
    load('BA_IGBPcroppy.mat','BA_crop','BA_cropPLUSmos')
    obsFire_crop = single(BA_crop(:,:,months.start_s01:months.end_s01)) ;
    obsFire_cropPLUSmos = single(BA_cropPLUSmos(:,:,months.start_s01:months.end_s01)) ;
    clear BA_crop*
elseif strcmp(methods.fireType,'randerson_all_Cemis')
    load('C_all_highres_01-10.mat')
    obsFire = single(C_all_highres(:,:,months.start_s01:months.end_s01)) ;
    obsFire = obsFire .* (landarea_Marray*1e6) ;   % Convert from g/m2/month to g/month
    clear C_all_lowres
else error('methods.fireType improperly specified.')
end
clear BA_*res

% Normalize
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    [~,~,fire_normConstant] = normObs(obsFire,methods) ;
%     [obsFireNorm,~,fire_normConstant] = normObs(obsFire,methods) ;
%     obsFireNorm = single(obsFireNorm) ;
elseif ~strcmp(methods.normType,'none')
    error('methods.normType improperly specified.')
end
obsFire = single(obsFire) ;

% Reconcile NaNs across products
bad = ( isnan(regions_bsruns.map) | isnan(mean(LCfrac_crop,3)) | isnan(mean(obsFire,3)) ) ;
regions_bsruns.map(bad) = NaN ;
regions_bsruns.Yarray = repmat(regions_bsruns.map,[1 1 years.num]) ;
regions_bsruns.Marray = repmat(regions_bsruns.Yarray,[1 1 12]) ;
landarea(bad) = NaN ;
bad_array = repmat(bad,[1 1 months.num]) ;
LCfrac_crop = LCfrac_crop(~bad_array) ;
LCfrac_past = LCfrac_past(~bad_array) ;
Marray_month = Marray_month(~bad_array) ;
% Marray_trueMonth = Marray_trueMonth(~bad_array) ;
if ~strcmp(methods.normType,'none')
%     obsFireNorm = obsFireNorm(~bad_array) ;
    fire_normConstant_Marray = repmat(fire_normConstant,[1 1 months.num]) ;
    fire_normConstant = fire_normConstant_Marray(~bad_array) ;
    clear fire_normConstant_Marray
    % fire_normConstant_orig = fire_normConstant_orig(~bad) ;
else
    fire_normConstant = [] ;
end

obsFire = obsFire(~bad_array) ;
if exist('obsFire_crop','var')
    obsFire_crop = obsFire_crop(~bad_array) ;
else obsFire_crop = [] ;
end
if exist('obsFire_cropPLUSmos','var')
    obsFire_cropPLUSmos = obsFire_cropPLUSmos(~bad_array) ;
else obsFire_cropPLUSmos = [] ;
end
clear landarea_Marray bad_array

regKey_bsruns = uint16(regions_bsruns.Marray(~isnan(regions_bsruns.Marray))) ;
regions_out.map(bad) = NaN ;
regions_out.Marray = repmat(regions_out.map,[1 1 months.num]) ;
% regKey_out = uint16(regions_out.Marray(~isnan(regions_out.Marray))) ;

for r = 1:regions_bsruns.num
    regions_bsruns.numCells(r) = length(find(regions_bsruns.map==r)) ;
end
    
methods.num_cells = length(find(~isnan(regions_bsruns.map))) ;

landarea_vectorONE = landarea(~isnan(landarea)) ;
% landarea_vectorALL = repmat(landarea_vectorONE,[years.num 1]) ;

disp('Done.') ; disp(' ')


%% (4) One country results
warning('Check and see if you get same results for CI95 when doing ANOTHER bootstrap, where you randomly select one trainRun FOR EACH REGION-MONTH.') ; disp(' ')
clear thisCtry*

country_name = 'India' ; thisCtry = 106 ;
% country_name = 'Ukraine' ; thisCtry = 232 ;
% country_name = 'USA' ; thisCtry = 233 ;
% country_name = 'Mali' ; thisCtry = 147 ;
% country_name = 'All Countries' ; thisCtry = 1 ;
disp(['Country: ' country_name]) ; disp(' ')

% Load countries map
countries_map = single(flipud(imread('/Users/sam/Geodata/New_regions_v4/GeoTIFFs--QuartDeg/countries_QD.tif','TIF'))) ;
countries_map(isnan(landarea)) = NaN ;
if strcmp(country_name,'All Countries')
    countries_map(~isnan(countries_map)) = 1 ;
end

% Check to see which region(s) this country is in
thisCtry_whichReg = unique(regions_bsruns.map(countries_map==thisCtry)) ;
if length(thisCtry_whichReg) > 1 && ~strcmp(country_name,'All Countries')
    warning('This country falls in >1 region. If that''s not intended, you should update the regions map used for analysis.')
end

% Get LCdata and obsFire for this country
close all
[thisCtry_LCdata_crop_mr,thisCtry_LCdata_past_mr, ...
 thisCtry_LCdata_othr_mr,thisCtry_obsFireRAW_total_my, ...
 thisCtry_obsFireRAW_crop_my, thisCtry_obsFireRAW_cropPLUSmos_my] =...
    get_thisCtry_LCdataCPO_obsFire_v2(thisCtry, thisCtry_whichReg, ...
                           LCfrac_crop, LCfrac_past, ...
                           obsFire, ...
                           months, years, methods, regKey_bsruns, ...
                           landarea, countries_map, ...
                           obsFire_crop, obsFire_cropPLUSmos, ...
                           fire_normConstant) ;
                       
% Get all Fk estimates for this country
thisCtry_Fc_mrt = allReg_Fc_mrt(:,thisCtry_whichReg,:) ;
thisCtry_Fp_mrt = allReg_Fp_mrt(:,thisCtry_whichReg,:) ;
thisCtry_Fo_mrt = allReg_Fo_mrt(:,thisCtry_whichReg,:) ;

% Get Fk estimates for best guess and 95% CI
CI_percentile = 95 ;
num_lo = (100-CI_percentile)/2 ; num_hi = num_lo + CI_percentile ; 
thisCtry_Fc_mrLMH(:,:,1) = prctile(allReg_Fc_mrt,num_lo,3) ;
thisCtry_Fc_mrLMH(:,:,2) = mean(allReg_Fc_mrt,3) ;
thisCtry_Fc_mrLMH(:,:,3) = prctile(allReg_Fc_mrt,num_hi,3) ;
thisCtry_Fp_mrLMH(:,:,1) = prctile(allReg_Fp_mrt,num_lo,3) ;
thisCtry_Fp_mrLMH(:,:,2) = mean(allReg_Fp_mrt,3) ;
thisCtry_Fp_mrLMH(:,:,3) = prctile(allReg_Fp_mrt,num_hi,3) ;
thisCtry_Fo_mrLMH(:,:,1) = prctile(allReg_Fo_mrt,num_lo,3) ;
thisCtry_Fo_mrLMH(:,:,2) = mean(allReg_Fo_mrt,3) ;
thisCtry_Fo_mrLMH(:,:,3) = prctile(allReg_Fo_mrt,num_hi,3) ;

% Calculate estimated BA for each bootstrapping run
[thisCtry_estFireRAW_crop_myt,thisCtry_estFireRAW_past_myt, ...
 thisCtry_estFireRAW_othr_myt,thisCtry_estFireRAW_total_myt] = ...
    get_thisCtry_estFireCPO(months,years,methods, ...
                            thisCtry_Fc_mrt,thisCtry_Fp_mrt,thisCtry_Fo_mrt, ...
                            thisCtry_LCdata_crop_mr,thisCtry_LCdata_past_mr,thisCtry_LCdata_othr_mr) ;
                        
% Convert from raw units to nicer ones
if strcmp(methods.fireType,'randerson_all')   
    units = 'Mha' ;
    % km2 to Mha
    thisCtry_estFire_crop_myt = thisCtry_estFireRAW_crop_myt * 1e-4 ;   
    thisCtry_estFire_past_myt = thisCtry_estFireRAW_past_myt * 1e-4 ;
    thisCtry_estFire_othr_myt = thisCtry_estFireRAW_othr_myt * 1e-4 ;
    thisCtry_estFire_total_myt = thisCtry_estFireRAW_total_myt * 1e-4 ;
    thisCtry_obsFire_total_my = thisCtry_obsFireRAW_total_my * 1e-4 ;
    thisCtry_obsFire_crop_my = thisCtry_obsFireRAW_crop_my * 1e-4 ;
    thisCtry_obsFire_cropPLUSmos_my = thisCtry_obsFireRAW_cropPLUSmos_my * 1e-4 ;
elseif strcmp(methods.fireType,'randerson_Cemis') || strcmp(methods.fireType,'randerson_all_Cemis')
    units = 'Tg (=Mt)' ;
    % gC to Tg (=Mt)
    thisCtry_estFire_crop_myt = thisCtry_FireRAW_crop_myt * 1e-12 ;   
    thisCtry_estFire_past_myt = thisCtry_FireRAW_past_myt * 1e-12 ;
    thisCtry_estFire_othr_myt = thisCtry_FireRAW_othr_myt * 1e-12 ;
    thisCtry_estFire_total_myt = thisCtry_FireRAW_g_total_myt * 1e-12 ;
    thisCtry_obsFire_total_my = thisCtry_FireRAW_total_my * 1e-12 ;
    thisCtry_obsFire_crop_my = thisCtry_obsFireRAW_crop_my * 1e-12 ;
    thisCtry_obsFire_cropPLUSmos_my = thisCtry_obsFireRAW_cropPLUSmos_my * 1e-12 ;
end
clear thisCtry_*FireRAW_*

% Calculate and display annual numbers (mean + 95% CI from percentiles)
thisCtry_estFire_Ann_CIloMeanCIhi = CPObs__calc_estFireAnn_CIloMeanCIhi(...
        thisCtry_estFire_crop_myt, thisCtry_estFire_past_myt, ...
        thisCtry_estFire_othr_myt, thisCtry_estFire_total_myt, ...
        thisCtry_obsFire_total_my, ...
        CI_percentile, units, country_name) ;
   
                
%% xxxxxOne country emissions results
warning('Check and see if you get same results for CI95 when doing ANOTHER bootstrap, where you randomly select one trainRun FOR EACH REGION-MONTH.') ; disp(' ')
clear thisCtry*

% country_name = 'India' ; thisCtry = 106 ;
% country_name = 'Ukraine' ; thisCtry = 232 ;
% country_name = 'USA' ; thisCtry = 233 ;
% country_name = 'Mali' ; thisCtry = 147 ;
country_name = 'All Countries' ; thisCtry = 1 ;

% Load countries map
countries_map = single(flipud(imread('/Users/sam/Geodata/New_regions_v4/GeoTIFFs--QuartDeg/countries_QD.tif','TIF'))) ;
countries_map(isnan(landarea)) = NaN ;
if strcmp(country_name,'All Countries')
    countries_map(~isnan(countries_map)) = 1 ;
end

% Check to see which region(s) this country is in
thisCtry_whichReg = unique(regions_bsruns.map(countries_map==thisCtry)) ;
if length(thisCtry_whichReg) > 1 && ~strcmp(country_name,'All Countries')
    warning('This country falls in >1 region. If that''s not intended, you should update the regions map used for analysis.')
end

% Get LCdata and obsFire for this country
close all
[thisCtry_LCdata_crop_mr,thisCtry_LCdata_past_mr, ...
 thisCtry_LCdata_othr_mr,thisCtry_obsEmis_g_total_my] =...
    get_thisCtry_LCdataCPO_obsFire(thisCtry, thisCtry_whichReg, ...
                           LCfrac_crop, LCfrac_past, ...
                           obsFire, ...
                           months, years, methods, regKey_bsruns, ...
                           Marray_month, ...
                           landarea, countries_map) ;
                       
% Get Fk estimates for this country
thisCtry_Fc_mrt = allReg_Fc_mrt(:,thisCtry_whichReg,:) ;
thisCtry_Fp_mrt = allReg_Fp_mrt(:,thisCtry_whichReg,:) ;
thisCtry_Fo_mrt = allReg_Fo_mrt(:,thisCtry_whichReg,:) ;

% Calculate estimated BA for each bootstrapping run
[thisCtry_estEmis_g_crop_myt,thisCtry_estEmis_g_past_myt, ...
 thisCtry_estEmis_g_othr_myt,thisCtry_estEmis_g_total_myt] = ...
    get_thisCtry_estFireCPO(months,years,methods, ...
                            thisCtry_Fc_mrt,thisCtry_Fp_mrt,thisCtry_Fo_mrt, ...
                            thisCtry_LCdata_crop_mr,thisCtry_LCdata_past_mr,thisCtry_LCdata_othr_mr) ;

% Convert from g to Tg
thisCtry_estEmisTg_crop_myt = thisCtry_estEmis_g_crop_myt * 1e-12 ;   
thisCtry_estEmisTg_past_myt = thisCtry_estEmis_g_past_myt * 1e-12 ;
thisCtry_estEmisTg_othr_myt = thisCtry_estEmis_g_othr_myt * 1e-12 ;
thisCtry_estEmisTg_total_myt = thisCtry_estEmis_g_total_myt * 1e-12 ;
thisCtry_obsEmisTg_total_my = thisCtry_obsEmis_g_total_my * 1e-12 ;
clear thisCtry_*Emis_g_*

% Calculate and display annual numbers (mean + 95% CI from percentiles)
CI_percentile = 95 ;
units = 'Tg (=Mt)' ;
thisCtry_estEmisTg_Ann_CIloMeanCIhi = CPObs__calc_estFireAnn_CIloMeanCIhi(...
        thisCtry_estEmisTg_crop_myt, thisCtry_estEmisTg_past_myt, ...
        thisCtry_estEmisTg_othr_myt, thisCtry_estEmisTg_total_myt, ...
        thisCtry_obsEmisTg_total_my, ...
        CI_percentile, units, country_name) ;

error('Something is wrong with calculation of mean observed annual emissions.')

    
%% One country: Make CPO seasonality plot

include_CPOTeTo = [1 1 1 1 1] ;
% include_CPOTeTo = [0 1 1 0 0] ;
normalize = 0 ;
% normalize = 1 ;

CPObs__plotSeas_CPOTeTo( ...
                    thisCtry_estFire_crop_myt, ...
                    thisCtry_estFire_past_myt, ...
                    thisCtry_estFire_othr_myt, ...
                    thisCtry_estFire_total_myt, ...
                    thisCtry_obsFire_total_my, ...
                    include_CPOTeTo, ...
                    normalize) ;

                
%% One country: Make whole-timeseries plot

include_CPOTeTo = [1 1 1 1 1] ;
% include_CPOTeTo = [0 1 1 0 0] ;
normalize = 0 ;
% normalize = 1 ;
indiv_months = 0 ;
% indiv_months = 1 ;

CPObs__plotTS_CPOTeTo( ...
                thisCtry_estFire_crop_myt, ...
                thisCtry_estFire_past_myt, ...
                thisCtry_estFire_othr_myt, ...
                thisCtry_estFire_total_myt, ...
                thisCtry_obsFire_total_my, ...
                years, ...
                include_CPOTeTo, normalize, indiv_months)


%% One country: Compare seasonality of estimated cropland fire to observed

normalize = 0 ;
% normalize = 1 ;

CPObs__plotSeas_CeCoCMo( ...
                    thisCtry_estFire_crop_myt, ...
                    thisCtry_obsFire_crop_my, ...
                    thisCtry_obsFire_cropPLUSmos_my, ...
                    normalize)




