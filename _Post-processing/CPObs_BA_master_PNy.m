%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Calculate burned area estimates based on bootstrapped parameters %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_PNmean_randerson' ;
% prefix = 'REGv5_allowNegFk_effFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_NOprenorm_randerson' ; % Nice
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmaxm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmeanSNB0_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_exclNB_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNnone_randerson_fake3m' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmeanSNB0_randerson_fake3m' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNnone_randerson_fake3mv2' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNnone_exclNB_randerson_fake3mv2' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_exclNB_randerson_fake3mv2' ;
prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNnone_exclNBproper_randerson_fake3mv2' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_exclNBproper_randerson_fake3mv2' ;

% % % prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randersonCemis' ;
% prefix = 'REGv5_allowNegFk_effFk_no0s_AA_NOprenorm_randersonCemis' ; warning('Still not doing all Fk''s')

inDir = '~/Geodata/Unpacking_bootstrap_analysis/Output/' ;
outDir = '/Users/sam/Documents/Dropbox/4th-year talk/Figures/' ;
outDir_geo = '/Users/sam/Geodata/Unpacking_bootstrap_analysis/PostProc/Maps/' ;
load('/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack/Input/quartdeg_R.mat') ;

%%% What years are we working with?
% years.list = 2003:2008 ;
% years.list = 2000:2009 ;   % To match Magi et al. (2012). Extent of HYDE 3.1.
years.list = 2001:2009 ;   % Overlap of HYDE 3.1 and Randerson.
% years.list = 2001:2003 ;   % To match Korontzi et al. (2006)

%% (1) Import bootstrapping run results (and get Methods) 
pause(1)
disp('Importing parameter sets from bootstrapping runs...')

% Make empty datasets
allReg_Fc_mrt = [] ;
allReg_Fp_mrt = [] ;
allReg_Fo_mrt = [] ;

f = 1 ;
inDir_version = 1 ;
while isempty(dir([inDir prefix '_' num2str(f) '.mat']))
    if inDir_version == 1
        inDir = '/Volumes/Repository/Unpacking_bootstrap_analysis/Output_Repository/' ;
        inDir_version = 2 ;
    else
        error('No files found with this prefix.')
    end
end
clear inDir_version
while ~isempty(dir([inDir prefix '_' num2str(f) '.mat']))
    disp(['Loading file ' num2str(f) '...'])
    filename = [inDir prefix '_' num2str(f) '.mat'] ;
    thisFile = load(filename) ;
    if f == 1
        methods = thisFile.methods_out ;
        if ~isfield(methods,'normStyle')
            methods.normStyle = 1 ;
        end
    end
    
    tmp = ~isnan(squeeze(thisFile.allReg_Fc_mrt(1,1,:))) ;
    tmp2 = thisFile.allReg_Fc_mrt(:,:,tmp) ;
    tmp3 = thisFile.exclC_mrt(:,:,tmp) ;
    tmp2(isnan(tmp2) & tmp3) = 0 ;
    allReg_Fc_mrt = cat(3,allReg_Fc_mrt,tmp2) ;
    clear tmp tmp2 tmp3
    tmp = ~isnan(squeeze(thisFile.allReg_Fp_mrt(1,1,:))) ;
    tmp2 = thisFile.allReg_Fp_mrt(:,:,tmp) ;
    tmp3 = thisFile.exclP_mrt(:,:,tmp) ;
    tmp2(isnan(tmp2) & tmp3) = 0 ;
    allReg_Fp_mrt = cat(3,allReg_Fp_mrt,tmp2) ;
    clear tmp tmp2 tmp3
    tmp = ~isnan(squeeze(thisFile.allReg_Fo_mrt(1,1,:))) ;
    tmp2 = thisFile.allReg_Fo_mrt(:,:,tmp) ;
    tmp3 = thisFile.exclO_mrt(:,:,tmp) ;
    tmp2(isnan(tmp2) & tmp3) = 0 ;
    allReg_Fo_mrt = cat(3,allReg_Fo_mrt,tmp2) ;
    if size(tmp2,3) < thisFile.methods_out.trainRuns
        warning(['This run not complete: Only ' num2str(size(tmp2,3)) ' of ' num2str(thisFile.methods_out.trainRuns) ' runs.'])
    end
    clear tmp tmp2 tmp3
    
    if isfield(thisFile,'toc_allruns')
        if f==1
            show_allruns_time = true ;
            toc_allruns = thisFile.toc_allruns ;
        else
            toc_allruns = toc_allruns + thisFile.toc_allruns ;
        end
    else
        show_allruns_time = false ;
    end
    
    clear thisFile filename
    f = f + 1 ;
end
disp('Done.') ; disp(' ')

if isempty(allReg_Fc_mrt)
    error('Could not find any data.')
end

Nruns = size(allReg_Fc_mrt,3) ;

if show_allruns_time
    disp([num2str(Nruns) ' runs in ' num2str(roundn(toc_allruns/3600,-1)) ' hours of total computing time.']) ; disp(' ')
end

% Deal with runs that don't have methods.ignoreNB
if ~isfield(methods,'ignoreNB')
    if strcmp(methods.normType,'none')
        methods.ignoreNB = 0 ;
    elseif methods.set_neverBurned == -1
        methods.ignoreNB = 1 ;
    elseif methods.set_neverBurned > 0
        methods.ignoreNB = 0 ;
    else
        error('Exception to handling of runs without methods.ignoreNB.')
    end
end


%% (2) Setup

% Working directory and path
cd '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack'
addpath(genpath(pwd))
addpath(genpath('/Users/sam/Geodata/'))
addpath(genpath('/Users/sam/Documents/Dropbox/Projects/FireModel_daily/support'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices_s00 = (years.start - 1999):(years.end - 1999) ;
years.indices_s01 = (years.start - 2000):(years.end - 2000) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start_s00 = (years.start-2000)*12 + 1 ;
months.end_s00 = months.start_s00 + months.num - 1 ;
months.start_s01 = (years.start-2001)*12 + 1 ;
months.end_s01 = months.start_s01 + months.num - 1 ;


%% (3) Import data

% Regions
disp('Loading regions_bsruns...')
load('/Users/sam/Geodata/New_regions_v4/MATLAB_QGIS/regions_v4.mat')
regions_all = regions ;
eval(['regions_bsruns = regions.' methods.regions ' ;']) ;
clear regions
regions_bsruns.map = single(regions_bsruns.map) ;
regions_bsruns.numCells = nan(regions_bsruns.num,1) ;
regions_bsruns.Yarray = repmat(regions_bsruns.map,[1 1 years.num]) ;
regions_bsruns.Marray = repmat(regions_bsruns.Yarray,[1 1 12]) ;

% Month datasets
Marray_month = zeros(720,1440,months.num,'int16') ;
for m = 1:months.num
    Marray_month(:,:,m) = m*ones(720,1440) ;
end ; clear m

% Land covers
[LCfrac_crop,LCfrac_past,~,~,~,~,landarea] = ...
    import_LCdata(methods,years,months,regions_bsruns) ;

% Observed fire
disp('Loading observed fire data...')
if strcmp(methods.fireType,'terra')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('/Users/sam/Documents/Dropbox/Projects/FireModel_globLoc/input/MOD14CMH_2001-2008_ssr.mat')
    obsFire = single(MOD14CMH(:,:,months.start_s01:months.end_s01)) ;
    clear MOD14CMH
elseif strcmp(methods.fireType,'aqua')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
elseif strcmp(methods.fireType,'gfed')
    load('gfed4_BA_highres_01-10_km2.mat')
    obsFire = single(gfed4_BA_highres(:,:,months.start_s01:months.end_s01)) ;
    clear gfed4
elseif strcmp(methods.fireType,'gfed3')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('GFED3.1_2000-2009_BAkm2.mat')
    obsFire = single(gfed31(:,:,months.start_s00:months.end_s00)) ;
    clear gfed31
elseif strcmp(methods.fireType,'randerson_all')
    load('BA_all_highres_01-10.mat')
    obsFire = single(BA_all_highres(:,:,months.start_s01:months.end_s01)) ;
    clear BA_all_lowres
    load('BA_IGBPcroppy.mat','BA_crop','BA_cropPLUSmos')
    obsFire_crop = single(BA_crop(:,:,months.start_s01:months.end_s01)) ;
    obsFire_cropPLUSmos = single(BA_cropPLUSmos(:,:,months.start_s01:months.end_s01)) ;
    clear BA_crop*
    if strcmp(methods.landcover,'fake_3member')
        filename = ['/Users/sam/Geodata/Randerson--SmallFires/MATfiles_v3/RandersonBAall_0110_fake3m_v' num2str(methods.F3Mv) '.mat'] ;
        load(filename) ;
        clear filename
        obsFire_fakeC = RandersonBAall_0110_fakeC(:,:,months.start_s01:months.end_s01) ;
        clear RandersonBAall_0110_fakeC
        obsFire_fakeP = RandersonBAall_0110_fakeP(:,:,months.start_s01:months.end_s01) ;
        clear RandersonBAall_0110_fakeP
        obsFire_fakeO = RandersonBAall_0110_fakeO(:,:,months.start_s01:months.end_s01) ;
        clear RandersonBAall_0110_fakeO
    end
elseif strcmp(methods.fireType,'randerson_all_Cemis')
    load('C_all_highres_01-10.mat')
    obsFire = single(C_all_highres(:,:,months.start_s01:months.end_s01)) ;
    obsFire = obsFire .* (landarea*1e6) ;   % Convert from g/m2/month to g/month
    clear C_all_lowres
else error('methods.fireType improperly specified.')
end
clear BA_*res

% Normalize
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    [~,~,fire_normConstant] = normObs(obsFire,methods) ;
%     [obsFireNorm,~,fire_normConstant] = normObs(obsFire,methods) ;
%     obsFireNorm = single(obsFireNorm) ;
elseif ~strcmp(methods.normType,'none')
    error('methods.normType improperly specified.')
end
obsFire = single(obsFire) ;

% Reconcile NaNs across products.
% Ignores any cells that ever have landarea==0.
bad = ( isnan(regions_bsruns.map) | isnan(mean(LCfrac_crop,3)) | isnan(mean(obsFire,3)) | min(landarea,[],3)==0 ) ;
regions_bsruns.map(bad) = NaN ;
regions_bsruns.Yarray = repmat(regions_bsruns.map,[1 1 years.num]) ;
regions_bsruns.Marray = repmat(regions_bsruns.Yarray,[1 1 12]) ;
bad_array = repmat(bad,[1 1 months.num]) ;
landarea = landarea(~bad_array) ;
LCfrac_crop = LCfrac_crop(~bad_array) ;
LCfrac_past = LCfrac_past(~bad_array) ;
Marray_month = Marray_month(~bad_array) ;
% Marray_trueMonth = Marray_trueMonth(~bad_array) ;
if ~strcmp(methods.normType,'none')
%     obsFireNorm = obsFireNorm(~bad_array) ;
    if methods.normStyle == 1
        fire_normConstant_Marray = repmat(fire_normConstant,[1 1 months.num]) ;
    elseif methods.normStyle == 2
        fire_normConstant_Marray = nan(size(bad_array)) ;
        for y = 1:years.num
            mStart = (y-1)*12 +1 ;
            mEnd = mStart + 11 ;
            fire_normConstant_Marray(:,:,mStart:mEnd) = repmat(fire_normConstant(:,:,y),[1 1 12]) ;
        end
    end
%     clear fire_normConstant_Marray
    % fire_normConstant_orig = fire_normConstant_orig(~bad) ;
else
%     fire_normConstant = [] ;
    fire_normConstant = max(obsFire,[],3) ;
    fire_normConstant_Marray = repmat(fire_normConstant,[1 1 months.num]) ;
end

obsFire = obsFire(~bad_array) ;
fire_normConstant = fire_normConstant_Marray(~bad_array) ;
if strcmp(methods.landcover,'hyde')
    if exist('obsFire_crop','var')
        obsFire_crop = obsFire_crop(~bad_array) ;
    else obsFire_crop = [] ;
    end
    if exist('obsFire_cropPLUSmos','var')
        obsFire_cropPLUSmos = obsFire_cropPLUSmos(~bad_array) ;
    else obsFire_cropPLUSmos = [] ;
    end
elseif strcmp(methods.landcover,'fake_3member')
    if exist('obsFire_fakeC','var')
        obsFire_fakeC = obsFire_fakeC(~bad_array) ;
        obsFire_fakeP = obsFire_fakeP(~bad_array) ;
        obsFire_fakeO = obsFire_fakeO(~bad_array) ;
    else
        obsFire_fakeC = [] ;
        obsFire_fakeP = [] ;
        obsFire_fakeO = [] ;
    end
else error('methods.landcover improperly specified.')
end
clear bad_array

regKey_bsruns = uint16(regions_bsruns.Marray(~isnan(regions_bsruns.Marray))) ;
regions_out.map(bad) = NaN ;
regions_out.Marray = repmat(regions_out.map,[1 1 months.num]) ;
% regKey_out = uint16(regions_out.Marray(~isnan(regions_out.Marray))) ;

for r = 1:regions_bsruns.num
    regions_bsruns.numCells(r) = length(find(regions_bsruns.map==r)) ;
end
    
methods.num_cells = length(find(~isnan(regions_bsruns.map))) ;

% landarea_vectorONE = landarea(~isnan(landarea)) ; error('This means nothing now that landarea is a 720x1440xNmonths array!!!')

disp('Done.') ; disp(' ')


%% (4) One country results
warning('Check and see if you get same results for CI95 when doing ANOTHER bootstrap, where you randomly select one trainRun FOR EACH REGION-MONTH.') ; disp(' ')
clear thisCtry*

% country_name = 'India' ; thisCtry = 106 ;
% country_name = 'Ukraine' ; thisCtry = 232 ;
% country_name = 'USA' ; thisCtry = 233 ;
% country_name = 'Mali' ; thisCtry = 147 ;
country_name = 'All_countries' ; thisCtry = 1 ;
disp(['Country: ' country_name]) ; disp(' ')

% Load countries map
countries_map = single(flipud(imread('/Users/sam/Geodata/New_regions_v4/GeoTIFFs--QuartDeg/countries_QD.tif','TIF'))) ;
countries_map(isnan(regions_bsruns.map)) = NaN ;
if strcmp(country_name,'All_countries')
    countries_map(~isnan(countries_map)) = 1 ;
end

% Which gridcells are in this country?
inThisCtry_map_1mth = false(size(regions_bsruns.map)) ;
inThisCtry_map_1mth(countries_map==thisCtry) = true ;
inThisCtry_1mth_x = inThisCtry_map_1mth(~isnan(regions_bsruns.map)) ;
thisCtry_Ncells_1mth = length(inThisCtry_1mth_x) ;
% inThisCtry_map_Marray = repmat(inThisCtry_map_1mth,[1 1 months.num]) ;

% Check to see which region(s) this country is in
thisCtry_whichReg = unique(regions_bsruns.map(countries_map==thisCtry)) ;
if length(thisCtry_whichReg) > 1 && ~strcmp(country_name,'All_countries')
    warning('This country falls in >1 region. If that''s not intended, you should update the regions map used for analysis.')
end

% Get LCdata and obsFire for this country
clear obsFire_perLC
if strcmp(methods.landcover,'hyde')
    obsFire_perLC{1} = obsFire_crop ;
    obsFire_perLC{2} = obsFire_cropPLUSmos ;
elseif strcmp(methods.landcover,'fake_3member')
    obsFire_perLC{1} = obsFire_fakeC ;
    obsFire_perLC{2} = obsFire_fakeP ;
    obsFire_perLC{3} = obsFire_fakeO ;
end
[thisCtry_LCdata_crop_mr,thisCtry_LCdata_past_mr, ...
 thisCtry_LCdata_othr_mr,thisCtry_obsFireRAW_total_myr, ...
 thisCtry_obsFireRAW_total_my, thisCtry_obsFireRAW_total, ...
 thisCtry_obsFireRAW_perLC, ...
 thisCtry_obsFireRAW_perLC_my, thisCtry_obsFire_perLC_myr, ...
 thisCtry_LCdata_crop, thisCtry_LCdata_past, thisCtry_LCdata_othr] = ...
    get_thisCtry_LCdataCPO_obsFire_v2(thisCtry, thisCtry_whichReg, ...
                           LCfrac_crop, LCfrac_past, ...
                           obsFire, regions_bsruns, ...
                           months, years, methods, regKey_bsruns, ...
                           landarea, countries_map, ...
                           obsFire_perLC, ...
                           fire_normConstant) ;
if strcmp(methods.landcover,'hyde')
    thisCtry_obsFireRAW_crop_my = thisCtry_obsFireRAW_perLC_my{1} ;
    thisCtry_obsFireRAW_cropPLUSmos_my = thisCtry_obsFireRAW_perLC_my{2} ;
elseif strcmp(methods.landcover,'fake_3member')
    thisCtry_obsFireRAW_fakeC = thisCtry_obsFireRAW_perLC{1} ;
    thisCtry_obsFireRAW_fakeP = thisCtry_obsFireRAW_perLC{2} ;
    thisCtry_obsFireRAW_fakeO = thisCtry_obsFireRAW_perLC{3} ;
    clear thisCtry_obsFireRAW_perLC
    thisCtry_obsFireRAW_fakeC_my = thisCtry_obsFireRAW_perLC_my{1} ;
    thisCtry_obsFireRAW_fakeP_my = thisCtry_obsFireRAW_perLC_my{2} ;
    thisCtry_obsFireRAW_fakeO_my = thisCtry_obsFireRAW_perLC_my{3} ;
    clear thisCtry_obsFireRAW_perLC_my
    thisCtry_obsFireRAW_fakeC_myr = thisCtry_obsFire_perLC_myr{1} ;
    thisCtry_obsFireRAW_fakeP_myr = thisCtry_obsFire_perLC_myr{2} ;
    thisCtry_obsFireRAW_fakeO_myr = thisCtry_obsFire_perLC_myr{3} ;
    clear thisCtry_obsFire_perLC_myr
end
clear thisCtry_obsFireRAW_perLC_my
                       
% Get all Fk estimates for this country
thisCtry_Fc_mrt = allReg_Fc_mrt(:,thisCtry_whichReg,:) ;
thisCtry_Fp_mrt = allReg_Fp_mrt(:,thisCtry_whichReg,:) ;
thisCtry_Fo_mrt = allReg_Fo_mrt(:,thisCtry_whichReg,:) ;

% Calculate estimated BA for each bootstrapping run
[thisCtry_estFireRAW_crop_myt,thisCtry_estFireRAW_past_myt, ...
 thisCtry_estFireRAW_othr_myt,thisCtry_estFireRAW_total_myt, ...
 thisCtry_estFireRAW_crop_myrt,thisCtry_estFireRAW_past_myrt, ...
 thisCtry_estFireRAW_othr_myrt,thisCtry_estFireRAW_total_myrt] = ...
    get_thisCtry_estFireCPO(months,years,methods, ...
                            thisCtry_Fc_mrt,thisCtry_Fp_mrt,thisCtry_Fo_mrt, ...
                            thisCtry_LCdata_crop_mr,thisCtry_LCdata_past_mr,thisCtry_LCdata_othr_mr) ;
                        
% Convert FIRE from raw units to nicer ones
if strcmp(methods.fireType,'randerson_all')
    % km2 to Mha
    fire_units = 'Mha' ;
    convFactor = 1e-4 ;
elseif strcmp(methods.fireType,'randerson_Cemis') || strcmp(methods.fireType,'randerson_all_Cemis')
    % gC to TgC (=Mt)
    fire_units = 'Tg (=Mt)' ;
    convFactor = 1e-12 ;
end
thisCtry_estFire_crop_myt = thisCtry_estFireRAW_crop_myt * convFactor ;
thisCtry_estFire_past_myt = thisCtry_estFireRAW_past_myt * convFactor ;
thisCtry_estFire_othr_myt = thisCtry_estFireRAW_othr_myt * convFactor ;
thisCtry_estFire_total_myt = thisCtry_estFireRAW_total_myt * convFactor ;
thisCtry_estFire_crop_myrt = thisCtry_estFireRAW_crop_myrt * convFactor ;
thisCtry_estFire_past_myrt = thisCtry_estFireRAW_past_myrt * convFactor ;
thisCtry_estFire_othr_myrt = thisCtry_estFireRAW_othr_myrt * convFactor ;
thisCtry_estFire_total_myrt = thisCtry_estFireRAW_total_myrt * convFactor ;
thisCtry_obsFire_total = thisCtry_obsFireRAW_total * convFactor ;
thisCtry_obsFire_total_my = thisCtry_obsFireRAW_total_my * convFactor ;
thisCtry_obsFire_total_myr = thisCtry_obsFireRAW_total_myr * convFactor ;

% Convert OBS. LAND COVER FIRE from raw units to nicer ones
if strcmp(fire_units,'Mha')
    if strcmp(methods.landcover,'hyde')
        thisCtry_obsFire_crop_my = thisCtry_obsFireRAW_crop_my * convFactor ;
        thisCtry_obsFire_cropPLUSmos_my = thisCtry_obsFireRAW_cropPLUSmos_my * convFactor ;
    elseif strcmp(methods.landcover,'fake_3member')
        thisCtry_obsFire_fakeC = thisCtry_obsFireRAW_fakeC * convFactor ;
        thisCtry_obsFire_fakeP = thisCtry_obsFireRAW_fakeP * convFactor ;
        thisCtry_obsFire_fakeO = thisCtry_obsFireRAW_fakeO * convFactor ;
        thisCtry_obsFire_fakeC_my = thisCtry_obsFireRAW_fakeC_my * convFactor ;
        thisCtry_obsFire_fakeP_my = thisCtry_obsFireRAW_fakeP_my * convFactor ;
        thisCtry_obsFire_fakeO_my = thisCtry_obsFireRAW_fakeO_my * convFactor ;
        thisCtry_obsFire_fakeC_myr = thisCtry_obsFireRAW_fakeC_myr * convFactor ;
        thisCtry_obsFire_fakeP_myr = thisCtry_obsFireRAW_fakeP_myr * convFactor ;
        thisCtry_obsFire_fakeO_myr = thisCtry_obsFireRAW_fakeO_myr * convFactor ;
    end
elseif ~strcmp(fire_units,'Tg (=Mt)')
    error('"To" units improperly specified.')
end

% clear thisCtry_*FireRAW_*_myt
clear thisCtry_*FireRAW_*

% Calculate and display annual numbers (mean + 95% CI from percentiles)
if exist('thisCtry_obsFire_fakeC_my','var')
    thisCtry_obsFire_perLC_my{1} = thisCtry_obsFire_fakeC_my ;
    thisCtry_obsFire_perLC_my{2} = thisCtry_obsFire_fakeP_my ;
    thisCtry_obsFire_perLC_my{3} = thisCtry_obsFire_fakeO_my ;
else thisCtry_obsFire_perLC_my = [] ;
end
CI_percentile = 95 ;
[thisCtry_estFire_Ann_CIloMeanCIhi_NET, ...
 thisCtry_estFire_Ann_CIloMeanCIhi_POS, ...
 thisCtry_estFire_Ann_CIloMeanCIhi_NEG] = ...
    CPObs__calc_estFireAnn_CIloMeanCIhi(...
        thisCtry_estFire_crop_myt, thisCtry_estFire_past_myt, ...
        thisCtry_estFire_othr_myt, thisCtry_estFire_total_myt, ...
        thisCtry_obsFire_total_my, ...
        CI_percentile, fire_units, country_name, methods, ...
        thisCtry_obsFire_perLC_my) ;
    

%% (5) Stop 
error('Stopping here, now that one-country results are done.')
disp('asdf')

%% One country: Make maps of mean annual estimated fire

% Get by-cell Fk's
if ~exist('thisCtry_Fc_rm_mean','var')
    disp('Getting by-cell Fk''s...')
    thisCtry_Fc_rm_mean = permute(mean(thisCtry_Fc_mrt,3),[2 1]) ;
    thisCtry_Fp_rm_mean = permute(mean(thisCtry_Fp_mrt,3),[2 1]) ;
    thisCtry_Fo_rm_mean = permute(mean(thisCtry_Fo_mrt,3),[2 1]) ;
    if strcmp(country_name,'All_countries')
        regKey_xm = reshape(regKey_bsruns,[thisCtry_Ncells_1mth months.num]) ;
        thisCtry_Fc_xm = nan(size(regKey_xm),'single') ;
        thisCtry_Fp_xm = nan(size(regKey_xm),'single') ;
        thisCtry_Fo_xm = nan(size(regKey_xm),'single') ;
        for r = 1:length(thisCtry_whichReg)
            disp(num2str(r))
            thisReg = thisCtry_whichReg(r) ;
            inThisReg = (regKey_xm==thisReg) ;
            inThisReg_num = sum(inThisReg(:,1)) ;
            thisCtry_Fc_xm(inThisReg) = repmat(thisCtry_Fc_rm_mean(r,:),[inThisReg_num 1]) ;
            thisCtry_Fp_xm(inThisReg) = repmat(thisCtry_Fp_rm_mean(r,:),[inThisReg_num 1]) ;
            thisCtry_Fo_xm(inThisReg) = repmat(thisCtry_Fo_rm_mean(r,:),[inThisReg_num 1]) ;
            clear *hisReg*
        end ; clear r
    else
    end
end

% Get by-cell estFire
disp('Getting by-cell estFire...')
thisCtry_LCdata_crop_xm = reshape(thisCtry_LCdata_crop,[thisCtry_Ncells_1mth months.num]) ;
thisCtry_LCdata_past_xm = reshape(thisCtry_LCdata_past,[thisCtry_Ncells_1mth months.num]) ;
thisCtry_LCdata_othr_xm = reshape(thisCtry_LCdata_othr,[thisCtry_Ncells_1mth months.num]) ;
thisCtry_estFire_crop_xm = thisCtry_Fc_xm .* thisCtry_LCdata_crop_xm * convFactor ;
thisCtry_estFire_past_xm = thisCtry_Fp_xm .* thisCtry_LCdata_past_xm * convFactor ;
thisCtry_estFire_othr_xm = thisCtry_Fo_xm .* thisCtry_LCdata_othr_xm * convFactor ;
thisCtry_estFire_crop_xmy = reshape(thisCtry_estFire_crop_xm(:),[thisCtry_Ncells_1mth 12 years.num]) ;
thisCtry_estFire_past_xmy = reshape(thisCtry_estFire_past_xm(:),[thisCtry_Ncells_1mth 12 years.num]) ;
thisCtry_estFire_othr_xmy = reshape(thisCtry_estFire_othr_xm(:),[thisCtry_Ncells_1mth 12 years.num]) ;

% Make maps of mean annual ESTIMATED burning
disp('Making and saving maps of mean annual burning...')
LCtypes_list_in = {'crop','past','othr'} ;
LCtypes_list_in2 = {'C','P','O'} ;
if strcmp(methods.landcover,'hyde')
    LCtypes_list_out = {'CROP','PAST','OTHR'} ;
elseif strcmp(methods.landcover,'fake_3member')
    LCtypes_list_out = {'FakeC','FakeP','FakeO'} ;
end
if ~exist('landarea_xmy','var')
    landarea_xmy = reshape(landarea,[thisCtry_Ncells_1mth 12 years.num]) ;
end
yrStr_1 = num2str(years.start) ;
yrStr_2 = num2str(years.end) ;
yrStr = [yrStr_1(3:4) yrStr_2(3:4)] ;
clear yrStr_*

% For each land cover
for c = 1:3
    
    eval(['LCarea_thisLC_xmy = reshape(LCfrac_' LCtypes_list_in{c} ',[thisCtry_Ncells_1mth 12 years.num]) ;']) ;
    
    % This LC: Estimated
    %%% Absolute
    eval(['thisCtry_estFire_thisLC_xmy = thisCtry_estFire_' LCtypes_list_in{c} '_xmy ;']) ;
    thisCtry_estFire_thisLC_xmy = thisCtry_estFire_thisLC_xmy / convFactor ;
    thisCtry_meanAnn_estFire_thisLC = mean(sum(thisCtry_estFire_thisLC_xmy,2),3) ;
    thisCtry_meanAnn_estFire_thisLC_map = nan(size(countries_map)) ;
    thisCtry_meanAnn_estFire_thisLC_map(inThisCtry_map_1mth) = thisCtry_meanAnn_estFire_thisLC ;
    filename = [outDir_geo 'meanAnnEst_' LCtypes_list_out{c} '_' prefix '.tif'] ;
    geotiffwrite(filename,flipud(thisCtry_meanAnn_estFire_thisLC_map),quartdeg_R) ;
    %%% Fractional
    thisCtry_estFireFrac_thisLC_xmy = thisCtry_estFire_thisLC_xmy ./ LCarea_thisLC_xmy ;
    thisCtry_estFireFrac_thisLC_xmy(LCarea_thisLC_xmy==0) = 0 ;
    thisCtry_meanAnn_estFireFrac_thisLC = mean(sum(thisCtry_estFireFrac_thisLC_xmy,2),3) ;
    thisCtry_meanAnn_estFireFrac_thisLC_map = nan(size(countries_map)) ;
    thisCtry_meanAnn_estFireFrac_thisLC_map(inThisCtry_map_1mth) = thisCtry_meanAnn_estFireFrac_thisLC ;
    filename = [outDir_geo 'meanAnnEstFrac_' LCtypes_list_out{c} '_' prefix '.tif'] ;
    geotiffwrite(filename,flipud(thisCtry_meanAnn_estFireFrac_thisLC_map),quartdeg_R) ;
    
    % For fake3member runs...
    if exist('thisCtry_obsFire_fakeC','var')
        
        % This LC: Observed
        eval(['thisCtry_obsFire_thisLC_xmy = reshape(thisCtry_obsFire_fake' LCtypes_list_in2{c} ',[thisCtry_Ncells_1mth 12 years.num]) ;']) ;
        thisCtry_obsFire_thisLC_xmy = thisCtry_obsFire_thisLC_xmy / convFactor ;
        %%% Absolute
        thisCtry_meanAnn_obsFire_thisLC = mean(sum(thisCtry_obsFire_thisLC_xmy,2),3) ;
        thisCtry_meanAnn_obsFire_thisLC_map = nan(size(countries_map)) ;
        thisCtry_meanAnn_obsFire_thisLC_map(inThisCtry_map_1mth) = thisCtry_meanAnn_obsFire_thisLC ;
        filename = [outDir_geo 'meanAnnObs_' LCtypes_list_out{c} 'v' num2str(methods.F3Mv) '_' yrStr '.tif'] ;
        geotiffwrite(filename,flipud(thisCtry_meanAnn_obsFire_thisLC_map),quartdeg_R) ;
        %%% Fractional
        thisCtry_obsFireFrac_thisLC_xmy = thisCtry_obsFire_thisLC_xmy ./ LCarea_thisLC_xmy ;
        thisCtry_obsFireFrac_thisLC_xmy(LCarea_thisLC_xmy==0) = 0 ;
        thisCtry_meanAnn_obsFireFrac_thisLC = mean(sum(thisCtry_obsFireFrac_thisLC_xmy,2),3) ;
        thisCtry_meanAnn_obsFireFrac_thisLC_map = nan(size(countries_map)) ;
        thisCtry_meanAnn_obsFireFrac_thisLC_map(inThisCtry_map_1mth) = thisCtry_meanAnn_obsFireFrac_thisLC ;
        filename = [outDir_geo 'meanAnnObsFrac_' LCtypes_list_out{c} 'v' num2str(methods.F3Mv) '_' yrStr '.tif'] ;
        geotiffwrite(filename,flipud(thisCtry_meanAnn_obsFireFrac_thisLC_map),quartdeg_R) ;
        
        % This LC: Error
        thisCtry_meanAnn_errAmt_thisLC_map = thisCtry_meanAnn_estFire_thisLC_map - thisCtry_meanAnn_obsFire_thisLC_map ;
        filename = [outDir_geo 'meanAnnErrAmt_' LCtypes_list_out{c} '_' prefix '.tif'] ;
        geotiffwrite(filename,flipud(thisCtry_meanAnn_errAmt_thisLC_map),quartdeg_R) ;
        thisCtry_meanAnn_errPct_thisLC_map = 100 * (thisCtry_meanAnn_errAmt_thisLC_map ./ thisCtry_meanAnn_obsFire_thisLC_map) ;
        filename = [outDir_geo 'meanAnnErrPct_' LCtypes_list_out{c} '_' prefix '.tif'] ;
        geotiffwrite(filename,flipud(thisCtry_meanAnn_errPct_thisLC_map),quartdeg_R) ;
        
    end
    
    clear filename *thisLC*
end ; clear c

% Total: Estimated
%%% Absolute
thisCtry_meanAnn_estFire_total = mean(sum(thisCtry_estFire_total_xmy,2),3) ;
thisCtry_meanAnn_estFire_total = thisCtry_meanAnn_estFire_total / convFactor ;
thisCtry_meanAnn_estFire_total_map = nan(size(countries_map)) ;
thisCtry_meanAnn_estFire_total_map(inThisCtry_map_1mth) = thisCtry_meanAnn_estFire_total ;
filename = [outDir_geo 'meanAnnEstTOTAL_' prefix '.tif'] ;
geotiffwrite(filename,flipud(thisCtry_meanAnn_estFire_total_map),quartdeg_R) ;
%%% Fractional
thisCtry_meanAnn_estFireFrac_total = mean(sum(thisCtry_estFire_total_xmy./landarea_xmy,2),3) ;
thisCtry_meanAnn_estFireFrac_total_map = nan(size(countries_map)) ;
thisCtry_meanAnn_estFireFrac_total_map(inThisCtry_map_1mth) = thisCtry_meanAnn_estFireFrac_total ;
filename = [outDir_geo 'meanAnnEstFracTOTAL_' prefix '.tif'] ;
geotiffwrite(filename,flipud(thisCtry_meanAnn_estFireFrac_total_map),quartdeg_R) ;

% Total: Observed 
%%% Absolute
thisCtry_obsFire_total_xmy = reshape(thisCtry_obsFire_total,[thisCtry_Ncells_1mth 12 years.num]) ;
thisCtry_meanAnn_obsFire_total = mean(sum(thisCtry_obsFire_total_xmy,2),3) ;
thisCtry_meanAnn_obsFire_total = thisCtry_meanAnn_obsFire_total / convFactor ;
thisCtry_meanAnn_obsFire_total_map = nan(size(countries_map)) ;
thisCtry_meanAnn_obsFire_total_map(inThisCtry_map_1mth) = thisCtry_meanAnn_obsFire_total ;
filename = [outDir_geo 'meanAnnObsTOTAL_' yrStr '.tif'] ;
geotiffwrite(filename,flipud(thisCtry_meanAnn_obsFire_total_map),quartdeg_R) ;
%%% Fractional
thisCtry_meanAnn_obsFireFrac_total = mean(sum(thisCtry_obsFire_total_xmy./landarea_xmy,2),3) ;
thisCtry_meanAnn_obsFireFrac_total_map = nan(size(countries_map)) ;
thisCtry_meanAnn_obsFireFrac_total_map(inThisCtry_map_1mth) = thisCtry_meanAnn_obsFireFrac_total ;
filename = [outDir_geo 'meanAnnObsFracTOTAL_' prefix '.tif'] ;
geotiffwrite(filename,flipud(thisCtry_meanAnn_obsFireFrac_total_map),quartdeg_R) ;

% Total: Error
thisCtry_meanAnn_errFireAmt_total_map = thisCtry_meanAnn_estFire_total_map - thisCtry_meanAnn_obsFire_total_map ;
thisCtry_meanAnn_errFirePct_total_map = thisCtry_meanAnn_errFireAmt_total_map ./ thisCtry_meanAnn_obsFire_total_map ;

clear filename thisMean thisMap
disp('Done.')


%% One country: Make maps of mean TOTAL fire season peak, and error

if ~exist('thisCtry_estFire_crop_xmy','var')
    error('Run code generating estimated fire first!')
end

yrStr_1 = num2str(years.start) ;
yrStr_2 = num2str(years.end) ;
yrStr = [yrStr_1(3:4) yrStr_2(3:4)] ;

% Get peak observed and estimated fire months for mean year
thisCtry_estFire_total_xmy = thisCtry_estFire_crop_xmy + ...
                             thisCtry_estFire_past_xmy + ...
                             thisCtry_estFire_othr_xmy ;
thisCtry_estFire_total_x_monthMean = mean(thisCtry_estFire_total_xmy,3) ;
[~, thisCtry_estFirePeak_total] = max(thisCtry_estFire_total_x_monthMean,[],2) ;
thisCtry_obsFire_total_xmy = reshape(thisCtry_obsFire_total,[thisCtry_Ncells_1mth 12 years.num]) ;
thisCtry_obsFire_total_x_monthMean = mean(thisCtry_obsFire_total_xmy,3) ;
[~, thisCtry_obsFirePeak_total] = max(thisCtry_obsFire_total_x_monthMean,[],2) ;
thisCtry_errFirePeak_total = thisCtry_estFirePeak_total - thisCtry_obsFirePeak_total ;
thisCtry_errFirePeak_total(thisCtry_errFirePeak_total>6) = -12 + thisCtry_errFirePeak_total(thisCtry_errFirePeak_total>6);
thisCtry_errFirePeak_total(thisCtry_errFirePeak_total<=-6) = 12 + thisCtry_errFirePeak_total(thisCtry_errFirePeak_total<=-6) ;

% Make maps
thisCtry_estFirePeak_map = nan(size(countries_map)) ;
thisCtry_obsFirePeak_map = nan(size(countries_map)) ;
thisCtry_errFirePeak_map = nan(size(countries_map)) ;
thisCtry_estFirePeak_map(countries_map==thisCtry) = thisCtry_estFirePeak_total ;
thisCtry_obsFirePeak_map(countries_map==thisCtry) = thisCtry_obsFirePeak_total ;
thisCtry_errFirePeak_map(countries_map==thisCtry) = thisCtry_errFirePeak_total ;

% Save maps
filename = [outDir_geo 'meanPeakMonth_obsFire' yrStr '.tif'] ;
geotiffwrite(filename,flipud(thisCtry_obsFirePeak_map),quartdeg_R) ;
filename = [outDir_geo 'meanPeakMonth_estFire' prefix '_' yrStr '.tif'] ;
geotiffwrite(filename,flipud(thisCtry_estFirePeak_map),quartdeg_R) ;
filename = [outDir_geo 'meanPeakMonth_errFire' prefix '_' yrStr '.tif'] ;
geotiffwrite(filename,flipud(thisCtry_errFirePeak_map),quartdeg_R) ;

shademap(thisCtry_errFirePeak_map) ;

%% Mean observations for whole planet -- Only do this for "All countries"!

if ~strcmp(country_name,'All_countries')
    error('Only do this for all countries, stupid.')
end

% Setup
yrStr_1 = num2str(years.start) ;
yrStr_2 = num2str(years.end) ;
yrStr = [yrStr_1(3:4) yrStr_2(3:4)] ;
global_Ncells = length(find(~isnan(regions_bsruns.map))) ;
LCtypes_list_in = {'crop','past','othr'} ;
if strcmp(methods.landcover,'hyde')
    LCtypes_list_out = {'CROP','PAST','OTHR'} ;
elseif strcmp(methods.landcover,'fake_3member')
    LCtypes_list_out = {'fakeC','fakeP','fakeO'} ;
end
if ~exist('landarea_xmy','var')
    landarea_xmy = reshape(landarea,[thisCtry_Ncells_1mth 12 years.num]) ;
end

% Make maps of mean land covers
LCfrac_crop_mean = mean(reshape(LCfrac_crop,[global_Ncells months.num]),2) ;
LCfrac_past_mean = mean(reshape(LCfrac_past,[global_Ncells months.num]),2) ;
LCfrac_othr_mean = mean(ones(size(LCfrac_crop_mean)) - (LCfrac_crop_mean + LCfrac_past_mean),2) ;
for c = 1:3
    filename = [outDir_geo 'meanLCfrac_' LCtypes_list_out{c} '_' yrStr '.tif'] ;
    thisMap = nan(size(regions_bsruns.map)) ;
    eval(['thisMap(~isnan(regions_bsruns.map)) = LCfrac_' LCtypes_list_in{c} '_mean ;']) ;
    geotiffwrite(filename,flipud(thisMap),quartdeg_R) ;
    clear filename thisMap
end

% Make maps of mean annual OBSERVED burning
filename = [outDir_geo 'meanAnnObsTOTAL_' methods.fireType '_' yrStr '.tif'] ;
tmp0 = reshape(thisCtry_obsFire_total,[thisCtry_Ncells_1mth 12 years.num]) ;
tmp0 = tmp0 / convFactor ;
tmp1 = mean(sum(tmp0,2),3) ;
thisMap = nan(size(regions_bsruns.map)) ;
thisMap(inThisCtry_map_1mth) = tmp1 ;
geotiffwrite(filename,flipud(thisMap),quartdeg_R) ;
filename = [outDir_geo 'meanAnnObsFracTOTAL_' methods.fireType '_' yrStr '.tif'] ;
tmp2 = tmp0 ./ landarea_xmy ;
tmp3 = mean(sum(tmp2,2),3) ;
thisMap = nan(size(regions_bsruns.map)) ;
thisMap(inThisCtry_map_1mth) = tmp3 ;
geotiffwrite(filename,flipud(thisMap),quartdeg_R) ;
clear filename tmp* thisMap
if strcmp(methods.landcover,'fake_3member')
    LCtypes_list_in2 = {'C','P','O'} ;
    % Calculate LCfrac_othr, correcting for rounding errors
    LCfrac_othr = 1 - (LCfrac_crop + LCfrac_past) ;
    if ~isempty(find(LCfrac_othr<0,1))
        if ~isempty(find(LCfrac_othr<-1e-6,1))
            error('Something went wrong in calculating LCfrac_othr -- you have nontrivial negative values.')
        else
            LCfrac_othr(LCfrac_othr<0 & LCfrac_othr>-1e-6) = 0 ;
        end
    end
    for c = 1:3
        eval(['tmp_xmy = reshape(thisCtry_obsFire_fake' LCtypes_list_in2{c} ',[thisCtry_Ncells_1mth 12 years.num]) ;']) ;
        tmp_xmy = tmp_xmy / convFactor ;
        eval(['tmpLC_xmy = landarea_xmy .* reshape(LCfrac_' LCtypes_list_in{c} ',[thisCtry_Ncells_1mth 12 years.num]) ;']) ;
        
        % Total
        filename = [outDir_geo 'meanAnnObs' LCtypes_list_out{c} 'v' num2str(methods.F3Mv) '_' yrStr '.tif'] ;
        tmp = mean(sum(tmp_xmy,2),3) ;
        thisMap = nan(size(regions_bsruns.map)) ;
        thisMap(inThisCtry_map_1mth) = tmp ;
        geotiffwrite(filename,flipud(thisMap),quartdeg_R) ;
        clear tmp
        
        % Fractional
        filename = [outDir_geo 'meanAnnObsFrac' LCtypes_list_out{c} 'v' num2str(methods.F3Mv) '_' yrStr '.tif'] ;
        tmp = tmp_xmy ./ tmpLC_xmy ;
        tmp = mean(sum(tmp,2),3) ;
        thisMap = nan(size(regions_bsruns.map)) ;
        thisMap(inThisCtry_map_1mth) = tmp ;
        geotiffwrite(filename,flipud(thisMap),quartdeg_R) ;
        clear tmp*_xmy tmp thisMap filename
        
    end ; clear c landarea_xmy
end

clear LCtypes_list_* yrStr




%% One country: Look at mean annual fire error for each LC

if ~strcmp(methods.landcover,'fake_3member')
    error('Can''t do this except for fake_3member runs.')
end

[estFire_meanAnn_rc, estFire_SdAnn_rc, ...
 estFire_CIlo_rc, estFire_CIhi_rc, ...
 obsFire_meanAnn_rc, obsFire_SdAnn_rc, ...
 obsFire_CIlo_rc, obsFire_CIhi_rc, ...
 difFire_meanAnn_rc, difFire_SdAnn_rc, ...
 difFire_CIlo_rc, difFire_CIhi_rc] = ...
       CPObs__calc_regAndCov_estFire_annMeanSD_v1( ...
            thisCtry_estFire_crop_myrt, ...
            thisCtry_estFire_past_myrt, ...
            thisCtry_estFire_othr_myrt, ...
            thisCtry_obsFire_fakeC_myr, ...
            thisCtry_obsFire_fakeP_myr, ...
            thisCtry_obsFire_fakeO_myr, ...
            CI_percentile) ;

figure
set(gcf,'Position',[1 47 1440 759])
for c = 1:3
    subplot(3,1,c)
    h = bar(difFire_meanAnn_rc(:,c)) ;
    hold on
    for r = 1:size(difFire_meanAnn_rc,1)
        plot([r r],[difFire_CIlo_rc(r,c) difFire_CIhi_rc(r,c)],'-k') ;
    end ; clear r
    set(gca,'XTick',min(thisCtry_whichReg):max(thisCtry_whichReg))
    set(gca,'XTickLabel',regions_bsruns.names(thisCtry_whichReg))
    rotateXLabels(gca,45)
    hold off
end


%% One country: Look at mean annual total fire error

[estFire_meanAnn_r, ...
          estFire_SdAnn_r, ...
          estFire_CIlo_r, ...
          estFire_CIhi_r, ...
          obsFire_meanAnn_r, ...
          obsFire_SdAnn_r, ...
          obsFire_CIlo_r, ...
          obsFire_CIhi_r, ...
          difFire_meanAnn_r, ...
          difFire_SdAnn_r, ...
          difFire_CIlo_r, ...
          difFire_CIhi_r] = ...
       CPObs__calc_regional_estFire_annMeanSD_v1( ...
            thisCtry_estFire_crop_myrt, ...
            thisCtry_estFire_past_myrt, ...
            thisCtry_estFire_othr_myrt, ...
            thisCtry_obsFire_total_myr, ...
            CI_percentile) ;
        
h = bar(difFire_meanAnn_r) ;
set(h,'FaceColor',[0.5 0.5 1])
hold on
for r = 1:length(difFire_meanAnn_r)
    plot([r r],[difFire_CIlo_r(r) difFire_CIhi_r(r)],'-k') ;
end ; clear r
hold off


%% One country: Make positive/negative/total bars with errors

bar_data = [thisCtry_estFire_Ann_CIloMeanCIhi_POS(1:3,2) ;
            thisCtry_estFire_Ann_CIloMeanCIhi_NEG(1:3,2) ;
            thisCtry_estFire_Ann_CIloMeanCIhi_NET(1:3,2)] ;
bar_CIWlo = bar_data - ...
            [thisCtry_estFire_Ann_CIloMeanCIhi_POS(1:3,1) ;
            thisCtry_estFire_Ann_CIloMeanCIhi_NEG(1:3,1) ;
            thisCtry_estFire_Ann_CIloMeanCIhi_NET(1:3,1)] ;
bar_CIWhi = [thisCtry_estFire_Ann_CIloMeanCIhi_POS(1:3,3) ;
            thisCtry_estFire_Ann_CIloMeanCIhi_NEG(1:3,3) ;
            thisCtry_estFire_Ann_CIloMeanCIhi_NET(1:3,3)] ...
            - bar_data ;
        
cmap = [155 187 89 ;
        146 122 181 ;
        250 168 70  ] / 255 ;
cmap = repmat(cmap,[3 1]) ;
        
fh = figure ;
ah = axes('parent', fh) ;
hold(ah,'on')
for i = 1:size(bar_data,1)
    bar(i,bar_data(i),'parent', ah, 'facecolor', cmap(i,:)) ;
    errorbar(i,bar_data(i),bar_CIWlo(i),bar_CIWhi(i),'k')
end
hold off

set(gca,'XTick', [2 5 8], ...
        'XTickLabel',{'Positive','Negative','Net'})

clear bar_* fh ah cmap


%% One country: Make CPOTeTo seasonality plot

% include_CPOTeTo = [1 1 1 1 1] ;
include_CPOTeTo = [0 1 1 0 0] ;
% normalize = 0 ;
normalize = 1 ;

CPObs__plotSeas_CPOTeTo( ...
                    thisCtry_estFire_crop_myt, ...
                    thisCtry_estFire_past_myt, ...
                    thisCtry_estFire_othr_myt, ...
                    thisCtry_estFire_total_myt, ...
                    thisCtry_obsFire_total_my, ...
                    include_CPOTeTo, ...
                    normalize) ;

                
%% One country: Make whole-timeseries plot

include_CPOTeTo = [1 1 1 1 1] ;
% include_CPOTeTo = [0 1 1 0 0] ;
normalize = 0 ;
% normalize = 1 ;
indiv_months = 0 ;      
% indiv_months = 1 ;

CPObs__plotTS_CPOTeTo( ...
                thisCtry_estFire_crop_myt, ...
                thisCtry_estFire_past_myt, ...
                thisCtry_estFire_othr_myt, ...
                thisCtry_estFire_total_myt, ...
                thisCtry_obsFire_total_my, ...
                years, ...
                include_CPOTeTo, normalize, indiv_months)


%% One country: Compare seasonality of estimated cropland fire to observed

normalize = 0 ;
% normalize = 1 ;

CPObs__plotSeas_CeCoCMo( ...
                    thisCtry_estFire_crop_myt, ...
                    thisCtry_obsFire_crop_my, ...
                    thisCtry_obsFire_cropPLUSmos_my, ...
                    normalize)
                
                
%% One country: Make maps of mean annual burned area, POSITIVE COMPONENT

do_log10 = false ;
% do_log10 = true ;
do_save_Gtiff = true ;
% do_save = false ;
do_fracLC = false ;
% do_fracLC = true ;

[mapPOS_CROP,mapPOS_PAST,mapPOS_OTHR,mapPOS_TOTAL,map_totalOBS] = ...
    CPObs__mapFire_avgAnn( ...
            thisCtry_Fc_mrt,thisCtry_Fp_mrt,thisCtry_Fo_mrt, ...
            thisCtry_estFireRAW_crop_mrt,thisCtry_estFireRAW_past_mrt,thisCtry_estFireRAW_othr_mrt,...
            thisCtry_obsFireRAW, ...
            LCfrac_crop, LCfrac_past, fire_normConstant, ...
            countries_map, landarea, methods, thisCtry, country_name, regKey_bsruns, ...
            do_log10,do_fracLC) ;

if do_save_Gtiff
    if ~exist('quartdeg_R','var')
        load('/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack/Input/quartdeg_R.mat') ;
    end
    if do_log10
        log10char = 'Log10' ;
    else log10char = '' ;
    end
    if do_fracLC
        doFracLCchar = 'PerKm2' ;
    else doFracLCchar = '' ;
    end
    lc_list = {'CROP','PAST','OTHR','TOTAL'} ;
    for c = 1:length(lc_list)
        thisLC = lc_list{c} ;
        disp(['Saving map of NET average annual fire: ' thisLC '...'])
        thisMap = ['mapPOS_' thisLC] ;
        eval(['thisVar = ' thisMap ' ;']) ;
        filename = [outDir thisMap '_annAvg' doFracLCchar log10char '_' country_name '_' prefix '.tif'] ;
        if ~isempty(dir(filename))
            warning([filename ' exists already; skipping.'])
        end
        thisVar = flipud(thisVar) ;
        geotiffwrite(filename,thisVar,quartdeg_R)
    end ; clear c thisLC thisMap thisVar filename lc_list
    filename_obs = [outDir 'obsFire_annAvg' log10char '_' country_name '_' prefix '.tif'] ;
    if ~exist(filename_obs,'file')
        disp('Saving map of total average annual observed fire...')
        geotiffwrite(filename_obs,flipud(map_totalOBS),quartdeg_R)
    end
    disp('Done.')
    clear filename_obs log10char
end



