function [thisCtry_estFire_annMean_rc, ...
    thisCtry_estFire_annCIlo_rc, ...
    thisCtry_estFire_annCIhi_rc, ...
    thisCtry_estFire_annStDv_rc] = ...
    ...%           thisCtry_obsFire_annMean_fakeC_byReg, ...
    ...%           thisCtry_obsFire_annMean_fakeP_byReg, ...
    ...%           thisCtry_obsFire_annMean_fakeO_byReg] = ...
    CPObs__calc_ESTvsOBS_byReg(...
    thisCtry_estFire_crop_myrt, ...
    thisCtry_estFire_past_myrt, ...
    thisCtry_estFire_othr_myrt, ...
    thisCtry_estFire_total_myrt, ...
    thisCtry_obsFire_total_my, ...
    ...%     thisCtry_obsFire_total_myr, ...
    CI_percentile, methods, ...
    thisCtry_obsFire_perLC_my)

% OLD METHOD
if ~isempty(thisCtry_obsFire_perLC_my)
    if strcmp(methods.landcover,'fake_3member')
        thisCtry_obsFire_fakeC_my = thisCtry_obsFire_perLC_my{1} ;
        thisCtry_obsFire_fakeP_my = thisCtry_obsFire_perLC_my{2} ;
        thisCtry_obsFire_fakeO_my = thisCtry_obsFire_perLC_my{3} ;
    else error('Not going to work with this methods.landcover.')
    end
end
% % % NEW METHOD
% % if ~isempty(thisCtry_obsFire_perLC_myr)
% %     if strcmp(methods.landcover,'fake_3member')
% %         thisCtry_obsFire_fakeC_myr = thisCtry_obsFire_perLC_myr{1} ;
% %         thisCtry_obsFire_fakeP_myr = thisCtry_obsFire_perLC_myr{2} ;
% %         thisCtry_obsFire_fakeO_myr = thisCtry_obsFire_perLC_myr{3} ;
% %     else error('thisCtry_obsFire_perLC_myr should be empty with this methods.landcover.')
% %     end
% % end

% CI_percentile_lo = (100-CI_percentile)/2 ;
% CI_percentile_hi = CI_percentile + CI_percentile_lo ;
CI_percentiles(1) = (100-CI_percentile)/2 ;
CI_percentiles(2) = CI_percentile + CI_percentiles(1) ;

Nruns = size(thisCtry_estFire_crop_myrt,4) ;

thisCtry_estFire_myrtc = cat(5,thisCtry_estFire_crop_myrt,...
    thisCtry_estFire_past_myrt,...
    thisCtry_estFire_othr_myrt,...
    thisCtry_estFire_total_myrt) ;
clear thisCtry_estFire_*_myrt

% % OLD METHOD: Calculate observed annual fire: Mean only
% thisCtry_obsFire_annMean_total = mean(sum(thisCtry_obsFire_total_my,1),2) ;
% if exist('thisCtry_obsFire_fakeC_my','var')
%     thisCtry_obsFire_annMean_fakeC = mean(sum(thisCtry_obsFire_fakeC_my,1),2) ;
%     thisCtry_obsFire_annMean_fakeP = mean(sum(thisCtry_obsFire_fakeP_my,1),2) ;
%     thisCtry_obsFire_annMean_fakeO = mean(sum(thisCtry_obsFire_fakeO_my,1),2) ;
% end
% % % % NEW METHOD: Calculate observed annual fire: Mean only
% % % thisCtry_obsFire_annMean_total_byReg = squeeze(mean(sum(thisCtry_obsFire_total_myr,1),2)) ;
% % % thisCtry_obsFire_annMean_total = sum(thisCtry_obsFire_annMean_total_byReg) ;
% % % if exist('thisCtry_obsFire_fakeC_my','var')
% % %     thisCtry_obsFire_annMean_fakeC_byReg = squeeze(mean(sum(thisCtry_obsFire_fakeC_myr,1),2)) ;
% % %     thisCtry_obsFire_annMean_fakeP_byReg = squeeze(mean(sum(thisCtry_obsFire_fakeP_myr,1),2)) ;
% % %     thisCtry_obsFire_annMean_fakeO_byReg = squeeze(mean(sum(thisCtry_obsFire_fakeO_myr,1),2)) ;
% % %     thisCtry_obsFire_annMean_fakeC = sum(thisCtry_obsFire_annMean_fakeC_byReg) ;
% % %     thisCtry_obsFire_annMean_fakeP = sum(thisCtry_obsFire_annMean_fakeP_byReg) ;
% % %     thisCtry_obsFire_annMean_fakeO = sum(thisCtry_obsFire_annMean_fakeO_byReg) ;
% % % end



tmp = squeeze(mean(sum(thisCtry_estFire_myrtc,1),2)) ;
tmp = permute(tmp,[1 3 2]) ;
thisCtry_estFire_annMean_rc = mean(tmp,3) ;
thisCtry_estFire_annCIlo_rc = prctile(tmp,CI_percentiles(1),3) ;
thisCtry_estFire_annCIhi_rc = prctile(tmp,CI_percentiles(2),3) ;
thisCtry_estFire_annStDv_rc = std(tmp,0,3) ;
clear tmp

% TOTAL: Calculate estimated mean and standard deviation
thisCtry_estFire_annMean_c = permute(sum(thisCtry_estFire_annMean_rc,1),[2 1]) ;
thisCtry_estFire_annStDv_c = sqrt(permute(sum(thisCtry_estFire_annStDv_rc.^2,1),[2 1])) ;   % Uncertainty propagation via quadrature addition
SEM = thisCtry_estFire_annStDv_c/sqrt(Nruns) ;
z = @(p) -sqrt(2) * erfcinv(p*2);
thisCtry_estFire_annCIlo_c = thisCtry_estFire_annMean_c + SEM*z(CI_percentiles(1)/100) ;
thisCtry_estFire_annCIhi_c = thisCtry_estFire_annMean_c + SEM*z(CI_percentiles(2)/100) ;
if isnan(thisCtry_estFire_annCIlo_c)
    error('isnan(thisCtry_estFire_annCIlo_c)')
end




end