function [thisCtry_estFireAnn_CIloMeanCIhi_NET, ...
          thisCtry_estFireAnn_CIloMeanCIhi_POS, ...
          thisCtry_estFireAnn_CIloMeanCIhi_NEG] = ...
    CPObs__calc_estFireAnn_CIloMeanCIhi(...
        thisCtry_estFire_crop_myt, ...
        thisCtry_estFire_past_myt, ...
        thisCtry_estFire_othr_myt, ...
        thisCtry_estFire_total_myt, ...
        thisCtry_obsFire_total_my, ...
        CI_percentile, units, country_name, methods, ...
        thisCtry_obsFire_perLC_my)
    
if ~isempty(thisCtry_obsFire_perLC_my)
    if strcmp(methods.landcover,'fake_3member')
        thisCtry_obsFire_fakeC_my = thisCtry_obsFire_perLC_my{1} ;
        thisCtry_obsFire_fakeP_my = thisCtry_obsFire_perLC_my{2} ;
        thisCtry_obsFire_fakeO_my = thisCtry_obsFire_perLC_my{3} ;
    else error('Not going to work with this methods.landcover.')
    end
end
    
CI_percentile_lo = (100-CI_percentile)/2 ;
CI_percentile_hi = CI_percentile + CI_percentile_lo ;

% Calculate observed annual fire: Mean only
thisCtry_obsFire_annMean_total = mean(sum(thisCtry_obsFire_total_my,1),2) ;
if exist('thisCtry_obsFire_fakeC_my','var')
    thisCtry_obsFire_annMean_fakeC = mean(sum(thisCtry_obsFire_fakeC_my,1),2) ;
    thisCtry_obsFire_annMean_fakeP = mean(sum(thisCtry_obsFire_fakeP_my,1),2) ;
    thisCtry_obsFire_annMean_fakeO = mean(sum(thisCtry_obsFire_fakeO_my,1),2) ;
end


%%%%%%%%%%%%%%%%%
%%% All cells %%%
%%%%%%%%%%%%%%%%%

% Calculate estimated annual fire: Mean and CI
tmp = squeeze(mean(sum(thisCtry_estFire_crop_myt,1),2)) ;
thisCtry_estFire_annMean(1,1) = mean(tmp) ;
thisCtry_estFire_annCIlo(1,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi(1,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = squeeze(mean(sum(thisCtry_estFire_past_myt,1),2)) ;
thisCtry_estFire_annMean(2,1) = mean(tmp) ;
thisCtry_estFire_annCIlo(2,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi(2,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = squeeze(mean(sum(thisCtry_estFire_othr_myt,1),2)) ;
thisCtry_estFire_annMean(3,1) = mean(tmp) ;
thisCtry_estFire_annCIlo(3,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi(3,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = squeeze(mean(sum(thisCtry_estFire_total_myt,1),2)) ;
thisCtry_estFire_annMean(4,1) = mean(tmp) ;
thisCtry_estFire_annCIlo(4,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi(4,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp

% Save result
thisCtry_estFireAnn_CIloMeanCIhi_NET = [thisCtry_estFire_annCIlo ...
                                    thisCtry_estFire_annMean ...
                                    thisCtry_estFire_annCIhi] ;
                                
% Display result
if strcmp(methods.landcover,'hyde')
    tmp1 = {'CROP';'PAST';'OTHR';'TOTL'} ;
elseif strcmp(methods.landcover,'fake_3member')
    tmp1 = {'fakeC';'fakeP';'fakeO';'TOTL'} ;
end
tmp2 = {' ' '95% CI lo' 'Best' '95% CI hi'} ;
tmp3 = [tmp1 num2cell(thisCtry_estFireAnn_CIloMeanCIhi_NET)] ;
disp(' ')
disp(['Estimated average annual fire in ' country_name ' (' units '):'])
disp([tmp2 ; tmp3])
disp(['Observed average annual fire (TOTAL) in ' country_name ': ' num2str(thisCtry_obsFire_annMean_total) ' ' units '.'])
if exist('thisCtry_obsFire_annMean_fakeC','var')
    disp(['   Observed average annual fire (fakeC) in ' country_name ': ' num2str(thisCtry_obsFire_annMean_fakeC) ' ' units '.'])
    disp(['   Observed average annual fire (fakeP) in ' country_name ': ' num2str(thisCtry_obsFire_annMean_fakeP) ' ' units '.'])
    disp(['   Observed average annual fire (fakeO) in ' country_name ': ' num2str(thisCtry_obsFire_annMean_fakeO) ' ' units '.'])
end
disp(' ')

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Positive cells only %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate estimated annual fire: Mean and CI
tmp = thisCtry_estFire_crop_myt ;
tmp(tmp<0) = 0 ;
tmp = squeeze(mean(sum(tmp,1),2)) ;
thisCtry_estFire_annMean_noNeg(1,1) = mean(tmp) ;
thisCtry_estFire_annCIlo_noNeg(1,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi_noNeg(1,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = thisCtry_estFire_past_myt ;
tmp(tmp<0) = 0 ;
tmp = squeeze(mean(sum(tmp,1),2)) ;
thisCtry_estFire_annMean_noNeg(2,1) = mean(tmp) ;
thisCtry_estFire_annCIlo_noNeg(2,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi_noNeg(2,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = thisCtry_estFire_othr_myt ;
tmp(tmp<0) = 0 ;
tmp = squeeze(mean(sum(tmp,1),2)) ;
thisCtry_estFire_annMean_noNeg(3,1) = mean(tmp) ;
thisCtry_estFire_annCIlo_noNeg(3,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi_noNeg(3,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = thisCtry_estFire_total_myt ;
tmp(tmp<0) = 0 ;
tmp = squeeze(mean(sum(tmp,1),2)) ;
thisCtry_estFire_annMean_noNeg(4,1) = mean(tmp) ;
thisCtry_estFire_annCIlo_noNeg(4,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi_noNeg(4,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp

% Save result
thisCtry_estFireAnn_CIloMeanCIhi_POS = [thisCtry_estFire_annCIlo_noNeg ...
                                          thisCtry_estFire_annMean_noNeg ...
                                          thisCtry_estFire_annCIhi_noNeg] ;

% Display result
tmp1 = {'CROP';'PAST';'OTHR';'TOTL'} ;
tmp2 = {' ' '95% CI lo' 'Best' '95% CI hi'} ;
tmp3 = [tmp1 num2cell(thisCtry_estFireAnn_CIloMeanCIhi_POS)] ;
disp(['POSITIVE estimated average annual fire in ' country_name ' (' units '):'])
disp([tmp2 ; tmp3])
disp(' ')



%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Negative cells only %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calculate estimated annual fire: Mean and CI
tmp = thisCtry_estFire_crop_myt ;
tmp(tmp>0) = 0 ;
tmp = squeeze(mean(sum(tmp,1),2)) ;
thisCtry_estFire_annMean_noPos(1,1) = mean(tmp) ;
thisCtry_estFire_annCIlo_noPos(1,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi_noPos(1,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = thisCtry_estFire_past_myt ;
tmp(tmp>0) = 0 ;
tmp = squeeze(mean(sum(tmp,1),2)) ;
thisCtry_estFire_annMean_noPos(2,1) = mean(tmp) ;
thisCtry_estFire_annCIlo_noPos(2,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi_noPos(2,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = thisCtry_estFire_othr_myt ;
tmp(tmp>0) = 0 ;
tmp = squeeze(mean(sum(tmp,1),2)) ;
thisCtry_estFire_annMean_noPos(3,1) = mean(tmp) ;
thisCtry_estFire_annCIlo_noPos(3,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi_noPos(3,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp
tmp = thisCtry_estFire_total_myt ;
tmp(tmp>0) = 0 ;
tmp = squeeze(mean(sum(tmp,1),2)) ;
thisCtry_estFire_annMean_noPos(4,1) = mean(tmp) ;
thisCtry_estFire_annCIlo_noPos(4,1) = prctile(tmp,CI_percentile_lo) ;
thisCtry_estFire_annCIhi_noPos(4,1) = prctile(tmp,CI_percentile_hi) ;
clear tmp

% Save result
thisCtry_estFireAnn_CIloMeanCIhi_NEG = [thisCtry_estFire_annCIlo_noPos ...
                                          thisCtry_estFire_annMean_noPos ...
                                          thisCtry_estFire_annCIhi_noPos] ;

% Display result
tmp1 = {'CROP';'PAST';'OTHR';'TOTL'} ;
tmp2 = {' ' '95% CI lo' 'Best' '95% CI hi'} ;
tmp3 = [tmp1 num2cell(thisCtry_estFireAnn_CIloMeanCIhi_NEG)] ;
disp(['NEGATIVE estimated average annual fire in ' country_name ' (' units '):'])
disp([tmp2 ; tmp3])
disp(' ')




end