function [thisCtry_estFireNET_annMean_rc, ...
          thisCtry_estFireNET_annCIlo_rc, ...
          thisCtry_estFireNET_annCIhi_rc, ...
          thisCtry_estFireNET_annStDv_rc, ...
          thisCtry_estFireNET_annMean_c, ...
          thisCtry_estFireNET_annCIlo_c, ...
          thisCtry_estFireNET_annCIhi_c, ...
          thisCtry_estFireNET_annStDv_c, ...
          thisCtry_estFireAnn_CIloMeanCIhi_NET, ...
          thisCtry_estFireAnn_CIloMeanCIhi_POS, ...
          thisCtry_estFireAnn_CIloMeanCIhi_NEG] = ...
...%           thisCtry_obsFire_annMean_fakeC_byReg, ...
...%           thisCtry_obsFire_annMean_fakeP_byReg, ...
...%           thisCtry_obsFire_annMean_fakeO_byReg] = ...
  CPObs__calc_estFireAnn_CIloMeanCIhi_v2(...
    thisCtry_estFire_crop_myrt, ...
    thisCtry_estFire_past_myrt, ...
    thisCtry_estFire_othr_myrt, ...
    thisCtry_estFire_total_myrt, ...
    thisCtry_obsFire_total_my, ...
...%     thisCtry_obsFire_total_myr, ...
    CI_percentile, units, country_name, methods, ...
        thisCtry_obsFire_perLC_my)
    
disp(' ')
disp('Using by-regionmonth summary.')
disp(' ')
    
% OLD METHOD
if ~isempty(thisCtry_obsFire_perLC_my)
    if strcmp(methods.landcover,'fake_3member')
        thisCtry_obsFire_fakeC_my = thisCtry_obsFire_perLC_my{1} ;
        thisCtry_obsFire_fakeP_my = thisCtry_obsFire_perLC_my{2} ;
        thisCtry_obsFire_fakeO_my = thisCtry_obsFire_perLC_my{3} ;
    else error('Not going to work with this methods.landcover.')
    end
end
% % % NEW METHOD
% % if ~isempty(thisCtry_obsFire_perLC_myr)
% %     if strcmp(methods.landcover,'fake_3member')
% %         thisCtry_obsFire_fakeC_myr = thisCtry_obsFire_perLC_myr{1} ;
% %         thisCtry_obsFire_fakeP_myr = thisCtry_obsFire_perLC_myr{2} ;
% %         thisCtry_obsFire_fakeO_myr = thisCtry_obsFire_perLC_myr{3} ;
% %     else error('thisCtry_obsFire_perLC_myr should be empty with this methods.landcover.')
% %     end
% % end

% CI_percentile_lo = (100-CI_percentile)/2 ;
% CI_percentile_hi = CI_percentile + CI_percentile_lo ;
CI_percentiles(1) = (100-CI_percentile)/2 ;
CI_percentiles(2) = CI_percentile + CI_percentiles(1) ;

thisCtry_numRegs = size(thisCtry_estFire_crop_myrt,3) ;
Nruns = size(thisCtry_estFire_crop_myrt,4) ;

thisCtry_estFire_myrtc = cat(5,thisCtry_estFire_crop_myrt,...
                               thisCtry_estFire_past_myrt,...
                               thisCtry_estFire_othr_myrt,...
                               thisCtry_estFire_total_myrt) ;
clear thisCtry_estFire_*_myrt

% OLD METHOD: Calculate observed annual fire: Mean only
thisCtry_obsFire_annMean_total = mean(sum(thisCtry_obsFire_total_my,1),2) ;
if exist('thisCtry_obsFire_fakeC_my','var')
    thisCtry_obsFire_annMean_fakeC = mean(sum(thisCtry_obsFire_fakeC_my,1),2) ;
    thisCtry_obsFire_annMean_fakeP = mean(sum(thisCtry_obsFire_fakeP_my,1),2) ;
    thisCtry_obsFire_annMean_fakeO = mean(sum(thisCtry_obsFire_fakeO_my,1),2) ;
end
% % % % NEW METHOD: Calculate observed annual fire: Mean only
% % % thisCtry_obsFire_annMean_total_byReg = squeeze(mean(sum(thisCtry_obsFire_total_myr,1),2)) ;
% % % thisCtry_obsFire_annMean_total = sum(thisCtry_obsFire_annMean_total_byReg) ;
% % % if exist('thisCtry_obsFire_fakeC_my','var')
% % %     thisCtry_obsFire_annMean_fakeC_byReg = squeeze(mean(sum(thisCtry_obsFire_fakeC_myr,1),2)) ;
% % %     thisCtry_obsFire_annMean_fakeP_byReg = squeeze(mean(sum(thisCtry_obsFire_fakeP_myr,1),2)) ;
% % %     thisCtry_obsFire_annMean_fakeO_byReg = squeeze(mean(sum(thisCtry_obsFire_fakeO_myr,1),2)) ;
% % %     thisCtry_obsFire_annMean_fakeC = sum(thisCtry_obsFire_annMean_fakeC_byReg) ;
% % %     thisCtry_obsFire_annMean_fakeP = sum(thisCtry_obsFire_annMean_fakeP_byReg) ;
% % %     thisCtry_obsFire_annMean_fakeO = sum(thisCtry_obsFire_annMean_fakeO_byReg) ;
% % % end


% All cells
in_estFire_myrtc = thisCtry_estFire_myrtc ;
[thisCtry_estFireAnn_CIloMeanCIhi_NET, ...
 thisCtry_estFireNET_annMean_rc, thisCtry_estFireNET_annCIlo_rc, ...
 thisCtry_estFireNET_annCIhi_rc, thisCtry_estFireNET_annStDv_rc, ...
 thisCtry_estFireNET_annMean_c, thisCtry_estFireNET_annCIlo_c, ...
 thisCtry_estFireNET_annCIhi_c, thisCtry_estFireNET_annStDv_c] = ...
    gen_CIloMeanCIhi_table_v2;%( ...
%     thisCtry_estFire_myrtc, Nruns, CI_percentiles, ...
%     country_name, units, methods, thisCtry_obsFire_annMean_total) ;

% Positive cells only
wherePos = (in_estFire_myrtc>0) ;
in_estFire_myrtc(wherePos) = 0 ;
[thisCtry_estFireAnn_CIloMeanCIhi_POS, ...
 ~,~,~,~,~,~,~,~] = ...
    gen_CIloMeanCIhi_table_v2;%( ...
%     in_estFire_myrtc, Nruns, CI_percentiles, ...
%     country_name, units, methods, thisCtry_obsFire_annMean_total) ;
clear in_estFire_myrtc

% Negative cells only
in_estFire_myrtc = thisCtry_estFire_myrtc ;
in_estFire_myrtc(~wherePos) = 0 ;
[thisCtry_estFireAnn_CIloMeanCIhi_NEG, ...
 ~,~,~,~,~,~,~,~] = ...
    gen_CIloMeanCIhi_table_v2;%( ...
%     in_estFire_myrtc, Nruns, CI_percentiles, ...
%     country_name, units, methods, thisCtry_obsFire_annMean_total) ;
clear in_estFire_myrtc




function [thisCtry_estFireAnn_CIloMeanCIhi_RES, ...
          thisCtry_estFire_annMean_rc, ...
          thisCtry_estFire_annCIlo_rc, ...
          thisCtry_estFire_annCIhi_rc, ...
          thisCtry_estFire_annStDv_rc, ...
          thisCtry_estFire_annMean_c, ...
          thisCtry_estFire_annCIlo_c, ...
          thisCtry_estFire_annCIhi_c, ...
          thisCtry_estFire_annStDv_c] = ...
        gen_CIloMeanCIhi_table_v2%( ...
%     in_estFire_myrtc, Nruns, CI_percentiles, ...
%     country_name, units, methods, thisCtry_obsFire_annMean_total)

% % % 	% Setup
% % %     num_coversInclTotal = 3 + 1 ;
% % %     thisCtry_estFire_annMean_rc = nan(thisCtry_numRegs,num_coversInclTotal) ;
% % %     thisCtry_estFire_annCIlo_rc = nan(thisCtry_numRegs,num_coversInclTotal) ;
% % %     thisCtry_estFire_annCIhi_rc = nan(thisCtry_numRegs,num_coversInclTotal) ;
% % %     thisCtry_estFire_annStDv_rc = nan(thisCtry_numRegs,num_coversInclTotal) ;
% % %   % FOR EACH REGION: Calculate estimated annual fire: Mean and CI
% % %     for c = 1:num_coversInclTotal
% % %         tmp = squeeze(mean(sum(in_estFire_myrtc(:,:,:,:,c),1),2)) ;
% % %         thisCtry_estFire_annMean_rc(:,c) = mean(tmp,2) ;
% % %         thisCtry_estFire_annCIlo_rc(:,c) = prctile(tmp,CI_percentiles(1),2) ;
% % %         thisCtry_estFire_annCIhi_rc(:,c) = prctile(tmp,CI_percentiles(2),2) ;
% % %         thisCtry_estFire_annStDv_rc(:,c) = std(tmp,0,2) ;
% % %         clear tmp
% % %     end
    tmp = squeeze(mean(sum(in_estFire_myrtc,1),2)) ;
    tmp = permute(tmp,[1 3 2]) ;
    thisCtry_estFire_annMean_rc = mean(tmp,3) ;
    thisCtry_estFire_annCIlo_rc = prctile(tmp,CI_percentiles(1),3) ;
    thisCtry_estFire_annCIhi_rc = prctile(tmp,CI_percentiles(2),3) ;
    thisCtry_estFire_annStDv_rc = std(tmp,0,3) ;
    clear tmp

    % TOTAL: Calculate estimated mean and standard deviation
    thisCtry_estFire_annMean_c = permute(sum(thisCtry_estFire_annMean_rc,1),[2 1]) ;
    thisCtry_estFire_annStDv_c = sqrt(permute(sum(thisCtry_estFire_annStDv_rc.^2,1),[2 1])) ;   % Uncertainty propagation via quadrature addition
    SEM = thisCtry_estFire_annStDv_c/sqrt(Nruns) ;
    z = @(p) -sqrt(2) * erfcinv(p*2);
    % CI_percentile_lo
    % if isnan(thisCtry_estFire_annMean_c)
    %     error('isnan(thisCtry_estFire_annMean_c)')
    % elseif isnan(SEM)
    %     error('isnan(SEM)')
    % elseif isnan(z(CI_percentile_lo))
    %     error('isnan(z(CI_percentile_lo))')
    % end
    thisCtry_estFire_annCIlo_c = thisCtry_estFire_annMean_c + SEM*z(CI_percentiles(1)/100) ;
    thisCtry_estFire_annCIhi_c = thisCtry_estFire_annMean_c + SEM*z(CI_percentiles(2)/100) ;
    if isnan(thisCtry_estFire_annCIlo_c)
        error('isnan(thisCtry_estFire_annCIlo_c)')
    end
    % Save result
    thisCtry_estFireAnn_CIloMeanCIhi_RES = [thisCtry_estFire_annCIlo_c ...
                                            thisCtry_estFire_annMean_c ...
                                            thisCtry_estFire_annCIhi_c] ;
	size(thisCtry_estFireAnn_CIloMeanCIhi_RES)

    % Display result
    if strcmp(methods.landcover,'hyde')
        tmp1 = {'CROP';'PAST';'OTHR';'TOTL'} ;
    elseif strcmp(methods.landcover,'fake_3member')
        tmp1 = {'fakeC';'fakeP';'fakeO';'TOTL'} ;
    end
    tmp2 = {' ' '95% CI lo' 'Best' '95% CI hi'} ;
    tmp3 = [tmp1 num2cell(thisCtry_estFireAnn_CIloMeanCIhi_RES)] ;
    disp(' ')
    disp(['Estimated average annual fire in ' country_name ' (' units '):'])
    disp([tmp2 ; tmp3])
    disp(['Observed average annual fire (TOTAL) in ' country_name ': ' num2str(thisCtry_obsFire_annMean_total) ' ' units '.'])
    if exist('thisCtry_obsFire_annMean_fakeC','var')
        disp(['   Observed average annual fire (fakeC) in ' country_name ': ' num2str(thisCtry_obsFire_annMean_fakeC) ' ' units '.'])
        disp(['   Observed average annual fire (fakeP) in ' country_name ': ' num2str(thisCtry_obsFire_annMean_fakeP) ' ' units '.'])
        disp(['   Observed average annual fire (fakeO) in ' country_name ': ' num2str(thisCtry_obsFire_annMean_fakeO) ' ' units '.'])
    end
    disp(' ')

end



end