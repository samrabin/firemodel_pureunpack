function [estFire_meanAnn_rc, ...
          estFire_SdAnn_rc, ...
          estFire_CIlo_rc, ...
          estFire_CIhi_rc, ...
          obsFire_meanAnn_rc, ...
          obsFire_SdAnn_rc, ...
          obsFire_CIlo_rc, ...
          obsFire_CIhi_rc, ...
          difFire_meanAnn_rc, ...
          difFire_SdAnn_rc, ...
          difFire_CIlo_rc, ...
          difFire_CIhi_rc] = ...
       CPObs__calc_regAndCov_estFire_annMeanSD_v1( ...
            thisCtry_estFire_crop_myrt, ...
            thisCtry_estFire_past_myrt, ...
            thisCtry_estFire_othr_myrt, ...
            thisCtry_obsFire_fakeC_myr, ...
            thisCtry_obsFire_fakeP_myr, ...
            thisCtry_obsFire_fakeO_myr, ...
            CI_percentile)
        

% Setup
Nyears = size(thisCtry_estFire_crop_myrt,2) ;
Nregs = size(thisCtry_estFire_crop_myrt,3) ;
Nruns = size(thisCtry_estFire_crop_myrt,4) ;
Ncovers = 3 ;
CI_percentiles(1) = (100-CI_percentile)/2 ;
CI_percentiles(2) = CI_percentile + CI_percentiles(1) ;
z = @(p) -sqrt(2) * erfcinv(p*2); 


%%%%%%%%%%%%%%%%%%%%%%
%%% Estimated fire %%%
%%%%%%%%%%%%%%%%%%%%%%

% Calculate annual mean and SD
estFire_myrtc = cat(5,thisCtry_estFire_crop_myrt, ...
                      thisCtry_estFire_past_myrt, ...
                      thisCtry_estFire_othr_myrt) ;
Ncov = size(estFire_myrtc,5) ;
estFire_yrtc = squeeze(sum(estFire_myrtc,1)) ;
estFire_yrc_mean = mean(estFire_yrtc,3) ;
cov_y1y2rc = nan(Nyears,Nyears,Nregs,Ncov) ;
for c = 1:Ncov
    for r = 1:Nregs
        cov_y1y2rc(:,:,r,c) = cov(permute(squeeze(estFire_yrtc(:,r,:,c)),[2 1])) ;
    end
end
estFire_meanAnn_rc = squeeze(mean(estFire_yrc_mean,1)) ;
estFire_SdAnn_rc = (1/Nyears) * sqrt(squeeze(sum(sum(cov_y1y2rc,2),1))) ;

% Calculate CI
estFire_SEM_rc = estFire_SdAnn_rc / sqrt(Nyears) ;
estFire_CIlo_rc = estFire_meanAnn_rc + estFire_SEM_rc*z(CI_percentiles(1)/100) ;
estFire_CIhi_rc = estFire_meanAnn_rc + estFire_SEM_rc*z(CI_percentiles(2)/100) ;


%%%%%%%%%%%%%%%%%%%%%
%%% Observed fire %%%
%%%%%%%%%%%%%%%%%%%%%

% Calculate annual mean and SD
obsFire_myrc = cat(4,thisCtry_obsFire_fakeC_myr,thisCtry_obsFire_fakeP_myr,thisCtry_obsFire_fakeO_myr) ;
obsFire_yrc = squeeze(sum(obsFire_myrc,1)) ;
obsFire_meanAnn_rc = squeeze(mean(obsFire_yrc,1)) ;
obsFire_SdAnn_rc = squeeze(std(obsFire_yrc,0,1)) ;

% Calculate CI
obsFire_SEM_rc = obsFire_SdAnn_rc ./ sqrt(Nyears) ;
obsFire_CIlo_rc = obsFire_meanAnn_rc + obsFire_SEM_rc*z(CI_percentiles(1)/100) ;
obsFire_CIhi_rc = obsFire_meanAnn_rc + obsFire_SEM_rc*z(CI_percentiles(2)/100) ;


%%%%%%%%%%%%%%%%%%
%%% Difference %%%
%%%%%%%%%%%%%%%%%%

difFire_yrct = permute(estFire_yrtc,[1 2 4 3]) - repmat(obsFire_yrc,[1 1 1 Nruns]) ;
difFire_yrc_mean = mean(difFire_yrct,4) ;
cov_y1y2rc = nan(Nyears,Nyears,Nregs) ;
for c = 1:Ncovers
    for r = 1:Nregs
        cov_y1y2rc(:,:,r,c) = cov(permute(squeeze(difFire_yrct(:,r,c,:)),[2 1])) ;
    end
end
difFire_meanAnn_rc = squeeze(mean(difFire_yrc_mean,1)) ;
difFire_SdAnn_rc = (1/Nyears) * sqrt(squeeze(sum(sum(cov_y1y2rc,2),1))) ;

% Calculate CI
difFire_SEM_rc = difFire_SdAnn_rc / sqrt(Nyears) ;
difFire_CIlo_rc = difFire_meanAnn_rc + difFire_SEM_rc*z(CI_percentiles(1)/100) ;
difFire_CIhi_rc = difFire_meanAnn_rc + difFire_SEM_rc*z(CI_percentiles(2)/100) ;



        
end