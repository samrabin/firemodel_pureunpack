function [estFire_meanAnn_r, ...
          estFire_SdAnn_r, ...
          estFire_CIlo_r, ...
          estFire_CIhi_r, ...
          obsFire_meanAnn_r, ...
          obsFire_SdAnn_r, ...
          obsFire_CIlo_r, ...
          obsFire_CIhi_r, ...
          difFire_meanAnn_r, ...
          difFire_SdAnn_r, ...
          difFire_CIlo_r, ...
          difFire_CIhi_r] = ...
       CPObs__calc_regional_estFire_annMeanSD_v1( ...
            thisCtry_estFire_crop_myrt, ...
            thisCtry_estFire_past_myrt, ...
            thisCtry_estFire_othr_myrt, ...
            thisCtry_obsFire_total_myr, ...
            CI_percentile)
% Calculates mean annual estimated fire for each region, considering both
% uncertainty (as captured by bootstrapping) and interannual variability.

% Setup
Nyears = size(thisCtry_estFire_crop_myrt,2) ;
Nregs = size(thisCtry_estFire_crop_myrt,3) ;
Nruns = size(thisCtry_estFire_crop_myrt,4) ;
CI_percentiles(1) = (100-CI_percentile)/2 ;
CI_percentiles(2) = CI_percentile + CI_percentiles(1) ;
z = @(p) -sqrt(2) * erfcinv(p*2);

%%%%%%%%%%%%%%%%%%%%%%
%%% Estimated fire %%%
%%%%%%%%%%%%%%%%%%%%%%

% Calculate annual mean and SD
estFire_myrt = thisCtry_estFire_crop_myrt + ...
               thisCtry_estFire_past_myrt + ...
               thisCtry_estFire_othr_myrt ;

estFire_yrt = squeeze(sum(estFire_myrt,1)) ;
estFire_yr_mean = mean(estFire_yrt,3) ;
% estFire_yr_sd = std(estFire_yrt,0,3) ;
cov_y1y2r = nan(Nyears,Nyears,Nregs) ;
for r = 1:Nregs
    cov_y1y2r(:,:,r) = cov(permute(squeeze(estFire_yrt(:,r,:)),[2 1])) ;
end
estFire_meanAnn_r = permute(squeeze(mean(estFire_yr_mean,1)),[2 1]) ;
estFire_SdAnn_r = (1/Nyears) * sqrt(squeeze(sum(sum(cov_y1y2r,2),1))) ;

% Calculate CI
estFire_SEM_r = estFire_SdAnn_r / sqrt(Nyears) ;
estFire_CIlo_r = estFire_meanAnn_r + estFire_SEM_r*z(CI_percentiles(1)/100) ;
estFire_CIhi_r = estFire_meanAnn_r + estFire_SEM_r*z(CI_percentiles(2)/100) ;


%%%%%%%%%%%%%%%%%%%%%
%%% Observed fire %%%
%%%%%%%%%%%%%%%%%%%%%

% Calculate annual mean and SD
obsFire_yr = squeeze(sum(thisCtry_obsFire_total_myr,1)) ;
obsFire_meanAnn_r = squeeze(mean(obsFire_yr,1)) ;
obsFire_SdAnn_r = std(obsFire_yr,0,1) ;

% Calculate CI
obsFire_SEM_r = obsFire_SdAnn_r ./ sqrt(Nyears) ;
obsFire_CIlo_r = obsFire_meanAnn_r + obsFire_SEM_r*z(CI_percentiles(1)/100) ;
obsFire_CIhi_r = obsFire_meanAnn_r + obsFire_SEM_r*z(CI_percentiles(2)/100) ;



%%%%%%%%%%%%%%%%%%
%%% Difference %%%
%%%%%%%%%%%%%%%%%%

difFire_yrt = estFire_yrt - repmat(obsFire_yr,[1 1 Nruns]) ;
difFire_yr_mean = mean(difFire_yrt,3) ;
% difFire_yr_sd = std(difFire_yrt,0,3) ;
cov_y1y2r = nan(Nyears,Nyears,Nregs) ;
for r = 1:Nregs
    cov_y1y2r(:,:,r) = cov(permute(squeeze(difFire_yrt(:,r,:)),[2 1])) ;
end
difFire_meanAnn_r = permute(squeeze(mean(difFire_yr_mean,1)),[2 1]) ;
difFire_SdAnn_r = (1/Nyears) * sqrt(squeeze(sum(sum(cov_y1y2r,2),1))) ;

% Calculate CI
difFire_SEM_r = difFire_SdAnn_r / sqrt(Nyears) ;
difFire_CIlo_r = difFire_meanAnn_r + difFire_SEM_r*z(CI_percentiles(1)/100) ;
difFire_CIhi_r = difFire_meanAnn_r + difFire_SEM_r*z(CI_percentiles(2)/100) ;

