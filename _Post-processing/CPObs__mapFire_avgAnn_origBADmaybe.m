function [map_crop,map_past,map_othr,map_total,...
          map_totalOBS] = ...
           CPObs__mapFire_avgAnn( ...
            thisCtry_Fc_mrt,thisCtry_Fp_mrt,thisCtry_Fo_mrt, ...
            thisCtry_estFireRAW_crop_mrt,thisCtry_estFireRAW_past_mrt,thisCtry_estFireRAW_othr_mrt,...
            thisCtry_obsFireRAW, ...
            LCfrac_crop, LCfrac_past, fire_normConstant, ...
            countries_map, landarea_map, methods, thisCtry, country_name, regKey_bsruns, ...
            varargin)

disp('Setting up...')

default_do_log10 = false ;
default_do_fracLC = false ;

if ~isempty(varargin)
    do_log10 = varargin{1} ;
    if length(varargin) > 1
        do_fracLC = varargin{2} ;
        if length(varargin) > 2
            warning('Ignoring all but first two elements in varargin.')
        end
    else
        do_fracLC = default_do_fracLC ;
    end
else
    do_log10 = default_do_log10 ;
    do_fracLC = default_do_fracLC ;
end
if ~islogical(do_log10)
    warning('Coercing do_log10 to logical.')
    do_log10 = logical(do_log10) ;
end
if ~islogical(do_fracLC)
    warning('Coercing do_fracLC to logical.')
    do_fracLC = logical(do_fracLC) ;
end

Nmonths = size(thisCtry_Fc_mrt,1) ;
Nyears = Nmonths / 12 ;
Nregs = size(thisCtry_Fc_mrt,2) ;

% Find elements in this country
countries_Marray = repmat(countries_map,[1 1 Nmonths]) ;
landarea_Marray = repmat(landarea_map,[1 1 Nmonths]) ;
landarea_vec_allMths = landarea_Marray(~isnan(landarea_Marray)) ;
countries_vec = countries_Marray(~isnan(landarea_Marray)) ;
inThisCtry = (countries_vec==thisCtry) ;

% Get misc. things for this country
thisCtry_regKey = regKey_bsruns(inThisCtry) ;
thisCtry_regKey_just1mth = thisCtry_regKey(1:(length(thisCtry_regKey)/Nmonths)) ;
thisCtry_regList = unique(thisCtry_regKey_just1mth) ;
thisCtry_Ncells = length(thisCtry_regKey_just1mth) ;
clear landarea_map landarea_Marray countries

% Get LCdata for this country, if needed
if ~strcmp(country_name,'All_countries')
    disp('Getting land cover data for this country...')
    thisCtry_LCfrac_crop = LCfrac_crop(inThisCtry) ;
    thisCtry_LCfrac_past = LCfrac_past(inThisCtry) ;
    thisCtry_LCfrac_othr = 1 - (thisCtry_LCfrac_crop + thisCtry_LCfrac_past) ;
else
end
    landarea_vec_allMths = landarea_vec_allMths(inThisCtry) ;
    if strcmp(methods.coverType,'prop')
        thisCtry_LCdata_crop = thisCtry_LCfrac_crop ;
        thisCtry_LCdata_past = thisCtry_LCfrac_past ;
        thisCtry_LCdata_othr = thisCtry_LCfrac_othr ;
    elseif strcmp(methods.coverType,'area')
        thisCtry_LCdata_crop = thisCtry_LCfrac_crop .* landarea_vec_allMths ;
        thisCtry_LCdata_past = thisCtry_LCfrac_past .* landarea_vec_allMths ;
        thisCtry_LCdata_othr = thisCtry_LCfrac_othr .* landarea_vec_allMths ;
    end
    if do_fracLC
        if strcmp(methods.coverType,'area')
            thisCtry_LCarea_crop = thisCtry_LCdata_crop ;
            thisCtry_LCarea_past = thisCtry_LCdata_past ;
            thisCtry_LCarea_othr = thisCtry_LCdata_othr ;
        else
            thisCtry_LCarea_crop = thisCtry_LCfrac_crop .* landarea_vec_allMths ;
            thisCtry_LCarea_past = thisCtry_LCfrac_past .* landarea_vec_allMths ;
            thisCtry_LCarea_othr = thisCtry_LCfrac_othr .* landarea_vec_allMths ;
        end
    else
    end
    if ~strcmp(methods.normType,'none')
        thisCtry_fire_normConstant = fire_normConstant(inThisCtry) ;
        thisCtry_LCdata_crop = thisCtry_LCdata_crop .* thisCtry_fire_normConstant ;
        thisCtry_LCdata_past = thisCtry_LCdata_past .* thisCtry_fire_normConstant ;
        thisCtry_LCdata_othr = thisCtry_LCdata_othr .* thisCtry_fire_normConstant ;
    end
else
    thisCtry_LCdata_crop = LCdata_crop ;
    thisCtry_LCdata_past = LCdata_past ;
    thisCtry_LCdata_othr = LCdata_othr ;
end
clear LCdata_*

% Find best-guess Fk's
disp('Finding best-guess Fk''s...')
estFireBG_crop_mr = mean(thisCtry_estFireRAW_crop_mrt,3) ;
estFireBG_past_mr = mean(thisCtry_estFireRAW_past_mrt,3) ;
estFireBG_othr_mr = mean(thisCtry_estFireRAW_othr_mrt,3) ;
FcBG_mr = nan(size(estFireBG_crop_mr),'single') ;
FpBG_mr = nan(size(estFireBG_past_mr),'single') ;
FoBG_mr = nan(size(estFireBG_othr_mr),'single') ;
for m = 1:Nmonths
    for r = 1:Nregs
        nrst = findnearest(thisCtry_estFireRAW_crop_mrt(m,r,:),estFireBG_crop_mr(m,r)) ;
        FcBG_mr(m,r) = thisCtry_Fc_mrt(m,r,nrst(1)) ;
        nrst = findnearest(thisCtry_estFireRAW_past_mrt(m,r,:),estFireBG_past_mr(m,r)) ;
        FpBG_mr(m,r) = thisCtry_Fp_mrt(m,r,nrst(1)) ;
        nrst = findnearest(thisCtry_estFireRAW_othr_mrt(m,r,:),estFireBG_past_mr(m,r)) ;
        FoBG_mr(m,r) = thisCtry_Fo_mrt(m,r,nrst(1)) ;
    end ; clear nrst
end

% Extend Fk's into vector of length thisCtry_Ncells*Nmonths
disp('Expanding Fk''s into vector...')
thisCtry_month = [] ;
for m = 1:Nmonths
    thisCtry_month = cat(1,thisCtry_month,m*ones([thisCtry_Ncells 1],'uint16')) ;
end
thisCtry_estFire_crop = nan(size(thisCtry_month),'single') ;
thisCtry_estFire_past = nan(size(thisCtry_month),'single') ;
thisCtry_estFire_othr = nan(size(thisCtry_month),'single') ;

for m = 1:Nmonths
    disp(['Month ' num2str(m) ' of ' num2str(Nmonths) '...'])
    inThisMonth = (thisCtry_month==m) ;
    LCdata_crop_thisMonth = thisCtry_LCdata_crop(inThisMonth) ;
    LCdata_past_thisMonth = thisCtry_LCdata_past(inThisMonth) ;
    LCdata_othr_thisMonth = thisCtry_LCdata_othr(inThisMonth) ;
    tmpC = nan(size(LCdata_crop_thisMonth)) ;
    tmpP = nan(size(LCdata_past_thisMonth)) ;
    tmpO = nan(size(LCdata_othr_thisMonth)) ;
    for r = 1:Nregs
        thisReg = thisCtry_regList(r) ;
        inThisReg_thisMonth = (thisCtry_regKey_just1mth==thisReg) ;
        tmpC(inThisReg_thisMonth) = LCdata_crop_thisMonth(inThisReg_thisMonth) ...
                                    .* FcBG_mr(m,r) ;
        tmpP(inThisReg_thisMonth) = LCdata_past_thisMonth(inThisReg_thisMonth) ...
                                    .* FpBG_mr(m,r) ;
        tmpO(inThisReg_thisMonth) = LCdata_othr_thisMonth(inThisReg_thisMonth) ...
                                    .* FoBG_mr(m,r) ;
        clear thisReg inThisReg_thisMonth
    end ; clear r 
    thisCtry_estFire_crop(inThisMonth) = tmpC ; clear tmpC ;
    thisCtry_estFire_past(inThisMonth) = tmpP ; clear tmpP ;
    thisCtry_estFire_othr(inThisMonth) = tmpO ; clear tmpO ;
    clear *thisMonth
end ; clear m

% JUST GET POSITIVE COMPONENT
warning('Only showing positive component!')
thisCtry_estFire_crop(thisCtry_estFire_crop<0) = 0 ;
thisCtry_estFire_past(thisCtry_estFire_past<0) = 0 ;
thisCtry_estFire_othr(thisCtry_estFire_othr<0) = 0 ;

% Make maps
disp('Making maps...')
thisCtry_estFire_crop_cmy = reshape(thisCtry_estFire_crop,[thisCtry_Ncells 12 Nyears]) ;
thisCtry_estFire_past_cmy = reshape(thisCtry_estFire_past,[thisCtry_Ncells 12 Nyears]) ;
thisCtry_estFire_othr_cmy = reshape(thisCtry_estFire_othr,[thisCtry_Ncells 12 Nyears]) ;
thisCtry_obsFire_total_cmy = reshape(thisCtry_obsFireRAW,[thisCtry_Ncells 12 Nyears]) ;
if do_fracLC
    if strcmp(methods.coverType,'prop') ; error('I''m not sure if this works when methods.coverType==''prop''.') ; end
    thisCtry_LCarea_crop_cmy = reshape(thisCtry_LCarea_crop,[thisCtry_Ncells 12 Nyears]) ;
    thisCtry_LCarea_past_cmy = reshape(thisCtry_LCarea_past,[thisCtry_Ncells 12 Nyears]) ;
    thisCtry_LCarea_othr_cmy = reshape(thisCtry_LCarea_othr,[thisCtry_Ncells 12 Nyears]) ;
    thisCtry_estFire_crop_cmy = thisCtry_estFire_crop_cmy ./ thisCtry_LCarea_crop_cmy ;
    thisCtry_estFire_past_cmy = thisCtry_estFire_past_cmy ./ thisCtry_LCarea_past_cmy ;
    thisCtry_estFire_othr_cmy = thisCtry_estFire_othr_cmy ./ thisCtry_LCarea_othr_cmy ;
end
thisCtry_estFire_crop_cANNMEAN = mean(sum(thisCtry_estFire_crop_cmy,2),3) ;
thisCtry_estFire_past_cANNMEAN = mean(sum(thisCtry_estFire_past_cmy,2),3) ;
thisCtry_estFire_othr_cANNMEAN = mean(sum(thisCtry_estFire_othr_cmy,2),3) ;
thisCtry_obsFire_total_cANNMEAN = mean(sum(thisCtry_obsFire_total_cmy,2),3) ;
map_crop = nan(size(countries_map)) ;
map_past = nan(size(countries_map)) ;
map_othr = nan(size(countries_map)) ;
map_totalOBS = nan(size(countries_map)) ;
if do_log10
%     min_to_add = min([min(map_crop(map_crop>0)) min(map_past(map_past>0)) min(map_othr(map_othr>0))]) ;
    warning('Setting estFire<1e-6 to 0.')
    thisCtry_estFire_crop_cANNMEAN(thisCtry_estFire_crop_cANNMEAN<1e-6) = 1e-6 ;
    thisCtry_estFire_past_cANNMEAN(thisCtry_estFire_past_cANNMEAN<1e-6) = 1e-6 ;
    thisCtry_estFire_othr_cANNMEAN(thisCtry_estFire_othr_cANNMEAN<1e-6) = 1e-6 ;
    thisCtry_obsFire_total_cANNMEAN(thisCtry_obsFire_total_cANNMEAN<1e-6) = 1e-6 ;
    map_crop(countries_map==thisCtry) = thisCtry_estFire_crop_cANNMEAN ;
    map_past(countries_map==thisCtry) = thisCtry_estFire_past_cANNMEAN ;
    map_othr(countries_map==thisCtry) = thisCtry_estFire_othr_cANNMEAN ;
    map_totalOBS(countries_map==thisCtry) = thisCtry_obsFire_total_cANNMEAN ;
    min_to_add = 0 ;
    map_total = log10(min_to_add+map_crop+map_past_map_othr) ;
    map_totalOBS = log10(min_to_add+map_totalOBS) ;
    map_crop = log10(min_to_add+map_crop) ;
    map_past = log10(min_to_add+map_past) ;
    map_othr = log10(min_to_add+map_othr) ;
else
    map_crop(countries_map==thisCtry) = thisCtry_estFire_crop_cANNMEAN ;
    map_past(countries_map==thisCtry) = thisCtry_estFire_past_cANNMEAN ;
    map_othr(countries_map==thisCtry) = thisCtry_estFire_othr_cANNMEAN ;
    map_total = map_crop + map_past + map_othr ;
    map_totalOBS(countries_map==thisCtry) = thisCtry_obsFire_total_cANNMEAN ;
end

% Display maps: Totals
figure ;
set(gcf, 'Color',[1 1 1])
subplot(2,2,4) ;
pcolor(map_total) ; shading flat ; axis equal ;
colorbar ; clims = caxis ;
subplot(2,2,1) ;
pcolor(map_crop) ; shading flat ; axis equal ;
caxis(clims) ; colorbar
subplot(2,2,2) ;
pcolor(map_past) ; shading flat ; axis equal ;
caxis(clims) ; colorbar
subplot(2,2,3) ;
pcolor(map_othr) ; shading flat ; axis equal ;
caxis(clims) ; colorbar

% % Display maps: Per unit land area
% figure ;
% set(gcf, 'Color',[1 1 1])
% subplot(2,2,4) ;
% pcolor(double(map_total./landarea_map)) ; shading flat ; axis equal ;
% colorbar ; clims = caxis ;
% subplot(2,2,1) ;
% pcolor(double(map_crop./landarea_map)) ; shading flat ; axis equal ;
% caxis(clims) ; colorbar
% subplot(2,2,2) ;
% pcolor(double(map_past./landarea_map)) ; shading flat ; axis equal ;
% caxis(clims) ; colorbar
% subplot(2,2,3) ;
% pcolor(double(map_othr./landarea_map)) ; shading flat ; axis equal ;
% caxis(clims) ; colorbar

disp('Done.')


end