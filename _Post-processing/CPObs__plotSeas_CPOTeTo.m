function hf = CPObs__plotSeas_CPOTeTo( ...
                    thisCtry_estFire_crop_myt, ...
                    thisCtry_estFire_past_myt, ...
                    thisCtry_estFire_othr_myt, ...
                    thisCtry_estFire_total_myt, ...
                    thisCtry_obsFire_total_my, ...
                    varargin)

if ~isempty(varargin)
    include_CPOTeTo = logical(varargin{1}) ;
    include_CPOTe = include_CPOTeTo(1:4) ;
    include_To = include_CPOTeTo(5) ;
    
    if length(varargin) > 1
        normalizing = logical(varargin{2}) ;
    else
        normalizing = false ;
    end
else
    include_CPOTeTo = true(5,1) ;
    include_CPOTe = true(4,1) ;
    include_To = true ;
    normalizing = false ;
end

thisCtry_estFire_crop_Mt = squeeze(mean(thisCtry_estFire_crop_myt,2)) ;
thisCtry_estFire_past_Mt = squeeze(mean(thisCtry_estFire_past_myt,2)) ;
thisCtry_estFire_othr_Mt = squeeze(mean(thisCtry_estFire_othr_myt,2)) ;
thisCtry_estFire_total_Mt = squeeze(mean(thisCtry_estFire_total_myt,2)) ;
thisCtry_estFire_crop_Mt_mean = mean(thisCtry_estFire_crop_Mt,2) ;
thisCtry_estFire_past_Mt_mean = mean(thisCtry_estFire_past_Mt,2) ;
thisCtry_estFire_othr_Mt_mean = mean(thisCtry_estFire_othr_Mt,2) ;
thisCtry_estFire_total_Mt_mean = mean(thisCtry_estFire_total_Mt,2) ;

warning('Maybe bad way of doing confidence intervals? Maybe should account for SD for each month WITHIN EACH BOOTSTRAP RUN as well?')
thisCtry_estFire_crop_Mt_CIWlo = thisCtry_estFire_crop_Mt_mean - prctile(thisCtry_estFire_crop_Mt,2.5,2) ;
thisCtry_estFire_past_Mt_CIWlo = thisCtry_estFire_past_Mt_mean - prctile(thisCtry_estFire_past_Mt,2.5,2) ;
thisCtry_estFire_othr_Mt_CIWlo = thisCtry_estFire_othr_Mt_mean - prctile(thisCtry_estFire_othr_Mt,2.5,2) ;
thisCtry_estFire_total_Mt_CIWlo = thisCtry_estFire_total_Mt_mean - prctile(thisCtry_estFire_total_Mt,2.5,2) ;
thisCtry_estFire_crop_Mt_CIWhi = prctile(thisCtry_estFire_crop_Mt,97.5,2) - thisCtry_estFire_crop_Mt_mean ;
thisCtry_estFire_past_Mt_CIWhi = prctile(thisCtry_estFire_past_Mt,97.5,2) - thisCtry_estFire_past_Mt_mean ;
thisCtry_estFire_othr_Mt_CIWhi = prctile(thisCtry_estFire_othr_Mt,97.5,2) - thisCtry_estFire_othr_Mt_mean ;
thisCtry_estFire_total_Mt_CIWhi = prctile(thisCtry_estFire_total_Mt,97.5,2) - thisCtry_estFire_total_Mt_mean ;
thisCtry_estFire_crop_Mt_CIW = [thisCtry_estFire_crop_Mt_CIWlo thisCtry_estFire_crop_Mt_CIWhi] ;
thisCtry_estFire_past_Mt_CIW = [thisCtry_estFire_past_Mt_CIWlo thisCtry_estFire_past_Mt_CIWhi] ;
thisCtry_estFire_othr_Mt_CIW = [thisCtry_estFire_othr_Mt_CIWlo thisCtry_estFire_othr_Mt_CIWhi] ;
thisCtry_estFire_total_Mt_CIW = [thisCtry_estFire_total_Mt_CIWlo thisCtry_estFire_total_Mt_CIWhi] ;

% Normalize to max, if doing so
if normalizing
    thisCtry_estFire_crop_Mt_mean = thisCtry_estFire_crop_Mt_mean / max(thisCtry_estFire_crop_Mt_mean) ;
    thisCtry_estFire_crop_Mt_CIW = thisCtry_estFire_crop_Mt_CIW / max(thisCtry_estFire_crop_Mt_mean) ;
    thisCtry_estFire_past_Mt_mean = thisCtry_estFire_past_Mt_mean / max(thisCtry_estFire_past_Mt_mean) ;
    thisCtry_estFire_past_Mt_CIW = thisCtry_estFire_past_Mt_CIW / max(thisCtry_estFire_past_Mt_mean) ;
    thisCtry_estFire_othr_Mt_mean = thisCtry_estFire_othr_Mt_mean / max(thisCtry_estFire_othr_Mt_mean) ;
    thisCtry_estFire_othr_Mt_CIW = thisCtry_estFire_othr_Mt_CIW / max(thisCtry_estFire_othr_Mt_mean) ;
    thisCtry_estFire_total_Mt_mean = thisCtry_estFire_total_Mt_mean / max(thisCtry_estFire_total_Mt_mean) ;
    thisCtry_estFire_total_Mt_CIW = thisCtry_estFire_total_Mt_CIW / max(thisCtry_estFire_total_Mt_mean) ;
end

% Plot estimated CPOT using boundedline()
hf = figure ;
x1 = 0:13 ;
y0 = [thisCtry_estFire_crop_Mt_mean';thisCtry_estFire_past_Mt_mean';thisCtry_estFire_othr_Mt_mean';thisCtry_estFire_total_Mt_mean'] ;
y0 = y0(include_CPOTe,:) ;   % Exclude curves
y1 = cat(2,y0(:,12),y0,y0(:,1)) ;
e1 = cat(3,thisCtry_estFire_crop_Mt_CIW,thisCtry_estFire_past_Mt_CIW,thisCtry_estFire_othr_Mt_CIW,thisCtry_estFire_total_Mt_CIW) ;
e1 = e1(:,:,include_CPOTe) ;   % Exclude curves
cmap = [155 187 89 ;
        146 122 181 ;
        250 168 70  ;
          0   0   0 ] / 255 ;
cmap = cmap(include_CPOTe,:) ;   % Exclude curves
[hl,hp] = boundedline(1:12,y0,e1,'cmap',cmap,'transparency',0.5) ;
outlinebounds(hl,hp) ;

% Plot lines for extra months before and after
hold on
for i = 1:size(cmap,1)
    plot(x1,y1(i,:),'Color',cmap(i,:))
end ; clear i
hold off
    
% Plot total observed fire
if include_To
    warning('Should I add error bars, or even include this boundedlines()?')
    thisCtry_obsFire_total_M = mean(thisCtry_obsFire_total_my,2) ;
    hold on
    y2 = cat(1,thisCtry_obsFire_total_M(12),thisCtry_obsFire_total_M,thisCtry_obsFire_total_M(1)) ;
    h_obs = plot(1:12,thisCtry_obsFire_total_M,'-r','LineWidth',3) ;
    plot(0:13,y2,'-r')
end

    
% Add legend
% legend(hp,'Cropland','Pasture','Other','Total', ...
%        'Location','NorthEastOutside')
if include_To
    H = [hp ; h_obs] ;
else
    H = hp ;
end
M = {'Cropland';'Pasture';'Other';'Total est.';'Total obs.'} ;
M = M(include_CPOTeTo) ;
legend(H,M, ...
       'Location','NorthEastOutside')

% Set figure settings
set(gca,'XLim',[-1 13], ...
        'XTick',1:12, ...
        'XTickLabel',{'Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.','Nov.','Dec.'}, ...
        'TickLength',[0 0])
set(gca, 'FontSize',20, ...
         'FontWeight','bold')
set(gcf, 'Color',[1 1 1])
% if normalizing
%     size(prctile(thisCtry_estFire_crop_Mt,2.5,2) / max(thisCtry_estFire_crop_Mt_mean))
%     tmp_forYmin = cat(2,prctile(thisCtry_estFire_crop_Mt,2.5,2) / max(thisCtry_estFire_crop_Mt_mean),... 
%                         prctile(thisCtry_estFire_past_Mt,2.5,2) / max(thisCtry_estFire_past_Mt_mean),...
%                         prctile(thisCtry_estFire_othr_Mt,2.5,2) / max(thisCtry_estFire_othr_Mt_mean) ) ;
%     tmp_forYmin = tmp_forYmin(:,include_CPOTe(1:3)) ;
%     tmp_forYmin = tmp_forYmin(:) ;
%     Ymin = min(0,min(tmp_forYmin(:))) ;
%     tmp_forYmax =  cat(2,prctile(thisCtry_estFire_crop_Mt,97.5,2) / max(thisCtry_estFire_crop_Mt_mean), ...
%                          prctile(thisCtry_estFire_past_Mt,97.5,2) / max(thisCtry_estFire_past_Mt_mean), ...
%                          prctile(thisCtry_estFire_othr_Mt,97.5,2) / max(thisCtry_estFire_othr_Mt_mean) ) ;
%     tmp_forYmax
%     tmp_forYmax = tmp_forYmax(:,include_CPOTe(1:3)) ;
%     tmp_forYmax
%     tmp_forYmax = tmp_forYmax(:) ;
%     Ymax = max(tmp_forYmax(:)) ;
%     set(gca,'YLim',[Ymin Ymax])
%     set(gca,'YTick',0:0.25:1)
% end
if min(get(gca,'YTick')) < 0
    % Plot zero-line
    hold on
    x2 = -1:13 ;
    plot(x2,zeros(size(x2)),'--k')
    hold off
end


end