function hf = CPObs__plotSeas_CeCoCMo( ...
                    thisCtry_estFire_crop_myt, ...
                    varargin)
                
default_normalizing = false ;
if ~isempty(varargin)
    thisCtry_obsFire_crop_my = varargin{1} ;
    thisCtry_obsFire_cropPLUSmos_my = varargin{2} ;
    if length(varargin) == 3
        normalizing = logical(varargin{3}) ;
    elseif length(varargin) == 2
        normalizing = default_normalizing ;
    else error('length(varargin) must be 0, 2, or 3.')
    end
else
    thisCtry_obsFire_crop_my = [] ;
    thisCtry_obsFire_cropPLUSmos_my = [] ;
    normalizing = default_normalizing ;
end

thisCtry_estFire_crop_Mt = squeeze(mean(thisCtry_estFire_crop_myt,2)) ;
thisCtry_estFire_crop_Mt_mean = mean(thisCtry_estFire_crop_Mt,2) ;

warning('Maybe bad way of doing confidence intervals? Maybe should account for SD for each month WITHIN EACH BOOTSTRAP RUN as well?')
thisCtry_estFire_crop_Mt_CIWlo = thisCtry_estFire_crop_Mt_mean - prctile(thisCtry_estFire_crop_Mt,2.5,2) ;
thisCtry_estFire_crop_Mt_CIWhi = prctile(thisCtry_estFire_crop_Mt,97.5,2) - thisCtry_estFire_crop_Mt_mean ;
thisCtry_estFire_crop_Mt_CIW = [thisCtry_estFire_crop_Mt_CIWlo thisCtry_estFire_crop_Mt_CIWhi] ;

% Normalize to max, if doing so
if normalizing
    thisCtry_estFire_crop_Mt_mean = thisCtry_estFire_crop_Mt_mean / max(thisCtry_estFire_crop_Mt_mean) ;
    thisCtry_estFire_crop_Mt_CIW = thisCtry_estFire_crop_Mt_CIW / max(thisCtry_estFire_crop_Mt_mean) ;
    if ~isempty(thisCtry_obsFire_crop_my)
        thisCtry_obsFire_crop_my = thisCtry_obsFire_crop_my / max(thisCtry_obsFire_crop_my) ;
    end
    if ~isempty(thisCtry_obsFire_cropPLUSmos_my)
        thisCtry_obsFire_cropPLUSmos_my = thisCtry_obsFire_cropPLUSmos_my / max(thisCtry_obsFire_cropPLUSmos_my) ;
    end
end

% Plot estimated CROP using boundedline()
hf = figure ;
x1 = 0:13 ;
y0 = thisCtry_estFire_crop_Mt_mean' ;
y1 = cat(2,y0(:,12),y0,y0(:,1)) ;
e1 = thisCtry_estFire_crop_Mt_CIW ;
% cmap = [171 197 193] / 255 ;
cmap = [155 187 89] / 255 ;
[hl,hp] = boundedline(1:12,y0,e1,'cmap',cmap,'transparency',0.5) ;
outlinebounds(hl,hp) ;

% Plot lines for extra months before and after
hold on
plot(x1,y1,'Color',cmap)
hold off
    
% Plot total observed fire: Cropland
if ~isempty(thisCtry_obsFire_crop_my)
    warning('Should I add error bars, or even include this in boundedlines()?')
    thisCtry_obsFire_crop_M = mean(thisCtry_obsFire_crop_my,2) ;
    hold on
    y2 = cat(1,thisCtry_obsFire_crop_M(12),thisCtry_obsFire_crop_M,thisCtry_obsFire_crop_M(1)) ;
    h_obs_crop = plot(1:12,thisCtry_obsFire_crop_M,':k') ;
    set(h_obs_crop,'Color',[0 92 21]/255,'LineWidth',3) ;
    tmp = plot(0:13,y2,':k') ;
    set(tmp,'Color',[0 92 21]/255) ;
    hold off
end

% Plot total observed fire: Cropland plus cropland-natural mosaic
if ~isempty(thisCtry_obsFire_cropPLUSmos_my)
    warning('Should I add error bars, or even include this in boundedlines()?')
    thisCtry_obsFire_cropPLUSmos_M = mean(thisCtry_obsFire_cropPLUSmos_my,2) ;
    hold on
    y2 = cat(1,thisCtry_obsFire_cropPLUSmos_M(12),thisCtry_obsFire_cropPLUSmos_M,thisCtry_obsFire_cropPLUSmos_M(1)) ;
    h_obs_cropPLUSmos = plot(1:12,thisCtry_obsFire_cropPLUSmos_M,'-k') ;
    set(h_obs_cropPLUSmos,'Color',[0 92 21]/255,'LineWidth',3) ;
    tmp = plot(0:13,y2,'-k') ;
    set(tmp,'Color',[0 92 21]/255) ;
    hold off
end

    
% Add legend
% legend(hp,'Cropland','Pasture','Other','Total', ...
%        'Location','NorthEastOutside')
incl_in_legend = true(3,1) ;
if ~isempty(thisCtry_obsFire_crop_my)
    H = [hp ; h_obs_crop] ;
else
    H = hp ;
    incl_in_legend(2) = false ;
end
if ~isempty(thisCtry_obsFire_cropPLUSmos_my)
    H = [H ; h_obs_cropPLUSmos] ;
else
    incl_in_legend(3) = false ;
end
M = {'Cropland (est.)';'Cropland (obs.)';'Cropland + nat. mosaic (obs.)';} ;
M = M(incl_in_legend) ;
legend(H,M, ...
       'Location','NorthEastOutside')

% Set figure settings
set(gca,'XLim',[-1 13], ...
        'XTick',1:12, ...
        'XTickLabel',{'Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.','Nov.','Dec.'}, ...
        'TickLength',[0 0])
set(gca, 'FontSize',20, ...
         'FontWeight','bold')
set(gcf, 'Color',[1 1 1])
if normalizing
    tmp_forYmin = [thisCtry_estFire_crop_Mt_mean - thisCtry_estFire_crop_Mt_CIW(:,1) ;
                   thisCtry_estFire_past_Mt_mean - thisCtry_estFire_past_Mt_CIW(:,1) ;
                   thisCtry_estFire_othr_Mt_mean - thisCtry_estFire_othr_Mt_CIW(:,1)] ;
    Ymin = min(0,min(tmp_forYmin(:))) ;
    tmp_forYmax = [thisCtry_estFire_crop_Mt_mean + thisCtry_estFire_crop_Mt_CIW(:,2) ;
                   thisCtry_estFire_past_Mt_mean + thisCtry_estFire_past_Mt_CIW(:,2) ;
                   thisCtry_estFire_othr_Mt_mean + thisCtry_estFire_othr_Mt_CIW(:,2)] ;
    Ymax = max(tmp_forYmax(:)) ;
    set(gca,'YLim',[Ymin Ymax])
    set(gca,'YTick',0:0.25:1)
end


end