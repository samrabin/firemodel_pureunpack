function hf = CPObs__plotTS_CPOTeTo( ...
                    thisCtry_estFire_crop_myt, ...
                    thisCtry_estFire_past_myt, ...
                    thisCtry_estFire_othr_myt, ...
                    thisCtry_estFire_total_myt, ...
                    thisCtry_obsFire_total_my, ...
                    years, ...
                    varargin)
                
Nruns = size(thisCtry_estFire_total_myt,3) ;
Nmonths = (size(thisCtry_estFire_total_myt,1)*size(thisCtry_estFire_total_myt,2)) ;
if Nmonths ~= 12*years.num
    error('Nmonths ~= 12*years.num')
end
                
default_include_CPOTeTo = true(5,1) ;
default_include_CPOTe = true(4,1) ;
default_include_To = true ;
default_normalizing = false ;
default_indiv_months = false ;

if ~isempty(varargin)
    include_CPOTeTo = logical(varargin{1}) ;
    include_CPOTe = include_CPOTeTo(1:4) ;
    include_To = include_CPOTeTo(5) ;
    
    if length(varargin) > 1
        normalizing = logical(varargin{2}) ;
        if length(varargin) > 2
            indiv_months = logical(varargin{3}) ;
        else
            indiv_months = default_indiv_months ;
        end
    else
        normalizing = default_normalizing ;
        indiv_months = default_indiv_months ;
    end
else
    include_CPOTeTo = default_include_CPOTeTo ;
    include_CPOTe = default_include_CPOTe ;
    include_To = default_include_To ;
    normalizing = default_normalizing ;
    indiv_months = default_indiv_months ;
end
clear default_*

% Get Z-by-t arrays, where Z is months or years depending on whether
% indiv_months == TRUE
if indiv_months
    thisCtry_estFire_crop_Zt = reshape(thisCtry_estFire_crop_myt,[Nmonths Nruns]) ;
    thisCtry_estFire_past_Zt = reshape(thisCtry_estFire_past_myt,[Nmonths Nruns]) ;
    thisCtry_estFire_othr_Zt = reshape(thisCtry_estFire_othr_myt,[Nmonths Nruns]) ;
    thisCtry_estFire_total_Zt = reshape(thisCtry_estFire_total_myt,[Nmonths Nruns]) ;
    thisCtry_obsFire_total_Z = thisCtry_obsFire_total_my(:) ; 
else
    thisCtry_estFire_crop_Zt = squeeze(sum(thisCtry_estFire_crop_myt,1)) ;
    thisCtry_estFire_past_Zt = squeeze(sum(thisCtry_estFire_past_myt,1)) ;
    thisCtry_estFire_othr_Zt = squeeze(sum(thisCtry_estFire_othr_myt,1)) ;
    thisCtry_estFire_total_Zt = squeeze(sum(thisCtry_estFire_total_myt,1)) ;
    thisCtry_obsFire_total_Z = sum(thisCtry_obsFire_total_my,1) ;
end

thisCtry_estFire_crop_Zt_mean = mean(thisCtry_estFire_crop_Zt,2) ;
thisCtry_estFire_past_Zt_mean = mean(thisCtry_estFire_past_Zt,2) ;
thisCtry_estFire_othr_Zt_mean = mean(thisCtry_estFire_othr_Zt,2) ;
thisCtry_estFire_total_Zt_mean = mean(thisCtry_estFire_total_Zt,2) ;

warning('Maybe bad way of doing confidence intervals? Maybe should account for SD for each month WITHIN EACH BOOTSTRAP RUN as well?')
thisCtry_estFire_crop_Zt_CIWlo = thisCtry_estFire_crop_Zt_mean - prctile(thisCtry_estFire_crop_Zt,2.5,2) ;
thisCtry_estFire_past_Zt_CIWlo = thisCtry_estFire_past_Zt_mean - prctile(thisCtry_estFire_past_Zt,2.5,2) ;
thisCtry_estFire_othr_Zt_CIWlo = thisCtry_estFire_othr_Zt_mean - prctile(thisCtry_estFire_othr_Zt,2.5,2) ;
thisCtry_estFire_total_Zt_CIWlo = thisCtry_estFire_total_Zt_mean - prctile(thisCtry_estFire_total_Zt,2.5,2) ;
thisCtry_estFire_crop_Zt_CIWhi = prctile(thisCtry_estFire_crop_Zt,97.5,2) - thisCtry_estFire_crop_Zt_mean ;
thisCtry_estFire_past_Zt_CIWhi = prctile(thisCtry_estFire_past_Zt,97.5,2) - thisCtry_estFire_past_Zt_mean ;
thisCtry_estFire_othr_Zt_CIWhi = prctile(thisCtry_estFire_othr_Zt,97.5,2) - thisCtry_estFire_othr_Zt_mean ;
thisCtry_estFire_total_Zt_CIWhi = prctile(thisCtry_estFire_total_Zt,97.5,2) - thisCtry_estFire_total_Zt_mean ;
thisCtry_estFire_crop_Zt_CIW = [thisCtry_estFire_crop_Zt_CIWlo thisCtry_estFire_crop_Zt_CIWhi] ;
thisCtry_estFire_past_Zt_CIW = [thisCtry_estFire_past_Zt_CIWlo thisCtry_estFire_past_Zt_CIWhi] ;
thisCtry_estFire_othr_Zt_CIW = [thisCtry_estFire_othr_Zt_CIWlo thisCtry_estFire_othr_Zt_CIWhi] ;
thisCtry_estFire_total_Zt_CIW = [thisCtry_estFire_total_Zt_CIWlo thisCtry_estFire_total_Zt_CIWhi] ;

% Normalize to max, if doing so
if normalizing
    thisCtry_estFire_crop_Zt_mean = thisCtry_estFire_crop_Zt_mean / max(thisCtry_estFire_crop_Zt_mean) ;
    thisCtry_estFire_crop_Zt_CIW = thisCtry_estFire_crop_Zt_CIW / max(thisCtry_estFire_crop_Zt_mean) ;
    thisCtry_estFire_past_Zt_mean = thisCtry_estFire_past_Zt_mean / max(thisCtry_estFire_past_Zt_mean) ;
    thisCtry_estFire_past_Zt_CIW = thisCtry_estFire_past_Zt_CIW / max(thisCtry_estFire_past_Zt_mean) ;
    thisCtry_estFire_othr_Zt_mean = thisCtry_estFire_othr_Zt_mean / max(thisCtry_estFire_othr_Zt_mean) ;
    thisCtry_estFire_othr_Zt_CIW = thisCtry_estFire_othr_Zt_CIW / max(thisCtry_estFire_othr_Zt_mean) ;
    thisCtry_estFire_total_Zt_mean = thisCtry_estFire_total_Zt_mean / max(thisCtry_estFire_total_Zt_mean) ;
    thisCtry_estFire_total_Zt_CIW = thisCtry_estFire_total_Zt_CIW / max(thisCtry_estFire_total_Zt_mean) ;
end

% Plot estimated CPOT using boundedline()
hf = figure ;
if indiv_months
    x1 = 1:Nmonths ;
    xticks = 1:12:Nmonths ;
else
    x1 = years.list ;
    xticks = years.list ;
end
y1 = [thisCtry_estFire_crop_Zt_mean';thisCtry_estFire_past_Zt_mean';thisCtry_estFire_othr_Zt_mean';thisCtry_estFire_total_Zt_mean'] ;
y1 = y1(include_CPOTe,:) ;   % Exclude curves
e1 = cat(3,thisCtry_estFire_crop_Zt_CIW,thisCtry_estFire_past_Zt_CIW,thisCtry_estFire_othr_Zt_CIW,thisCtry_estFire_total_Zt_CIW) ;
e1 = e1(:,:,include_CPOTe) ;   % Exclude curves
cmap = [155 187 89 ;
        146 122 181 ;
        250 168 70  ;
          0   0   0 ] / 255 ;
cmap = cmap(include_CPOTe,:) ;   % Exclude curves
[hl,hp] = boundedline(x1,y1,e1,'cmap',cmap,'transparency',0.5) ;
outlinebounds(hl,hp) ;
    
% Plot total observed fire
if include_To
    hold on
    h_obs = plot(x1,thisCtry_obsFire_total_Z,'-r','LineWidth',2) ;
    hold off
end

    
% Add legend
% legend(hp,'Cropland','Pasture','Other','Total', ...
%        'Location','NorthEastOutside')
if include_To
    H = [hp ; h_obs] ;
else
    H = hp ;
end
M = {'Cropland';'Pasture';'Other';'Total est.';'Total obs.'} ;
M = M(include_CPOTeTo) ;
legend(H,M, ...
       'Location','NorthEastOutside')

% Set figure settings
set(gca, ...
...%         'XLim',[-1 13], ...
        'XTick',xticks, ...
        'XTickLabel',cellfun(@num2str,num2cell(years.list),'UniformOutput',false), ...
        'TickLength',[0 0])
set(gca, 'FontSize',20, ...
         'FontWeight','bold')
set(gcf, 'Color',[1 1 1])
if normalizing
    tmp_forYmin = [thisCtry_estFire_crop_Zt_mean - thisCtry_estFire_crop_Zt_CIW(:,1) ;
                   thisCtry_estFire_past_Zt_mean - thisCtry_estFire_past_Zt_CIW(:,1) ;
                   thisCtry_estFire_othr_Zt_mean - thisCtry_estFire_othr_Zt_CIW(:,1)] ;
    Ymin = min(0,min(tmp_forYmin(:))) ;
    tmp_forYmax = [thisCtry_estFire_crop_Zt_mean + thisCtry_estFire_crop_Zt_CIW(:,2) ;
                   thisCtry_estFire_past_Zt_mean + thisCtry_estFire_past_Zt_CIW(:,2) ;
                   thisCtry_estFire_othr_Zt_mean + thisCtry_estFire_othr_Zt_CIW(:,2)] ;
    Ymax = max(tmp_forYmax(:)) ;
    set(gca,'YLim',[Ymin Ymax])
    set(gca,'YTick',0:0.25:1)
end

% Plot zero-line
if min(get(gca,'YTick')) < 0
    hold on
    plot(x1,zeros(size(x1)),'--k')
    hold off
end



end