function [thisCtry_LCdata_crop_mr, ...
    thisCtry_LCdata_past_mr, ...
    thisCtry_LCdata_othr_mr, ...
    thisCtry_obsFire_total_my, ...
    thisCtry_obsFire_crop_my, ...
    thisCtry_obsFire_cropPLUSmos_my] =...
    get_thisCtry_LCdataCPO_obsFire( ...
    thisCtry, thisCtry_whichReg, ...
    LCfrac_crop, LCfrac_past, ...
    obsFire, ...
    months,years,methods,regKey_bsruns, ...
    Marray_month, ...
    landarea_map,countries_map, ...
    varargin)

fprintf('%s','Making obsFire and LCdata arrays for this country... ')

if ~isempty(varargin)
    if length(varargin)==1 || length(varargin)>2
        error('varargin length must be 0, 2 ,or 3.')
    end
    obsFire_crop = varargin{1} ;
    obsFire_cropPLUSmos = varargin{2} ;
    if isempty(obsFire_crop)
        do_obsFire_crop = false ;
    else do_obsFire_crop = true ;
    end
    if isempty(obsFire_cropPLUSmos)
        do_obsFire_cropPLUSmos = false ;
    else do_obsFire_cropPLUSmos = true ;
    end
    if length(varargin)==3
        if ~strcmp(methods.normType,'none')
            fire_normConstant = varargin{3} ;
        else
            warning('fire_normConstant provided, but ignored because methods.normType==''none''')
        end
    elseif ~strcmp(methods.normType,'none')
        error(['methods.normType==''' methods.normType ''', but fire_normConstant not provided.'])
    end
else
    do_obsFire_crop = false ;
    do_obsFire_cropPLUSmos = false ;
end

% Find elements in this country
countries_Marray = repmat(countries_map,[1 1 months.num]) ;
landarea_Marray = repmat(landarea_map,[1 1 months.num]) ;
countries_vec = countries_Marray(~isnan(landarea_Marray)) ;
inThisCtry = (countries_vec==thisCtry) ;
thisCtry_Ncells_1mth = length(find(countries_map==thisCtry)) ;

% Extract things for this country
thisCtry_LCfrac_crop = LCfrac_crop(inThisCtry) ;
thisCtry_LCfrac_past = LCfrac_past(inThisCtry) ;
thisCtry_LCfrac_othr = 1 - (thisCtry_LCfrac_crop + thisCtry_LCfrac_past) ;
thisCtry_obsFire = obsFire(inThisCtry) ;
thisCtry_obsFire_crop = obsFire_crop(inThisCtry) ;
thisCtry_obsFire_cropPLUSmos = obsFire_cropPLUSmos(inThisCtry) ;

% thisCtry_Marray_month = Marray_month(inThisCtry) ;
thisCtry_regKey = regKey_bsruns(inThisCtry) ;
thisCtry_regKey_just1mth = thisCtry_regKey(1:(length(thisCtry_regKey)/months.num)) ;
thisCtry_Ncells = length(thisCtry_regKey_just1mth) ;
landarea_vec_just1mth = landarea_map(countries_map==thisCtry) ;
clear landarea_map landarea_Marray countries

% Make obsFire arrays
thisCtry_obsFire_xmy = reshape(thisCtry_obsFire, ...
    [thisCtry_Ncells_1mth 12 years.num]) ;
thisCtry_obsFire_total_my = squeeze(sum(thisCtry_obsFire_xmy,1)) ;
clear thisCtry_obsFire_xmy
if do_obsFire_crop
%     size(thisCtry_obsFire_crop)
%     [thisCtry_Ncells_1mth 12 years.num]
    thisCtry_obsFire_crop_xmy = reshape(thisCtry_obsFire_crop, ...
        [thisCtry_Ncells_1mth 12 years.num]) ;
    thisCtry_obsFire_crop_my = squeeze(sum(thisCtry_obsFire_crop_xmy,1)) ;
    clear thisCtry_obsFire_crop_xmy
else
    thisCtry_obsFire_crop_my = [] ;
end
if do_obsFire_cropPLUSmos
    thisCtry_obsFire_cropPLUSmos_xmy = reshape(thisCtry_obsFire_cropPLUSmos, ...
        [thisCtry_Ncells_1mth 12 years.num]) ;
    thisCtry_obsFire_cropPLUSmos_my = squeeze(sum(thisCtry_obsFire_cropPLUSmos_xmy,1)) ;
    clear thisCtry_obsFire_cropPLUSmos_xmy
else
    thisCtry_obsFire_cropPLUSmos_my = [] ;
end

% Set up empty LCarea arrays
thisCtry_numRegs = length(thisCtry_whichReg) ;
LCarea_crop_mr = nan(months.num,thisCtry_numRegs,'single') ;
LCarea_past_mr = nan(months.num,thisCtry_numRegs,'single') ;
LCarea_othr_mr = nan(months.num,thisCtry_numRegs,'single') ;

% Make LCdata arrays
LCfrac_crop_xm = reshape(thisCtry_LCfrac_crop, [thisCtry_Ncells months.num]) ;
LCfrac_past_xm = reshape(thisCtry_LCfrac_past, [thisCtry_Ncells months.num]) ;
LCfrac_othr_xm = reshape(thisCtry_LCfrac_othr, [thisCtry_Ncells months.num]) ;

landarea_vec_xm = repmat(landarea_vec_just1mth,[1 months.num]) ;
LCarea_crop_xm = LCfrac_crop_xm .* landarea_vec_xm ;
LCarea_past_xm = LCfrac_past_xm .* landarea_vec_xm ;
LCarea_othr_xm = LCfrac_othr_xm .* landarea_vec_xm ;
clear landarea_vec_xm

thisCtry_regKey_xm = repmat(thisCtry_regKey_just1mth,[1 months.num]) ;

for r = 1:thisCtry_numRegs
    thisReg = thisCtry_whichReg(r) ;
    inThisReg = (thisCtry_regKey_xm==thisReg) ;
    inThisReg_num = length(find(inThisReg)) ;
    tmp = reshape(LCarea_crop_xm(inThisReg),[inThisReg_num/months.num months.num]) ;
    LCarea_crop_mr(:,r) = sum(tmp,1) ;
    clear tmp
    tmp = reshape(LCarea_past_xm(inThisReg),[inThisReg_num/months.num months.num]) ;
    LCarea_past_mr(:,r) = sum(tmp,1) ;
    clear tmp
    tmp = reshape(LCarea_othr_xm(inThisReg),[inThisReg_num/months.num months.num]) ;
    LCarea_othr_mr(:,r) = sum(tmp,1) ;
    clear tmp
    
end ; clear r thisReg
clear LCarea_*_xm thisCtry_regKey_xm


thisCtryRegs_landarea_sum_xr = nan(1,thisCtry_numRegs,'single') ;
thisCtryRegs_numcells_xr = nan(1,thisCtry_numRegs,'single') ;
for r = 1:thisCtry_numRegs
    thisReg = thisCtry_whichReg(r) ;
    thisReg_landarea_vec = landarea_vec_just1mth(thisCtry_regKey_just1mth==thisReg) ;
    thisCtryRegs_landarea_sum_xr(r) = sum(thisReg_landarea_vec) ;
    thisCtryRegs_numcells_xr(r) = length(find(thisCtry_regKey_just1mth==r)) ;
end ; clear r thisReg_landarea_vec
thisCtryRegs_landarea_sum_mr = repmat(thisCtryRegs_landarea_sum_xr,[months.num 1]) ;

% Error check
if ~isempty(find(LCarea_crop_mr<0,1))
    fprintf('%s\n',' ')
    tmp = length(find(LCarea_crop_mr<0)) ;
    error([num2str(tmp) ' negative element(s) in LCarea_crop_mr.'])
elseif ~isempty(find(LCarea_past_mr<0,1))
    fprintf('%s\n',' ')
    tmp = length(find(LCarea_past_mr<0)) ;
    error([num2str(tmp) ' negative element(s) in LCarea_past_mr.'])
elseif ~isempty(find(LCarea_othr_mr<0,1))
    fprintf('%s\n',' ')
    tmp = length(find(LCarea_othr_mr<0)) ;
    error([num2str(tmp) ' negative element(s) in LCarea_othr_mr.'])
end

if strcmp(methods.coverType,'prop')
    thisCtryRegs_numcells_mr = repmat(thisCtryRegs_numcells_xr,[months.num 1]) ;
    thisCtry_LCdata_crop_mr = (LCarea_crop_mr ./ thisCtryRegs_landarea_sum_mr) .* thisCtryRegs_numcells_mr ;
    thisCtry_LCdata_past_mr = (LCarea_past_mr ./ thisCtryRegs_landarea_sum_mr) .* thisCtryRegs_numcells_mr ;
    thisCtry_LCdata_othr_mr = (LCarea_othr_mr ./ thisCtryRegs_landarea_sum_mr) .* thisCtryRegs_numcells_mr ;
elseif strcmp(methods.coverType,'area')
    thisCtry_LCdata_crop_mr = LCarea_crop_mr ;
    thisCtry_LCdata_past_mr = LCarea_past_mr ;
    thisCtry_LCdata_othr_mr = LCarea_othr_mr ;
end

% Apply fire_normConstant, if necessary
if ~strcmp(methods.normType,'none')
    thisCtry_LCdata_crop_mr = thisCtry_LCdata_crop_mr .* fire_normConstant_mr ;
    thisCtry_LCdata_past_mr = thisCtry_LCdata_past_mr .* fire_normConstant_mr ;
    thisCtry_LCdata_othr_mr = thisCtry_LCdata_othr_mr .* fire_normConstant_mr ;
end

fprintf('%s\n','Done.')


end