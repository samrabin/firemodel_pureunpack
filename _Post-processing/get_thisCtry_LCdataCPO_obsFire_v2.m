function [thisCtry_LCdata_crop_mr, ...
    thisCtry_LCdata_past_mr, ...
    thisCtry_LCdata_othr_mr, ...
    thisCtry_obsFire_myr, ...
    thisCtry_obsFire_total_my, ...
    thisCtry_obsFire, ...
    thisCtry_obsFire_perLC, ...
    thisCtry_obsFire_perLC_my, ...
    thisCtry_obsFire_perLC_myr, ...
    thisCtry_LCdata_crop, ...
    thisCtry_LCdata_past, ...
    thisCtry_LCdata_othr] =...
        get_thisCtry_LCdataCPO_obsFire_v2( ...
            thisCtry, thisCtry_whichReg, ...
            LCfrac_crop, LCfrac_past, ...
            obsFire, regions_bsruns,...
            months,years,methods,regKey_bsruns, ...
            landarea,countries_map, ...
            varargin)

fprintf('%s','Making obsFire and LCdata arrays for this country... ')

if ~isempty(varargin)
    if length(varargin) ~= 2
        error('Number of optional arguments must be 0 or 2.')
    end
    obsFire_perLC = varargin{1} ;
    if strcmp(methods.landcover,'hyde')
        obsFire_crop = obsFire_perLC{1} ;
        obsFire_cropPLUSmos = obsFire_perLC{2} ;
        obsFire_fakeC = [] ;
        obsFire_fakeP = [] ;
        obsFire_fakeO = [] ;
    elseif strcmp(methods.landcover,'fake_3member')
        obsFire_fakeC = obsFire_perLC{1} ;
        obsFire_fakeP = obsFire_perLC{2} ;
        obsFire_fakeO = obsFire_perLC{3} ;
        obsFire_crop = [] ;
        obsFire_cropPLUSmos = [] ;
        clear obsFire_perLC
    else error('methods.landcover improperly specified.')
    end
    
    fire_normConstant = varargin{2} ;
    if strcmp(methods.normType,'none') && ~isempty(fire_normConstant) && methods.ignoreNB==0
        warning('fire_normConstant provided, but ignored because methods.normType==''none'' and methods.ignoreNB==0')
    elseif ~strcmp(methods.normType,'none') && isempty(fire_normConstant)
        error(['methods.normType==''' methods.normType ''', but fire_normConstant not provided.'])
    end
else
    obsFire_crop = [] ;
    obsFire_cropPLUSmos = [] ;
    obsFire_fakeC = [] ;
end

% Find elements in this country
countries_Marray = repmat(countries_map,[1 1 months.num]) ;
countries_vec = countries_Marray(~isnan(regions_bsruns.Marray)) ;
inThisCtry = (countries_vec==thisCtry) ;
thisCtry_Ncells_1mth = length(find(inThisCtry))/months.num ;

% Get misc. things for this country
thisCtry_regKey = regKey_bsruns(inThisCtry) ;
thisCtry_regKey_just1mth = thisCtry_regKey(1:(length(thisCtry_regKey)/months.num)) ;
thisCtry_Ncells = length(thisCtry_regKey_just1mth) ;
thisCtry_landarea = landarea(inThisCtry) ;
clear landarea countries
thisCtry_numRegs = length(thisCtry_whichReg) ;


%%%%%%%%%%%%%%%
%%% obsFire %%%
%%%%%%%%%%%%%%%

% Get obsFire for this country
thisCtry_obsFire = obsFire(inThisCtry) ;
if ~isempty(obsFire_crop)
    thisCtry_obsFire_crop = obsFire_crop(inThisCtry) ;
end
if ~isempty(obsFire_cropPLUSmos)
    thisCtry_obsFire_cropPLUSmos = obsFire_cropPLUSmos(inThisCtry) ;
end
if ~isempty(obsFire_fakeC)
    thisCtry_obsFire_fakeC = obsFire_fakeC(inThisCtry) ;
    thisCtry_obsFire_fakeP = obsFire_fakeP(inThisCtry) ;
    thisCtry_obsFire_fakeO = obsFire_fakeO(inThisCtry) ;
    clear obsFire_fakeP obsFire_fakeO
end

% Make obsFire arrays
thisCtry_obsFire_xmy = reshape(thisCtry_obsFire, ...
    [thisCtry_Ncells_1mth 12 years.num]) ;
thisCtry_obsFire_myr = nan(12,years.num,thisCtry_numRegs) ;
for r = 1:thisCtry_numRegs
    thisReg = thisCtry_whichReg(r) ;
    thisCtry_obsFire_myr(:,:,r) = squeeze(sum(thisCtry_obsFire_xmy(thisCtry_regKey_just1mth==thisReg,:,:),1)) ;
end
thisCtry_obsFire_total_my = squeeze(sum(thisCtry_obsFire_xmy,1)) ;
clear thisCtry_obsFire_xmy
if strcmp(methods.landcover,'hyde')
    if ~isempty(obsFire_crop)
        thisCtry_obsFire_crop_xmy = reshape(thisCtry_obsFire_crop, ...
            [thisCtry_Ncells_1mth 12 years.num]) ;
        thisCtry_obsFire_crop_my = squeeze(sum(thisCtry_obsFire_crop_xmy,1)) ;
        clear thisCtry_obsFire_crop_xmy
    else
        thisCtry_obsFire_crop_my = [] ;
    end
    if ~isempty(obsFire_cropPLUSmos)
        thisCtry_obsFire_cropPLUSmos_xmy = reshape(thisCtry_obsFire_cropPLUSmos, ...
            [thisCtry_Ncells_1mth 12 years.num]) ;
        thisCtry_obsFire_cropPLUSmos_my = squeeze(sum(thisCtry_obsFire_cropPLUSmos_xmy,1)) ;
        clear thisCtry_obsFire_cropPLUSmos_xmy
    else
        thisCtry_obsFire_cropPLUSmos_my = [] ;
    end
    thisCtry_obsFire_perLC_my{1} = thisCtry_obsFire_crop_my ;
    thisCtry_obsFire_perLC_my{2} = thisCtry_obsFire_cropPLUSmos_my ;
elseif strcmp(methods.landcover,'fake_3member')
    if ~isempty(obsFire_fakeC)
        thisCtry_obsFire_fakeC_xmy = reshape(thisCtry_obsFire_fakeC, ...
            [thisCtry_Ncells_1mth 12 years.num]) ;
        thisCtry_obsFire_fakeC_my = squeeze(sum(thisCtry_obsFire_fakeC_xmy,1)) ;
        thisCtry_obsFire_fakeP_xmy = reshape(thisCtry_obsFire_fakeP, ...
            [thisCtry_Ncells_1mth 12 years.num]) ;
        thisCtry_obsFire_fakeP_my = squeeze(sum(thisCtry_obsFire_fakeP_xmy,1)) ;
        thisCtry_obsFire_fakeO_xmy = reshape(thisCtry_obsFire_fakeO, ...
            [thisCtry_Ncells_1mth 12 years.num]) ;
        thisCtry_obsFire_fakeO_my = squeeze(sum(thisCtry_obsFire_fakeO_xmy,1)) ;
        thisCtry_obsFire_fakeC_myr = nan(12,years.num,thisCtry_numRegs,'single') ;
        thisCtry_obsFire_fakeP_myr = nan(12,years.num,thisCtry_numRegs,'single') ;
        thisCtry_obsFire_fakeO_myr = nan(12,years.num,thisCtry_numRegs,'single') ;
        for r = 1:thisCtry_numRegs
            thisReg = thisCtry_whichReg(r) ;
%             disp(num2str(r))
%             size(thisCtry_obsFire_fakeC_xmy(thisCtry_regKey_just1mth==thisReg,:,:))
%             size(sum(thisCtry_obsFire_fakeC_xmy(thisCtry_regKey_just1mth==thisReg,:,:),1))
%             size(squeeze(sum(thisCtry_obsFire_fakeC_xmy(thisCtry_regKey_just1mth==thisReg,:,:),1)))
%             size(thisCtry_obsFire_fakeC_myr(:,:,r))
            thisCtry_obsFire_fakeC_myr(:,:,r) = squeeze(sum(thisCtry_obsFire_fakeC_xmy(thisCtry_regKey_just1mth==thisReg,:,:),1)) ;
            thisCtry_obsFire_fakeP_myr(:,:,r) = squeeze(sum(thisCtry_obsFire_fakeP_xmy(thisCtry_regKey_just1mth==thisReg,:,:),1)) ;
            thisCtry_obsFire_fakeO_myr(:,:,r) = squeeze(sum(thisCtry_obsFire_fakeO_xmy(thisCtry_regKey_just1mth==thisReg,:,:),1)) ;
        end
    else
        thisCtry_obsFire_fakeC = [] ;
        thisCtry_obsFire_fakeP = [] ;
        thisCtry_obsFire_fakeO = [] ;
        thisCtry_obsFire_fakeC_my = [] ;
        thisCtry_obsFire_fakeP_my = [] ;
        thisCtry_obsFire_fakeO_my = [] ;
        thisCtry_obsFire_fakeC_myr = [] ;
        thisCtry_obsFire_fakeP_myr = [] ;
        thisCtry_obsFire_fakeO_myr = [] ;
    end
    thisCtry_obsFire_perLC{1} = thisCtry_obsFire_fakeC ;
    thisCtry_obsFire_perLC{2} = thisCtry_obsFire_fakeP ;
    thisCtry_obsFire_perLC{3} = thisCtry_obsFire_fakeO ;
    thisCtry_obsFire_perLC_my{1} = thisCtry_obsFire_fakeC_my ;
    thisCtry_obsFire_perLC_my{2} = thisCtry_obsFire_fakeP_my ;
    thisCtry_obsFire_perLC_my{3} = thisCtry_obsFire_fakeO_my ;
    thisCtry_obsFire_perLC_myr{1} = thisCtry_obsFire_fakeC_myr ;
    thisCtry_obsFire_perLC_myr{2} = thisCtry_obsFire_fakeP_myr ;
    thisCtry_obsFire_perLC_myr{3} = thisCtry_obsFire_fakeO_myr ;
end



%%%%%%%%%%%%%%
%%% LCdata %%%
%%%%%%%%%%%%%%

% Get LCdata for this country
thisCtry_LCfrac_crop = LCfrac_crop(inThisCtry) ;
thisCtry_LCfrac_past = LCfrac_past(inThisCtry) ;
thisCtry_LCfrac_othr = 1 - (thisCtry_LCfrac_crop + thisCtry_LCfrac_past) ;
if strcmp(methods.coverType,'prop')
    thisCtry_LCdata_crop = thisCtry_LCfrac_crop ;
    thisCtry_LCdata_past = thisCtry_LCfrac_past ;
    thisCtry_LCdata_othr = thisCtry_LCfrac_othr ;
elseif strcmp(methods.coverType,'area')
    thisCtry_LCdata_crop = thisCtry_LCfrac_crop .* thisCtry_landarea ;
    thisCtry_LCdata_past = thisCtry_LCfrac_past .* thisCtry_landarea ;
    thisCtry_LCdata_othr = thisCtry_LCfrac_othr .* thisCtry_landarea ;
end
if ~strcmp(methods.normType,'none')
    thisCtry_fire_normConstant = fire_normConstant(inThisCtry) ;
    thisCtry_LCdata_crop = thisCtry_LCdata_crop .* thisCtry_fire_normConstant ;
    thisCtry_LCdata_past = thisCtry_LCdata_past .* thisCtry_fire_normConstant ;
    thisCtry_LCdata_othr = thisCtry_LCdata_othr .* thisCtry_fire_normConstant ;
elseif methods.ignoreNB==1
    thisCtry_fire_normConstant = fire_normConstant(inThisCtry) ;
    thisCtry_LCdata_crop(thisCtry_fire_normConstant==0) = 0 ;
    thisCtry_LCdata_past(thisCtry_fire_normConstant==0) = 0 ;
    thisCtry_LCdata_othr(thisCtry_fire_normConstant==0) = 0 ;
    warning('thisCtry_LCdata only includes that from ever-burned cells.')
end

% Set up empty LCarea arrays
LCarea_crop_mr = nan(months.num,thisCtry_numRegs,'single') ;
LCarea_past_mr = nan(months.num,thisCtry_numRegs,'single') ;
LCarea_othr_mr = nan(months.num,thisCtry_numRegs,'single') ;

% Make LCdata_xm arrays
LCdata_crop_xm = reshape(thisCtry_LCdata_crop, [thisCtry_Ncells months.num]) ;
LCdata_past_xm = reshape(thisCtry_LCdata_past, [thisCtry_Ncells months.num]) ;
LCdata_othr_xm = reshape(thisCtry_LCdata_othr, [thisCtry_Ncells months.num]) ;
thisCtry_regKey_xm = repmat(thisCtry_regKey_just1mth,[1 months.num]) ;
thisCtry_landarea_xm = reshape(thisCtry_landarea,[thisCtry_Ncells months.num]) ;
thisCtryRegs_landarea_sum_rm = nan([thisCtry_numRegs months.num],'single') ;
% thisCtryRegs_numcells_0r = nan(1,thisCtry_numRegs,'single') ;
for r = 1:thisCtry_numRegs
    thisReg = thisCtry_whichReg(r) ;
    inThisReg = (thisCtry_regKey_xm==thisReg) ;
    inThisReg_num = length(find(inThisReg)) ;
    tmp = reshape(LCdata_crop_xm(inThisReg),[inThisReg_num/months.num months.num]) ;
    LCarea_crop_mr(:,r) = sum(tmp,1) ;
    clear tmp
    tmp = reshape(LCdata_past_xm(inThisReg),[inThisReg_num/months.num months.num]) ;
    LCarea_past_mr(:,r) = sum(tmp,1) ;
    clear tmp
    tmp = reshape(LCdata_othr_xm(inThisReg),[inThisReg_num/months.num months.num]) ;
    LCarea_othr_mr(:,r) = sum(tmp,1) ;
    clear tmp
    
    thisReg_landarea_xm = thisCtry_landarea_xm(inThisReg) ;
    thisCtryRegs_landarea_sum_rm(r,:) = sum(thisReg_landarea_xm,1) ;
%     thisCtryRegs_numcells_0r(r) = length(find(thisCtry_regKey_just1mth==r)) ;
    
end ; clear r thisReg_landarea_vec thisCtry_regKey_xm
clear LCarea_*_xm

% Error check
if ~isempty(find(LCarea_crop_mr<0,1))
    fprintf('%s\n',' ')
    tmp = length(find(LCarea_crop_mr<0)) ;
    error([num2str(tmp) ' negative element(s) in LCarea_crop_mr.'])
elseif ~isempty(find(LCarea_past_mr<0,1))
    fprintf('%s\n',' ')
    tmp = length(find(LCarea_past_mr<0)) ;
    error([num2str(tmp) ' negative element(s) in LCarea_past_mr.'])
elseif ~isempty(find(LCarea_othr_mr<0,1))
    fprintf('%s\n',' ')
    tmp = length(find(LCarea_othr_mr<0)) ;
    error([num2str(tmp) ' negative element(s) in LCarea_othr_mr.'])
end

disp('Completing...')
if strcmp(methods.coverType,'prop')
    thisCtryRegs_numcells_mr = repmat(thisCtryRegs_numcells_xr,[months.num 1]) ;
    thisCtryRegs_landarea_sum_mr = permute(thisCtryRegs_landarea_sum_rm,[2 1]) ;
    thisCtry_LCdata_crop_mr = (LCarea_crop_mr ./ thisCtryRegs_landarea_sum_mr) .* thisCtryRegs_numcells_mr ;
    thisCtry_LCdata_past_mr = (LCarea_past_mr ./ thisCtryRegs_landarea_sum_mr) .* thisCtryRegs_numcells_mr ;
    thisCtry_LCdata_othr_mr = (LCarea_othr_mr ./ thisCtryRegs_landarea_sum_mr) .* thisCtryRegs_numcells_mr ;
elseif strcmp(methods.coverType,'area')
    thisCtry_LCdata_crop_mr = LCarea_crop_mr ;
    thisCtry_LCdata_past_mr = LCarea_past_mr ;
    thisCtry_LCdata_othr_mr = LCarea_othr_mr ;
end

fprintf('%s\n','Done.')


end