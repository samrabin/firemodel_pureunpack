function [thisCtry_estFire_crop_myt,thisCtry_estFire_past_myt, ...
          thisCtry_estFire_othr_myt,thisCtry_estFire_total_myt, ...
          thisCtry_estFire_crop_myrt,thisCtry_estFire_past_myrt, ...
          thisCtry_estFire_othr_myrt,thisCtry_estFire_total_myrt] = ...
    get_thisCtry_estFireCPO(months,years,methods, ...
                            thisCtry_Fc_mrt,thisCtry_Fp_mrt,thisCtry_Fo_mrt, ...
                            thisCtry_LCdata_crop_mr,thisCtry_LCdata_past_mr,thisCtry_LCdata_othr_mr)

    % Setup
    thisCtry_numRegs = size(thisCtry_Fc_mrt,2) ;
    Nruns = size(thisCtry_Fc_mrt,3) ;
    
    % Stretch out Fk_mr if necessary
    if methods.efficient_Fs ~= 0
        thisCtry_Fc_mrt = repmat(thisCtry_Fc_mrt,[years.num 1 1]) ;
        thisCtry_Fp_mrt = repmat(thisCtry_Fp_mrt,[years.num 1 1]) ;
        thisCtry_Fo_mrt = repmat(thisCtry_Fo_mrt,[years.num 1 1]) ;
    end
    
    % Set up empty arrays for estFire results
    thisCtry_estFire_crop_mrt = nan(months.num,thisCtry_numRegs,Nruns,'single') ;
    thisCtry_estFire_past_mrt = nan(months.num,thisCtry_numRegs,Nruns,'single') ;
    thisCtry_estFire_othr_mrt = nan(months.num,thisCtry_numRegs,Nruns,'single') ;

    fprintf('%s','Calculating BA for each bootstrapping run... ')
    for t = 1:Nruns

        % Choose Fk_mr
        Fc_mr = thisCtry_Fc_mrt(:,:,t) ;
        Fp_mr = thisCtry_Fp_mrt(:,:,t) ;
        Fo_mr = thisCtry_Fo_mrt(:,:,t) ;

        % There should be no NaNs after having corrected for Fk NaNs due to
        % non-presence.
        if ~isempty(find(isnan(Fc_mr),1))
            error('Some value of Fc_mr is NaN.')
        elseif ~isempty(find(isnan(Fp_mr),1))
            error('Some value of Fp_mr is NaN.')
        elseif ~isempty(find(isnan(Fo_mr),1))
            error('Some value of Fo_mr is NaN.')
        end

        % Calculate estimated amount of fire
        estFireTmp_crop = Fc_mr .* thisCtry_LCdata_crop_mr ;
        estFireTmp_past = Fp_mr .* thisCtry_LCdata_past_mr ;
        estFireTmp_othr = Fo_mr .* thisCtry_LCdata_othr_mr ;

        % De-normalization happened in get_thisCtry_LCdata function

        % Re-distribute negative fire, if doing so
        if methods.redistNegOTHR == 1
            error('Must update previous code to generate thisCtry_LCfrac_k, and this code to use it.')
            [tmpC, tmpP, tmpO] = reDistNeg_pureUnpack_CPO( ...
                estFireTmp_crop(:), estFireTmp_past(:), estFireTmp_othr(:), ...
                LCfrac_crop(:),  LCfrac_past(:),  LCfrac_othr(:)) ;
            estFireTmp_crop = reshape(tmpC,size(estFireTmp_crop)) ;
            estFireTmp_past = reshape(tmpP,size(estFireTmp_past)) ;
            estFireTmp_othr = reshape(tmpO,size(estFireTmp_othr)) ;
            clear tmpC tmpP tmpO
        end

        % Save
        thisCtry_estFire_crop_mrt(:,:,t) = estFireTmp_crop ;
        thisCtry_estFire_past_mrt(:,:,t) = estFireTmp_past ;
        thisCtry_estFire_othr_mrt(:,:,t) = estFireTmp_othr ;

        clear estFireTmp_*

    end ; clear t
    
    % Convert _mrt to _myrt arrays
    thisCtry_estFire_crop_myrt = reshape(thisCtry_estFire_crop_mrt,[12 years.num thisCtry_numRegs Nruns]) ;
    thisCtry_estFire_past_myrt = reshape(thisCtry_estFire_past_mrt,[12 years.num thisCtry_numRegs Nruns]) ;
    thisCtry_estFire_othr_myrt = reshape(thisCtry_estFire_othr_mrt,[12 years.num thisCtry_numRegs Nruns]) ;
    thisCtry_estFire_total_myrt = thisCtry_estFire_crop_myrt + thisCtry_estFire_past_myrt + thisCtry_estFire_othr_myrt ;

    % Calculate total numbers
    thisCtry_estFire_crop_mt = squeeze(sum(thisCtry_estFire_crop_mrt,2)) ;
    thisCtry_estFire_past_mt = squeeze(sum(thisCtry_estFire_past_mrt,2)) ;
    thisCtry_estFire_othr_mt = squeeze(sum(thisCtry_estFire_othr_mrt,2)) ;
    thisCtry_estFire_total_mt = thisCtry_estFire_crop_mt + thisCtry_estFire_past_mt + thisCtry_estFire_othr_mt ;
    
    % Convert _mt to _myt arrays
    thisCtry_estFire_crop_myt = reshape(thisCtry_estFire_crop_mt,[12 years.num Nruns]) ;
    thisCtry_estFire_past_myt = reshape(thisCtry_estFire_past_mt,[12 years.num Nruns]) ;
    thisCtry_estFire_othr_myt = reshape(thisCtry_estFire_othr_mt,[12 years.num Nruns]) ;
    thisCtry_estFire_total_myt = reshape(thisCtry_estFire_total_mt,[12 years.num Nruns]) ;
    
    


    fprintf('%s\n','Done.')