% prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_PNmean_randerson' ;
% prefix = 'REGv5_allowNegFk_effFk_no0s_AA_NOprenorm_randerson' ;
prefix = 'REGv5_allowNegFk_allFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmaxm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmeanSNB0_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmeanSNB0_exclNB_randerson' ;

% % % prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randersonCemis' ;
% prefix = 'REGv5_allowNegFk_effFk_no0s_AA_NOprenorm_randersonCemis' ; warning('Still not doing all Fk''s')

inDir = '~/Geodata/Unpacking_bootstrap_analysis/Output/' ;
outDir = '/Users/sam/Documents/Dropbox/4th-year talk/Figures/' ;

%%% What years are we working with?
% years.list = 2003:2008 ;
% years.list = 2000:2009 ;   % To match Magi et al. (2012). Extent of HYDE 3.1.
years.list = 2001:2009 ;   % Overlap of HYDE 3.1 and Randerson.
% years.list = 2001:2003 ;   % To match Korontzi et al. (2006)


%% (1) Import bootstrapping run results (and get Methods)
pause(1)
disp('Importing parameter sets from bootstrapping runs...')

% Make empty datasets
allReg_Fc_mrt = [] ;
allReg_Fp_mrt = [] ;
allReg_Fo_mrt = [] ;

f = 1 ;
disp(['Loading file ' num2str(f) '...'])
filename = [inDir prefix '_' num2str(f) '.mat'] ;
thisFile = load(filename) ;
if f == 1
    methods = thisFile.methods_out ;
    if ~isfield(methods,'normStyle')
        methods.normStyle = 1 ;
    end
end

tmp = ~isnan(squeeze(thisFile.allReg_Fc_mrt(1,1,:))) ;
tmp2 = thisFile.allReg_Fc_mrt(:,:,tmp) ;
tmp3 = thisFile.exclC_mrt(:,:,tmp) ;
tmp2(isnan(tmp2) & tmp3) = 0 ;
allReg_Fc_mrt = cat(3,allReg_Fc_mrt,tmp2) ;
clear tmp tmp2 tmp3
tmp = ~isnan(squeeze(thisFile.allReg_Fp_mrt(1,1,:))) ;
tmp2 = thisFile.allReg_Fp_mrt(:,:,tmp) ;
tmp3 = thisFile.exclP_mrt(:,:,tmp) ;
tmp2(isnan(tmp2) & tmp3) = 0 ;
allReg_Fp_mrt = cat(3,allReg_Fp_mrt,tmp2) ;
clear tmp tmp2 tmp3
tmp = ~isnan(squeeze(thisFile.allReg_Fo_mrt(1,1,:))) ;
tmp2 = thisFile.allReg_Fo_mrt(:,:,tmp) ;
tmp3 = thisFile.exclO_mrt(:,:,tmp) ;
tmp2(isnan(tmp2) & tmp3) = 0 ;
allReg_Fo_mrt = cat(3,allReg_Fo_mrt,tmp2) ;
if size(tmp2,3) < thisFile.methods_out.trainRuns
    warning(['This run not complete: Only ' num2str(size(tmp2,3)) ' of ' num2str(thisFile.methods_out.trainRuns) ' runs.'])
end
clear tmp tmp2 tmp3

if isfield(thisFile,'toc_allruns')
    if f==1
        show_allruns_time = true ;
        toc_allruns = thisFile.toc_allruns ;
    else
        toc_allruns = toc_allruns + thisFile.toc_allruns ;
    end
else
    show_allruns_time = false ;
end

clear thisFile filename

disp('Done.') ; disp(' ')

if isempty(allReg_Fc_mrt)
    error('Could not find any data.')
end

Nruns = size(allReg_Fc_mrt,3) ;

if show_allruns_time
    disp([num2str(Nruns) ' runs in ' num2str(roundn(toc_allruns/3600,-1)) ' hours of total computing time.']) ; disp(' ')
end


%% (2) Setup

% Working directory and path
cd '/Users/sam/Documents/Dropbox/Projects/FireModel_pureUnpack'
addpath(genpath(pwd))
addpath(genpath('/Users/sam/Geodata/'))
addpath(genpath('/Users/sam/Documents/Dropbox/Projects/FireModel_daily/support'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices_s00 = (years.start - 1999):(years.end - 1999) ;
years.indices_s01 = (years.start - 2000):(years.end - 2000) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start_s00 = (years.start-2000)*12 + 1 ;
months.end_s00 = months.start_s00 + months.num - 1 ;
months.start_s01 = (years.start-2001)*12 + 1 ;
months.end_s01 = months.start_s01 + months.num - 1 ;


%% (3) Import data

% Regions
disp('Loading regions_bsruns...')
load('/Users/sam/Geodata/New_regions_v4/MATLAB_QGIS/regions_v4.mat')
regions_all = regions ;
eval(['regions_bsruns = regions.' methods.regions ' ;']) ;
clear regions
regions_bsruns.map = single(regions_bsruns.map) ;
regions_bsruns.numCells = nan(regions_bsruns.num,1) ;
regions_bsruns.Yarray = repmat(regions_bsruns.map,[1 1 years.num]) ;
regions_bsruns.Marray = repmat(regions_bsruns.Yarray,[1 1 12]) ;

% Month datasets
Marray_month = zeros(720,1440,months.num,'int16') ;
for m = 1:months.num
    Marray_month(:,:,m) = m*ones(720,1440) ;
end ; clear m
% Marray_trueMonth = zeros(720,1440,12,'int8') ;
% for m = 1:12
%     Marray_trueMonth(:,:,m) = m*ones(720,1440) ;
% end ; clear m
% Marray_trueMonth = repmat(Marray_trueMonth,[1 1 years.num]) ;

% Land covers: HYDE
disp('Loading HYDE v3.1 land cover data...')
load('/Users/sam/Geodata/HYDE31/MAT-files/HYDE31prop_quartDeg_0009.mat')
landarea = HYDE_landarea_km2 ; clear HYDE_landarea_km2
landarea_Marray = repmat(landarea,[1 1 months.num]) ;
LCcrop_tmp = LCprop_crop_HYDE31_0009(:,:,years.indices_s00) ;
LCpast_tmp = LCprop_past_HYDE31_0009(:,:,years.indices_s00) ;
clear LCprop_*_HYDE31_0009
LCcrop_tmp = single(LCcrop_tmp) ;
LCpast_tmp = single(LCpast_tmp) ;
LCothr_tmp = ones(size(LCcrop_tmp)) - (LCcrop_tmp + LCpast_tmp) ;
clear LCprop_*_HYDE31_0009
LCfrac_crop = nan(size(regions_bsruns.Marray),'single') ;
LCfrac_past = nan(size(regions_bsruns.Marray),'single') ;
LCfrac_othr = nan(size(regions_bsruns.Marray),'single') ;
for y = 1:years.num
    monthTMP_start = (y-1)*12 + 1 ;
    monthTMP_end = monthTMP_start + 11 ;
    LCfrac_crop(:,:,monthTMP_start:monthTMP_end) = repmat(LCcrop_tmp(:,:,y),[1 1 12]) ;
    LCfrac_past(:,:,monthTMP_start:monthTMP_end) = repmat(LCpast_tmp(:,:,y),[1 1 12]) ;
    LCfrac_othr(:,:,monthTMP_start:monthTMP_end) = repmat(LCothr_tmp(:,:,y),[1 1 12]) ;
end ; clear y monthTMP_*
clear LC*_tmp

% Observed fire
disp('Loading observed fire data...')
if strcmp(methods.fireType,'terra')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('/Users/sam/Documents/Dropbox/Projects/FireModel_globLoc/input/MOD14CMH_2001-2008_ssr.mat')
    obsFire = single(MOD14CMH(:,:,months.start_s01:months.end_s01)) ;
    clear MOD14CMH
elseif strcmp(methods.fireType,'aqua')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
elseif strcmp(methods.fireType,'gfed')
    load('gfed4_BA_highres_01-10_km2.mat')
    obsFire = single(gfed4_BA_highres(:,:,months.start_s01:months.end_s01)) ;
    clear gfed4
elseif strcmp(methods.fireType,'gfed3')
    error(['Make a quarter-degree version of ' methods.fireType ' fire data.'])
    load('GFED3.1_2000-2009_BAkm2.mat')
    obsFire = single(gfed31(:,:,months.start_s00:months.end_s00)) ;
    clear gfed31
elseif strcmp(methods.fireType,'randerson_all')
    load('BA_all_highres_01-10.mat')
    obsFire = single(BA_all_highres(:,:,months.start_s01:months.end_s01)) ;
    clear BA_all_lowres
    load('BA_IGBPcroppy.mat','BA_crop','BA_cropPLUSmos')
    obsFire_crop = single(BA_crop(:,:,months.start_s01:months.end_s01)) ;
    obsFire_cropPLUSmos = single(BA_cropPLUSmos(:,:,months.start_s01:months.end_s01)) ;
    clear BA_crop*
elseif strcmp(methods.fireType,'randerson_all_Cemis')
    load('C_all_highres_01-10.mat')
    obsFire = single(C_all_highres(:,:,months.start_s01:months.end_s01)) ;
    obsFire = obsFire .* (landarea_Marray*1e6) ;   % Convert from g/m2/month to g/month
    clear C_all_lowres
else error('methods.fireType improperly specified.')
end
clear BA_*res

% Normalize
if strcmp(methods.normType,'max') || strcmp(methods.normType,'mean') || strcmp(methods.normType,'median')
    [~,~,fire_normConstant] = normObs(obsFire,methods) ;
    %     [obsFireNorm,~,fire_normConstant] = normObs(obsFire,methods) ;
    %     obsFireNorm = single(obsFireNorm) ;
elseif ~strcmp(methods.normType,'none')
    error('methods.normType improperly specified.')
end
obsFire = single(obsFire) ;

% Reconcile NaNs across products
bad = ( isnan(regions_bsruns.map) | isnan(mean(LCfrac_crop,3)) | isnan(mean(obsFire,3)) ) ;
regions_bsruns.map(bad) = NaN ;
regions_bsruns.Yarray = repmat(regions_bsruns.map,[1 1 years.num]) ;
regions_bsruns.Marray = repmat(regions_bsruns.Yarray,[1 1 12]) ;
landarea(bad) = NaN ;
bad_array = repmat(bad,[1 1 months.num]) ;
LCfrac_crop = LCfrac_crop(~bad_array) ;
LCfrac_past = LCfrac_past(~bad_array) ;
LCfracFAKE_crop = LCfracFAKE_crop(~bad_array) ;
LCfracFAKE_past = LCfracFAKE_past(~bad_array) ;
Marray_month = Marray_month(~bad_array) ;
% Marray_trueMonth = Marray_trueMonth(~bad_array) ;
if ~strcmp(methods.normType,'none')
    %     obsFireNorm = obsFireNorm(~bad_array) ;
    if methods.normStyle == 1
        fire_normConstant_Marray = repmat(fire_normConstant,[1 1 months.num]) ;
    elseif methods.normStyle == 2
        fire_normConstant_Marray = nan(size(bad_array)) ;
        for y = 1:years.num
            mStart = (y-1)*12 +1 ;
            mEnd = mStart + 11 ;
            fire_normConstant_Marray(:,:,mStart:mEnd) = repmat(fire_normConstant(:,:,y),[1 1 12]) ;
        end
    end
    fire_normConstant = fire_normConstant_Marray(~bad_array) ;
    clear fire_normConstant_Marray
    % fire_normConstant_orig = fire_normConstant_orig(~bad) ;
else
    fire_normConstant = [] ;
end

obsFire = obsFire(~bad_array) ;
if exist('obsFire_crop','var')
    obsFire_crop = obsFire_crop(~bad_array) ;
else obsFire_crop = [] ;
end
if exist('obsFire_cropPLUSmos','var')
    obsFire_cropPLUSmos = obsFire_cropPLUSmos(~bad_array) ;
else obsFire_cropPLUSmos = [] ;
end
clear landarea_Marray bad_array

regKey_bsruns = uint16(regions_bsruns.Marray(~isnan(regions_bsruns.Marray))) ;
regions_out.map(bad) = NaN ;
regions_out.Marray = repmat(regions_out.map,[1 1 months.num]) ;
% regKey_out = uint16(regions_out.Marray(~isnan(regions_out.Marray))) ;

for r = 1:regions_bsruns.num
    regions_bsruns.numCells(r) = length(find(regions_bsruns.map==r)) ;
end

methods.num_cells = length(find(~isnan(regions_bsruns.map))) ;

landarea_vectorONE = landarea(~isnan(landarea)) ;
% landarea_vectorALL = repmat(landarea_vectorONE,[years.num 1]) ;

disp('Done.') ; disp(' ')


%% Make maps comparing HYDE to MCD12 cover

[LCfracC_mcd12crop, ~] = geotiffread('/Users/sam/Geodata/MCD12C1_quartDeg/GeoTIFFs/MCD12C1_crop_frac_0110.tif') ;
LCfracC_mcd12crop = flipud(single(LCfracC_mcd12crop)) ;

LCfracC_mcd12_toCompare = LCfracC_mcd12crop ;

Ncells = length(LCfrac_crop) / (12*years.num) ;

LCfracC_hyde_cmy = reshape(LCfrac_crop,[Ncells 12 years.num]) ;
LCfracC_hyde_mean = mean(squeeze(LCfracC_hyde_cmy(:,1,:)),2) ;

LCfracC_hyde_map = nan(size(regions_bsruns.map),'single') ;
LCfracC_hyde_map(~isnan(regions_bsruns.map)) = LCfracC_hyde_mean ;

figure('Color',[1 1 1]) ;
pcolor(double(LCfracC_hyde_map-LCfracC_mcd12_toCompare)) ;
colorbar
shading flat
axis equal
set(gca,'xcolor',get(gcf,'color'));
set(gca,'ycolor',get(gcf,'color'));
set(gca,'ytick',[]);
set(gca,'xtick',[]);
