%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Run 3-member (CPO) unpacking, with proper bootstrapping, ON ANALYSIS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Random samples already chosen and saved in external files %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sampleSet_num = 1 ;

% prefix = 'REGv5_constrain_effFk_no0s_AA_NOprenorm_randersonCemis' ;
% prefix = 'REGv5_allowNegFk_effFk_no0s_AA_NOprenorm_randersonCemis' ;
% prefix = 'REGv5_allowNegFk_effFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_NOprenorm_randerson' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_PNmean_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_randerson' ;
% prefix = 'REGv5_constrain_allFk_no0s_AA_PNmaxm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmaxm_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmeanSNB0_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmeanSNB0_exclNB_randerson' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_exclNB_randerson_fake3m' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNnone_exclNB_randerson_fake3m' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_exclNB_randerson_fake3mv2' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNnone_exclNB_randerson_fake3mv2' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNnone_randerson_fake3mv2' ;

% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNnone_exclNBproper_randerson_fake3mv2' ;
% prefix = 'REGv5_allowNegFk_allFk_no0s_AA_PNmean_exclNBproper_randerson_fake3mv2' ;
% prefix = 'REGv5biomes_allowNegFk_allFk_no0s_AA_PNnone_exclNBproper_randerson_fake3mv2' ;
% prefix = 'REGv5biomes_allowNegFk_allFk_no0s_AA_PNmean_exclNBproper_randerson_fake3mv2' ;

prefix = 'test' ;

if ~exist('prefix','var')
    error('You forgot to set a prefix, dummy.')
end

%%% What set of regions?
% methods.regions = 'gfed' ;
% methods.regions = 'v1' ;   % NAmER_WAAR_fixedWAAR
% methods.regions = 'v2' ;   % NAmER_WAAR+Turkey, Australian ecoregions (WWF), WWF-defined southern border for MIDE.
% methods.regions = 'v3' ;   % NAmER_WAAR (NO Turkey), Australian ecoregions (WWF), WWF-defined southern border for MIDE.
% methods.regions = 'v4' ;   % NAmER_WAAR+Turkey, Australian ecoregions (WWF), WWF-defined southern border for MIDE, NH/SH South American ecoregions (WWF).
% methods.regions = 'v5' ;   % NAmER_WAAR (NO Turkey), Australian ecoregions (WWF), WWF-defined southern border for MIDE, NH/SH South American ecoregions (WWF).
methods.regions = 'v5biomes' ;

%%% What kind of observed fire data?
% methods.fireType = 'gfed3' ;   % To match Magi et al. (2012)
% methods.fireType = 'gfed' ;
methods.fireType = 'randerson_all' ;
% methods.fireType = 'randerson_all_Cemis' ;

%%% What land cover data to use?
% methods.landcover = 'hyde' ;
methods.landcover = 'fake_3member' ;
if strcmp(methods.landcover,'fake_3member')
    % Which version of fake 3-member land cover?
%     methods.F3Mv = 1 ;
    methods.F3Mv = 2 ;
%     methods.F3Mv = 3 ; warning('No unclassified/fill area in MCD12... Switching methods.F3Mv to 2.') ; methods.F3Mv = 2 ;
end

%%% Constrain Fk values to >= 1?
methods.constrain = 0 ;
% methods.constrain = 1 ;   % To match Magi et al. (2012)

% Do "efficient" version of FcFp code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Fs = 0 ;   % To match Magi et al. (2012)
% methods.efficient_Fs = 1 ;

% Ignore neverBurned grid cells in Fk calculations?
% methods.ignoreNB = false ;   % To match Magi et al. (2012)
methods.ignoreNB = true ;
if methods.ignoreNB
	% Do proper bootstrapping for ever-burned cells? Or original style?
%	methods.ignoreNB_proper = false ;   % Original style
	methods.ignoreNB_proper = true ;   % Proper bootstrapping
end

%%% Are we normalizing?
methods.normType = 'none' ;
% methods.normType = 'max' ;   % To match Magi et al. (2012)
% methods.normType = 'mean' ;
% methods.normType = 'median' ;
if ~strcmp(methods.normType,'none')
    %%% When normConstant==0, leave as 
    %%% HOW are we normalizing? Entire TS or by year?
    methods.normStyle = 1 ;   % Pre-normalize across entire time series. To match Magi et al. (2012).
%     methods.normStyle = 2 ;   % Pre-normalize each year separately.
    if ~methods.ignoreNB
        %%% What should cells that never burned have their normConstant
        %%% changed to?
        methods.set_neverBurned = 0 ;
%         methods.set_neverBurned = 1 ; warning('All cells that never burned are having their normObs set to 1! This is probably not what you want!!!')
    end
end


%% (1) Namelist

outDir = 'Output_bootstrap/' ;

% Number of bootstrapping runs?
methods.trainRuns = 2000 ;

%%% What years are we working with?
% years.list = 2003:2008 ;
% years.list = 2000:2009 ;   % To match Magi et al. (2012). Extent of HYDE 3.1.
years.list = 2001:2009 ;   % Overlap of HYDE 3.1 and Randerson.
% years.list = 2001:2003 ;   % To match Korontzi et al. (2006)

%%% Minimum mean coverage in a region-month for an LC to be included in
%%% unpacking? I.e., mean(LCfrac_*)
%%% (<= this, won't be included) (fraction)
methods.min_coverage = 0 ;   % Only exclude covers that don't exist
% methods.min_coverage = 0.005 ;
% methods.min_coverage = 0.03 ;

%%% Do two-step unpacking (ie, Pk and Fk) or just one step (ie, Fk only a
%%% la Magi et al. 2012)?
methods.numUnpackSteps = 1 ;   % To match Magi et al. (2012)
% methods.numUnpackSteps = 2 ;

%%% Area or proportion land cover?
% methods.coverType = 'prop' ;   % To match Magi et al. (2012)
methods.coverType = 'area' ;

%%% Match input data (e.g., regress proportion cover vs. frac. burned)?
% methods.match = 0 ;   % To match Magi et al. (2012)
methods.match = 1 ;

% Check parameters for observed fire data
if strcmp(methods.fireType,'gfed3') && max(years.list)>2009
    error('GFED3 data only go through 2009.')
elseif (strcmp(methods.fireType,'gfed') || strcmp(methods.fireType,'randerson_all')) && min(years.list)<2001
    error('GFED4 and Randerson data start in 2001.')
elseif strcmp(methods.fireType,'terra') && min(years.list)>2008
    error('GFED4 data only go through 2008.')
end

%%% Include a constant term in the logistic regression?
methods.constant = 0 ;
% methods.constant = 1 ;

% Re-distribute negative OTHR BA?
methods.redistNegOTHR = 0 ;   % To match Magi et al. (2012)
% methods.redistNegOTHR = 1 ;

% Do "efficient" version of Pa code? (I.e., calculate one parameter for all
% [Januaries] instead of one parameter for EACH [January].)
methods.efficient_Ps = 0 ;
% methods.efficient_Ps = 1 ;



%% (2) Setup

% Working directory and path
addpath(genpath(pwd))
addpath(genpath('/archive/ssr/firemodel_pureunpack/'))

% Months & years info
years.start = min(years.list) ;
years.end = max(years.list) ;
years.indices_s00 = (years.start - 1999):(years.end - 1999) ;
years.indices_s01 = (years.start - 2000):(years.end - 2000) ;
years.num = length(years.list) ;
months.num = 12*years.num ;
months.start_s00 = (years.start-2000)*12 + 1 ;
months.end_s00 = months.start_s00 + months.num - 1 ;
months.start_s01 = (years.start-2001)*12 + 1 ;
months.end_s01 = months.start_s01 + months.num - 1 ;
methods.num_months = months.num ;

% Turn off warning for "Matrix is close to singular or badly scaled."
disp(' ')
warning('off','MATLAB:nearlySingularMatrix')
warning('Disabled warning about near-singular/badly-scaled matrices.')
disp(' ')


%% (3) Import data

% Sample set
disp(['Loading sample set ' num2str(sampleSet_num) '...'])
if strcmp(methods.landcover,'fake_3member')
   	lctext = ['F3Mv' num2str(methods.F3Mv)] ;
else
   	lctext = '' ;
end
if methods.ignoreNB && methods.ignoreNB_proper
   tmp = num2str(years.list([1 length(years.list)])') ;
   y1 = tmp(1,3:4) ;
   y2 = tmp(2,3:4) ;
   if ~strcmp(methods.regions,'v5biomes')
       filename = ['samples' lctext '_ignoreNB_' y1 y2 '_' methods.fireType '_' num2str(sampleSet_num) '.mat'] ;
   else
       filename = ['samples_' methods.regions '_' lctext '_ignoreNB_' y1 y2 '_' methods.fireType '_' num2str(sampleSet_num) '.mat'] ;
   end
   clear tmp y1 y2
else
    filename = ['samples' lctext '_' num2str(sampleSet_num) '.mat'] ;
end
sampleSet = load([filename]) ;
clear filename lctext
%%%%% if strcmp(methods.landcover,'hyde')
%%%%%     sampleSet = load(['samples_' num2str(sampleSet_num) '.mat']) ;
%%%%% elseif strcmp(methods.landcover,'fake_3member')
%%%%% 	if methods.ignoreNB & methods.ignoreNB_proper
%%%%% 		tmp = num2str(years.list([1 length(years.list)])') ;
%%%%%         y1 = tmp(1,3:4) ;
%%%%%         y2 = tmp(2,3:4) ;
%%%%%         filename = ['samplesF3M_ignoreNB_' y1 y2 '_' methods.fireType '_' num2str(f) '.mat'] ;
%%%%%         if isempty(dir(filename)) ; error(['Sample set does not exist; filename: ' filename]) ; end
%%%%% 		sampleSet = load(filename) ;
%%%%% 		clear tmp y1 y2 filename
%%%%% 	else
%%%%%     	sampleSet = load(['samplesF3M_' num2str(sampleSet_num) '.mat']) ;
%%%%%     end
%%%%% else error('methods.coverType improperly specified.')
%%%%% end


% Regions
disp('Loading regions...')
if length(methods.regions)>=8 && strcmp(methods.regions(1:8),'v5biomes')
    load('regions_v5biomes.mat')
    regions_all = regions ;
    regions = regions_all.v51 ;
else
    load('regions_v4.mat')
    regions_all = regions ;
    eval(['regions = regions.' methods.regions ' ;']) ;
end
regions.map = single(regions.map) ;
regions.numCells = nan(regions.num,1) ;
regions.Yarray = repmat(regions.map,[1 1 years.num]) ;
regions.Marray = repmat(regions.Yarray,[1 1 12]) ;

% Month datasets
Marray_month = zeros(720,1440,months.num,'int16') ;
for m = 1:months.num
    Marray_month(:,:,m) = m*ones(720,1440) ;
end ; clear m
Marray_trueMonth = zeros(720,1440,12,'int8') ;
for m = 1:12
    Marray_trueMonth(:,:,m) = m*ones(720,1440) ;
end ; clear m
Marray_trueMonth = repmat(Marray_trueMonth,[1 1 years.num]) ;

% Land covers
disp('Loading land covers...')
[LCfrac_crop,LCfrac_past,LCfrac_othr, ...
		  landarea] = ...
			import_LCdata(methods,years,months,regions) ;

% Observed fire
disp('Loading observed fire...')
[obsFire, obsFireNorm, fire_normConstant] = ...
		 	import_obsFire(methods,months) ;
obsFire_yn = (obsFire>0) ;

% Reconcile NaNs across products
disp('Reconciling NaN''s...')
[landarea, regions, ...
		  LCfrac_crop,LCfrac_past,LCfrac_othr, ...
		  LCdata_crop,LCdata_past,LCdata_othr, ...
		  Marray_month, Marray_trueMonth, ...
		  obsFire, obsFireNorm, obsFire_yn, ...
		  fire_normConstant, ...
		  obsFire_frac] = ...
			import_reconcileNaN(methods, regions, landarea, ...
								years, months, ...
								LCfrac_crop,LCfrac_past,LCfrac_othr, ...
								Marray_month, Marray_trueMonth, ...
								obsFire, obsFireNorm, obsFire_yn, ...
								fire_normConstant) ;
								
regKey = regions.Marray(~isnan(regions.Marray)) ;
regKey_oneMonth = regions.map(~isnan(regions.map)) ;
for r = 1:regions.num
    regions.numCells(r) = length(find(regions.map==r)) ;
end ; clear r
methods.num_cells = length(find(~isnan(regions.map))) ;
disp('Done.') ; disp(' ')


%% (4) Do unpacking and get results for each run, saving every 10

disp('Setting up for unpacking...')

numCells_train = sum(regions.numCells) ;

% Set up for output
if methods.numUnpackSteps == 2
    if methods.efficient_Ps == 0
        allReg_Pc_mrt = nan(months.num,regions.num,methods.trainRuns,'single') ;
        allReg_Pp_mrt = nan(months.num,regions.num,methods.trainRuns,'single') ;
        allReg_Po_mrt = nan(months.num,regions.num,methods.trainRuns,'single') ;
        allReg_const_mrt = nan(months.num,regions.num,methods.trainRuns,'single') ;
    elseif methods.efficient_Ps == 1
        allReg_Pc_mrt = nan(12,regions.num,methods.trainRuns,'single') ;
        allReg_Pp_mrt = nan(12,regions.num,methods.trainRuns,'single') ;
        allReg_Po_mrt = nan(12,regions.num,methods.trainRuns,'single') ;
        allReg_const_mrt = nan(12,regions.num,methods.trainRuns,'single') ;
    end
end
if methods.efficient_Fs == 0
    allReg_Fc_mrt = nan(months.num,regions.num,methods.trainRuns,'single') ;
    allReg_Fp_mrt = nan(months.num,regions.num,methods.trainRuns,'single') ;
    allReg_Fo_mrt = nan(months.num,regions.num,methods.trainRuns,'single') ;
    exclC_mrt = false(months.num,regions.num,methods.trainRuns) ;
    exclP_mrt = false(months.num,regions.num,methods.trainRuns) ;
    exclO_mrt = false(months.num,regions.num,methods.trainRuns) ;
elseif methods.efficient_Fs==0.1 || methods.efficient_Fs==1
    allReg_Fc_mrt = nan(12,regions.num,methods.trainRuns,'single') ;
    allReg_Fp_mrt = nan(12,regions.num,methods.trainRuns,'single') ;
    allReg_Fo_mrt = nan(12,regions.num,methods.trainRuns,'single') ;
    exclC_mrt = false(12,regions.num,methods.trainRuns) ;
    exclP_mrt = false(12,regions.num,methods.trainRuns) ;
    exclO_mrt = false(12,regions.num,methods.trainRuns) ;
end

tic_allruns = tic ;
% for t = 1 ; warning('Only doing one run!')
for t = 1:methods.trainRuns
    disp(['Run ' num2str(t) ' of ' num2str(methods.trainRuns) ':'])
    
    if t > size(sampleSet.choseTrain,2)
        disp('Skipping because not this many sample sets were provided.')
        continue
    end
    
    tic_thisRun = tic ;
    
    fprintf('%s','   Setting up for this run...')
    tic
    
    if methods.numUnpackSteps == 2
        if methods.efficient_Ps == 0
            allReg_Pc_mr_tmp = nan(months.num,regions.num,'single') ;
            allReg_Pp_mr_tmp = nan(months.num,regions.num,'single') ;
            allReg_Po_mr_tmp = nan(months.num,regions.num,'single') ;
            allReg_const_mr_tmp = nan(months.num,regions.num,'single') ;
        elseif methods.efficient_Ps == 1
            allReg_Pc_mr_tmp = nan(12,regions.num,'single') ;
            allReg_Pp_mr_tmp = nan(12,regions.num,'single') ;
            allReg_Po_mr_tmp = nan(12,regions.num,'single') ;
            allReg_const_mr_tmp = nan(12,regions.num,'single') ;
        end
    end
    
    if methods.efficient_Fs == 0
        allReg_Fc_mr_tmp = nan(months.num,regions.num,'single') ;
        allReg_Fp_mr_tmp = nan(months.num,regions.num,'single') ;
        allReg_Fo_mr_tmp = nan(months.num,regions.num,'single') ;
        exclC_mr_tmp = false(months.num,regions.num) ;
        exclP_mr_tmp = false(months.num,regions.num) ;
        exclO_mr_tmp = false(months.num,regions.num) ;
    elseif methods.efficient_Fs==0.1 || methods.efficient_Fs==1
        allReg_Fc_mr_tmp = nan(12,regions.num,'single') ;
        allReg_Fp_mr_tmp = nan(12,regions.num,'single') ;
        allReg_Fo_mr_tmp = nan(12,regions.num,'single') ;
        exclC_mr_tmp = false(12,regions.num) ;
        exclP_mr_tmp = false(12,regions.num) ;
        exclO_mr_tmp = false(12,regions.num) ;
    end
    
    % Choose sample
    regions.num = regions.num ;
    regions.map = regions.map ;
    regions.names = regions.names ;
    allCells_train = nan(size(regKey)) ;
    choseTrain = sampleSet.choseTrain(:,t) ;
    for m = 1:months.num   % Make a regKey-length vector showing which values of regKey-length vectors are being sampled
        xstart = (m-1)*methods.num_cells + 1 ;
        xend = xstart + methods.num_cells - 1 ;
        toAdd = (m-1)*methods.num_cells ;
        allCells_train(xstart:xend) = choseTrain + toAdd ;
    end ; clear m
    regKey_train = regKey(allCells_train) ;
    
    if methods.numUnpackSteps==2
        allRegTrain_P = nan(size(regKey),'single') ;
    end
    
	landarea_chosen = landarea(allCells_train) ;
    if ~strcmp(methods.normType,'none') && methods.normStyle ~= 1
        error('Figure out what to do here.')
    end
    fire_normConstant_chosen = repmat(fire_normConstant,[months.num 1]) ;
    fire_normConstant_chosen = fire_normConstant_chosen(allCells_train) ;

    fprintf('%s\n','Done.')
    
    fprintf('%s','   Unpacking...')
    tic
    for r = 1:regions.num

        % Set up thisReg
        if methods.ignoreNB
            thisReg_indices = allCells_train(regKey_train==r & fire_normConstant_chosen>0) ;
        else
            thisReg_indices = allCells_train(regKey_train==r) ;
        end
        thisReg.crop = LCdata_crop(thisReg_indices) ;
        thisReg.past = LCdata_past(thisReg_indices) ;
        thisReg.othr = LCdata_othr(thisReg_indices) ;
        if strcmp(methods.normType,'none')
            thisReg.obsFire = obsFire(thisReg_indices) ;
        else
            thisReg.obsFire = obsFireNorm(thisReg_indices) ;
        end
        if ~methods.ignoreNB && ~isempty(find(isnan(thisReg.obsFire),1))
            error('Some element(s) of thisReg.obsFire is/are NaN, even after extracting non-neverBurned cells.')
        end
        thisReg.month = Marray_month(thisReg_indices) ;
        thisReg.trueMonth = Marray_trueMonth(thisReg_indices) ;
        if methods.efficient_Fs==1
            if ~strcmp(methods.normType,'none') & methods.set_beverBurned==-1
                error('Figure out how to deal with landarea for efficient Fs given set_neverBurned==-1.')
            end
            thisReg.landarea = landarea_chosen(regKey==r) ;
        else
%            thisReg.landarea = landarea_vectorONE_chosen(regKey_oneMonth==r) ; 
           thisReg.landarea = landarea_chosen(thisReg_indices) ;
        end
        
        % Calculate probability of fire, if doing so
        if methods.numUnpackSteps == 2
            thisReg.obsFire_yn = obsFire_yn(thisReg_indices) ;
            [P_train,Pc,Pp,Po,const] = calcP_pureUnpack_CPO(thisReg,methods,months) ;
            if ~isempty(find(isnan(P),1))
                error(['Some value of P is NaN (region ' num2str(r) ', ' regions.names{r} ')'])
            end
            allRegTrain_P(regKey_train) = P_train ;
            allReg_Pc_mr_tmp(:,r) = Pc ;
            allReg_Pp_mr_tmp(:,r) = Pp ;
            allReg_Po_mr_tmp(:,r) = Po ;
            allReg_const_mr_tmp(:,r) = const ;
            clear P_train Pc Pp Po const
        end
        
        % Calculate Fk's
        if methods.numUnpackSteps == 1
            [Fc_m,Fp_m,Fo_m,excl_mc] = calcF_pureUnpackBootstrap_noZeros_CPO_1step(thisReg,months,methods) ;
        elseif methods.numUnpackSteps == 2
            [Fc_m,Fp_m,Fo_m,excl_mc] = calcF_pureUnpackBootstrap_noZeros_CPO(thisReg,months,methods) ;
        end
        allReg_Fc_mr_tmp(:,r) = Fc_m ;
        allReg_Fp_mr_tmp(:,r) = Fp_m ;
        allReg_Fo_mr_tmp(:,r) = Fo_m ;
        exclC_mr_tmp(:,r) = excl_mc(:,1) ;
        exclP_mr_tmp(:,r) = excl_mc(:,2) ;
        exclO_mr_tmp(:,r) = excl_mc(:,3) ;
        clear Fc_m Fp_m Fo_m excl_mc
        clear thisReg
    end ; clear r
    
    % Save parameter values for this run
    if methods.numUnpackSteps==2
        allReg_Pc_mrt(:,:,t) = allReg_Pc_mr_tmp ;
        allReg_Pp_mrt(:,:,t) = allReg_Pp_mr_tmp ;
        allReg_Po_mrt(:,:,t) = allReg_Po_mr_tmp ;
        allReg_const_mrt(:,:,t) = allReg_const_mr_tmp ;
    end
    allReg_Fc_mrt(:,:,t) = allReg_Fc_mr_tmp ;
    allReg_Fp_mrt(:,:,t) = allReg_Fp_mr_tmp ;
    allReg_Fo_mrt(:,:,t) = allReg_Fo_mr_tmp ;
    exclC_mrt(:,:,t) = exclC_mr_tmp ;
    exclP_mrt(:,:,t) = exclP_mr_tmp ;
    exclO_mrt(:,:,t) = exclO_mr_tmp ;
    
    
    fprintf('%s\n',[' Done (' toc_hms(toc) ').'])
    
    % Save every 10 iterations
    if rem(t,10)==0
        fprintf('%s','   Saving progress...')
        tic
        if t==10
            methods_out = methods ;
            filename = [outDir prefix '_' num2str(sampleSet_num) '.mat'] ;
        end
        save(filename, 'methods_out', ...
            'allReg_Fc_mrt','allReg_Fp_mrt','allReg_Fo_mrt',...
            'exclC_mrt','exclP_mrt','exclO_mrt') ;
        if methods.numUnpackSteps==2
            save(filename,...
                'allReg_Pc_mrt','allReg_Pp_mrt','allReg_Po_mrt',...
                '-append')
        end
        fprintf('%s\n',[' Done (' toc_hms(toc) ').'])
    end
    
    disp(['Done with run ' num2str(t) ' of ' num2str(methods.trainRuns) ' (' toc_hms(toc(tic_thisRun)) ').']) ; disp(' ')
end
toc_allruns = toc(tic_allruns) ;



% (5) Save again

if methods.trainRuns < 10
    methods_out = methods ;
    filename = [outDir prefix '_' num2str(sampleSet_num) '.mat'] ;
end

save(filename,'methods_out',...
    'allReg_Fc_mrt','allReg_Fp_mrt','allReg_Fo_mrt',...
    'exclC_mrt','exclP_mrt','exclO_mrt', ...
    'toc_allruns') ;
if methods.numUnpackSteps==2
    save(filename,...
        'allReg_Pc_mrt','allReg_Pp_mrt','allReg_Po_mrt',...
        '-append')
end
